package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class WalletAmountTransactionResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data = WalletData()
}

class WalletData : Serializable {

    @SerializedName("page")
    var page = ""
    @SerializedName("list")
    var list = ArrayList<WalletList>()

}

class WalletList : Serializable {

    @SerializedName("id")
    var id: Int? = 0

    @SerializedName("userId")
    var userId: Int? = 0

    @SerializedName("orderId")
    var orderId: Int? = 0

    @SerializedName("amount")
    var amount: String? = ""

    @SerializedName("transactionType")
    var transactionType: String? = ""

    @SerializedName("typeOfTrans")
    var typeOfTrans: String? = ""

    @SerializedName("updateAt")
    var updateAt: String? = ""

}
