package com.app.a3naab.model

import com.google.gson.annotations.SerializedName

class PaymentRefundResponse {
    @SerializedName("resultCode")
    var rcode: Int? = null

    @SerializedName("message")
    var message: String? = null
}