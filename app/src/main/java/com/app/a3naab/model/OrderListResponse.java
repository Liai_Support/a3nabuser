package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderListResponse implements Serializable {

    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    OrderListDataRes data;

    public OrderListDataRes getData() {
        return data;
    }

    public void setData(OrderListDataRes data) {
        this.data = data;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static class OrderListDataRes implements Serializable {

        @SerializedName("pages")
        int pages;

        @SerializedName("tax")
        String tax;

        @SerializedName("adminNumber")
        String adminNumber;

        @SerializedName("settings")
        OrderSetting settings;

        public String getAdminNumber(){
            return adminNumber;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public OrderSetting getSettings() {
            return settings;
        }

        public void setSettings(OrderSetting settings) {
            this.settings = settings;
        }

        @SerializedName("orderList")
        ArrayList<OrderListData> orderList = new ArrayList<>();

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public ArrayList<OrderListData> getOrderList() {
            return orderList;
        }

        public void setOrderList(ArrayList<OrderListData> orderList) {
            this.orderList = orderList;
        }
    }

    public static class OrderListData implements Serializable {

        @SerializedName("id")
        int id;

        @SerializedName("orderIDs")
        String orderIDs;

        @SerializedName("userId")
        String userId;

        @SerializedName("orderStatus")
        String orderStatus;

        @SerializedName("isApproved")
        String isApproved;

        @SerializedName("acceptByStore")
        String acceptByStore;

        @SerializedName("packedByDriver")
        String packedByDriver;

        @SerializedName("driverNumber")
        String driverNumber;



        @SerializedName("type")
        String type;

        @SerializedName("orderOn")
        String orderOn;

        @SerializedName("deliveryOn")
        String deliveryOn;

        @SerializedName("deliveryDate")
        String deliveryDate;

        @SerializedName("totalAmount")
        String totalAmount;

        @SerializedName("discountAmount")
        String discountAmount;

        @SerializedName("grandTotal")
        String grandTotal;

        @SerializedName("deleteItems")
        String deleteItems;

        @SerializedName("fastDelievryCharge")
        String fastDelievryCharge;

        @SerializedName("fastDelivery")
        String fastDelivery;

        @SerializedName("couponDiscount")
        String couponDiscount;

        @SerializedName("off_types")
        String off_types;

        @SerializedName("taxvalue")
        String taxvalue;

        @SerializedName("pointsAmount")
        String pointsAmount;

        @SerializedName("cancelDate")
        String cancelDate;

        @SerializedName("cancelReason")
        String cancelReason;

        @SerializedName("paidByWallet")
        String paidByWallet;

        @SerializedName("giveToCustomer")
        String giveToCustomer;

        @SerializedName("takeToCustomer")
        String takeToCustomer;

        @SerializedName("deliveryOn_fromTime")
        String deliveryOn_fromTime;

        @SerializedName("deliveryOn_toTime")
        String deliveryOn_toTime;

        @SerializedName("otherTotal")
        String otherTotal;

        public String getFastDelivery(){
            return fastDelivery;
        }

        public String getOtherTotal(){
            return otherTotal;
        }

        public String getDeliveryOn_fromTime(){
            return deliveryOn_fromTime;
        }

        public String getDeliveryOn_toTime(){
            return deliveryOn_toTime;
        }

        public String getGiveToCustomer(){
            return giveToCustomer;
        }

        public String getTakeToCustomer(){
            return takeToCustomer;
        }

        public String getPaidByWallet(){
            return paidByWallet;
        }

        public String getPointsAmount(){
            return pointsAmount;
        }

        public String getTaxvalue(){
            return taxvalue;
        }

        public String getCouponDiscount(){
            return couponDiscount;
        }

        public String getOff_types(){
            return off_types;
        }

        public String getIsApproved(){
            return isApproved;
        }

        public String getFastDelievryCharge(){
            return fastDelievryCharge;
        }

        public String getAcceptByStore(){
            return acceptByStore;
        }

        public String getPackedByDriver(){
            return packedByDriver;
        }

        public String getDriverNumber(){
            return driverNumber;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOrderIDs() {
            return orderIDs;
        }

        public void setOrderIDs(String orderIDs) {
            this.orderIDs = orderIDs;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getOrderOn() {
            return orderOn;
        }

        public void setOrderOn(String orderOn) {
            this.orderOn = orderOn;
        }

        public String getDeliveryOn() {
            return deliveryOn;
        }

        public void setDeliveryOn(String deliveryOn) {
            this.deliveryOn = deliveryOn;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(String discountAmount) {
            this.discountAmount = discountAmount;
        }

        public String getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(String grandTotal) {
            this.grandTotal = grandTotal;
        }

        public String getDeleteItems() {
            return deleteItems;
        }

        public void setDeleteItems(String deleteItems) {
            this.deleteItems = deleteItems;
        }

        public String getCancelDate() {
            return cancelDate;
        }

        public void setCancelDate(String cancelDate) {
            this.cancelDate = cancelDate;
        }

        public String getCancelReason() {
            return cancelReason;
        }

        public void setCancelReason(String cancelReason) {
            this.cancelReason = cancelReason;
        }
    }

    public static class OrderSetting implements Serializable {

        @SerializedName("minimumOrderValue")
        String minimumOrderValue;

        @SerializedName("quickDelivery")
        String quickDelivery;

        @SerializedName("flatRate")
        String flatRate;

        @SerializedName("perKM")
        String perKM;

        @SerializedName("taxAmount")
        String taxAmount;

        @SerializedName("QuickDeliveryPerKM")
        String QuickDeliveryPerKM;

        @SerializedName("walletSAR")
        String walletSAR;

        @SerializedName("walletPoints")
        String walletPoints;

        public String getWalletPoints(){
            return walletPoints;
        }

        public void setWalletPoints(String walletPoints){
            this.walletPoints = walletPoints;
        }

        public String getMinimumOrderValue() {
            return minimumOrderValue;
        }

        public void setMinimumOrderValue(String minimumOrderValue) {
            this.minimumOrderValue = minimumOrderValue;
        }

        public String getWalletSAR(){
            return walletSAR;
        }

        public void setWalletSAR(String walletSAR){
            this.walletSAR = walletSAR;
        }

        public String getQuickDelivery() {
            return quickDelivery;
        }

        public void setQuickDelivery(String quickDelivery) {
            this.quickDelivery = quickDelivery;
        }

        public String getFlatRate() {
            return flatRate;
        }

        public void setFlatRate(String flatRate) {
            this.flatRate = flatRate;
        }

        public String getPerKM() {
            return perKM;
        }

        public void setPerKM(String perKM) {
            this.perKM = perKM;
        }

        public String getTaxAmount() {
            return taxAmount;
        }

        public void setTaxAmount(String taxAmount) {
            this.taxAmount = taxAmount;
        }

        public String getQuickDeliveryPerKM() {
            return QuickDeliveryPerKM;
        }

        public void setQuickDeliveryPerKM(String quickDeliveryPerKM) {
            QuickDeliveryPerKM = quickDeliveryPerKM;
        }
    }

}


