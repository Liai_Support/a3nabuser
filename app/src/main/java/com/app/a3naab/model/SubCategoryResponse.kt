package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SubCategoryResponse : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data: SubCategoryData? = null


}

class SubCategoryData : Serializable {

    @SerializedName("productCategory")
    var productCategory: ArrayList<ProductSubCategoryData>? = null


}

class ProductSubCategoryData : Serializable {


    @SerializedName("id")
    var id = 0

    @SerializedName("productCategoryId")
    var productCategoryId = 0

    @SerializedName("storeId")
    var storeId = 0

    @SerializedName("productSubCategoryName")
    var productSubCategoryName = ""

    @SerializedName("minimumOrderValue")
    var minimumOrderValue = ""

    @SerializedName("orderProcessing")
    var orderProcessing = ""

    @SerializedName("categoryDescription")
    var categoryDescription = ""

    @SerializedName("minOrderTime")
    var minOrderTime = ""

    @SerializedName("managerPrice")
    var managerPrice = ""

    @SerializedName("isComingSoon")
    var isComingSoon = ""

    @SerializedName("productSubCategoryImage")
    var productSubCategoryImage = ""

    @SerializedName("productSubCategoryStatus")
    var productSubCategoryStatus = ""

    @SerializedName("arabicName")
    var arabicName = ""

    var isSelected = false


}
