package com.app.a3naab.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ListFaqResponse implements Serializable {


    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    ViewfaqData data;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ViewfaqData getData() {
        return data;
    }

    public void setData(ViewfaqData data) {
        this.data = data;
    }

    public static class ViewfaqData implements Serializable {

        @SerializedName("faq")
        ArrayList<FaqDetails> myfaq = new ArrayList<>();

        public ArrayList<FaqDetails> getMyCart() {
            return myfaq;
        }

        public void setMyCart(ArrayList<FaqDetails> myfaq) {
            this.myfaq = myfaq;
        }
    }

    public static class FaqDetails implements Serializable {


        @SerializedName("id")
        int id;

        @SerializedName("question")
        String questione;

        @SerializedName("answer")
        String answere;

        @SerializedName("arabicQuestion")
        String questiona;

        @SerializedName("arabicAnswer")
        String answera;

        @SerializedName("faqCategory")
        int category;

        @SerializedName("type")
        String type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getQuestione(){
            return questione;
        }

        public void setQuestione(String questione){
            this.questione = questione;
        }

        public String getQuestiona(){
            return questiona;
        }

        public void setQuestiona(String questiona){
            this.questiona = questiona;
        }

        public String getAnswere(){
            return answere;
        }

        public void setAnswere(String answere){
            this.answere = answere;
        }

        public String getAnswera(){
            return answera;
        }

        public void setAnswera(String answera){
            this.answera = answera;
        }




    }


}
