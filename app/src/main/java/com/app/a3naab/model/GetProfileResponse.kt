package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetProfileResponse : Serializable {

    @SerializedName("error")
    var error: Boolean = true

    @SerializedName("message")
    var message: String = ""

    @SerializedName("data")
    var data: ProfileData? = null

}

class ProfileData : Serializable {

    @SerializedName("profile")
    var profile: ProfilesProfile? = null

}

class ProfilesProfile : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("firstName")
    var firstName: String? = null

    @SerializedName("lastName")
    var lastName: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("DOB")
    var DOB: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("walletAmount")
    var walletAmount: String? = null

    @SerializedName("referralCode")
    var referralCode: String? = null

    @SerializedName("orders")
    var orders: String? = null

    @SerializedName("packageValue")
    var packageValue: String? = null

    @SerializedName("trustUser")
    var trustUser: String? = null

    @SerializedName("points")
    var points: String? = null


}

