package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentResponse implements Serializable {

    @SerializedName("resultCode")
    String resultCode;

    @SerializedName("message")
    String message;

    @SerializedName("result")
    Result result;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result implements Serializable {

        @SerializedName("order")
        OrderResult order;

        @SerializedName("checkoutData")
        CheckoutData checkoutData;


        public OrderResult getOrder() {
            return order;
        }

        public void setOrder(OrderResult message) {
            this.order = order;
        }

        public CheckoutData getCheckoutData() {
            return checkoutData;
        }

        public void setCheckoutData(CheckoutData checkoutData) {
            this.checkoutData = checkoutData;
        }
    }

    public class OrderResult implements Serializable {

        @SerializedName("id")
        String id;

        @SerializedName("status")
        String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus(){
            return status;
        }
    }

    public class CheckoutData implements Serializable {

        @SerializedName("postUrl")
        String postUrl;

        public String getPostUrl() {
            return postUrl;
        }

        public void setPostUrl(String postUrl) {
            this.postUrl = postUrl;
        }
    }

}


/*{
    "resultCode": 0,
    "message": "Processed successfully",
    "resultClass": 0,
    "classDescription": "",
    "actionHint": "",
    "requestReference": "33bfb2d2-48e1-475a-af4f-f512d6aabfb1",
    "result": {
        "nextActions": "ADD_PAYMENT_INFO",
        "order": {
            "status": "INITIATED",
            "creationTime": "2021-04-14T06:17:03.2336507Z",
            "id": 216170809999,
            "amount": 10.0,
            "currency": "SAR",
            "name": "Sample order name",
            "reference": "Ref#1234",
            "category": "pay",
            "channel": "Mobile"
        },
        "configuration": {
            "tokenizeCc": true,
            "returnUrl": "https://www.merchantresponseback.com",
            "locale": "en"
        },
        "business": {
            "id": "aanaab",
            "name": "AANAAB"
        },
        "checkoutData": {
            "postUrl": "https://checkout-stg.noonpayments.com/en/default/index?data=T216170809999",
            "jsUrl": "https://checkout-stg.noonpayments.com/en/scripts/checkout?url=https%3A%2F%2Fcheckout-stg.noonpayments.com%2Fen%2Fdefault%2Findex%3Fdata%3DT216170809999"
        },
        "deviceFingerPrint": {
            "sessionId": "216170809999"
        },
        "paymentOptions": [
            {
                "method": "CARD_SANDBOX",
                "type": "Card",
                "action": "Card",
                "data": {
                    "supportedCardBrands": [
                        "VISA",
                        "MASTERCARD",
                        "MADA"
                    ],
                    "cvvRequired": "True"
                }
            },
            {
                "method": "Applepay_CheckoutPro",
                "type": "ApplePay",
                "action": "ApplePay",
                "data": {
                    "merchantIdentifier": "merchant.com.noonpayments.stg-checkout",
                    "paymentRequest": {
                        "countryCode": "AE",
                        "currencyCode": "SAR",
                        "total": {
                            "label": "Sample order name",
                            "amount": 10.0
                        },
                        "supportedNetworks": [
                            "visa",
                            "masterCard",
                            "maestro",
                            "mada"
                        ],
                        "merchantCapabilities": [
                            "supports3DS"
                        ]
                    }
                }
            }
        ]
    }
}*/