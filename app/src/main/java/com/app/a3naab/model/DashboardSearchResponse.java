package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DashboardSearchResponse implements Serializable {

    @SerializedName("error")
    boolean error;

    @SerializedName("message")
    String message;


    @SerializedName("data")
    ArrayList<DashboardResponse.Categories> data = new ArrayList<>();

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DashboardResponse.Categories> getData() {
        return data;
    }

    public void setData(ArrayList<DashboardResponse.Categories> data) {
        this.data = data;
    }
}


