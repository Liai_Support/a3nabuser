package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SearchCategoryDataResponse : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data: ArrayList<SearchCategoryData>? = null

}

class SearchCategoryData : Serializable {

    @SerializedName("id")
    var id = 0

    @SerializedName("categoryId")
    var categoryId = 0

    @SerializedName("isSubcate")
    var isSubcate = 0

    @SerializedName("productCategoryName")
    var productCategoryName = ""

    @SerializedName("productCategoryImage")
    var productCategoryImage = ""

    @SerializedName("arabicName")
    var arabicName = ""


}
