package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentResponse1 implements Serializable {

    @SerializedName("resultCode")
    String resultCode;

    @SerializedName("message")
    String message;

    @SerializedName("result")
    Result result;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result implements Serializable {

        @SerializedName("transaction")
        Transaction transaction;


        public Transaction getTransaction() {
            return transaction;
        }

        public void setTransaction(Transaction message) {
            this.transaction = transaction;
        }

    }

    public class Transaction implements Serializable {

        @SerializedName("id")
        String id;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

}
