package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse implements Serializable{


    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;


    @SerializedName("data")
    LoginData data;


    public static class LoginData implements Serializable {

        @SerializedName("id")
        String id;

        @SerializedName("firstName")
        String firstName;

        @SerializedName("lastName")
        String lastName;

        @SerializedName("email")
        String email;

        @SerializedName("otp")
        String otp;

        @SerializedName("latitude")
        String latitude;

        @SerializedName("longitude")
        String longitude;

        @SerializedName("countryCode")
        String countryCode;

        @SerializedName("mobileNumber")
        String mobileNumber;

        @SerializedName("profilePic")
        String profilePic;

        @SerializedName("token")
        String token;

        @SerializedName("userExists")
        Boolean userExists;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Boolean getUserExists() {
            return userExists;
        }

        public void setUserExists(Boolean userExists) {
            this.userExists = userExists;
        }
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }
}
