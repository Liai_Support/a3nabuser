package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProductDetailsResponse : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data: ProductDataData? = null

}

class ProductDataData : Serializable {

    @SerializedName("productDetails")
    var productDetails: DashboardResponse.BestProduct? = null

    @SerializedName("bestProducts")
    var bestProducts: ArrayList<DashboardResponse.BestProduct>? = null

    @SerializedName("relatedProducts")
    var relatedProducts: ArrayList<DashboardResponse.BestProduct>? = null

}

