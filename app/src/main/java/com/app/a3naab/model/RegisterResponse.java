package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterResponse implements Serializable {

    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    RegisterData data;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RegisterData getData() {
        return data;
    }

    public void setData(RegisterData data) {
        this.data = data;
    }

    public static class RegisterData implements Serializable {


        @SerializedName("id")
        String id;


        @SerializedName("firstName")
        String firstName;

        @SerializedName("lastName")
        String lastName;

        @SerializedName("token")
        String token;

        @SerializedName("notify")
        Notify notify;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Notify getNotify() {
            return notify;
        }

        public void setNotify(Notify notify) {
            this.notify = notify;
        }
    }

    public static class Notify implements Serializable {
        @SerializedName("notifyMessage")
        String notifyMessage;

        @SerializedName("notifyTitle")
        String notifyTitle;

        public String getNotifyMessage() {
            return notifyMessage;
        }

        public void setNotifyMessage(String notifyMessage) {
            this.notifyMessage = notifyMessage;
        }

        public String getNotifyTitle() {
            return notifyTitle;
        }

        public void setNotifyTitle(String notifyTitle) {
            this.notifyTitle = notifyTitle;
        }
    }
}

