package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CouponResponse : Serializable {

    @SerializedName("error")
    var error: Boolean = true

    @SerializedName("message")
    var message: String = ""

    @SerializedName("data")
    var data: Data? = null

}

class Data : Serializable {

    @SerializedName("couponCode")
    var couponcode: CouponCode? = null

}

class CouponCode : Serializable {

    @SerializedName("couponId")
    var couponId: Int? = null

    @SerializedName("error")
    var error: String? = null

    @SerializedName("discountVal")
    var discountVal: String? = null

    @SerializedName("discount")
    var discount: String? = null

    @SerializedName("uptoAmount")
    var uptoAmount: String? = null

    @SerializedName("off_types")
    var off_types: String? = null

    @SerializedName("minimumValue")
    var minimumValue: String? = null

}

