package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProfileDataResponse(
    @SerializedName("error") var error: Boolean,
    @SerializedName("message") var message: String,
    @SerializedName("data") var data: ProfileDataRes?
) : Serializable

data class ProfileDataRes(@SerializedName("profile") var profile: ProfileDatas) : Serializable

data class ProfileDatas(
    @SerializedName("id") var id: Int,
    @SerializedName("firstName") var firstName: String,
    @SerializedName("lastName") var lastName: String,
    @SerializedName("email") var email: String?,
    @SerializedName("countryCode") var countryCode: String,
    @SerializedName("mobileNumber") var mobileNumber: String,
    @SerializedName("profilePic") var profilePic: String?,
    @SerializedName("DOB") var DOB: String?,
    @SerializedName("currentAddress") var caddr: String?,
    @SerializedName("gender") var gender: String?,
    @SerializedName("walletAmount") var walletAmount: String,
    @SerializedName("referralCode") var referralCode: String,
    @SerializedName("orders") var orders: String,
    @SerializedName("packageValue") var packageValue: String,
    @SerializedName("trustUser") var trustUser: String,
    @SerializedName("points") var points: String,
    @SerializedName("year") var year: String,
    @SerializedName("month") var month: String,
    @SerializedName("day") var day: String,
    @SerializedName("offersNotify") var offersNotify: Boolean,
    @SerializedName("ordersNotify") var ordersNotify: Boolean,
    @SerializedName("announcementNotify") var announcementNotify: Boolean,
    @SerializedName("othersNotify") var othersNotify: Boolean
) : Serializable


