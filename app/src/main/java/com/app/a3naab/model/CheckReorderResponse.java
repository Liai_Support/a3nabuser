package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckReorderResponse implements Serializable {

    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    CheckreorderData data;

    public static class CheckreorderData implements Serializable {
        @SerializedName("overAllDeliveryAmount")
        String overAllDeliveryAmount;

        @SerializedName("overAllFastDeliveryAmount")
        String overAllFastDeliveryAmount;

        @SerializedName("freeDeliveryAmt")
        String freeDeliveryAmt;

        public String getOverAllDeliveryAmount(){
            return overAllDeliveryAmount;
        }

        public String getOverAllFastDeliveryAmount(){
            return overAllFastDeliveryAmount;
        }

        public String getFreeDeliveryAmt(){
            return freeDeliveryAmt;
        }
    }

    public CheckreorderData getData() {
        return data;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
