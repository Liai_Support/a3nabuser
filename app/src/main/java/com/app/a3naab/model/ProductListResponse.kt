package com.app.a3naab.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProductListResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var productListDataData: ProductListDataData? = null

}

class ProductListDataData : Serializable {

    @SerializedName("pages")
    var pages = 0

    @SerializedName("products")
    var products: ArrayList<DashboardResponse.BestProduct>? = null
}

class ProductListData() : Serializable, Parcelable {


    @SerializedName("id")
    var id = 0

    @SerializedName("productCategoryId")
    var productCategoryId = 0

    @SerializedName("productSubCategoryId")
    var productSubCategoryId = 0

    @SerializedName("isBestProduct")
    var isBestProduct = 0

    @SerializedName("productName")
    var productName = ""

    @SerializedName("productStatus")
    var productStatus = ""

    @SerializedName("productWeight")
    var productWeight = ""

    @SerializedName("productPrice")
    var productPrice = ""

    @SerializedName("storeId")
    var storeId = ""

    @SerializedName("productDiscount")
    var productDiscount = ""

    @SerializedName("productDiscountStatus")
    var productDiscountStatus = ""

    @SerializedName("instruction")
    var instruction = ""

    @SerializedName("status")
    var status = ""

    @SerializedName("createdAt")
    var createdAt = ""

    @SerializedName("arabicName")
    var arabicName = ""

    @SerializedName("productImages")
    var productImages: ArrayList<ProductImage>? = null

    var addedInCart = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        productCategoryId = parcel.readInt()
        productSubCategoryId = parcel.readInt()
        isBestProduct = parcel.readInt()
        productName = parcel.readString().toString()
        productStatus = parcel.readString().toString()
        productWeight = parcel.readString().toString()
        productPrice = parcel.readString().toString()
        productDiscount = parcel.readString().toString()
        productDiscountStatus = parcel.readString().toString()
        instruction = parcel.readString().toString()
        status = parcel.readString().toString()
        createdAt = parcel.readString().toString()
        arabicName = parcel.readString().toString()
        addedInCart = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(productCategoryId)
        parcel.writeInt(productSubCategoryId)
        parcel.writeInt(isBestProduct)
        parcel.writeString(productName)
        parcel.writeString(productStatus)
        parcel.writeString(productWeight)
        parcel.writeString(productPrice)
        parcel.writeString(productDiscount)
        parcel.writeString(productDiscountStatus)
        parcel.writeString(instruction)
        parcel.writeString(status)
        parcel.writeString(createdAt)
        parcel.writeString(arabicName)
        parcel.writeByte(if (addedInCart) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductListData> {
        override fun createFromParcel(parcel: Parcel): ProductListData {
            return ProductListData(parcel)
        }

        override fun newArray(size: Int): Array<ProductListData?> {
            return arrayOfNulls(size)
        }
    }


}

class ProductImage() : Serializable, Parcelable {


    @SerializedName("id")
    var id = 0

    @SerializedName("productId")
    var productId = 0

    @SerializedName("productImage")
    var productImage = ""

    @SerializedName("productImageStatus")
    var productImageStatus = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        productId = parcel.readInt()
        productImage = parcel.readString().toString()
        productImageStatus = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(productId)
        parcel.writeString(productImage)
        parcel.writeString(productImageStatus)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductImage> {
        override fun createFromParcel(parcel: Parcel): ProductImage {
            return ProductImage(parcel)
        }

        override fun newArray(size: Int): Array<ProductImage?> {
            return arrayOfNulls(size)
        }
    }

}
