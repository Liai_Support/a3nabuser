package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AdsResponse implements Serializable {

    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    ArrayList<AdsData> adsData = new ArrayList<>();

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AdsData> getAdsData() {
        return adsData;
    }

    public void setAdsData(ArrayList<AdsData> adsData) {
        this.adsData = adsData;
    }

    public static class AdsData implements Serializable {


        @SerializedName("id")
        int id;

        @SerializedName("image")
        String image;

        @SerializedName("status")
        String status;

        @SerializedName("title")
        String title;

        @SerializedName("description")
        String description;

        @SerializedName("couponCode")
        String couponCode;

        @SerializedName("discount")
        String discount;

        @SerializedName("minimumValue")
        String minimumValue;

        @SerializedName("startDate")
        String startDate;

        @SerializedName("endDate")
        String endDate;

        @SerializedName("count")
        int count;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCouponCode() {
            return couponCode;
        }

        public void setCouponCode(String couponCode) {
            this.couponCode = couponCode;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getMinimumValue() {
            return minimumValue;
        }

        public void setMinimumValue(String minimumValue) {
            this.minimumValue = minimumValue;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }
    }

}
