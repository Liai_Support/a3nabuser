package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CategoryResponse : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data: CategoryData? = null

}

class CategoryData : Serializable {

    @SerializedName("productCategory")
    var data: ArrayList<ProductCategoryData>? = null

    @SerializedName("bestProducts")
    var bestProducts: ArrayList<DashboardResponse.BestProduct>? = null

}

class ProductCategoryData : Serializable {

    @SerializedName("id")
    var id = 0

    @SerializedName("categoryId")
    var categoryId = 0

    @SerializedName("isSubcate")
    var isSubcate = 0

    @SerializedName("productCategoryName")
    var productCategoryName = ""

    @SerializedName("arabicName")
    var arabicName = ""

    @SerializedName("productCategoryImage")
    var productCategoryImage = ""

    @SerializedName("productCategoryStatus")
    var productCategoryStatus = ""

    @SerializedName("createdAt")
    var createdAt = ""

    var isSelected = false

}
