package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ViewCartResponse implements Serializable {

    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    ViewCartData data;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ViewCartData getData() {
        return data;
    }

    public void setData(ViewCartData data) {
        this.data = data;
    }

    public static class ViewCartData implements Serializable {

        @SerializedName("totalAmount")
        String totalAmount;

        @SerializedName("overAllDeliveryAmount")
        String damount;

        @SerializedName("overAllFastDeliveryAmount")
        String fdamount;


        @SerializedName("trustUser")
        TrustUser trustUser = new TrustUser();

        @SerializedName("discountAmount")
        String discountAmount;

        @SerializedName("settings")
        SettingsUser settings;

        @SerializedName("userWallet")
        UserWallet userWallet;

        @SerializedName("myCart")
        ArrayList<CartDetails> myCart = new ArrayList<>();

        public UserWallet getUserWallet() {
            return userWallet;
        }

        public void setUserWallet(UserWallet userWallet) {
            this.userWallet = userWallet;
        }

        public SettingsUser getSettings() {
            return settings;
        }

        public void setSettings(SettingsUser settings) {
            this.settings = settings;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getFdamount() {
            return fdamount;
        }

        public void setFdamount(String fdamount) {
            this.fdamount = fdamount;
        }

        public String getDamount() {
            return damount;
        }

        public void setDamount(String damount) {
            this.damount = damount;
        }

        public String getDiscountAmount() {
            return discountAmount;
        }

        public TrustUser getTrustUser() {
            return trustUser;
        }

        public void setTrustUser(TrustUser trustUser) {
            this.trustUser = trustUser;
        }

        public void setDiscountAmount(String discountAmount) {
            this.discountAmount = discountAmount;
        }

        public ArrayList<CartDetails> getMyCart() {
            return myCart;
        }

        public void setMyCart(ArrayList<CartDetails> myCart) {
            this.myCart = myCart;
        }
    }

    public class UserWallet implements Serializable{
        @SerializedName("walletAmount")
        String walletAmount;

        @SerializedName("points")
        String points;

        public String getWalletAmount(){
            return walletAmount;
        }

        public String getPoints(){
            return points;
        }
    }

    public static class SettingsUser implements Serializable {

        @SerializedName("minimumOrderValue")
        String minimumOrderValue;

        @SerializedName("quickDelivery")
        String quickDelivery;

        @SerializedName("flatRate")
        String flatRate;

        @SerializedName("perKM")
        String perKM;

        @SerializedName("taxAmount")
        String taxAmount;

        @SerializedName("QuickDeliveryPerKM")
        String QuickDeliveryPerKM;

        @SerializedName("walletSAR")
        String walletSAR;

        @SerializedName("walletPoints")
        String walletPoints;

        @SerializedName("freeDeliveryAmt")
        String freeDeliveryAmt;

        public String getFreeDeliveryAmt(){
            return freeDeliveryAmt;
        }

        public void setFreeDeliveryAmt(String freeDeliveryAmt){
            this.freeDeliveryAmt = freeDeliveryAmt;
        }

        public String getWalletPoints(){
            return walletPoints;
        }

        public void setWalletPoints(String walletPoints){
            this.walletPoints = walletPoints;
        }

        public String getMinimumOrderValue() {
            return minimumOrderValue;
        }

        public void setMinimumOrderValue(String minimumOrderValue) {
            this.minimumOrderValue = minimumOrderValue;
        }

        public String getWalletSAR(){
            return walletSAR;
        }

        public void setWalletSAR(String walletSAR){
            this.walletSAR = walletSAR;
        }

        public String getQuickDelivery() {
            return quickDelivery;
        }

        public void setQuickDelivery(String quickDelivery) {
            this.quickDelivery = quickDelivery;
        }

        public String getFlatRate() {
            return flatRate;
        }

        public void setFlatRate(String flatRate) {
            this.flatRate = flatRate;
        }

        public String getPerKM() {
            return perKM;
        }

        public void setPerKM(String perKM) {
            this.perKM = perKM;
        }

        public String getTaxAmount() {
            return taxAmount;
        }

        public void setTaxAmount(String taxAmount) {
            this.taxAmount = taxAmount;
        }

        public String getQuickDeliveryPerKM() {
            return QuickDeliveryPerKM;
        }

        public void setQuickDeliveryPerKM(String quickDeliveryPerKM) {
            QuickDeliveryPerKM = quickDeliveryPerKM;
        }
    }


    public static class CartDetails implements Serializable {


        @SerializedName("id")
        int id;

        @SerializedName("productId")
        int productId;

        @SerializedName("productName")
        String productName;

        @SerializedName("arabicName")
        String arabicName;

        @SerializedName("categoryName")
        String categoryName;

        @SerializedName("categoryNameArabic")
        String categoryNameArabic;

        @SerializedName("cate_minimum")
        String cate_minimum;

        @SerializedName("sub_minimum")
        String sub_minimum;

        @SerializedName("specialInstructions")
        String specialInstructions;

        @SerializedName("storeId")
        int storeId;

        @SerializedName("maxQty")
        int maxQty;

        @SerializedName("productPrice")
        float productPrice;

        @SerializedName("storeStock")
        int storeStock;

        @SerializedName("productDiscount")
        int productDiscount;

        @SerializedName("quantity")
        int quantity;

        @SerializedName("singlePrice")
        String singlePrice;

        @SerializedName("totalPrice")
        String totalPrice;

        @SerializedName("discountPrice")
        String discountPrice;

        @SerializedName("cuttingPrice")
        String cuttingPrice;

        @SerializedName("boxPrice")
        String boxPrice;

        @SerializedName("cuttingStyle")
        String cuttingStyle;

        @SerializedName("boxStyle")
        String boxStyle;

        @SerializedName("cate_processingMin")
        Integer cate_processingMin;

        @SerializedName("sub_processingMin")
        Integer sub_processingMin;

        Boolean movp = false;
        Boolean movc = false;

        Boolean sp = false;

        @SerializedName("productImages")
        ArrayList<ProductCartImages> productImages = new ArrayList<>();

        public Boolean getMovC(){
            return movc;
        }

        public void setMovC(Boolean movc){
            this.movc = movc;
        }

        public Boolean getMovP(){
            return movp;
        }

        public void setMovP(Boolean movp){
            this.movp = movp;
        }

        public Boolean getSp(){
            return sp;
        }

        public void setSp(Boolean sp){
            this.sp = sp;
        }



        public String getCategoryName(){
            return categoryName;
        }

        public String getCategoryNameArabic(){
            return categoryNameArabic;
        }

        public ArrayList<ProductCartImages> getProductImages() {
            return productImages;
        }

        public void setProductImages(ArrayList<ProductCartImages> productImages) {
            this.productImages = productImages;
        }

        public void setCuttingPrice(String cuttingPrice){
            this.cuttingPrice = cuttingPrice;
        }

        public String getCuttingPrice(){
            return cuttingPrice;
        }

        public void setBoxPrice(String boxPrice){
            this.boxPrice = boxPrice;
        }

        public String getBoxPrice(){
            return boxPrice;
        }

        public void setBoxStyle(String boxStyle){
            this.boxStyle = boxStyle;
        }

        public String getBoxStyle(){
            return boxStyle;
        }

        public void setCuttingStyle(String cuttingStyle){
            this.cuttingStyle = cuttingStyle;
        }

        public String getCuttingStyle(){
            return cuttingStyle;
        }

        public int getMaxQty() {
            return maxQty;
        }

        public void setMaxQty(int maxQty) {
            this.maxQty = maxQty;
        }

        public float getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(float productPrice) {
            this.productPrice = productPrice;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public void setArabicName(String arabicName){
            this.arabicName = arabicName;
        }

        public String getArabicName(){
            return arabicName;
        }
        public String getSpecialInstructions() {
            return specialInstructions;
        }

        public void setSpecialInstructions(String specialInstructions) {
            this.specialInstructions = specialInstructions;
        }

        public int getStoreId() {
            return storeId;
        }

        public void setStoreId(int storeId) {
            this.storeId = storeId;
        }


        public void setProductPrice(int productPrice) {
            this.productPrice = productPrice;
        }

        public int getStoreStock() {
            return storeStock;
        }

        public void setStoreStock(int storeStock) {
            this.storeStock = storeStock;
        }

        public int getProductDiscount() {
            return productDiscount;
        }

        public void setProductDiscount(int productDiscount) {
            this.productDiscount = productDiscount;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }


        public String getSinglePrice() {
            return singlePrice;
        }

        public void setSinglePrice(String singlePrice) {
            this.singlePrice = singlePrice;
        }

        public String getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(String discountPrice) {
            this.discountPrice = discountPrice;
        }

        public Integer getCate_processingMin() {
            return cate_processingMin;
        }

        public void setCate_processingMin(Integer cate_processingMin) {
            this.cate_processingMin = cate_processingMin;
        }

        public Integer getSub_processingMin() {
            return sub_processingMin;
        }

        public void setSub_processingMin(Integer sub_processingMin) {
            this.sub_processingMin = sub_processingMin;
        }

        public String getCate_minimum() {
            return cate_minimum;
        }

        public void setCate_minimum(String cate_minimum) {
            this.cate_minimum = cate_minimum;
        }

        public String getSub_minimum() {
            return sub_minimum;
        }

        public void setSub_minimum(String sub_minimum) {
            this.sub_minimum = sub_minimum;
        }
    }

    public static class ProductCartImages implements Serializable {

        @SerializedName("id")
        int id;

        @SerializedName("productImage")
        String productImage;

        @SerializedName("productId")
        int productId;

        @SerializedName("productImageStatus")
        String productImageStatus;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProductImageStatus() {
            return productImageStatus;
        }

        public void setProductImageStatus(String productImageStatus) {
            this.productImageStatus = productImageStatus;
        }
    }

    public static class TrustUser implements Serializable {

        @SerializedName("id")
        int id;

        @SerializedName("isTrustUser")
        String isTrustUser;

        @SerializedName("packageValue")
        String packageValue;

        @SerializedName("points")
        String points;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPackageValue() {
            return packageValue;
        }

        public String getIsTrustUser() {
            return isTrustUser;
        }

        public void setIsTrustUser(String isTrustUser) {
            this.isTrustUser = isTrustUser;
        }

        public void setPackageValue(String packageValue) {
            this.packageValue = packageValue;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getPoints() {
            return points;
        }
    }
}
