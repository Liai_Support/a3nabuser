package com.app.a3naab.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ListMyAddressResponse implements Serializable {


    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    ArrayList<AddressData> addressData = new ArrayList<>();

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AddressData> getAddressData() {
        return addressData;
    }

    public void setAddressData(ArrayList<AddressData> addressData) {
        this.addressData = addressData;
    }

    public static class AddressData implements Serializable, Parcelable {


        @SerializedName("id")
        int id;

        @SerializedName("userId")
        int userId;

        @SerializedName("addressPinDetails")
        String addressPinDetails;

        @SerializedName("addressType")
        String addressType;

        @SerializedName("instruction")
        String instruction;

        @SerializedName("landmark")
        String landmark;

        @SerializedName("latitude")
        String latitude;

        @SerializedName("longitude")
        String longitude;

        @SerializedName("buildingName")
        String buildingName;

        @SerializedName("createdAt")
        String createdAt;

        protected AddressData(Parcel in) {
            id = in.readInt();
            userId = in.readInt();
            addressPinDetails = in.readString();
            addressType = in.readString();
            instruction = in.readString();
            landmark = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            buildingName = in.readString();
            createdAt = in.readString();
        }

        public static final Creator<AddressData> CREATOR = new Creator<AddressData>() {
            @Override
            public AddressData createFromParcel(Parcel in) {
                return new AddressData(in);
            }

            @Override
            public AddressData[] newArray(int size) {
                return new AddressData[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getAddressPinDetails() {
            return addressPinDetails;
        }

        public void setAddressPinDetails(String addressPinDetails) {
            this.addressPinDetails = addressPinDetails;
        }

        public String getAddressType() {
            return addressType;
        }

        public void setAddressType(String addressType) {
            this.addressType = addressType;
        }

        public String getInstruction() {
            return instruction;
        }

        public void setInstruction(String instruction) {
            this.instruction = instruction;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getBuildingName() {
            return buildingName;
        }

        public void setBuildingName(String buildingName) {
            this.buildingName = buildingName;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(id);
            parcel.writeInt(userId);
            parcel.writeString(addressPinDetails);
            parcel.writeString(addressType);
            parcel.writeString(instruction);
            parcel.writeString(landmark);
            parcel.writeString(latitude);
            parcel.writeString(longitude);
            parcel.writeString(buildingName);
            parcel.writeString(createdAt);
        }
    }
}
