package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ViewOrdersResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data = OrderDetails()
}

class OrderDetails : Serializable {

    @SerializedName("orderInfo")
    var orderInfo = OrderInfo()

    @SerializedName("tax")
    var tax = ""

    @SerializedName("orderList")
    var orderList = ArrayList<OrderList>()

}

class OrderInfo : Serializable {

    @SerializedName("id")
    var id = 0

    @SerializedName("orderIDs")
    var orderIDs = ""

    @SerializedName("userId")
    var userId = ""

    @SerializedName("orderStatus")
    var orderStatus = ""

    @SerializedName("orderOn")
    var orderOn = ""

    @SerializedName("deliveryOn")
    var deliveryOn: String? = ""

    @SerializedName("isApproved")
    var isApproved: String? = ""

    @SerializedName("acceptByStore")
    var acceptByStore: String? = ""

    @SerializedName("packedByDriver")
    var packedByDriver: String? = ""

    @SerializedName("deliveryDate")
    var deliveryDate: String? = ""

    @SerializedName("totalAmount")
    var totalAmount = ""

    @SerializedName("discountAmount")
    var discountAmount = ""

    @SerializedName("grandTotal")
    var grandTotal = ""

    @SerializedName("totalQuantity")
    var totalQuantity = ""

    @SerializedName("couponDiscount")
    var couponDiscount: String? = ""

    @SerializedName("off_types")
    var off_types = ""

    @SerializedName("couponDiscountPer")
    var couponDiscountPer = ""

    @SerializedName("deleteItems")
    var deleteItems = ""

    @SerializedName("cancelDate")
    var cancelDate: String? = ""

    @SerializedName("cancelReason")
    var cancelReason: String? = ""

    @SerializedName("fastDelivery")
    var fastDelivery: String? = ""

    @SerializedName("fastDelievryCharge")
    var fastDelievryCharge: String? = ""

    @SerializedName("giveToCustomer")
    var giveToCustomer: String? = ""

    @SerializedName("takeToCustomer")
    var takeToCustomer: String? = ""




    @SerializedName("addressType")
    var addressType = ""

    @SerializedName("addressPinDetails")
    var addressPinDetails = ""

    @SerializedName("landmark")
    var landmark: String? = ""

    @SerializedName("instruction")
    var instruction: String? = ""

    @SerializedName("latitude")
    var latitude = ""

    @SerializedName("longitude")
    var longitude = ""

    @SerializedName("buildingName")
    var buildingName = ""

    @SerializedName("addressId")
    var addressId = ""

    @SerializedName("timeId")
    var timeId = ""

    @SerializedName("timeText")
    var timeText = ""

    @SerializedName("paytype")
    var paytype = ""

    @SerializedName("paymentId")
    var paymentId = ""

    @SerializedName("returnOrderId")
    var returnOrderId = ""

    @SerializedName("returnTransId")
    var returnTransId = ""

    @SerializedName("ordertax")
    var ordertax = ""

    @SerializedName("taxvalue")
    var taxvalue = ""

    @SerializedName("pointsAmount")
    var pointsAmount = ""

    @SerializedName("paidByWallet")
    var paidByWallet = ""

    @SerializedName("deliveryOn_fromTime")
    var deliveryOn_fromTime = ""

    @SerializedName("deliveryOn_toTime")
    var deliveryOn_toTime = ""

}

class OrderList : Serializable {


    @SerializedName("id")
    var id = 0

    @SerializedName("orderId")
    var orderId = 0

    @SerializedName("productId")
    var productId = 0

    @SerializedName("cate_processingMin")
    var cate_processingMin: Int? = 0

    @SerializedName("sub_processingMin")
    var sub_processingMin: Int? = 0

    @SerializedName("cuttingName")
    var cuttingName: String? = ""

    @SerializedName("arabicName")
    var arabicName: String? = ""

    @SerializedName("cuttingPrice")
    var cuttingPrice: String? = ""

    @SerializedName("boxName")
    var boxName: String? = ""

    @SerializedName("boxPrice")
    var boxPrice: String? = ""

    @SerializedName("quantity")
    var quantity: String? = ""

    @SerializedName("productName")
    var productName: String? = ""

    @SerializedName("productPrice")
    var productPrice: String? = ""

    @SerializedName("productDiscount")
    var productDiscount: String? = ""

    @SerializedName("totalPrice")
    var totalPrice: String? = ""

    @SerializedName("isFavourite")
    var isFavourite: String? = ""

    @SerializedName("productImage")
    var productImage = ArrayList<ProductImages>()


}

class ProductImages : Serializable {


    @SerializedName("id")
    var id = 0

    @SerializedName("productImage")
    var productImage: String? = ""

    @SerializedName("productId")
    var productId: String? = ""

    @SerializedName("productImageStatus")
    var productImageStatus: String? = ""


}

