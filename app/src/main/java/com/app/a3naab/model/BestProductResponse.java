package com.app.a3naab.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BestProductResponse implements Serializable {

    @SerializedName("error")
    Boolean error;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    BestProductdata data;


    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BestProductdata getData() {
        return data;
    }

    public void setData(BestProductdata data) {
        this.data = data;
    }

    public static class BestProductdata implements Serializable {

        @SerializedName("products")
        ArrayList<DashboardResponse.BestProduct> products = new ArrayList<>();

        public ArrayList<DashboardResponse.BestProduct> getProducts() {
            return products;
        }

        public void setProducts(ArrayList<DashboardResponse.BestProduct> products) {
            this.products = products;
        }
    }


}

