package com.app.a3naab.model

class FireBaseVerificationResponse {

    var error: String? = null
    var message: String? = null
    var verificationCode: String? = null
}