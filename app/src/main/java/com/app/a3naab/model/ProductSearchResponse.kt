package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProducSearchtListResponse : Serializable {

    @SerializedName("error")
    var error = false

    @SerializedName("message")
    var message = ""


    @SerializedName("data")
    var data: ArrayList<ProductListData>? = null

}

