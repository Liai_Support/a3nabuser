package com.app.a3naab.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DeliveryTimeResponse : Serializable {

    @SerializedName("error")
    var error: Boolean = false

    @SerializedName("message")
    var message: String = ""

    @SerializedName("data")
    var data: DeliveryTimeList = DeliveryTimeList()


}

class DeliveryTimeList : Serializable {

    @SerializedName("list")
    var list = ArrayList<DeliveryTimeData>()

}

class DeliveryTimeData : Serializable {

    @SerializedName("id")
    var id: Int = 0

    @SerializedName("timeText")
    var timeText: String = ""

    @SerializedName("fromTime")
    var fromTime: String = ""

    @SerializedName("toTime")
    var toTime: String = ""

    @SerializedName("status")
    var status: String = ""

    @SerializedName("maxOrder")
    var maxOrder: String = ""

    @SerializedName("usedCount")
    var usedCount: String = ""

    var isSelected = false

    var isDisabled = true

}

