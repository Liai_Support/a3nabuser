package com.app.a3naab.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.model.BestProductResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ProductDetailsResponse;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.model.WalletAmountTransactionResponse;
import com.app.a3naab.model.WalletPointsTransactionResponse;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.repository.ProductRepository;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductViewModel extends AndroidViewModel {

    ProductRepository repository = new ProductRepository();
    SharedHelper sharedHelper;


    public ProductViewModel(@NonNull Application application) {
        super(application);
        sharedHelper = new SharedHelper(application.getBaseContext());
    }

    public MutableLiveData<ProductDetailsResponse> getProductDetails(Context context, int categoryId,String storeid) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("productId", String.valueOf(categoryId));
            jsonObject.put("userId", sharedHelper.getUserId());
            jsonObject.put("storeId", storeid);

            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getProductDetails(Utils.getInputs(context, jsonObject, URLHelper.VIEWPRODUCTDETAILS));

    }

    public MutableLiveData<CommonResponse> addItemToCart(Context context, int id, String storeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("productId", id);
            jsonObject.put("key", 1);
            jsonObject.put("storeId", storeId);
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.addItemToCart(Utils.getInputs(context, jsonObject, URLHelper.ADDCART));
    }

    public MutableLiveData<CommonResponse> addtoCartWithStyle(Context context, int id, String storeId, String cutStyle, String boxStyle, String special) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("productId", id);
            jsonObject.put("key", 1);
            jsonObject.put("storeId", storeId);
            jsonObject.put("cuttingStyle", cutStyle);
            jsonObject.put("boxStyle", boxStyle);
            jsonObject.put("specialInstructions", special);

            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.addtoCartWithStyle(Utils.getInputs(context, jsonObject, URLHelper.ADDCART));
    }

    public MutableLiveData<CommonResponse> removeItemToCart(Context context, int id, String storeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("productId", id);
            jsonObject.put("key", 0);
            jsonObject.put("storeId", storeId);
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.removeItemFromCart(Utils.getInputs(context, jsonObject, URLHelper.ADDCART));
    }

    public MutableLiveData<CommonResponse> removeItemToCartwithstyle(Context context, int id, String storeId, String cutStyle, String boxStyle, String special) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("productId", id);
            jsonObject.put("key", 0);
            jsonObject.put("storeId", storeId);
            jsonObject.put("cuttingStyle", cutStyle);
            jsonObject.put("boxStyle", boxStyle);
            jsonObject.put("specialInstructions", special);

            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.addtoCartWithStyle(Utils.getInputs(context, jsonObject, URLHelper.ADDCART));
    }

    public MutableLiveData<CommonResponse> setFavorite(Context context, int proId, int sid,String status) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("productId", proId);
            jsonObject.put("isFavourite", status);
            jsonObject.put("storeId", sid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.setFavorite(Utils.getInputs(context, jsonObject, URLHelper.ADDFAVOURITEPRODUCT));
    }

    public MutableLiveData<BestProductResponse> getBestProduct(Context context, int pageNumber) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", pageNumber);
            jsonObject.put("userId", sharedHelper.getUserId());
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getBestProduct(Utils.getInputs(context, jsonObject, URLHelper.VIEWBESTPRODUCT));

    }

    public MutableLiveData<WalletAmountTransactionResponse> getWalletAmountTransaction(Context context, int pageNumber) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", pageNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getWalletAmountTransaction(Utils.getInputs(context, jsonObject, URLHelper.USERWALLETAMOUNTTRASACTION));

    }

    public MutableLiveData<WalletPointsTransactionResponse> getWalletPointTransaction(Context context, int pageNumber) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", pageNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getWalletPointsTransaction((Utils.getInputs(context, jsonObject, URLHelper.USERWALLETPOINTSTRASACTION)));

    }

    public MutableLiveData<BestProductResponse> getFavouriteList(Context context, int pageNumber) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", pageNumber);
            jsonObject.put("userId", sharedHelper.getUserId());
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getFavouriteList(Utils.getInputs(context, jsonObject, URLHelper.VIEWFAVOURITEPRODUCT));

    }

    public MutableLiveData<BestProductResponse> getRelatedProduct(Context context, int pageNumber, String productId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", pageNumber);
            jsonObject.put("userId", sharedHelper.getUserId());
            jsonObject.put("productId", productId);
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getBestProduct(Utils.getInputs(context, jsonObject, URLHelper.VIEWRELATEDPRODUCT));

    }

    public MutableLiveData<ViewCartResponse> getCartDetails(Context context) {
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("user_lat", sharedHelper.getSelectedDeliveryToLat());
            jsonObject.put("user_lng", sharedHelper.getSelectedDeliveryToLng());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return repository.getAddCart(Utils.getInputs(context, jsonObject, URLHelper.VIEWCART));

    }

    public LiveData<CommonResponse> removeCartFromCart(Context context, int id) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cartId", id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.removeCartFromCart(Utils.getInputs(context, jsonObject, URLHelper.DELETE_CART));


    }

    public LiveData<ProfileDataResponse> getProfileDetails(Context context) {
        return repository.getUserProfile(Utils.getInputs(context, null, URLHelper.GETUSERPROFILE));


    }

    public LiveData<CommonResponse> updateProfile(Context context, String trim, String trim1, String trim2, String trim3, String
            trim4, String trim5, String trim6, String cpp, String gender) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("firstName", trim);
            jsonObject.put("lastName", trim1);
            jsonObject.put("email", trim2);
            jsonObject.put("DOB", trim6 + "-" + trim5 + "-" + trim4);
            jsonObject.put("mobileNumber", trim3);
            jsonObject.put("gender", gender);
            jsonObject.put("countryCode", cpp);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.updateProfile(Utils.getInputs(context, jsonObject, URLHelper.UPDATEPROFILE));


    }

    public LiveData<CommonResponse> setFeedBack(Context requireContext, Float app, String comments) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("apps", "android");
            jsonObject.put("commemts", comments);
            jsonObject.put("rating", app);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.setFeedBack(Utils.getInputs(requireContext, jsonObject, URLHelper.ADDFEEDBACK));


    }

    public LiveData<CommonResponse> updateSettings(Context context, Boolean offersNotify,
                                                   Boolean ordersNotify, Boolean announcementNotify, Boolean othersNotify, int type) {


        JSONObject jsonObject = new JSONObject();
        try {
            if (type == 1)
                if (offersNotify)
                    jsonObject.put("offersNotify", "true");
                else
                    jsonObject.put("offersNotify", "false");
            if (type == 2)
                if (ordersNotify)
                    jsonObject.put("ordersNotify", "true");
                else
                    jsonObject.put("ordersNotify", "false");

            if (type == 3)
                if (announcementNotify)
                    jsonObject.put("announcementNotify", "true");
                else
                    jsonObject.put("announcementNotify", "false");
            if (type == 4)
                if (othersNotify)
                    jsonObject.put("othersNotify", "true");
                else
                    jsonObject.put("othersNotify", "false");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.updateSettings(Utils.getInputs(context, jsonObject, URLHelper.UPDATEUSERSETTINGS));


    }
}
