package com.app.a3naab.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.a3naab.R;
import com.app.a3naab.model.AutoCompleteAddressResponseModel;
import com.app.a3naab.model.AutoCompleteResponse;
import com.app.a3naab.model.GetLatLongFromIdResponseModel;
import com.app.a3naab.network.InputForAPI;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.repository.MapRepository;
import com.app.a3naab.utils.SharedHelper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MapViewModel extends AndroidViewModel {


    MapRepository repository = new MapRepository();
    Application applicationIns;
    SharedHelper sharedHelper;

    public MapViewModel(@NonNull Application application) {
        super(application);
        applicationIns = application;
        sharedHelper = new SharedHelper(application);
    }


    public LiveData<AutoCompleteResponse> getAddress(Context context, InputForAPI input) {
        return repository.getAddress(input);
    }

    public LiveData<AutoCompleteAddressResponseModel> searchAutoComplete(
            Context context,
            String place,
            String lat,
            String lng
    ) {

        InputForAPI apiInputs = new InputForAPI(context,"");
        apiInputs.setUrl(getAutoCompleteUrl(place, lat, lng));

        return repository.getAutoCompleteAddress(apiInputs);
    }


    private String getAutoCompleteUrl(String input, String currentLat, String currentLong) {
        StringBuilder urlString = new StringBuilder();
        urlString.append(URLHelper.GOOGLE_API_AUTOCOMPLETE_BASE_URL);
        urlString.append("input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        urlString.append("&language=en");
        urlString.append("&location=$currentLat,$currentLong&radius=50");
        urlString.append("&key=").append(applicationIns.getString(R.string.map_api_key));
        return urlString.toString();
    }

    private String getPlaceDetails(String placeID) {
        return URLHelper.GOOGLE_API_PLACE_DETAILS_BASE_URL + "placeid=" + placeID + "&key=" + applicationIns.getString(
                R.string.map_api_key
        );
    }

    public LiveData<GetLatLongFromIdResponseModel> getLatLngDetails(Context context, String placeID) {

        InputForAPI apiInputs = new InputForAPI(context,"");
        apiInputs.setUrl(getPlaceDetails(placeID));

        return repository.getLatLngDetails(apiInputs);
    }


}
