package com.app.a3naab.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.BuildConfig;
import com.app.a3naab.model.AdsResponse;
import com.app.a3naab.model.CheckReorderResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.CouponResponse;
import com.app.a3naab.model.CurrentLocationIdResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.DashboardSearchResponse;
import com.app.a3naab.model.DeliveryTimeResponse;
import com.app.a3naab.model.GetProfileResponse;
import com.app.a3naab.model.ListFaqResponse;
import com.app.a3naab.model.ListMyAddressResponse;
import com.app.a3naab.model.OrderListResponse;
import com.app.a3naab.model.PaymentRefundResponse;
import com.app.a3naab.model.PaymentResponse;
import com.app.a3naab.model.PaymentResponse1;
import com.app.a3naab.model.ViewOrdersResponse;
import com.app.a3naab.network.InputForAPI;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.repository.ShoppingRepository;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;

public class ShoppingViewModel extends AndroidViewModel {

    ShoppingRepository repository = new ShoppingRepository();
    SharedHelper sharedHelper;

    public ShoppingViewModel(@NonNull Application application) {
        super(application);
        sharedHelper = new SharedHelper(application.getBaseContext());
    }


    public void updateToken(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("os", "android");
            jsonObject.put("fcmToken", sharedHelper.getFirebaseToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        repository.updateToken(Utils.getInputs(context, jsonObject, URLHelper.UPDATEDEVICETOKEN));
    }

    public MutableLiveData<DashboardResponse> getDashBoardDetails(Context context,String avalue) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", sharedHelper.getUserId());
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            }
            else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return repository.getDashBoardDetails(Utils.getInputs(context, jsonObject, URLHelper.HOMEDASHBOARD));

    }

    public MutableLiveData<AdsResponse> getAds(Context context) {
        return repository.getAds(Utils.getInputs(context, null, URLHelper.GETALLADS));
    }

    public MutableLiveData<AdsResponse> searchAds(Context context, String title) {


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", title);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getSearchAds(Utils.getInputs(context, jsonObject, URLHelper.SEARCHADS));
    }


    public MutableLiveData<ListMyAddressResponse> getAddressList(Context requireContext) {
        return repository.getAddressList(Utils.getInputs(requireContext, null, URLHelper.GETMYADDRESS));
    }

    public MutableLiveData<ListFaqResponse> getfaqList(Context requireContext) {
        return repository.getfaqlist(Utils.getInputs(requireContext, null, URLHelper.GETFAQ));
    }


    public MutableLiveData<CommonResponse> addAddress(Context context, String selectedItem,
                                                      String latitude, String longitude, String pinDetails,
                                                      String appartMentNumber, String nearestTeacher, String deliveryInstruction, String tag) {

        JSONObject jsonObject = new JSONObject();
        try {
            if (selectedItem.equals("Other") || selectedItem.equals("آخر")) {
                jsonObject.put("addressType", tag);
            } else {
                jsonObject.put("addressType", selectedItem);
            }
            jsonObject.put("addressPinDetails", pinDetails);

            if (appartMentNumber.equalsIgnoreCase("")) {
                jsonObject.put("buildingName", " ");
            } else {
                jsonObject.put("buildingName", appartMentNumber);
            }


            if (nearestTeacher.equalsIgnoreCase("")) {
                jsonObject.put("landmark", " ");
            } else {
                jsonObject.put("landmark", nearestTeacher);
            }

            if (deliveryInstruction.equalsIgnoreCase("")) {
                jsonObject.put("instruction", " ");
            } else {
                jsonObject.put("instruction", deliveryInstruction);
            }


            jsonObject.put("latitude", latitude);
            jsonObject.put("longitude", longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return repository.addAddress(Utils.getInputs(context, jsonObject, URLHelper.ADDADDRESS));
    }

    public LiveData<CommonResponse> updateAddress(Context context, String selectedItem,
                                                  String latitude, String longitude, String pinDetails,
                                                  String appartMentNumber, String nearestTeacher, String deliveryInstruction, String tag, int addressId) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (selectedItem.equals("Other") || selectedItem.equals("آخر")) {
                jsonObject.put("addressType", tag);
            } else {
                jsonObject.put("addressType", selectedItem);
            }
            jsonObject.put("addressPinDetails", pinDetails);

            if (appartMentNumber.equalsIgnoreCase("")) {
                jsonObject.put("buildingName", " ");
            } else {
                jsonObject.put("buildingName", appartMentNumber);
            }

            if (nearestTeacher.equalsIgnoreCase("")) {
                jsonObject.put("landmark", " ");
            } else {
                jsonObject.put("landmark", nearestTeacher);
            }

            if (deliveryInstruction.equalsIgnoreCase("")) {
                jsonObject.put("instruction", " ");
            } else {
                jsonObject.put("instruction", deliveryInstruction);
            }

            jsonObject.put("latitude", latitude);
            jsonObject.put("longitude", longitude);
            jsonObject.put("addressId", String.valueOf(addressId));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return repository.updateAddress(Utils.getInputs(context, jsonObject, URLHelper.UPDATEADDRESS));
    }

    public MutableLiveData<DashboardSearchResponse> getDashboardSearch(Context context, String text) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", text);
            jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
            jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            //new
            //jsonObject.put("id", 0);
            //jsonObject.put("type", "SUBCATEGORY");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //new url changed
        return repository.getDashboardSearch(Utils.getInputs(context, jsonObject, URLHelper.HOMESEARCH));

    }

    public MutableLiveData<DeliveryTimeResponse> getDeliverydate(Context context, int selectedDayId,String date) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dayId", selectedDayId);
            jsonObject.put("date", date);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getDeliverydate(Utils.getInputs(context, jsonObject, URLHelper.GETDELIVERYTIME));
    }

    public MutableLiveData<CouponResponse> checkCouponCode(Context context, String code) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("couponCode", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.checkCouponCode(Utils.getInputs(context, jsonObject, URLHelper.CHECKCOUPONCODE));
    }

    public MutableLiveData<CommonResponse> placeOrder(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("addressId", CartSessionManager.getInstance().getSelectedId());
            jsonObject.put("deliveryTimeId", CartSessionManager.getInstance().getSelectedTime());
            jsonObject.put("paymentId", CartSessionManager.getInstance().getPaymentOption());
            jsonObject.put("deliveryDate", CartSessionManager.getInstance().getSelectedDate());
            jsonObject.put("couponCode", CartSessionManager.getInstance().getCouponCode());
            jsonObject.put("fastDelivery", CartSessionManager.getInstance().getIsFastDeliverySelected());
            jsonObject.put("isTrustUser", CartSessionManager.getInstance().getTrustUser());
            if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("true")) {
                jsonObject.put("deleteItems", 0);
            } else {
                jsonObject.put("deleteItems", 1);
            }
            jsonObject.put("isWallet", CartSessionManager.getInstance().getwallet());
            jsonObject.put("isPoint", CartSessionManager.getInstance().isEnablePoints());
            jsonObject.put("ordertax", CartSessionManager.getInstance().getFlatDiscount());
            if(CartSessionManager.getInstance().getIsFastDeliverySelected() == 1){
                if(CartSessionManager.getInstance().getIsFreeDeliverySelected() == 1){
                    jsonObject.put("deliveryCharge", 0);
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());
                }
                else {
                    jsonObject.put("deliveryCharge", sharedHelper.getFastDeliverycharge());
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());

                }
            }
            else {
                if(CartSessionManager.getInstance().getIsFreeDeliverySelected() == 1){
                    jsonObject.put("deliveryCharge", 0);
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());
                }
                else {
                    jsonObject.put("deliveryCharge", sharedHelper.getDeliverycharge());
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());
                }
            }
            jsonObject.put("paymentorderid", CartSessionManager.getInstance().getpaymentorderid());
            jsonObject.put("paymenttransactionid", CartSessionManager.getInstance().getpaymenttransactionid());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.placeOrder(Utils.getInputs(context, jsonObject, URLHelper.PLACEORDER));

    }

    public MutableLiveData<CommonResponse> placeReOrder(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("addressId", CartSessionManager.getInstance().getSelectedId());
            jsonObject.put("deliveryTimeId", CartSessionManager.getInstance().getSelectedTime());
            jsonObject.put("paymentId", CartSessionManager.getInstance().getPaymentOption());
            jsonObject.put("deliveryDate", CartSessionManager.getInstance().getSelectedDate());
            jsonObject.put("couponCode", CartSessionManager.getInstance().getCouponCode());
            jsonObject.put("fastDelivery", CartSessionManager.getInstance().getIsFastDeliverySelected());
            jsonObject.put("orderId", CartSessionManager.getInstance().getOrderId());
            jsonObject.put("isTrustUser", CartSessionManager.getInstance().getTrustUser());

            if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("true")) {
                jsonObject.put("deleteItems", 0);
            } else {
                jsonObject.put("deleteItems", 1);
            }

            jsonObject.put("isWallet", CartSessionManager.getInstance().getwallet());
            jsonObject.put("isPoint", CartSessionManager.getInstance().isEnablePoints());
            jsonObject.put("ordertax", CartSessionManager.getInstance().getFlatDiscount());
            if(CartSessionManager.getInstance().getIsFastDeliverySelected() == 1){
                if(CartSessionManager.getInstance().getIsFreeDeliverySelected() == 1){
                    jsonObject.put("deliveryCharge", 0);
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());
                }
                else {
                    jsonObject.put("deliveryCharge", sharedHelper.getFastDeliverycharge());
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());

                }
            }
            else {
                if(CartSessionManager.getInstance().getIsFreeDeliverySelected() == 1){
                    jsonObject.put("deliveryCharge", 0);
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());
                }
                else {
                    jsonObject.put("deliveryCharge", sharedHelper.getDeliverycharge());
                    jsonObject.put("discountVal", CartSessionManager.getInstance().getDiscount());
                    jsonObject.put("off_types", CartSessionManager.getInstance().getOfferType());
                }
            }
            jsonObject.put("paymentorderid", CartSessionManager.getInstance().getpaymentorderid());
            jsonObject.put("paymenttransactionid", CartSessionManager.getInstance().getpaymenttransactionid());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.placeReOrder(Utils.getInputs(context, jsonObject, URLHelper.REORDER));

    }

    public MutableLiveData<OrderListResponse> getActiveOrders(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", 1);
            jsonObject.put("type", "ACTIVE");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getActiveOrders(Utils.getInputs(context, jsonObject, URLHelper.USERORDERLIST));
    }

    public MutableLiveData<OrderListResponse> getPastOrders(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", 1);
            jsonObject.put("type", "PAST");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getPastOrders(Utils.getInputs(context, jsonObject, URLHelper.USERORDERLIST));
    }

    public MutableLiveData<CommonResponse> rescheduelOrder(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("orderId", CartSessionManager.getInstance().getOrderId());
            jsonObject.put("deliveryDate", CartSessionManager.getInstance().getSelectedDate());
            jsonObject.put("deliveryTimeId", CartSessionManager.getInstance().getSelectedTime());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.rescheduelOrder(Utils.getInputs(context, jsonObject, URLHelper.RESCHEDULE));

    }

    public MutableLiveData<ViewOrdersResponse> getOrderDetails(Context context, int orderId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("orderId", orderId);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getOrderDetails(Utils.getInputs(context, jsonObject, URLHelper.USERVIEWORDER));

    }


    public MutableLiveData<CheckReorderResponse> checkreorder(Context context, int orderId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("orderId", orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.checkreorder(Utils.getInputs(context, jsonObject, URLHelper.CHECKREORDER));

    }

    public MutableLiveData<CommonResponse> cancelOrder(Context context, String orderId, String reason) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("orderId", orderId);
            jsonObject.put("reason", reason);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.cancelOrder(Utils.getInputs(context, jsonObject, URLHelper.USERORDERCANCELLED));

    }


    public MutableLiveData<CommonResponse> deleteAddress(Context context, String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("addressId", id);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.deleteAddress(Utils.getInputs(context, jsonObject, URLHelper.DELETEADDRESS));

    }

    public MutableLiveData<CommonResponse> getclearcartData(Context context) {
        return repository.getcartclear(Utils.getInputs(context, null, URLHelper.CLEARCART));

    }

    public MutableLiveData<GetProfileResponse> getProfileData(Context context) {
        return repository.getProfileData(Utils.getInputs(context, null, URLHelper.GETUSERPROFILE));

    }


    public MutableLiveData<CurrentLocationIdResponse> getCurrentAddressId(Context context,
                                                                          String pinDetails, String latitude, String longitude) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("addressType", "currentAddress");
            jsonObject.put("addressPinDetails", pinDetails);
            jsonObject.put("buildingName", " ");
            jsonObject.put("landmark", " ");
            jsonObject.put("instruction", " ");

            jsonObject.put("latitude", latitude);
            jsonObject.put("longitude", longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return repository.getCurrentAddressId(Utils.getInputs(context, jsonObject, URLHelper.UPDATECURRENTADDRESS));
    }


    public void updateUnAvailableProduct(Context context, String latitude, String longitude, String lastKnownAddress, String userId) {


        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("address", lastKnownAddress);
            jsonObject.put("userId", userId);
            jsonObject.put("latitude", latitude);
            jsonObject.put("longitude", longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        repository.updateUnAvailableProduct(Utils.getInputs(context, jsonObject, URLHelper.UPDATEUNAVAILABLEPRODUCT));

    }

    public MutableLiveData<CommonResponse> setRating(Context context, Float app, Float driver, Float product, String comments, int orderId) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("orderId", orderId);
            jsonObject.put("appRating", app);
            jsonObject.put("driverRating", driver);
            jsonObject.put("productRating", product);
            jsonObject.put("commemts", comments);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return repository.setRating(Utils.getInputs(context, jsonObject, URLHelper.ADDRATING));
    }

    public MutableLiveData<PaymentResponse> startPayment(Context context, String amount) {

        JSONObject jsonObject = new JSONObject();
        JSONObject order = new JSONObject();
        JSONObject configuration = new JSONObject();


        try {

            jsonObject.put("apiOperation", "INITIATE");

            order.put("reference","Ref#1234");
            order.put("amount",amount);
            order.put("currency","SAR");
            order.put("name","Payment Order");
            order.put("channel","Mobile");
            order.put("category","pay");

            jsonObject.put("order",order);

            configuration.put("tokenizeCc","true");
            configuration.put("returnUrl","https://a3naab.com");
            configuration.put("locale","en");

            jsonObject.put("configuration",configuration);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI=  Utils.getInputs(context, jsonObject, URLHelper.PAYMENT_URL);

        HashMap<String, String> headers = new HashMap<>();

//        headers.put("Content-Type","application/json");
//        headers.put("Content-Type","application/x-www-form-urlencoded");
        // headers.put("Authorization", BuildConfig.PAYMENT_KEY);
        headers.put("Authorization", URLHelper.PAYMENT_KEY);

        inputForAPI.setHeaders(headers);

        return repository.startPayment(inputForAPI);
    }

    public MutableLiveData<PaymentResponse> getorder(Context context,String orderid) {

        InputForAPI inputForAPI=  Utils.getInputs(context, null, URLHelper.PAYMENT_URL+"/"+orderid);

        HashMap<String, String> headers = new HashMap<>();

//        headers.put("Content-Type","application/json");
//        headers.put("Content-Type","application/x-www-form-urlencoded");
        // headers.put("Authorization", BuildConfig.PAYMENT_KEY);
        headers.put("Authorization", URLHelper.PAYMENT_KEY);

        inputForAPI.setHeaders(headers);

        return repository.getorder(inputForAPI);
    }

    public MutableLiveData<PaymentResponse1> sale(Context context, String orderid) {

        JSONObject jsonObject = new JSONObject();
        JSONObject order = new JSONObject();
        try {
            jsonObject.put("apiOperation", "SALE");
            order.put("id",orderid);
            jsonObject.put("order",order);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI=  Utils.getInputs(context, jsonObject, URLHelper.PAYMENT_URL);

        HashMap<String, String> headers = new HashMap<>();

//        headers.put("Content-Type","application/json");
//        headers.put("Content-Type","application/x-www-form-urlencoded");
        // headers.put("Authorization", BuildConfig.PAYMENT_KEY);
        headers.put("Authorization", URLHelper.PAYMENT_KEY);

        inputForAPI.setHeaders(headers);

        return repository.sale(inputForAPI);
    }

    public MutableLiveData<PaymentRefundResponse> refundpayment(Context context, String amount, String orderid) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("apiOperation", "REFUND");

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Id", orderid);

            jsonObject.put("order", jsonObject1);

            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("amount", amount.toString());
            jsonObject2.put("currency", "SAR");

            jsonObject.put("transaction", jsonObject2);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI1=  Utils.getInputs(context, jsonObject, URLHelper.PAYMENT_URL);

        HashMap<String, String> headers = new HashMap<>();

        headers.put("Authorization", URLHelper.PAYMENT_KEY);

        inputForAPI1.setHeaders(headers);

        return repository.refundpayment(inputForAPI1);
        //return repository.refundpayment(Utils.getInputs(context, jsonObject, URLHelper.USERORDERCANCELLED));

    }


}
