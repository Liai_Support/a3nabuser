package com.app.a3naab.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.app.a3naab.model.CategoryResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ProducSearchtListResponse;
import com.app.a3naab.model.ProductListData;
import com.app.a3naab.model.ProductListResponse;
import com.app.a3naab.model.SearchCategoryDataResponse;
import com.app.a3naab.model.SubCategoryResponse;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.paging.ProductListDataSource;
import com.app.a3naab.paging.ProductListDataSourceFactory;
import com.app.a3naab.repository.CategoryRepository;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class CategoryViewModel extends AndroidViewModel {

    CategoryRepository repository = new CategoryRepository();
    SharedHelper sharedHelper;
    public int selectedCategoryId = 0;
    public int selectedSubCategoryId = -1;
    public int havingSubCategory = 0;

    public LiveData<PagedList<ProductListData>> itemPagedList = new MutableLiveData<>();
    LiveData<PageKeyedDataSource<Integer, ProductListData>> liveDataSource;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
        sharedHelper = new SharedHelper(application.getBaseContext());
    }

    public MutableLiveData<CategoryResponse> getCategoryDetails(Context context, int categoryId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("categoryId", String.valueOf(categoryId));
            jsonObject.put("userId", sharedHelper.getUserId());
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getCategoryDetails(Utils.getInputs(context, jsonObject, URLHelper.GETPRODUCTCATEGORY));

    }

    public void configDataSource(Activity activity) {

        ProductListDataSource.PAGENUMBER = 1;
        ProductListDataSource.TOTAL_PAGENUMBER = 300;

        ProductListDataSourceFactory taxiDataSourceFactory = new ProductListDataSourceFactory();
        if (havingSubCategory == 0) {
            taxiDataSourceFactory.setInputs(activity, "CATEGORY", selectedCategoryId);
        } else {
            taxiDataSourceFactory.setInputs(activity, "SUBCATEGORY", selectedSubCategoryId);
        }

        liveDataSource = taxiDataSourceFactory.getItemLiveDataSource();
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(ProductListDataSource.TOTAL_PAGENUMBER).build();

        itemPagedList = new LivePagedListBuilder<>(taxiDataSourceFactory, pagedListConfig).build();

    }

    public MutableLiveData<SubCategoryResponse> getSubCategory(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", selectedCategoryId);
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getSubCategory(Utils.getInputs(context, jsonObject, URLHelper.GETPRODUCTSUBCATEGORY));

    }

    public LiveData<SearchCategoryDataResponse> getSearchedCategory(Context searchCategoryActivity, int id, String str) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("categoryId", id);
            jsonObject.put("name", str);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getSearchedCategory(Utils.getInputs(searchCategoryActivity, jsonObject, URLHelper.PRODUCTCATEGORYSEARCH));
    }

    public LiveData<ProducSearchtListResponse> getProductSearch(Context searchSubCategoryActivity, int id, String str, int isHavingSub) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            if (isHavingSub == 0) {
                jsonObject.put("type", "CATEGORY");
            } else {
                jsonObject.put("type", "SUBCATEGORY");
            }
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }


            jsonObject.put("name", str);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getProductSearch(Utils.getInputs(searchSubCategoryActivity, jsonObject, URLHelper.PRODUCTSEARCH));
    }

    public MutableLiveData<ProductListResponse> getProductList(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {

            if (havingSubCategory == 0) {
                jsonObject.put("id", selectedCategoryId);
                jsonObject.put("type", "CATEGORY");
            } else {
                jsonObject.put("id", selectedSubCategoryId);
                jsonObject.put("type", "SUBCATEGORY");
            }

            jsonObject.put("pageNumber", 1);
            jsonObject.put("userId", sharedHelper.getUserId());
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getProductListStatis(Utils.getInputs(context, jsonObject, URLHelper.GETPRODUCTLIST));


    }


    public MutableLiveData<ProductListResponse> getAllProductData(Context context, int subId) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("id", subId);
            jsonObject.put("type", "SUBCATEGORY");

            jsonObject.put("pageNumber", 1);
            jsonObject.put("userId", sharedHelper.getUserId());
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getProductListStatis(Utils.getInputs(context, jsonObject, URLHelper.GETPRODUCTLIST));


    }


    public MutableLiveData<CommonResponse> requestTypingText(Context context, String type, String searchedText) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("searchText", searchedText);
            jsonObject.put("type", type);
            jsonObject.put("userId", sharedHelper.getUserId());
            if (sharedHelper.getSelectedDeliveryToLat().equalsIgnoreCase("")) {
                jsonObject.put("lat", sharedHelper.getLastKnownLat());
                jsonObject.put("lng", sharedHelper.getLastKnownLng());
            } else {
                jsonObject.put("lat", sharedHelper.getSelectedDeliveryToLat());
                jsonObject.put("lng", sharedHelper.getSelectedDeliveryToLng());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.requestTypingText(Utils.getInputs(context, jsonObject, URLHelper.USERSEARCH));

    }
}
