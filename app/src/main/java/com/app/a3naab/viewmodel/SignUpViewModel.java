package com.app.a3naab.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.CountDownTimer;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.FireBaseVerificationResponse;
import com.app.a3naab.model.LoginResponse;
import com.app.a3naab.model.RegisterResponse;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.repository.SignupRepository;
import com.app.a3naab.utils.Utils;
import com.google.firebase.auth.PhoneAuthCredential;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpViewModel extends AndroidViewModel {

    SignupRepository signupRepository = new SignupRepository();
    public MutableLiveData<String> time = new MutableLiveData<>();


    public SignUpViewModel(@NonNull Application application) {
        super(application);
    }


    public MutableLiveData<FireBaseVerificationResponse> requestVerificationCode(Activity context, String mobile) {
        return signupRepository.requestVerificationCode(context, mobile);
    }

    public MutableLiveData<FireBaseVerificationResponse> verifyOtp(PhoneAuthCredential credential) {
        return signupRepository.verifyOtp(credential);
    }

    public void resetTimer() {

        new CountDownTimer(1000 * 60, 1000) {

            @Override
            public void onTick(long l) {
                int minute = (int) ((l / 1000) / 60);
                int seconds = (int) ((l / 1000) % 60);
                time.setValue(Utils.formatedValues(minute) + ":" + Utils.formatedValues(seconds));

            }

            @Override
            public void onFinish() {
                time.setValue("");
            }
        }.start();


    }

    public LiveData<LoginResponse> login(Context context, String countryCode, String mobile) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("countryCode", countryCode);
            jsonObject.put("mobileNumber", mobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return signupRepository.login(Utils.getInputs(context, jsonObject, URLHelper.LOGIN));
    }

    public LiveData<CommonResponse> verifyotp(Context context, String mobile, String otp) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile", mobile);
            jsonObject.put("otp", otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return signupRepository.verifyotp(Utils.getInputs(context, jsonObject, URLHelper.VERIFYOTP));
    }




    public MutableLiveData<RegisterResponse> createAccount(Context context, String firstName, String lastName, String gender, String countryCode, String mobile, String referalCode) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("firstName", firstName);
            jsonObject.put("lastName", lastName);
            jsonObject.put("countryCode", countryCode);
            jsonObject.put("mobileNumber", mobile);
            jsonObject.put("gender", gender);
            jsonObject.put("referralCode", referalCode);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return signupRepository.createAccount(Utils.getInputs(context, jsonObject, URLHelper.REGISTER));

    }
}
