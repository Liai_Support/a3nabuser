package com.app.a3naab.interfaces;

public interface OnItemClick {

     void onClick(int position);
}
