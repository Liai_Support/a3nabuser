package com.app.a3naab.interfaces;

public interface AddressSelection {

    void addressSelected(int position);

    void addressEdit(int position);

    void addressDelete(int position);
}
