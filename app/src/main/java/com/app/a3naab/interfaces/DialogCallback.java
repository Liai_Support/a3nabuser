package com.app.a3naab.interfaces;

public interface DialogCallback {

    void onPositiveClick();

    void onNegativeClick();
}
