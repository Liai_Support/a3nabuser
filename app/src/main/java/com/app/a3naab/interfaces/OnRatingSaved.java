package com.app.a3naab.interfaces;

public interface OnRatingSaved {
    void onSaved(Float app, Float driver, Float product, String comments);
}
