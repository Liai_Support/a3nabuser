package com.app.a3naab.utils;

import android.content.Context;

import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ProductListData;
import com.app.a3naab.model.SearchCategoryData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class SharedHelper {

    private SharedPreference sharedPreference;


    public SharedHelper(Context context) {
        sharedPreference = new SharedPreference(context);
    }


    private String authToken;
    private String walletpoints;
    private String walletamount;
    private String fdelivery;
    private String fdeliverycharge;
    private String deliverycharge;
    private String selectedLanguage;
    private String firebaseToken;

    private String userId;
    private String payid;
    private String firstName;
    private String lastName;
    private String email;
    private String latitude;
    private String longitude;
    private String countryCode;
    private String mobileNumber;
    private String profilePic;
    private Boolean isLoggedIn;
    private String walletpointsar;
    private String walletpoint;

    private Boolean isHomeAvailable;
    private Boolean isOfficeAvailable;
    private int cartcount;

    private String lastKnownLat;
    private String lastKnownLng;
    private String lastKnownAddress;
    private String selectedDeliveryToAddress;
    private String selectedDeliveryToTag;
    private String selectedDeliveryToLat;
    private String selectedDeliveryToLng;
    private String selectedDeliveryToId;
    private ArrayList<String> addedCategory;

    private Boolean isAddAddress;
    private Boolean offersNotify;
    private Boolean ordersNotify;
    private Boolean announcementNotify;
    private Boolean othersNotify;


    ArrayList<DashboardResponse.Categories> mainCategorySearch = new ArrayList<>();
    ArrayList<SearchCategoryData> subCategoryRecent = new ArrayList<>();
    ArrayList<ProductListData> productSearchRecent = new ArrayList<>();

    public Boolean getHomeAvailable() {
        isHomeAvailable = sharedPreference.getBoolean("isHomeAvailable");
        return isHomeAvailable;
    }

    public void setHomeAvailable(Boolean isHomeAvailable) {
        sharedPreference.putBoolean("isHomeAvailable", isHomeAvailable);
        this.isHomeAvailable = isHomeAvailable;
    }


    public Boolean getOfficeAvailable() {
        isOfficeAvailable = sharedPreference.getBoolean("isOfficeAvailable");
        return isOfficeAvailable;
    }

    public void setOfficeAvailable(Boolean isOfficeAvailable) {
        sharedPreference.putBoolean("isOfficeAvailable", isOfficeAvailable);
        this.isOfficeAvailable = isOfficeAvailable;
    }


    public Boolean getLoggedIn() {
        isLoggedIn = sharedPreference.getBoolean("loggedIn");
        return isLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        sharedPreference.putBoolean("loggedIn", loggedIn);
        isLoggedIn = loggedIn;
    }

    public String getAuthToken() {
        authToken = sharedPreference.getKey("authToken");
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.sharedPreference.putKey("authToken", authToken);
        this.authToken = authToken;
    }

    public int getCartCount() {
        cartcount = sharedPreference.getInt("cartcount");
        return cartcount;
    }

    public void setCartCount(int cartcount) {
        this.sharedPreference.putInt("cartcount", cartcount);
        this.cartcount = cartcount;
    }

    public void setAvaliableWalletpoints(String walletpoints) {
        this.sharedPreference.putKey("walletpoints", walletpoints);
        this.walletpoints = walletpoints;
    }

    public String getAvaliableWalletpoints() {
        walletpoints = sharedPreference.getKey("walletpoints");
        return walletpoints;
    }

    public void setAvaliableWalletamount(String walletamount) {
        this.sharedPreference.putKey("walletamount", walletamount);
        this.walletamount = walletamount;
    }

    public String getAvaliableWalletamount() {
        walletamount = sharedPreference.getKey("walletamount");
        return walletamount;
    }


    public String getIsFastDelivery() {
        fdelivery = sharedPreference.getKey("fdelivery");
        return fdelivery;
    }

    public void setIsFastDelivery(String fdelivery) {
        this.sharedPreference.putKey("fdelivery", fdelivery);
        this.fdelivery = fdelivery;
    }

    public String getFastDeliverycharge() {
        fdeliverycharge = sharedPreference.getKey("fdeliverycharge");
        return fdeliverycharge;
    }

    public void setFastDeliverycharge(String fdeliverycharge) {
        this.sharedPreference.putKey("fdeliverycharge", fdeliverycharge);
        this.fdeliverycharge = fdeliverycharge;
    }

    public String getDeliverycharge() {
        deliverycharge = sharedPreference.getKey("deliverycharge");
        return deliverycharge;
    }


    public void setwalletpointsar(String walletpointsar) {
        this.sharedPreference.putKey("walletpointsar", walletpointsar);
        this.walletpointsar = walletpointsar;
    }

    public String getwalletpointsar() {
        walletpointsar = sharedPreference.getKey("walletpointsar");
        return walletpointsar;
    }

    public void setwalletpoint(String walletpoint) {
        this.sharedPreference.putKey("walletpoint", walletpoint);
        this.walletpoint = walletpoint;
    }

    public String getwalletpoint() {
        walletpoint = sharedPreference.getKey("walletpoint");
        return walletpoint;
    }

    public void setDeliverycharge(String deliverycharge) {
        this.sharedPreference.putKey("deliverycharge", deliverycharge);
        this.deliverycharge = deliverycharge;
    }


    public String getSelectedLanguage() {
        selectedLanguage = sharedPreference.getKey("selectedLanguage");
        if (selectedLanguage.equals("")) {
            return "ar";
        }
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.sharedPreference.putKey("selectedLanguage", selectedLanguage);
        this.selectedLanguage = selectedLanguage;
    }

    public String getFirebaseToken() {
        firebaseToken = sharedPreference.getKey("firebaseToken");
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.sharedPreference.putKey("firebaseToken", firebaseToken);
        this.firebaseToken = firebaseToken;
    }

    public String getUserId() {
        userId = sharedPreference.getKey("userId");
        return userId;
    }

    public void setUserId(String userId) {
        this.sharedPreference.putKey("userId", userId);
        this.userId = userId;
    }

    public String getTransactionId() {
        payid = sharedPreference.getKey("payid");
        return payid;
    }

    public void setTransactionId(String payid) {
        this.sharedPreference.putKey("payid", payid);
        this.payid = payid;
    }


    public String getFirstName() {
        firstName = sharedPreference.getKey("firstName");
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName != null)
            this.sharedPreference.putKey("firstName", firstName);
        this.firstName = firstName;
    }

    public String getLastName() {
        lastName = sharedPreference.getKey("lastName");
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName != null)
            this.sharedPreference.putKey("lastName", lastName);
        this.lastName = lastName;
    }

    public String getEmail() {
        email = sharedPreference.getKey("email");
        return email;
    }

    public void setEmail(String email) {
        if (email != null)
            this.sharedPreference.putKey("email", email);
        this.email = email;
    }

    public String getLatitude() {
        latitude = sharedPreference.getKey("latitude");
        return latitude;
    }

    public void setLatitude(String latitude) {
        if (latitude != null)
            this.sharedPreference.putKey("latitude", latitude);
        this.latitude = latitude;
    }

    public String getLongitude() {
        longitude = sharedPreference.getKey("longitude");
        return longitude;
    }

    public void setLongitude(String longitude) {
        if (longitude != null)
            this.sharedPreference.putKey("longitude", longitude);
        this.longitude = longitude;
    }

    public String getCountryCode() {
        countryCode = sharedPreference.getKey("countryCode");
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        if (countryCode != null)
            this.sharedPreference.putKey("countryCode", countryCode);
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        mobileNumber = sharedPreference.getKey("mobileNumber");
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        if (mobileNumber != null)
            this.sharedPreference.putKey("mobileNumber", mobileNumber);
        this.mobileNumber = mobileNumber;
    }

    public String getProfilePic() {
        profilePic = sharedPreference.getKey("profilePic");
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        if (profilePic != null)
            this.sharedPreference.putKey("profilePic", profilePic);
        this.profilePic = profilePic;
    }


    public String getLastKnownLat() {
        lastKnownLat = sharedPreference.getKey("lastKnownLat");
        return lastKnownLat;
    }

    public void setLastKnownLat(String lastKnownLat) {
        this.sharedPreference.putKey("lastKnownLat", lastKnownLat);
        this.lastKnownLat = lastKnownLat;
    }


    public String getLastKnownAddress() {
        lastKnownAddress = sharedPreference.getKey("lastKnownAddress");
        return lastKnownAddress;
    }

    public void setLastKnownAddress(String lastKnownAddress) {
        this.sharedPreference.putKey("lastKnownAddress", lastKnownAddress);
        this.lastKnownAddress = lastKnownAddress;
    }

    public String getLastKnownLng() {
        lastKnownLng = sharedPreference.getKey("lastKnownLng");
        return lastKnownLng;
    }

    public void setLastKnownLng(String lastKnownLng) {
        this.sharedPreference.putKey("lastKnownLng", lastKnownLng);
        this.lastKnownLng = lastKnownLng;
    }

    public String getSelectedDeliveryToAddress() {
        selectedDeliveryToAddress = sharedPreference.getKey("selectedDeliveryToAddress");
        return selectedDeliveryToAddress;
    }


    public void setSelectedDeliveryToAddress(String selectedDeliveryToAddress) {
        this.sharedPreference.putKey("selectedDeliveryToAddress", selectedDeliveryToAddress);
        this.selectedDeliveryToAddress = selectedDeliveryToAddress;
    }

    public String getSelectedDeliveryToTag() {
        selectedDeliveryToTag = sharedPreference.getKey("selectedDeliveryToTag");
        return selectedDeliveryToTag;
    }

    public void setSelectedDeliveryToTag(String selectedDeliveryToTag) {
        this.sharedPreference.putKey("selectedDeliveryToTag", selectedDeliveryToTag);
        this.selectedDeliveryToTag = selectedDeliveryToTag;
    }


    public String getSelectedDeliveryToLat() {
        selectedDeliveryToLat = sharedPreference.getKey("selectedDeliveryToLat");
        return selectedDeliveryToLat;
    }

    public void setSelectedDeliveryToLat(String selectedDeliveryToLat) {
        this.sharedPreference.putKey("selectedDeliveryToLat", selectedDeliveryToLat);
        this.selectedDeliveryToLat = selectedDeliveryToLat;
    }

    public String getSelectedDeliveryToLng() {
        selectedDeliveryToLng = sharedPreference.getKey("selectedDeliveryToLng");
        return selectedDeliveryToLng;
    }

    public void setSelectedDeliveryToLng(String selectedDeliveryToLng) {
        this.sharedPreference.putKey("selectedDeliveryToLng", selectedDeliveryToLng);
        this.selectedDeliveryToLng = selectedDeliveryToLng;
    }

    public String getSelectedDeliveryToId() {
        selectedDeliveryToId = sharedPreference.getKey("selectedDeliveryToId");
        return selectedDeliveryToId;
    }

    public void setSelectedDeliveryToId(String selectedDeliveryToId) {
        this.sharedPreference.putKey("selectedDeliveryToId", selectedDeliveryToId);
        this.selectedDeliveryToId = selectedDeliveryToId;
    }

    public ArrayList<String> getAddedCategory() {

        ArrayList<String> playersList = new Gson().fromJson(sharedPreference.getKey("addedCategory"),
                new TypeToken<ArrayList<String>>() {
                }.getType());

        if (playersList == null) {
            playersList = new ArrayList<>();
        }
        this.addedCategory = playersList;
        return playersList;
    }

    public void setAddedCategory(ArrayList<String> addedCategory) {

        String listString = new Gson().toJson(
                addedCategory,
                new TypeToken<ArrayList<String>>() {
                }.getType());

        this.sharedPreference.putKey("addedCategory", listString);
        this.addedCategory = addedCategory;
    }


    public ArrayList<DashboardResponse.Categories> getMainCategorySearch() {

        ArrayList<DashboardResponse.Categories> mainCategorySearch = new Gson().fromJson(sharedPreference.getKey("mainCategorySearch"),
                new TypeToken<ArrayList<DashboardResponse.Categories>>() {
                }.getType());

        if (mainCategorySearch == null) {
            mainCategorySearch = new ArrayList<>();
        }
        this.mainCategorySearch = mainCategorySearch;
        return mainCategorySearch;
    }

    public void setMainCategorySearch(ArrayList<DashboardResponse.Categories> mainCategorySearch) {

        String listString = new Gson().toJson(
                mainCategorySearch,
                new TypeToken<ArrayList<DashboardResponse.Categories>>() {
                }.getType());

        this.sharedPreference.putKey("mainCategorySearch", listString);
        this.mainCategorySearch = mainCategorySearch;
    }

    public ArrayList<SearchCategoryData> getSubCategoryRecent() {

        ArrayList<SearchCategoryData> subCategoryRecent = new Gson().fromJson(sharedPreference.getKey("subCategoryRecent"),
                new TypeToken<ArrayList<SearchCategoryData>>() {
                }.getType());

        if (subCategoryRecent == null) {
            subCategoryRecent = new ArrayList<>();
        }
        this.subCategoryRecent = subCategoryRecent;

        return subCategoryRecent;
    }

    public void setSubCategoryRecent(ArrayList<SearchCategoryData> subCategoryRecent) {

        String listString = new Gson().toJson(
                subCategoryRecent,
                new TypeToken<ArrayList<SearchCategoryData>>() {
                }.getType());

        this.sharedPreference.putKey("subCategoryRecent", listString);
        this.subCategoryRecent = subCategoryRecent;
    }

    public ArrayList<ProductListData> getProductSearchRecent() {

        ArrayList<ProductListData> productSearchRecent = new Gson().fromJson(sharedPreference.getKey("productSearchRecent"),
                new TypeToken<ArrayList<ProductListData>>() {
                }.getType());

        if (productSearchRecent == null) {
            productSearchRecent = new ArrayList<>();
        }
        this.productSearchRecent = productSearchRecent;

        return productSearchRecent;
    }

    public void setProductSearchRecent(ArrayList<ProductListData> productSearchRecent) {

        String listString = new Gson().toJson(
                productSearchRecent,
                new TypeToken<ArrayList<ProductListData>>() {
                }.getType());

        this.sharedPreference.putKey("productSearchRecent", listString);


        this.productSearchRecent = productSearchRecent;
    }



    public Boolean getIsAddAddress() {
        isAddAddress = sharedPreference.getBoolean("isAddAddress");
        return isAddAddress;
    }

    public void setIsAddAddress(Boolean isAddAddress) {
        sharedPreference.putBoolean("isAddAddress", isAddAddress);
        this.isAddAddress = isAddAddress;
    }


}
