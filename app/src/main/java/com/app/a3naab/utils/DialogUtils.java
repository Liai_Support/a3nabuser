package com.app.a3naab.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.DialogAddressBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.interfaces.OnRatingSaved;
import com.app.a3naab.model.DashboardResponse;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class DialogUtils {

    static Dialog loaderDialog;
    static BottomSheetDialog dialog;

    public static BottomSheetDialog getAddressDialog(Context context, DialogAddressBinding dialogBinding) {

        BottomSheetDialog dialog = new BottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogBinding.getRoot());
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        BottomSheetBehavior mBehavior = BottomSheetBehavior.from((View) dialogBinding.getRoot().getParent());
        mBehavior.setPeekHeight((int) (context.getResources().getDisplayMetrics().heightPixels * 0.4));

//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, height);
        return dialog;

    }


    public static BottomSheetDialog getRating(Context context,String orderIds,OnRatingSaved onRatingSaved) {

        if (dialog != null && dialog.isShowing()) {
            return dialog;
        }

        dialog = new BottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_order_rating);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

//        BottomSheetBehavior mBehavior = BottomSheetBehavior.from((View) dialogBinding.getRoot().getParent());
//        mBehavior.setPeekHeight((int) (context.getResources().getDisplayMetrics().heightPixels * 0.4));

//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, height);


        RatingBar appRating = dialog.findViewById(R.id.appRating);
        RatingBar driverRating = dialog.findViewById(R.id.driverRating);
        RatingBar productRating = dialog.findViewById(R.id.productRating);
        EditText comments = dialog.findViewById(R.id.comments);
        TextView save = dialog.findViewById(R.id.save);
        TextView cancel = dialog.findViewById(R.id.cancel);
        TextView orderId = dialog.findViewById(R.id.orderId);

        orderId.setText(""+context.getString(R.string.order_id)+" "+orderIds);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appRating.getRating() != 0f) {
                    if (driverRating.getRating() != 0f) {
                        if (productRating.getRating() != 0f) {
                            if (comments.getText().toString().trim().length() >= 0) {

                                onRatingSaved.onSaved(appRating.getRating(), driverRating.getRating(), productRating.getRating(), comments.getText().toString().trim());

                            }
                        }
                    }
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appRating.getRating() >= 0f) {
                    if (driverRating.getRating() >= 0f) {
                        if (productRating.getRating() >= 0f) {
                            if (comments.getText().toString().trim().length() >= 0) {

                                onRatingSaved.onSaved(appRating.getRating(), driverRating.getRating(), productRating.getRating(), comments.getText().toString().trim());

                            }
                        }
                    }
                }
            }
        });

        dialog.show();

        return dialog;
    }


    public static BottomSheetDialog getFeedback(Context context, OnRatingSaved onRatingSaved) {


        BottomSheetDialog dialog = new BottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_order_feedback);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RatingBar appRating = dialog.findViewById(R.id.appRating);

        EditText comments = dialog.findViewById(R.id.comments);
        TextView save = dialog.findViewById(R.id.save);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appRating.getRating() != 0f) {

                    if (comments.getText().toString().trim().length() >= 0) {

                        onRatingSaved.onSaved(appRating.getRating(), 0f, 0f, comments.getText().toString().trim());

                    }


                }
            }
        });

        dialog.show();

        return dialog;
    }


    public static void showLoader(Context context) {

        if (loaderDialog != null) {
            if (loaderDialog.isShowing()) {
                loaderDialog.dismiss();
            }
        }

        loaderDialog = new Dialog(context);
        loaderDialog.setCancelable(false);
        loaderDialog.setCanceledOnTouchOutside(false);
        loaderDialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        );
        loaderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_loader, null);
        if (view != null) {
            loaderDialog.setContentView(view);
        }

        Glide.with(context).asGif().load(R.raw.loader).listener(new RequestListener<GifDrawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {


                return false;
            }

            @Override
            public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {

                resource.setLoopCount(1);

                return false;
            }


        }).into((ImageView) loaderDialog.findViewById(R.id.gifLoader));

//        ((LottieAnimationView) loaderDialog.findViewById(R.id.anim_view)).playAnimation();

        if (!loaderDialog.isShowing()) {
            loaderDialog.show();
        }

    }

    public static void dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog.isShowing()) {
                loaderDialog.dismiss();
            }
        }
    }


    public static void showAlertDialog(
            Context context,
            String content,
            String title,
            String positiveText,
            DialogCallback callBack
    ) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);


        TextView headerTextView = dialog.findViewById(R.id.header);
        TextView contentTextView = dialog.findViewById(R.id.content);
        TextView ok = dialog.findViewById(R.id.ok);

        headerTextView.setText(title);
        contentTextView.setText(content);

        ok.setText(positiveText);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onPositiveClick();
                dialog.dismiss();
            }
        });


        dialog.show();

    }

    public static void showAlertDialogWithCancel(
            Context context,
            String content,
            String title,
            String positiveText,
            String negativeText,
            DialogCallback callBack
    ) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_alert_cancel);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);


        TextView headerTextView = dialog.findViewById(R.id.header);
        TextView contentTextView = dialog.findViewById(R.id.content);
        TextView ok = dialog.findViewById(R.id.ok);
        TextView cancel = dialog.findViewById(R.id.cancel);

        headerTextView.setText(title);
        contentTextView.setText(content);

        ok.setText(positiveText);
        cancel.setVisibility(View.VISIBLE);
        cancel.setText(negativeText);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onPositiveClick();
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onNegativeClick();
                dialog.dismiss();
            }
        });


        dialog.show();

    }


    public static void getBoxDialog(String boxid,Context context, ArrayList<DashboardResponse.BoxStyle> list, OnItemClick onItemClick) {


        AtomicReference<Integer> selectedItem = new AtomicReference<>(-1);

        BottomSheetDialog dialog = new BottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_select_item);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.75);

        dialog.getWindow().setLayout(width, height);


        RecyclerView view = dialog.findViewById(R.id.items);
        TextView cancel = dialog.findViewById(R.id.cancel);
        TextView save = dialog.findViewById(R.id.save);
        TextView text = dialog.findViewById(R.id.text);
        assert text != null;
        text.setText(context.getString(R.string.box_style));


        assert save != null;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedItem.get() != -1) {
                    onItemClick.onClick(selectedItem.get());
                    dialog.dismiss();
                }
                else if(selectedItem.get() == -1){
                    onItemClick.onClick(-1);
                    dialog.dismiss();
                }
                else {
                    DialogUtils.showAlertDialog(context, context.getString(R.string.select),
                            "Oops", context.getString(R.string.ok), new DialogCallback() {
                                @Override
                                public void onPositiveClick() {

                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });
                }

            }
        });

        assert cancel != null;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        assert view != null;
        view.setLayoutManager(new LinearLayoutManager(context));
        view.setAdapter(new SpinnerAdapter(boxid,context, list, selectedItem::set));

        dialog.show();

    }


    public static void getCuttingDialog(String cutid,Context context, ArrayList<DashboardResponse.CuttingStyle> list, OnItemClick onItemClick) {


        AtomicReference<Integer> selectedItem = new AtomicReference<>(-1);

        BottomSheetDialog dialog = new BottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_select_item);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.75);

        dialog.getWindow().setLayout(width, height);


        RecyclerView view = dialog.findViewById(R.id.items);
        TextView cancel = dialog.findViewById(R.id.cancel);
        TextView save = dialog.findViewById(R.id.save);
        TextView text = dialog.findViewById(R.id.text);
        assert text != null;
        text.setText(context.getString(R.string.cutting_style));

        assert save != null;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedItem.get() != -1) {
                    onItemClick.onClick(selectedItem.get());
                    dialog.dismiss();
                }
                else if(selectedItem.get() == -1){
                    onItemClick.onClick(-1);
                    dialog.dismiss();
                }
                else {
                    DialogUtils.showAlertDialog(context, context.getString(R.string.select),
                            "Oops", context.getString(R.string.ok), new DialogCallback() {
                                @Override
                                public void onPositiveClick() {

                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });
                }

            }
        });

        assert cancel != null;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        assert view != null;
        view.setLayoutManager(new LinearLayoutManager(context));
        view.setAdapter(new SpinnerAdapter2(cutid,context, list, selectedItem::set));

        dialog.show();

    }


    static class SpinnerAdapter extends RecyclerView.Adapter<SpinnerAdapter.SpinnerViewHolder> {

        Context context;
        ArrayList<DashboardResponse.BoxStyle> list;
        OnItemClick onItemClick;

        public SpinnerAdapter(String boxid,Context context, ArrayList<DashboardResponse.BoxStyle> list, OnItemClick onItemClick) {
            this.context = context;
            this.list = list;
            for (int i = 0; i < list.size(); i++) {
                if(list.get(i).getId() == Integer.parseInt(boxid)){
                    list.get(i).setSelected(true);
                }
                else {
                    list.get(i).setSelected(false);
                }
            }
            this.onItemClick = onItemClick;
        }

        @NonNull
        @Override
        public SpinnerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new SpinnerViewHolder(LayoutInflater.from(context).inflate(R.layout.child_spinner_place, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull SpinnerViewHolder holder, int position) {

            if (new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase("ar") && !list.get(position).getArabicName().equalsIgnoreCase("")) {
                holder.text.setText(list.get(position).getArabicName() + " - " + list.get(position).getBoxPrice() + " SAR");
            } else {
                holder.text.setText(list.get(position).getBoxName() + " - " + list.get(position).getBoxPrice() + " SAR");
            }


            if (list.get(position).isSelected()) {
                onItemClick.onClick(position);
                holder.selection.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.selected_rdt));
            } else {
                holder.selection.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.selected_dot));
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int i = 0; i < list.size(); i++) {
                        if (position == i) {
                            if(list.get(i).isSelected()) {
                                list.get(i).setSelected(false);
                                onItemClick.onClick(-1);
                            }
                            else {
                                list.get(i).setSelected(true);
                                onItemClick.onClick(position);
                            }
                        } else {
                            list.get(i).setSelected(false);
                        }
                    }

                    notifyDataSetChanged();

                }
            });


        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        static class SpinnerViewHolder extends RecyclerView.ViewHolder {

            TextView text;
            ImageView selection;

            public SpinnerViewHolder(@NonNull View itemView) {
                super(itemView);

                text = itemView.findViewById(R.id.text);
                selection = itemView.findViewById(R.id.selection);
            }

        }
    }


    static class SpinnerAdapter2 extends RecyclerView.Adapter<SpinnerAdapter2.SpinnerViewHolder2> {

        Context context;
        ArrayList<DashboardResponse.CuttingStyle> list;
        OnItemClick onItemClick;

        public SpinnerAdapter2(String cutid,Context context, ArrayList<DashboardResponse.CuttingStyle> list, OnItemClick onItemClick) {
            this.context = context;
            this.list = list;
            for (int i = 0; i < list.size(); i++) {
                if(list.get(i).getId() == Integer.parseInt(cutid)){
                    list.get(i).setSelected(true);
                }
                else {
                    list.get(i).setSelected(false);
                }
            }
            this.onItemClick = onItemClick;
        }

        @NonNull
        @Override
        public SpinnerViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new SpinnerViewHolder2(LayoutInflater.from(context).inflate(R.layout.child_spinner_place, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull SpinnerViewHolder2 holder, int position) {

            if (new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase("ar") && !list.get(position).getArabicName().equalsIgnoreCase("")) {
                holder.text.setText(list.get(position).getArabicName() + " - " + list.get(position).getCuttingPrice() + " SAR");
            } else {
                holder.text.setText(list.get(position).getCuttingName() + " - " + list.get(position).getCuttingPrice() + " SAR");
            }


            if (list.get(position).isSelected()) {
                onItemClick.onClick(position);
                holder.selection.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.selected_rdt));
            } else {
                holder.selection.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.selected_dot));
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int i = 0; i < list.size(); i++) {
                        if (position == i) {
                            if(list.get(i).isSelected()){
                                list.get(i).setSelected(false);
                                onItemClick.onClick(-1);
                            }
                            else {
                                onItemClick.onClick(position);
                                list.get(i).setSelected(true);
                            }
                        }
                        else {
                            list.get(i).setSelected(false);
                        }
                    }

                    notifyDataSetChanged();

                }
            });


        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        static class SpinnerViewHolder2 extends RecyclerView.ViewHolder {

            TextView text;
            ImageView selection;

            public SpinnerViewHolder2(@NonNull View itemView) {
                super(itemView);

                text = itemView.findViewById(R.id.text);
                selection = itemView.findViewById(R.id.selection);
            }

        }
    }


    public static void showCouponApplied(Context context, boolean b,String text1) {


        Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_coupon_applied);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.55);

        dialog.getWindow().setLayout(width, height);


        ImageView imageView44 = dialog.findViewById(R.id.imageView44);
        TextView text = dialog.findViewById(R.id.text);
        TextView confirm = dialog.findViewById(R.id.confirm);

        if (b) {
            imageView44.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirmed));
            text.setText(context.getString(R.string.coupon_code_has_been_added_successfully));
            confirm.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.searchcolor_green));
        } else {
            imageView44.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.failure));
            text.setText(text1);
            confirm.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.colorPrimary));

        }

        confirm.setOnClickListener(view -> {
            dialog.dismiss();
        });


        dialog.show();

    }

    public static void showOrderSuccessDialog(Context context, DialogCallback callBack) {

        Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_success_order);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, height);


        TextView text = dialog.findViewById(R.id.text);
        TextView confirm = dialog.findViewById(R.id.confirm);


        confirm.setOnClickListener(view -> {
            callBack.onPositiveClick();
            dialog.dismiss();
        });


        dialog.show();
    }


    public static void showOrderCancelledDialog(Context context, DialogCallback callBack) {

        Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_success_order);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, height);


        TextView text = dialog.findViewById(R.id.text);
        TextView confirm = dialog.findViewById(R.id.confirm);

        text.setText(R.string.order_Cancelled_success);

        confirm.setOnClickListener(view -> {
            callBack.onPositiveClick();
            dialog.dismiss();
        });


        dialog.show();
    }


    public static void showProductNotAvailable(Context context, OnItemClick onItemClick) {

        Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_product_not_available);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.5);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView cancel = dialog.findViewById(R.id.cancel);
        TextView save = dialog.findViewById(R.id.save);
        TextView textView36 = dialog.findViewById(R.id.textView36);
        TextView textView37 = dialog.findViewById(R.id.textView37);

        RadioButton deleteItem = dialog.findViewById(R.id.deleteItem);
        RadioButton callMe = dialog.findViewById(R.id.callMe);

        textView36.setOnClickListener(view -> {
            deleteItem.setChecked(true);
            callMe.setChecked(false);
        });

        textView37.setOnClickListener(view -> {
            deleteItem.setChecked(false);
            callMe.setChecked(true);
        });


        if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("true")) {
            deleteItem.setChecked(true);
            callMe.setChecked(false);
        } else if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("false")) {
            deleteItem.setChecked(false);
            callMe.setChecked(true);
        } else {
            deleteItem.setChecked(false);
            callMe.setChecked(false);
        }

        save.setOnClickListener(view -> {

            if (deleteItem.isChecked()) {
                CartSessionManager.getInstance().setDeleteProductNotAvailable("true");
                onItemClick.onClick(0);
                dialog.dismiss();
            } else if (callMe.isChecked()) {
                CartSessionManager.getInstance().setDeleteProductNotAvailable("false");
                onItemClick.onClick(0);
                dialog.dismiss();
            }

        });

        cancel.setOnClickListener(view -> {
            dialog.dismiss();
        });


        dialog.show();

    }


    public static void showMaximumLimit(
            Context context
    ) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);


        TextView headerTextView = dialog.findViewById(R.id.header);
        TextView contentTextView = dialog.findViewById(R.id.content);
        TextView ok = dialog.findViewById(R.id.ok);

        headerTextView.setText(context.getString(R.string.oops));
        contentTextView.setText(context.getString(R.string.maximum_limit_reached));


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }

}
