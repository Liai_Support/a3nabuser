package com.app.a3naab.utils;

public class CartSessionManager {


    private CartSessionManager() {

    }

    private static CartSessionManager instance = null;

    public static CartSessionManager getInstance() {
        if (instance == null) {
            instance = new CartSessionManager();
        }

        return instance;
    }

    public static void removeAllValues() {
        instance = new CartSessionManager();
    }


    private String selectedAddress = "";
    private String selectedAddressType = "";
    private String selectedLat = "";
    private String selectedLng = "";
    private String selectedId = "";

    private int selectedDayId = 0;
    private String selectedDate = "";
    private int selectedTime = 0;
    private String selectedTimeStr = "";
    private int isFastDeliverySelected = 0;
    private int isFreeDeliverySelected = 0;

    private String couponCode = "";
    private String couponId = "";
    private String discount = "";
    private String offertype = "2";
    private String totalAmount = "";
    private String cbstyleAmount = "";
    private String deleteProductNotAvailable = "";
    private String wps= "";
    private String wp= "";
    private String awp= "";
    private String awa= "";
    private String poid= "";
    private String ptid= "";
    private String fordertax= "";


    private Boolean isReorder = false;
    private Boolean wallet = false;

    private String orderId = "0";


    private String trustUser;
    private String packageValue = "0.00";
    private String flatDiscount = "0.0";
    private boolean enablePoints = false;
    private int orderMinimumMins = 0;
    private int paymentOption = 0;

    public String getFlatDiscount() {
        return flatDiscount;
    }

    public void setFlatDiscount(String flatDiscount) {
        this.flatDiscount = flatDiscount;
    }

    public String getFordertax() {
        return fordertax;
    }

    public void setFordertax(String fordertax) {
        this.fordertax = fordertax;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String Discount) {
        this.discount = Discount;
    }

    public String getOfferType() {
        return offertype;
    }

    public void setOfferType(String Offertype) {
        this.offertype = Offertype;
    }


    public int getSelectedDayId() {
        return selectedDayId;
    }

    public void setSelectedDayId(int selectedDayId) {
        this.selectedDayId = selectedDayId;
    }

    public int getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(int paymentOption) {
        this.paymentOption = paymentOption;
    }

    public int getOrderMinimumMins() {
        return orderMinimumMins;
    }

    public void setOrderMinimumMins(int orderMinimumMins) {
        this.orderMinimumMins = orderMinimumMins;
    }

    public String getPackageValue() {
        return packageValue;
    }

    public void setPackageValue(String packageValue) {
        this.packageValue = packageValue;
    }


    public String getTrustUser() {
        return trustUser;
    }

    public void setTrustUser(String trustUser) {
        this.trustUser = trustUser;
    }

    public Boolean getReorder() {
        return isReorder;
    }

    public void setReorder(Boolean reorder) {
        isReorder = reorder;
    }

    public Boolean getwallet() {
        return wallet;
    }

    public void setwallet(Boolean walletamount) {
        wallet = walletamount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(String selectedId) {
        this.selectedId = selectedId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getcbstyleamount() {
        return cbstyleAmount;
    }

    public void setcbstyleamount(String cbstyleAmount) {
        this.cbstyleAmount = cbstyleAmount;
    }
    public String getSelectedTimeStr() {

        return selectedTimeStr;
    }

    public void setSelectedTimeStr(String selectedTimeStr) {
        this.selectedTimeStr = selectedTimeStr;
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }

    public int getSelectedTime() {
        return selectedTime;
    }

    public void setSelectedTime(int selectedTime) {
        this.selectedTime = selectedTime;
    }

    public int getIsFastDeliverySelected() {
        return isFastDeliverySelected;
    }

    public void setIsFastDeliverySelected(int isFastDeliverySelected) {
        this.isFastDeliverySelected = isFastDeliverySelected;
    }

    public int getIsFreeDeliverySelected() {
        return isFreeDeliverySelected;
    }

    public void setIsFreeDeliverySelected(int isFreeDeliverySelected) {
        this.isFreeDeliverySelected = isFreeDeliverySelected;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(String selectedAddress) {
        this.selectedAddress = selectedAddress;
    }

    public String getSelectedAddressType() {
        return selectedAddressType;
    }

    public void setSelectedAddressType(String selectedAddressType) {
        this.selectedAddressType = selectedAddressType;
    }

    public String getSelectedLat() {
        return selectedLat;
    }

    public void setSelectedLat(String selectedLat) {
        this.selectedLat = selectedLat;
    }

    public String getSelectedLng() {
        return selectedLng;
    }

    public void setSelectedLng(String selectedLng) {
        this.selectedLng = selectedLng;
    }

    public String getDeleteProductNotAvailable() {
        return deleteProductNotAvailable;
    }

    public void setDeleteProductNotAvailable(String deleteProductNotAvailable) {
        this.deleteProductNotAvailable = deleteProductNotAvailable;
    }

    public boolean isEnablePoints() {
        return enablePoints;
    }

    public void setEnablePoints(boolean enablePoints) {
        this.enablePoints = enablePoints;
    }

    public void setavailableWalletpoint(String awp) {
        this.awp = awp;
    }

    public String getavailableWalletpoint() {
        return awp;
    }

    public void setavailableWalletamount(String awa) {
        this.awa = awa;
    }

    public String getavailableWalletamount() {
        return awa;
    }

    public void setpaymentorderid(String poid) {
        this.poid = poid;
    }

    public String getpaymentorderid() {
        return poid;
    }

    public void setpaymenttransactionid(String ptid) {
        this.ptid = ptid;
    }

    public String getpaymenttransactionid() {
        return ptid;
    }
}
