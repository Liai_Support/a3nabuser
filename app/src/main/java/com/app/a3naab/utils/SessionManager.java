package com.app.a3naab.utils;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import androidx.appcompat.app.AlertDialog;

import com.app.a3naab.R;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ProductCategoryData;

import java.util.ArrayList;

public class SessionManager {

    private static SessionManager instance = null;

    public static SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }

        return instance;
    }

    private boolean isRegisterLaterFlow;
    private String countryCode = "";
    private String mobileNumber = "";
    private String verificationCode = "";
    private Boolean userexist;
    private Boolean version;



    private ArrayList<ProductCategoryData> productCategoryList = new ArrayList<>();
    private ArrayList<DashboardResponse.BestProduct> bestProduct = new ArrayList<>();
    private ArrayList<DashboardResponse.Categories> categoryStore = new ArrayList<>();
    private ArrayList<DashboardResponse.BannerData> bannerData = new ArrayList<>();


    public boolean isRegisterLaterFlow() {
        return isRegisterLaterFlow;
    }

    public void setRegisterLaterFlow(boolean registerLaterFlow) {
        isRegisterLaterFlow = registerLaterFlow;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Boolean getUserexists() {
        return userexist;
    }

    public void setUserexists(Boolean verificationCode) {
        this.userexist = verificationCode;
    }

    public ArrayList<ProductCategoryData> getProductCategoryList() {
        return productCategoryList;
    }

    public void setProductCategoryList(ArrayList<ProductCategoryData> list) {
        this.productCategoryList = list;
    }

    public ArrayList<DashboardResponse.BestProduct> getBestProduct() {
        return bestProduct;
    }

    public void setBestProduct(ArrayList<DashboardResponse.BestProduct> bestProduct) {
        this.bestProduct = bestProduct;
    }

    public ArrayList<DashboardResponse.Categories> getCategoryStore() {
        return categoryStore;
    }

    public void setCategoryStore(ArrayList<DashboardResponse.Categories> categoryStore) {
        this.categoryStore = categoryStore;
    }

    public ArrayList<DashboardResponse.BannerData> getBannerData() {
        return bannerData;
    }

    public void setBannerData(ArrayList<DashboardResponse.BannerData> bannerData) {
        this.bannerData = bannerData;
    }

    public Boolean getIsVersion() {
        return version;
    }

    public void setIsVersion(Boolean version) {
        this.version = version;
    }
}
