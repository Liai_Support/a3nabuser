package com.app.a3naab.utils;

import android.Manifest;

public class Constants {
    public static class Value {
        public static Boolean first = true;
        public static String api_appversion = "0";
    }

    public static class ApiKeys {

        public static final String LANG = "lang";
        public static final String AUTHORIZATION = "authorization";
        public static final String ROLE = "role";
    }

    public static class Permissions {

        public static final String[] LOCATIONPERMISSION_LIST = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE};

        public static final int LOCATIONREQUEST_CODE = 201;
    }

    public static class RequestCode {
        public static final int GPS_REQUEST = 400;
        public static final int ADDRESS_REQUEST = 401;
    }
}
