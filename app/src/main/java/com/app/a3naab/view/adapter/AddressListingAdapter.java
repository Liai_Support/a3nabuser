package com.app.a3naab.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildAddressListBinding;

import com.app.a3naab.interfaces.AddressSelection;
import com.app.a3naab.model.ListMyAddressResponse;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.activity.DashboardActivity;
import com.app.a3naab.view.fragment.ShoppingFragment;

import java.util.ArrayList;

public class AddressListingAdapter extends RecyclerView.Adapter<AddressListingAdapter.MyViewHolder> {

    Context context;
    ArrayList<ListMyAddressResponse.AddressData> addressData;
    AddressSelection addressSelection;
    boolean isEditable;
    SharedHelper sharedHelper;
    ShoppingFragment shoppingFragment;
    boolean come;


    public AddressListingAdapter(Boolean come,ShoppingFragment shoppingFragment, Context context, ArrayList<ListMyAddressResponse.AddressData> addressData, AddressSelection addressSelection, boolean isEditable) {
        this.context = context;
        this.addressData = addressData;
        this.addressSelection = addressSelection;
        this.isEditable = isEditable;
        this.come = come;
        this.shoppingFragment = shoppingFragment;
    }

    @NonNull
    @Override
    public AddressListingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        sharedHelper = new SharedHelper(context);

        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_address_list, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AddressListingAdapter.MyViewHolder holder, int position) {


        holder.binding.otherLoc.setText(addressData.get(position).getAddressType());
        holder.binding.otherLocationAddress.setText(addressData.get(position).getBuildingName() + " " + addressData.get(position).getAddressPinDetails());

        holder.binding.otherAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addressSelection.addressEdit(position);
            }
        });

        holder.binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addressSelection.addressDelete(position);
            }
        });
        if(!sharedHelper.getSelectedDeliveryToTag().equals("")){
            if(sharedHelper.getSelectedDeliveryToTag().equalsIgnoreCase(holder.binding.otherLoc.getText().toString())){
                holder.binding.getRoot().findViewById(R.id.otherLocationView).setBackground(context.getDrawable(R.drawable.my_address_bg));
            }
        }


        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(come==true){
                    if(sharedHelper.getSelectedDeliveryToTag().equalsIgnoreCase(holder.binding.otherLoc.getText().toString())){
                        shoppingFragment.addressDialog.dismiss();
                    }
                    else {
                        addressSelection.addressSelected(position);
                    }
                }
                //holder.binding.getRoot().findViewById(R.id.otherLocationView).setBackground(context.getDrawable(R.drawable.my_address_bg));
            }
        });

        if (isEditable) {
            if(come==true){
                holder.binding.otherAdd.setVisibility(View.VISIBLE);
                holder.binding.delete.setVisibility(View.VISIBLE);
            }
            else {
                holder.binding.otherAdd.setVisibility(View.GONE);
                holder.binding.delete.setVisibility(View.GONE);
            }
        } else {
            holder.binding.otherAdd.setVisibility(View.GONE);
            holder.binding.delete.setVisibility(View.GONE);
            holder.binding.otherLocationView.setBackground(context.getResources().getDrawable(R.drawable.address_unselected_bg));
        }

    }

    @Override
    public int getItemCount() {
        return addressData.size();
    }

    public void notifyDataChanged(ArrayList<ListMyAddressResponse.AddressData> addressList) {
        addressData = addressList;
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildAddressListBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildAddressListBinding.bind(itemView);
        }

    }
}
