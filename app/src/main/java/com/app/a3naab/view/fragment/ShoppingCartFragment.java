package com.app.a3naab.view.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.customview.swipeHelprs.SwipeHelper;
import com.app.a3naab.databinding.FragmentCartBinding;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.CartDetailsActivity;
import com.app.a3naab.view.activity.DashboardActivity;
import com.app.a3naab.view.activity.ProductDetailActivity;
import com.app.a3naab.view.adapter.CartListAdapter;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ShoppingCartFragment extends Fragment {

    FragmentCartBinding binding;
    SharedHelper sharedHelper;

    ProductViewModel viewModel;

    CartListAdapter adapter;
    ArrayList<ViewCartResponse.CartDetails> list = new ArrayList<>();
    boolean cartReady = true;
    boolean allProductInStock = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_cart, container, false);
        binding = FragmentCartBinding.bind(layout);

        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(requireContext());

        ((DashboardActivity)getActivity()).fvalue = "cart";

        initView();
        getCartDetails();
        initListener();

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.imageView24.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.cart_top_1));
        }

        return binding.getRoot();
    }

    private void initListener() {

        binding.completeOrderClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartReady = true;
                /*for (int position = 0; position < list.size(); position++) {
                    if (Float.parseFloat(list.get(position).getSub_minimum()) != 0.0f) {
                        if (Float.parseFloat(list.get(position).getTotalPrice()) < Float.parseFloat(list.get(position).getSub_minimum())) {
                            cartReady = false;
                        }
                    } else {
                        if (Float.parseFloat(list.get(position).getTotalPrice()) < Float.parseFloat(list.get(position).getCate_minimum())) {
                            cartReady = false;
                        }
                    }
                }*/

                for (int i=0;i<list.size();i++){
                    if(list.get(i).getMovC() == true){
                        cartReady = false;
                    }
                }

                allProductInStock = true;
                for (int position = 0; position < list.size(); position++) {
                    if (list.get(position).getStoreStock() == 0) {
                        allProductInStock = false;
                        break;
                    }
                }
                if (cartReady) {
                    if (allProductInStock) {
                        if(CartSessionManager.getInstance().getFlatDiscount()!=null &&sharedHelper.getwalletpoint()!=null && sharedHelper.getwalletpointsar()!=null) {
                            startActivity(new Intent(requireContext(), CartDetailsActivity.class));
                        }
                        else {
                            Utils.showSnack(binding.parent, getString(R.string.no_result_found));
                        }
                    }
                    else {
                        Utils.showSnack(binding.parent, getString(R.string.out_of_stock));
                    }
                }
                else {
                    Utils.showSnack(binding.parent, getString(R.string.minimum_order_nvalue_required_toast));
                }
            }
        });
    }

    private void initView() {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.view10.setRotation(180f);
        }


        adapter = new CartListAdapter(requireContext(), list, position -> {

            if(list.get(position).getSpecialInstructions()!=null || list.get(position).getCuttingStyle()!= null || list.get(position).getBoxStyle()!=null){
                DialogUtils.showLoader(requireContext());
                viewModel.addtoCartWithStyle(requireContext(), list.get(position).getProductId(),String.valueOf(list.get(position).getStoreId()),list.get(position).getCuttingStyle(), list.get(position).getBoxStyle(),list.get(position).getSpecialInstructions())
                        .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                            @Override
                            public void onChanged(CommonResponse commonResponse) {
                                DialogUtils.dismissLoader();
                                if (!commonResponse.getError()) {
                                    getCartDetails();
                                }
                            }
                        });
            }
            else {
                DialogUtils.showLoader(requireContext());
                viewModel.addItemToCart(requireContext(), list.get(position).getProductId(), String.valueOf(list.get(position).getStoreId()))
                        .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                            @Override
                            public void onChanged(CommonResponse commonResponse) {
                                DialogUtils.dismissLoader();
                                if (!commonResponse.getError()) {
//                                adapter.notifyAddData(position);
                                    getCartDetails();
                                }
                            }
                        });
            }

        }, position -> {

            if(list.get(position).getSpecialInstructions()!=null || list.get(position).getCuttingStyle()!= null || list.get(position).getBoxStyle()!=null){
                DialogUtils.showLoader(requireContext());
                viewModel.removeItemToCartwithstyle(requireContext(), list.get(position).getProductId(),String.valueOf(list.get(position).getStoreId()),list.get(position).getCuttingStyle(), list.get(position).getBoxStyle(),list.get(position).getSpecialInstructions())
                        .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                            @Override
                            public void onChanged(CommonResponse commonResponse) {
                                DialogUtils.dismissLoader();
                                if (!commonResponse.getError()) {
                                    getCartDetails();
                                }
                            }
                        });
            }
            else {
                DialogUtils.showLoader(requireContext());
                viewModel.removeItemToCart(requireContext(), list.get(position).getProductId(), String.valueOf(list.get(position).getStoreId()))
                        .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                            @Override
                            public void onChanged(CommonResponse commonResponse) {
                                DialogUtils.dismissLoader();
                                if (!commonResponse.getError()) {
                                    getCartDetails();
//                                adapter.notifyRemoveData(position);
                                }
                            }
                        });
            }

        }, cartDetails -> {
            list = cartDetails;
            ((DashboardActivity) requireActivity()).setCount(list.size());
            return null;
        });
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.recyclerView.setAdapter(adapter);


        SwipeHelper swipeHelper = new SwipeHelper(requireContext(), binding.recyclerView) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {

                underlayButtons.add(new SwipeHelper.UnderlayButton(
                        getString(R.string.delete),
                        0,
                        Color.parseColor("#BD0040"),
                        pos -> {


                            DialogUtils.showLoader(requireContext());
                            viewModel.removeCartFromCart(requireContext(), list.get(pos).getId())
                                    .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                        @Override
                                        public void onChanged(CommonResponse commonResponse) {
                                            DialogUtils.dismissLoader();
                                            if (!commonResponse.getError()) {
                                                getCartDetails();
                                            }
                                        }
                                    });

                        }
                ));

            }
        };


        swipeHelper.attachSwipe();
    }

    @Override
    public void onResume() {
        super.onResume();
        getCartDetails();
    }

    private void getCartDetails() {

        DialogUtils.showLoader(requireContext());
        viewModel.getCartDetails(requireContext()).observe(getViewLifecycleOwner(), it -> {
            DialogUtils.dismissLoader();
            binding.bot.setVisibility(View.GONE);
            if (!it.getError()) {
                if(it.getData().getSettings().getQuickDelivery().equals("true")){
                    sharedHelper.setIsFastDelivery("true");
                    sharedHelper.setFastDeliverycharge(it.getData().getFdamount());
                    sharedHelper.setDeliverycharge(it.getData().getDamount());
                }
                else {
                    sharedHelper.setIsFastDelivery("false");
                    sharedHelper.setDeliverycharge(it.getData().getDamount());
                    sharedHelper.setFastDeliverycharge(it.getData().getFdamount());
                }

                list = it.getData().getMyCart();
                if(list.isEmpty()){
                    binding.recyclerView.setVisibility(View.GONE);
                    binding.txt.setVisibility(View.VISIBLE);
                    binding.text1.setVisibility(View.GONE);
                    binding.text2.setVisibility(View.GONE);
                    ((DashboardActivity) requireActivity()).setCount(list.size());
                    sharedHelper.setCartCount(list.size());
                }
                else {
                    binding.bot.setVisibility(View.VISIBLE);
                    binding.recyclerView.setVisibility(View.VISIBLE);
                    binding.txt.setVisibility(View.GONE);

                    adapter.addItems(it.getData().getMyCart());
                    ((DashboardActivity) requireActivity()).setCount(list.size());
                    sharedHelper.setCartCount(list.size());

                    setValues(it.getData());
                    sharedHelper.setwalletpointsar(it.getData().getSettings().getWalletSAR());
                    sharedHelper.setwalletpoint(it.getData().getSettings().getWalletPoints());
                }
                sharedHelper.setwalletpointsar(it.getData().getSettings().getWalletSAR());
                sharedHelper.setwalletpoint(it.getData().getSettings().getWalletPoints());
            }
        });

      //  initView();
    }

    private void setValues(ViewCartResponse.ViewCartData data) {

        if (data != null) {
            Double subtot = 0.0;
            for (int i=0;i<data.getMyCart().size();i++){
                if(i != 0 ){
                    if(data.getMyCart().get(i).getCategoryName().equals(data.getMyCart().get(i-1).getCategoryName())){
                        subtot = 0.0;
                        data.getMyCart().get(i).setSp(false);
                        for(int a =0;a<data.getMyCart().size();a++){
                            if(data.getMyCart().get(i).getCategoryName().equalsIgnoreCase(data.getMyCart().get(a).getCategoryName())){
                                subtot = subtot+Double.parseDouble(data.getMyCart().get(a).getTotalPrice());
                            }
                        }

                        if (Double.parseDouble(data.getMyCart().get(i).getCate_minimum()) != 0.0f) {
                            if (subtot < Double.parseDouble(data.getMyCart().get(i).getCate_minimum())) {
                                data.getMyCart().get(i).setMovC(true);
                            } else {
                                data.getMyCart().get(i).setMovC(false);
                            }
                        }
                        else {
                            if(Double.parseDouble(data.getMyCart().get(i).getSub_minimum()) != 0.0f){
                                if (subtot < Double.parseDouble(data.getMyCart().get(i).getSub_minimum())) {
                                    data.getMyCart().get(i).setMovC(true);
                                } else {
                                    data.getMyCart().get(i).setMovC(false);
                                }
                            }
                        }
                    }
                    else {
                        subtot = 0.0;
                        data.getMyCart().get(i).setSp(true);
                        for(int a =0;a<data.getMyCart().size();a++){
                            if(data.getMyCart().get(i).getCategoryName().equalsIgnoreCase(data.getMyCart().get(a).getCategoryName())){
                                subtot = subtot+Double.parseDouble(data.getMyCart().get(a).getTotalPrice());
                            }
                        }

                        if (Double.parseDouble(data.getMyCart().get(i).getCate_minimum()) != 0.0f) {
                            if (subtot < Double.parseDouble(data.getMyCart().get(i).getCate_minimum())) {
                                data.getMyCart().get(i).setMovC(true);
                            } else {
                                data.getMyCart().get(i).setMovC(false);
                            }
                        }
                        else {
                            if(Double.parseDouble(data.getMyCart().get(i).getSub_minimum()) != 0.0f){
                                if (subtot < Double.parseDouble(data.getMyCart().get(i).getSub_minimum())) {
                                    data.getMyCart().get(i).setMovC(true);
                                } else {
                                    data.getMyCart().get(i).setMovC(false);
                                }
                            }
                        }
                    }
                }
                else {
                    data.getMyCart().get(i).setSp(true);
                    subtot = 0.0;
                    for(int a =0;a<data.getMyCart().size();a++){
                        if(data.getMyCart().get(i).getCategoryName().equalsIgnoreCase(data.getMyCart().get(a).getCategoryName())){
                            subtot = subtot+Double.parseDouble(data.getMyCart().get(a).getTotalPrice());
                        }
                    }

                    if (Double.parseDouble(data.getMyCart().get(i).getCate_minimum()) != 0.0f) {
                            if (subtot < Double.parseDouble(data.getMyCart().get(i).getCate_minimum())) {
                                data.getMyCart().get(i).setMovC(true);
                            } else {
                                data.getMyCart().get(i).setMovC(false);
                            }
                        }
                    else {
                            if(Double.parseDouble(data.getMyCart().get(i).getSub_minimum()) != 0.0f){
                                if (subtot < Double.parseDouble(data.getMyCart().get(i).getSub_minimum())) {
                                    data.getMyCart().get(i).setMovC(true);
                                } else {
                                    data.getMyCart().get(i).setMovC(false);
                                }
                            }
                        }

                }
            }



            if (data.getTotalAmount().equalsIgnoreCase("0")) {
                binding.completeGroup.setVisibility(View.GONE);
            } else {
                binding.completeGroup.setVisibility(View.VISIBLE);
            }

            double valu1 = 0;
            for (int i=0;i<data.getMyCart().size();i++){
                if(data.getMyCart().get(i).getBoxStyle()!=null && data.getMyCart().get(i).getCuttingStyle()!=null){
                    double valu = (data.getMyCart().get(i).getQuantity())*(Double.parseDouble(data.getMyCart().get(i).getBoxPrice()) + Double.parseDouble(data.getMyCart().get(i).getCuttingPrice()));
                    valu1 = valu1+valu;
                }
                else {
                    if(data.getMyCart().get(i).getBoxStyle()!=null){
                        double valu = (data.getMyCart().get(i).getQuantity())*(Double.parseDouble(data.getMyCart().get(i).getBoxPrice()));
                        valu1 = valu1+valu;
                    }
                    else if(data.getMyCart().get(i).getCuttingStyle()!=null){
                        double valu = (data.getMyCart().get(i).getQuantity())*(Double.parseDouble(data.getMyCart().get(i).getCuttingPrice()));
                        valu1 = valu1+valu;
                    }
                }
            }

            double tot = Double.parseDouble(data.getTotalAmount())+valu1;

            if(data.getDiscountAmount().equalsIgnoreCase("0")){
                binding.totalAmount.setText(String.format(Locale.ENGLISH,"%.0f",tot));
                binding.savedAmount.setText(data.getDiscountAmount());
                binding.savedAmount.setVisibility(View.GONE);
                binding.savedAmountcurrency.setVisibility(View.GONE);
                binding.savedAmountHeader.setVisibility(View.GONE);
                binding.view10.setVisibility(View.GONE);
            }
            else {
                binding.totalAmount.setText(String.format(Locale.ENGLISH,"%.0f",tot));
                binding.savedAmount.setText(data.getDiscountAmount());
                binding.savedAmount.setVisibility(View.VISIBLE);
                binding.savedAmountcurrency.setVisibility(View.VISIBLE);
                binding.savedAmountHeader.setVisibility(View.VISIBLE);
                binding.view10.setVisibility(View.VISIBLE);
            }


            CartSessionManager.removeAllValues();
            CartSessionManager.getInstance().setReorder(false);
            CartSessionManager.getInstance().setTotalAmount(data.getTotalAmount());
            CartSessionManager.getInstance().setcbstyleamount(String.valueOf(valu1));
            CartSessionManager.getInstance().setFlatDiscount(data.getSettings().getTaxAmount());


            if (data.getTrustUser() != null) {
                CartSessionManager.getInstance().setPackageValue(data.getTrustUser().getPackageValue());
                CartSessionManager.getInstance().setTrustUser(data.getTrustUser().getIsTrustUser());
            }

            if(data.getSettings().getMinimumOrderValue()!= null){
                double value = Double.parseDouble(data.getSettings().getMinimumOrderValue()) - tot;
                if(!list.isEmpty() && value > 0){
                    binding.text1.setVisibility(View.VISIBLE);
                    binding.text2.setVisibility(View.VISIBLE);
                    String first = getString(R.string.addmoreproductsworth);
                    String next = String.valueOf(String.format(Locale.ENGLISH,"%.0f",value)+" "+getString(R.string.sar));
                    String last = getString(R.string.togetfreedelivery);
                    String fine = first+" "+next+" "+last;
                    Spannable spannable = new SpannableString(fine);
                    spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.searchcolor_green)), first.length(), (first+" "+next).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    binding.text2.setText(spannable, TextView.BufferType.SPANNABLE);
                }
                else {
                    binding.text1.setVisibility(View.GONE);
                    binding.text2.setVisibility(View.GONE);
                    sharedHelper.setDeliverycharge("0");
                }
            }
            else{
                binding.text1.setVisibility(View.GONE);
                binding.text2.setVisibility(View.GONE);
            }

            setMaxTime(data.getMyCart());
        }


    }

    private void setMaxTime(ArrayList<ViewCartResponse.CartDetails> data) {

        int maxTime = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getSub_processingMin() == null || data.get(i).getSub_processingMin() == 0) {
                if (data.get(i).getCate_processingMin() == null || data.get(i).getCate_processingMin() == 0) {

                } else {
                    if (maxTime < data.get(i).getCate_processingMin()) {
                        maxTime = data.get(i).getCate_processingMin();
                    }
                }
            } else {
                if (maxTime < data.get(i).getSub_processingMin()) {
                    maxTime = data.get(i).getSub_processingMin();
                }
            }
        }
        CartSessionManager.getInstance().setOrderMinimumMins(maxTime);

    }
}
