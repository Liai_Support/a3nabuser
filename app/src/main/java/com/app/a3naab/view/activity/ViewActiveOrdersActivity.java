package com.app.a3naab.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityViewActiveOrdersBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.CheckReorderResponse;
import com.app.a3naab.model.OrderDetails;
import com.app.a3naab.model.ViewOrdersResponse;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.CartViewcartProductAdapter;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.util.Locale;

public class ViewActiveOrdersActivity extends BaseActivity {

    ActivityViewActiveOrdersBinding binding;
    ShoppingViewModel shoppingViewModel;
    String adminno="",driverno="";
    String totalamount = "";
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_active_orders);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        sharedHelper = new SharedHelper(this);
        getOrderDetails();
        initListeners();

    }

    private void initListeners() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.back.setOnClickListener((view) -> {
            finish();
        });
    }


    private void getOrderDetails() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            int orderId = bundle.getInt("orderId");
            adminno = bundle.getString("adminno");
            driverno = bundle.getString("driverno");

            DialogUtils.showLoader(this);
            shoppingViewModel.getOrderDetails(this, orderId).observe(this, new Observer<ViewOrdersResponse>() {
                @Override
                public void onChanged(ViewOrdersResponse viewOrdersResponse) {
                    DialogUtils.dismissLoader();
                    if (!viewOrdersResponse.getError()) {
                        loadData(viewOrdersResponse.getData());
                    }
                }
            });
        }

    }

    @SuppressLint("SetTextI18n")
    private void loadData(OrderDetails data) {

        binding.orderId.setText(getString(R.string.order_id) + " " + data.getOrderInfo().getOrderIDs());

        if (data.getOrderInfo().getOrderOn() != null) {
            binding.orderDate.setText(getString(R.string.order_placed) + "\n" + Utils.getConvertedTime(data.getOrderInfo().getOrderOn())+ "\n" +Utils.convertDateutc(data.getOrderInfo().getOrderOn(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
        }

        if (data.getOrderInfo().getDeliveryDate() != null) {
            binding.deliveryDate.setVisibility(View.VISIBLE);
            binding.deliveryDateImage.setVisibility(View.VISIBLE);
            if(data.getOrderInfo().getFastDelivery().equals("1")){
                binding.deliveryDate.setText(getString(R.string.delivery_by) + "\n" + getString(R.string.fast_delivery));
            }
            else {
                String selectedtime = Utils.convertDate(data.getOrderInfo().getDeliveryOn_fromTime(), "HH:mm:ss", "hh:mm a")
                        + "-" +
                        Utils.convertDate(data.getOrderInfo().getDeliveryOn_toTime(), "HH:mm:ss", "hh:mm a");
                binding.deliveryDate.setText(getString(R.string.delivery_by) + "\n" + Utils.getConvertedTime(data.getOrderInfo().getDeliveryDate()) +"\n" +selectedtime);
            }
        } else {
            binding.deliveryDate.setVisibility(View.GONE);
            binding.deliveryDateImage.setVisibility(View.GONE);
        }

        binding.address.setText(data.getOrderInfo().getAddressType() + "," + data.getOrderInfo().getAddressPinDetails());

        Double cbstyleamount =0.0;
        for (int i=0;i<data.getOrderList().size();i++){
            Double v = Integer.parseInt(data.getOrderList().get(i).getQuantity()) * (Double.parseDouble(data.getOrderList().get(i).getBoxPrice()) + Double.parseDouble(data.getOrderList().get(i).getCuttingPrice()));
            cbstyleamount = cbstyleamount+v;
        }

        Double subtotal = (cbstyleamount)+(Double.parseDouble(data.getOrderInfo().getTotalAmount()) - Double.parseDouble(data.getOrderInfo().getDiscountAmount()));
        Double delivery = Double.parseDouble(data.getOrderInfo().getFastDelievryCharge());
       // Double subtotal1 = (subtotal-Double.parseDouble(data.getOrderInfo().getTakeToCustomer()))+Double.parseDouble(data.getOrderInfo().getGiveToCustomer())+delivery;

        if(data.getOrderInfo().getOff_types().equalsIgnoreCase("0")){
            delivery = Double.parseDouble(data.getOrderInfo().getCouponDiscount());
        }

        Double subtotal1 = subtotal+delivery;
        Double coupondiscount = Double.parseDouble(data.getOrderInfo().getCouponDiscount());
        Double pointamount = Double.parseDouble(data.getOrderInfo().getPointsAmount());
        Double walletamount = Double.parseDouble(data.getOrderInfo().getPaidByWallet());
        Double vat = ((subtotal1-(coupondiscount+pointamount+walletamount)) * (Double.parseDouble(data.getOrderInfo().getTaxvalue()) / 100));
        Double total = ((subtotal1-(coupondiscount+pointamount+walletamount))+vat);
        if(total < 0){
            totalamount = String.format(Locale.ENGLISH,"%.2f",0.0);
        }
        else {
            totalamount = String.format(Locale.ENGLISH,"%.2f",total);
        }

        if(coupondiscount==0.0){
            binding.discountVal.setVisibility(View.GONE);
            binding.disTxt.setVisibility(View.GONE);
        }
        if(pointamount==0.0){
            binding.walletpointVal.setVisibility(View.GONE);
            binding.walletpointTxt.setVisibility(View.GONE);
        }
        if(walletamount==0.0){
            binding.walletamountTxt.setVisibility(View.GONE);
            binding.walletamountVal.setVisibility(View.GONE);
        }

       // binding.totalAmount.setText("SAR " + data.getOrderInfo().getGrandTotal());
        //binding.totalAmountTop.setText("SAR " + data.getOrderInfo().getGrandTotal());
        // binding.textView33.setText("SAR "+Double.parseDouble(data.getOrderInfo().getTaxvalue()));
        binding.textView29.setText(""+getString(R.string.vat)+"("+data.getOrderInfo().getTaxvalue()+" %)");
        binding.totalAmount.setText("SAR " +totalamount);
        binding.totalAmountTop.setText("SAR " +totalamount);
        binding.subtotal.setText("SAR " + String.format(Locale.ENGLISH,"%.2f",subtotal));
        binding.textView33.setText("SAR " + String.format(Locale.ENGLISH,"%.2f",vat));
        binding.walletpointVal.setText("SAR "+String.format(Locale.ENGLISH,"%.2f",pointamount));
        binding.walletamountVal.setText("SAR "+String.format(Locale.ENGLISH,"%.2f",walletamount));
        binding.discountVal.setText("SAR "+"-"+String.format(Locale.ENGLISH,"%.2f",coupondiscount));
        binding.amounttobepaid.setText("SAR "+ data.getOrderInfo().getTakeToCustomer());
        binding.amounttobereceived.setText("SAR "+ data.getOrderInfo().getGiveToCustomer());
        binding.deliveryfee.setText("SAR "+String.format(Locale.ENGLISH,"%.2f",delivery));



        if(data.getOrderInfo().getPaytype().equalsIgnoreCase("CASH")){
            binding.paymentType.setText(""+getString(R.string.codmachine));
        }
        else {
            binding.paymentType.setText(data.getOrderInfo().getPaytype());
        }


      /*  if(data.getOrderInfo().getFastDelivery().equals("1") || data.getOrderInfo().getFastDelivery().equals("0")){
            binding.deliveryfee.setText("SAR "+data.getOrderInfo().getFastDelievryCharge());
        }*/

        binding.orderedItems.setLayoutManager(new LinearLayoutManager(this));
        binding.orderedItems.setAdapter(new CartViewcartProductAdapter(this, data.getOrderList(), position -> {

        }, position -> {

        }));


        binding.reschedule.setOnClickListener(view -> {
            DialogUtils.showLoader(this);
            shoppingViewModel.checkreorder(this, data.getOrderInfo().getId()).observe(this, new Observer<CheckReorderResponse>() {
                @Override
                public void onChanged(CheckReorderResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (!commonResponse.getError()) {
                        CartSessionManager.removeAllValues();
                        CartSessionManager.getInstance().setOrderId(String.valueOf(data.getOrderInfo().getId()));
                        startActivity(new Intent(ViewActiveOrdersActivity.this, RescheduleOrderActivity.class));

                    }
                    else {
                        DialogUtils.showAlertDialog(ViewActiveOrdersActivity.this,  getString(R.string.storenotavailable),getString(R.string.oops), getString(R.string.ok), new DialogCallback() {
                            @Override
                            public void onPositiveClick() {

                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });                        //commonResponse.getMessage()
                    }
                }
            });
        });

        binding.cancelOrder.setOnClickListener(view -> {
            startActivity(new Intent(this, CancelOrderActivity.class)
                    .putExtra("orderId", data.getOrderInfo().getId())
                    .putExtra("paytype", data.getOrderInfo().getPaytype())
                    .putExtra("amount",totalamount)
                    .putExtra("noonorderid",data.getOrderInfo().getReturnOrderId())
            );
        });

        if (data.getOrderInfo().getAcceptByStore().equalsIgnoreCase("0")) {
            binding.reschedule.setVisibility(View.VISIBLE);
            binding.cancelOrder.setVisibility(View.VISIBLE);
        } else {
            binding.reschedule.setVisibility(View.GONE);
            binding.cancelOrder.setVisibility(View.GONE);
        }

        if (data.getOrderInfo().getDeleteItems().equals("0")) {
            binding.replaceText.setText(getString(R.string.delete_the_item_from_the_shopping_cart));
        } else {
            binding.replaceText.setText(getString(R.string.contact_me_to_introduce_another_product));
        }

        if(data.getOrderInfo().getFastDelivery().equals("1")) {
            binding.reschedule.setVisibility(View.GONE);
        }

       /* if (data.getOrderInfo().getOrderStatus().equalsIgnoreCase("PENDING")) {


            binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirm_gray));

            binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag));

            binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck));


            binding.delivered.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveredImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.orderbox));

            binding.linear.setVisibility(View.GONE);


        }
        else if (data.getOrderInfo().getOrderStatus().equalsIgnoreCase("ACCEPTED")) {
            binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirmed));

            binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag));

            binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck));
            binding.linear.setVisibility(View.VISIBLE);
            binding.linear1.setVisibility(View.VISIBLE);
            binding.rela1.setVisibility(View.GONE);


        }
        else if (data.getOrderInfo().getOrderStatus().equalsIgnoreCase("ONGOING")) {

            binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirmed));

            binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag_green));

            binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck));
            binding.linear.setVisibility(View.VISIBLE);
            binding.linear1.setVisibility(View.GONE);
            binding.rela1.setVisibility(View.VISIBLE);
            binding.txt1.setText(""+getString(R.string.wearenowshoppingyourorder));
            binding.call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(ViewActiveOrdersActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions((Activity) ViewActiveOrdersActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                100);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    } else {
                        //You already have permission
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL,
                                    Uri.parse("tel:" + "+" +"966598765570"));
                            startActivity(intent);
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });


        }
        else if (data.getOrderInfo().getOrderStatus().equalsIgnoreCase("PICKUP")) {

            binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirmed));

            binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag_green));

            binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck_green));
            binding.linear.setVisibility(View.VISIBLE);
            binding.linear1.setVisibility(View.GONE);
            binding.rela1.setVisibility(View.VISIBLE);
            binding.txt1.setText(""+getString(R.string.theorderisonyourway));
            binding.call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(ViewActiveOrdersActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions((Activity) ViewActiveOrdersActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                100);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    } else {
                        //You already have permission
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL,
                                    Uri.parse("tel:" + "+" +"966598765570"));
                            startActivity(intent);
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
        else {
            binding.linear.setVisibility(View.GONE);
        }
*/

        Log.d("zcjjc",""+data.getOrderInfo().isApproved());
        if(data.getOrderInfo().isApproved().equalsIgnoreCase("ACCEPTED")){
            binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
            binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirmed));

            binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag));

            binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck));
            if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_truck_arabic_white));
            }
            binding.groupLoad.setVisibility(View.VISIBLE);
            binding.linear.setVisibility(View.VISIBLE);
            binding.linear1.setVisibility(View.VISIBLE);
            binding.rela1.setVisibility(View.GONE);
            if(data.getOrderInfo().getAcceptByStore().equalsIgnoreCase("1")){
                binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirmed));

                binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag_green));

                binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
                binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
                binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck));
                if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                    binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_truck_arabic_white));
                }
                binding.linear.setVisibility(View.VISIBLE);
                binding.linear1.setVisibility(View.GONE);
                binding.rela1.setVisibility(View.VISIBLE);
                binding.txt1.setText(""+getString(R.string.wearenowshoppingyourorder));
                binding.call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ContextCompat.checkSelfPermission(ViewActiveOrdersActivity.this,
                                Manifest.permission.CALL_PHONE)
                                != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions((Activity) ViewActiveOrdersActivity.this,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    100);

                            // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        } else {
                            //You already have permission
                            try {
                                Intent intent = new Intent(Intent.ACTION_CALL,
                                        Uri.parse("tel:" +adminno));
                                startActivity(intent);
                            } catch (SecurityException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                if(data.getOrderInfo().getPackedByDriver().equalsIgnoreCase("1")){
                    binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                    binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                    binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirmed));

                    binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                    binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                    binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag_green));

                    binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                    binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.searchcolor_green));
                    binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck_green));
                    if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                        binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_truck_arabic_green));
                    }
                    binding.linear.setVisibility(View.VISIBLE);
                    binding.linear1.setVisibility(View.GONE);
                    binding.rela1.setVisibility(View.VISIBLE);
                    binding.txt1.setText(""+getString(R.string.theorderisonyourway));
                    binding.call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (ContextCompat.checkSelfPermission(ViewActiveOrdersActivity.this,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) ViewActiveOrdersActivity.this,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        100);

                                // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            } else {
                                //You already have permission
                                try {
                                    Intent intent = new Intent(Intent.ACTION_CALL,
                                            Uri.parse("tel:" +"+966"+driverno));
                                    startActivity(intent);
                                } catch (SecurityException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        }
        else {
            binding.confirmed.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.confirmedView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.confirm_gray));

            binding.shopping.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bag));

            binding.delivery.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryView.setBackgroundColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.truck));
            if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_truck_arabic_white));
            }


            binding.delivered.setTextColor(ContextCompat.getColor(this, R.color.view_line_color_2));
            binding.deliveredImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.orderbox));

            binding.linear.setVisibility(View.GONE);
            binding.groupLoad.setVisibility(View.GONE);
        }

    }
}
