package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityBestProductBinding;
import com.app.a3naab.databinding.ActivityFavouriteListBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.BestProductResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.BestProductListAdapter;
import com.app.a3naab.view.adapter.FavouriteProductListAdapter;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.ArrayList;

public class FavouriteProductListActivity extends BaseActivity {

    ProductViewModel productViewModel;
    ActivityFavouriteListBinding binding;
    FavouriteProductListAdapter bestAdapter;
    ArrayList<DashboardResponse.BestProduct> list;
    SharedHelper sharedHelper;
    String boxid = "0";
    String cutid = "0";
    String sinstruction = "";
    ArrayList<ViewCartResponse.CartDetails> cartlist = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favourite_list);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
       // initListener();
       // getCartCount();
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

        binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private void getCartCount() {

        productViewModel.getCartDetails(this).observe(this, response -> {
            if (!response.getError()) {
                if (response.getData().getMyCart().size() == 0) {
                    binding.foot.badge.setVisibility(View.GONE);
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                } else {
                    binding.foot.badge.setVisibility(View.VISIBLE);
                    binding.foot.badge.setText(response.getData().getMyCart().size() + "");
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                    cartlist = response.getData().getMyCart();
                }
            }
        });

    }



    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void initListener() {

        productViewModel.getFavouriteList(this, 1).observe(this, new Observer<BestProductResponse>() {
            @Override
            public void onChanged(BestProductResponse categoryResponse) {
                if (!categoryResponse.getError()) {


                     list = categoryResponse.getData().getProducts();

                    binding.bestProduct.setLayoutManager(new GridLayoutManager(FavouriteProductListActivity.this, 2));

                    if (categoryResponse.getData() != null) {
                        bestAdapter = new FavouriteProductListAdapter(FavouriteProductListActivity.this,list , new OnItemClick() {
                            @Override
                            public void onClick(int position) {
                                if(list.get(position).getBoxStyle().size()!=0 || list.get(position).getCuttingStyle().size()!=0){
                                    if(checkiscart(list.get(position).getId(),Integer.parseInt(list.get(position).getStoreId()))){
                                        DialogUtils.showLoader(FavouriteProductListActivity.this);
                                        productViewModel.addtoCartWithStyle(FavouriteProductListActivity.this, list.get(position).getId(), list.get(position).getStoreId(), cutid, boxid, sinstruction)
                                                .observe(FavouriteProductListActivity.this, new Observer<CommonResponse>() {
                                                    @Override
                                                    public void onChanged(CommonResponse commonResponse) {
                                                        DialogUtils.dismissLoader();
                                                        if (!commonResponse.getError()) {
                                                            bestAdapter.notifyAddData(position);
                                                            getCartCount();
                                                        }
                                                    }
                                                });

                                    }
                                    else {
                                        DialogUtils.showLoader(FavouriteProductListActivity.this);
                                        Log.d("xcxc", "" + list.get(position).getId() + "cdc" + "" + list.get(position).getStoreId());
                                        productViewModel.addItemToCart(FavouriteProductListActivity.this, list.get(position).getId(), list.get(position).getStoreId())
                                                .observe(FavouriteProductListActivity.this, new Observer<CommonResponse>() {
                                                    @Override
                                                    public void onChanged(CommonResponse commonResponse) {
                                                        DialogUtils.dismissLoader();
                                                        if (!commonResponse.getError()) {
                                                            bestAdapter.notifyAddData(position);
                                                            getCartCount();
                                                        }
                                                    }
                                                });
                                    }
                                }
                                else {
                                    DialogUtils.showLoader(FavouriteProductListActivity.this);
                                    Log.d("xcxc", "" + list.get(position).getId() + "cdc" + "" + list.get(position).getStoreId());
                                    productViewModel.addItemToCart(FavouriteProductListActivity.this, list.get(position).getId(), list.get(position).getStoreId())
                                            .observe(FavouriteProductListActivity.this, new Observer<CommonResponse>() {
                                                @Override
                                                public void onChanged(CommonResponse commonResponse) {
                                                    DialogUtils.dismissLoader();
                                                    if (!commonResponse.getError()) {
                                                        bestAdapter.notifyAddData(position);
                                                        getCartCount();
                                                    }
                                                }
                                            });
                                }
                            }
                        }, new OnItemClick() {
                            @Override
                            public void onClick(int position) {
                                if(list.get(position).getBoxStyle().size()!=0 || list.get(position).getCuttingStyle().size()!=0){
                                    if(checkiscart(list.get(position).getId(),Integer.parseInt(list.get(position).getStoreId()))){
                                        DialogUtils.showLoader(FavouriteProductListActivity.this);
                                        productViewModel.removeItemToCartwithstyle(FavouriteProductListActivity.this,  list.get(position).getId(), list.get(position).getStoreId(), cutid, boxid, sinstruction)
                                                .observe(FavouriteProductListActivity.this, new Observer<CommonResponse>() {
                                                    @Override
                                                    public void onChanged(CommonResponse commonResponse) {
                                                        DialogUtils.dismissLoader();
                                                        if (!commonResponse.getError()) {
                                                            bestAdapter.notifyRemoveData(position);
                                                            getCartCount();
                                                        }
                                                    }
                                                });
                                    }
                                    else {
                                        DialogUtils.showLoader(FavouriteProductListActivity.this);
                                        Log.d("dfvdfv", "remove");
                                        productViewModel.removeItemToCart(FavouriteProductListActivity.this, list.get(position).getId(), list.get(position).getStoreId())
                                                .observe(FavouriteProductListActivity.this, new Observer<CommonResponse>() {
                                                    @Override
                                                    public void onChanged(CommonResponse commonResponse) {
                                                        DialogUtils.dismissLoader();
                                                        if (!commonResponse.getError()) {
                                                            bestAdapter.notifyRemoveData(position);
                                                            getCartCount();
                                                        }

                                                    }
                                                });
                                    }
                                }
                                else {
                                    DialogUtils.showLoader(FavouriteProductListActivity.this);
                                    Log.d("dfvdfv", "remove");
                                    productViewModel.removeItemToCart(FavouriteProductListActivity.this, list.get(position).getId(), list.get(position).getStoreId())
                                            .observe(FavouriteProductListActivity.this, new Observer<CommonResponse>() {
                                                @Override
                                                public void onChanged(CommonResponse commonResponse) {
                                                    DialogUtils.dismissLoader();
                                                    if (!commonResponse.getError()) {
                                                        bestAdapter.notifyRemoveData(position);
                                                        getCartCount();
                                                    }

                                                }
                                            });
                                }
                            }
                        }, new OnItemClick() {
                            @Override
                            public void onClick(int position) {
                                Log.d("xcxc",""+list.get(position).getId()+"vcv");
                                productViewModel.setFavorite(FavouriteProductListActivity.this, list.get(position).getId(),Integer.parseInt(list.get(position).getStoreId()), "false").observe(FavouriteProductListActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.removeItemAndRefresh(position);
                                        }
                                    }
                                });
                            }
                        });

                        binding.bestProduct.setAdapter(bestAdapter);
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        initListener();
        getCartCount();
    }

    private Boolean checkiscart(int productid,int storeid){
        boxid = "0";
        cutid = "0";
        sinstruction = "";
        for (int i=0;i<cartlist.size();i++){
            if(cartlist.get(i).getProductId() == productid && cartlist.get(i).getStoreId() == storeid){
                if(cartlist.get(i).getBoxStyle()!=null){
                    boxid = cartlist.get(i).getBoxStyle();
                }
                if(cartlist.get(i).getBoxStyle()!=null){
                    cutid = cartlist.get(i).getCuttingStyle();
                }
                if(cartlist.get(i).getSpecialInstructions()!=null){
                    sinstruction = cartlist.get(i).getSpecialInstructions();
                }
                return true;
            }
        }
        return false;
    }


}
