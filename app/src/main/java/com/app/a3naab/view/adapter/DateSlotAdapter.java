package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildDateSelectionBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.DateSlotModel;

import java.util.ArrayList;

public class DateSlotAdapter extends RecyclerView.Adapter<DateSlotAdapter.MyViewHolder> {

    Context context;
    ArrayList<DateSlotModel> slotData;
    OnItemClick onItemClick;

    public DateSlotAdapter(Context context, ArrayList<DateSlotModel> slotData, OnItemClick onItemClick) {
        this.context = context;
        this.slotData = slotData;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public DateSlotAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_date_selection, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull DateSlotAdapter.MyViewHolder holder, int position) {


        holder.binding.time.setText(slotData.get(position).getDisplayDay());
        holder.binding.date.setText(slotData.get(position).getDisplayDate());

        if (slotData.get(position).isSelected()) {

            holder.binding.time.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_red));
            holder.binding.date.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_red));

            holder.binding.view18.setBackground(ContextCompat.getDrawable(context, R.drawable.rectangle_corner_bg_primary));

        } else {

            holder.binding.time.setTextColor(ContextCompat.getColor(context, R.color.header_text_color));
            holder.binding.date.setTextColor(ContextCompat.getColor(context, R.color.header_text_color));

            holder.binding.view18.setBackground(ContextCompat.getDrawable(context, R.drawable.rectangle_corner_not_selected_bg));

        }

        holder.binding.view18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelected(position);
            }
        });

    }

    public void setSelected(int position) {
        onItemClick.onClick(position);
        for (int i = 0; i < slotData.size(); i++) {
            slotData.get(i).setSelected(position == i);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return slotData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildDateSelectionBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildDateSelectionBinding.bind(itemView);
        }

    }
}
