package com.app.a3naab.view.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityBestProductBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.BestProductResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.BestProductListAdapter;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.ArrayList;

public class BestProductListActivity extends BaseActivity {

    ProductViewModel productViewModel;
    ActivityBestProductBinding binding;
    BestProductListAdapter bestAdapter;
    ArrayList<ViewCartResponse.CartDetails> cartlist = new ArrayList<>();
    String boxid = "0";
    String cutid = "0";
    String sinstruction = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_best_product);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
       // initListener();

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }

    }

    private void initListener() {

        productViewModel.getBestProduct(this, 1).observe(this, new Observer<BestProductResponse>() {
            @Override
            public void onChanged(BestProductResponse categoryResponse) {
                if (!categoryResponse.getError()) {


                    binding.bestProduct.setLayoutManager(new GridLayoutManager(BestProductListActivity.this, 2));

                    if (categoryResponse.getData() != null) {

                        if(categoryResponse.getData().getProducts().size()!=0){
                            binding.txtEmpty.setVisibility(View.GONE);
                            bestAdapter = new BestProductListAdapter(BestProductListActivity.this, categoryResponse.getData().getProducts(), new OnItemClick() {
                                @Override
                                public void onClick(int position) {
                                    if(categoryResponse.getData().getProducts().get(position).getBoxStyle().size()!=0 || categoryResponse.getData().getProducts().get(position).getCuttingStyle().size()!=0){
                                        if(checkiscart(categoryResponse.getData().getProducts().get(position).getId(),Integer.parseInt(categoryResponse.getData().getProducts().get(position).getStoreId()))){
                                            DialogUtils.showLoader(BestProductListActivity.this);
                                            productViewModel.addtoCartWithStyle(BestProductListActivity.this, categoryResponse.getData().getProducts().get(position).getId(), categoryResponse.getData().getProducts().get(position).getStoreId(), cutid, boxid, sinstruction)
                                                    .observe(BestProductListActivity.this, new Observer<CommonResponse>() {
                                                        @Override
                                                        public void onChanged(CommonResponse commonResponse) {
                                                            DialogUtils.dismissLoader();
                                                            if (!commonResponse.getError()) {
                                                                bestAdapter.notifyAddData(position);
                                                                getCartCount();
                                                            }
                                                        }
                                                    });
                                        }
                                        else {
                                            DialogUtils.showLoader(BestProductListActivity.this);
                                            productViewModel.addItemToCart(BestProductListActivity.this, categoryResponse.getData().getProducts().get(position).getId(), categoryResponse.getData().getProducts().get(position).getStoreId())
                                                    .observe(BestProductListActivity.this, new Observer<CommonResponse>() {
                                                        @Override
                                                        public void onChanged(CommonResponse commonResponse) {
                                                            DialogUtils.dismissLoader();
                                                            if (!commonResponse.getError()) {
                                                                bestAdapter.notifyAddData(position);
                                                                getCartCount();
                                                            }
                                                        }
                                                    });
                                        }
                                    }
                                    else {
                                        DialogUtils.showLoader(BestProductListActivity.this);
                                        productViewModel.addItemToCart(BestProductListActivity.this, categoryResponse.getData().getProducts().get(position).getId(), categoryResponse.getData().getProducts().get(position).getStoreId())
                                                .observe(BestProductListActivity.this, new Observer<CommonResponse>() {
                                                    @Override
                                                    public void onChanged(CommonResponse commonResponse) {
                                                        DialogUtils.dismissLoader();
                                                        if (!commonResponse.getError()) {
                                                            bestAdapter.notifyAddData(position);
                                                            getCartCount();
                                                        }
                                                    }
                                                });
                                    }
                                }
                            }, new OnItemClick() {
                                @Override
                                public void onClick(int position) {
                                    if(categoryResponse.getData().getProducts().get(position).getBoxStyle().size()!=0 || categoryResponse.getData().getProducts().get(position).getCuttingStyle().size()!=0){
                                        if(checkiscart(categoryResponse.getData().getProducts().get(position).getId(),Integer.parseInt(categoryResponse.getData().getProducts().get(position).getStoreId()))){
                                            DialogUtils.showLoader(BestProductListActivity.this);
                                            productViewModel.removeItemToCartwithstyle(BestProductListActivity.this,  categoryResponse.getData().getProducts().get(position).getId(), categoryResponse.getData().getProducts().get(position).getStoreId(), cutid, boxid, sinstruction)
                                                    .observe(BestProductListActivity.this, new Observer<CommonResponse>() {
                                                        @Override
                                                        public void onChanged(CommonResponse commonResponse) {
                                                            DialogUtils.dismissLoader();
                                                            if (!commonResponse.getError()) {
                                                                bestAdapter.notifyRemoveData(position);
                                                                getCartCount();
                                                            }
                                                        }
                                                    });
                                        }
                                        else {
                                            DialogUtils.showLoader(BestProductListActivity.this);
                                            productViewModel.removeItemToCart(BestProductListActivity.this, categoryResponse.getData().getProducts().get(position).getId(), categoryResponse.getData().getProducts().get(position).getStoreId())
                                                    .observe(BestProductListActivity.this, new Observer<CommonResponse>() {
                                                        @Override
                                                        public void onChanged(CommonResponse commonResponse) {
                                                            DialogUtils.dismissLoader();
                                                            if (!commonResponse.getError()) {
                                                                bestAdapter.notifyRemoveData(position);
                                                                getCartCount();
                                                            }
                                                        }
                                                    });
                                        }
                                    }
                                    else {
                                        DialogUtils.showLoader(BestProductListActivity.this);
                                        productViewModel.removeItemToCart(BestProductListActivity.this, categoryResponse.getData().getProducts().get(position).getId(), categoryResponse.getData().getProducts().get(position).getStoreId())
                                                .observe(BestProductListActivity.this, new Observer<CommonResponse>() {
                                                    @Override
                                                    public void onChanged(CommonResponse commonResponse) {
                                                        DialogUtils.dismissLoader();
                                                        if (!commonResponse.getError()) {
                                                            bestAdapter.notifyRemoveData(position);
                                                            getCartCount();
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                            binding.bestProduct.setAdapter(bestAdapter);
                        }
                        else {
                            binding.txtEmpty.setVisibility(View.VISIBLE);
                            binding.txtEmpty.setText(getString(R.string.notavailable));
                        }
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        initListener();
        getCartCount();
    }

    private void getCartCount() {
        productViewModel.getCartDetails(this).observe(this, response -> {
            if (!response.getError()) {
                cartlist = response.getData().getMyCart();
            }
        });

    }

    private Boolean checkiscart(int productid,int storeid){
        boxid = "0";
        cutid = "0";
        sinstruction = "";
        for (int i=0;i<cartlist.size();i++){
            if(cartlist.get(i).getProductId() == productid && cartlist.get(i).getStoreId() == storeid){
                if(cartlist.get(i).getBoxStyle()!=null){
                    boxid = cartlist.get(i).getBoxStyle();
                }
                if(cartlist.get(i).getBoxStyle()!=null){
                    cutid = cartlist.get(i).getCuttingStyle();
                }
                if(cartlist.get(i).getSpecialInstructions()!=null){
                    sinstruction = cartlist.get(i).getSpecialInstructions();
                }
                return true;
            }
        }
        return false;
    }
}
