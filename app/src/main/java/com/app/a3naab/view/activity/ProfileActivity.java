package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityEditProfileBinding;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.model.ProfileDatas;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.Calendar;

public class ProfileActivity extends BaseActivity {

    ActivityEditProfileBinding binding;
    ProductViewModel viewModel;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        initListener();
        callObservers();
        if (sharedHelper.getCartCount() == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setVisibility(View.VISIBLE);
            binding.foot.badge.setText(""+sharedHelper.getCartCount());
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        initListener();
        callObservers();
    }

    private void callObservers() {

        binding.maleRdt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    binding.femaleRdt.setChecked(false);
                }
            }
        });

        binding.femaleRdt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    binding.maleRdt.setChecked(false);
                }
            }
        });

        DialogUtils.showLoader(this);
        viewModel.getProfileDetails(this).observe(this, new Observer<ProfileDataResponse>() {
            @Override
            public void onChanged(ProfileDataResponse profileDataResponse) {
                DialogUtils.dismissLoader();
                if (profileDataResponse.getError()) {
                    Utils.showSnack(binding.parentLayout, profileDataResponse.getMessage());
                } else {
                    setData(profileDataResponse.getData().getProfile());
                }
            }
        });

        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

    }

    private void setData(ProfileDatas profile) {

        binding.firstName.setText(profile.getFirstName());
        binding.lastName.setText(profile.getLastName());
        if (profile.getEmail() != null)
            binding.emailAddress.setText(profile.getEmail());
        binding.number.setText(profile.getMobileNumber());
        try {
            binding.countryCodePicker.setCountryForPhoneCode(Integer.parseInt(profile.getCountryCode()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (profile.getGender().equalsIgnoreCase("MALE")) {
            binding.maleRdt.setChecked(true);
            binding.femaleRdt.setChecked(false);
        } else if (profile.getGender().equalsIgnoreCase("FEMALE")) {
            binding.maleRdt.setChecked(false);
            binding.femaleRdt.setChecked(true);
        } else {
            binding.maleRdt.setChecked(false);
            binding.femaleRdt.setChecked(false);
        }

        if (profile.getDOB() != null) {
            binding.date.setText(profile.getDay());
            binding.month.setText(profile.getMonth());
            binding.year.setText(profile.getYear());
        }

        makeFieldEditable(false);
    }


    private void makeFieldEditable(boolean enable) {
        binding.firstName.setEnabled(enable);
        binding.lastName.setEnabled(enable);
        binding.emailAddress.setEnabled(enable);
        binding.number.setEnabled(enable);
        binding.countryCodePicker.setEnabled(enable);
        binding.countryCodePicker.setCcpClickable(enable);
        binding.maleRdt.setEnabled(enable);
        binding.femaleRdt.setEnabled(enable);
        binding.date.setEnabled(enable);
        binding.month.setEnabled(enable);
        binding.year.setEnabled(enable);

    }

    private void initListener() {

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeFieldEditable(true);
                binding.save.setVisibility(View.VISIBLE);
            }
        });

        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidInputs()) {

                    String gender = "";
                    if (binding.maleRdt.isChecked()) {
                        gender = "MALE";
                    } else if (binding.femaleRdt.isChecked()) {
                        gender = "FEMALE";
                    }
                    viewModel.updateProfile(ProfileActivity.this, binding.firstName.getText().toString().trim(),
                            binding.lastName.getText().toString().trim(),
                            binding.emailAddress.getText().toString().trim(),
                            binding.number.getText().toString().trim(),
                            Utils.formatedValues(Integer.parseInt(binding.date.getText().toString().trim())),
                            Utils.formatedValues(Integer.parseInt(binding.month.getText().toString().trim())),
                            Utils.formatedValues(Integer.parseInt(binding.year.getText().toString().trim())),
                            Utils.formatedValues(Integer.parseInt(binding.countryCodePicker.getSelectedCountryCode())),
                            gender)
                            .observe(ProfileActivity.this, commonResponse -> {
                                if (commonResponse.getError()) {
                                    Utils.showSnack(binding.parentLayout, commonResponse.getMessage());
                                } else {
                                    finish();
                                }
                            });
                }
            }
        });


    }

    private boolean isValidInputs() {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (binding.firstName.getText().toString().trim().length() == 0) {
            Utils.showSnack(binding.parentLayout, getString(R.string.first_name_not_empty));
            return false;
        } else if (binding.lastName.getText().toString().trim().length() == 0) {
            Utils.showSnack(binding.parentLayout, getString(R.string.last_name_not_empty));
            return false;
        } else if (binding.emailAddress.getText().toString().trim().length() == 0 || (!binding.emailAddress.getText().toString().matches(emailPattern))) {
            Utils.showSnack(binding.parentLayout, getString(R.string.email_not_empty));
            return false;
        } else if (binding.number.getText().toString().trim().length() <= 5) {
            Utils.showSnack(binding.parentLayout, getString(R.string.enter_mobile_number));
            return false;
        } else if (binding.date.getText().toString().trim().length() == 0 || Integer.parseInt(binding.date.getText().toString()) >= 32 || Integer.parseInt(binding.date.getText().toString()) == 0) {
            if (binding.date.getText().toString().trim().length() == 0) {
                Utils.showSnack(binding.parentLayout, getString(R.string.enter_date));
            } else if (Integer.parseInt(binding.date.getText().toString()) >= 32 || Integer.parseInt(binding.date.getText().toString()) == 0) {
                Utils.showSnack(binding.parentLayout, getString(R.string.invalid_date));
            }
            return false;
        } else if (binding.month.getText().toString().trim().length() == 0 || Integer.parseInt(binding.month.getText().toString()) >= 13 || Integer.parseInt(binding.month.getText().toString()) == 0) {
            if (binding.month.getText().toString().trim().length() == 0) {
                Utils.showSnack(binding.parentLayout, getString(R.string.enter_month));
            } else if (Integer.parseInt(binding.month.getText().toString()) >= 13 || Integer.parseInt(binding.month.getText().toString()) == 0) {
                Utils.showSnack(binding.parentLayout, getString(R.string.invalid_month));
            }
            return false;
        } else if (binding.year.getText().toString().trim().length() == 0 || Integer.parseInt(binding.year.getText().toString()) > Calendar.getInstance().get(Calendar.YEAR)
                || Integer.parseInt(binding.year.getText().toString()) == 0 || Integer.parseInt(binding.year.getText().toString()) < (Calendar.getInstance().get(Calendar.YEAR) - 100)) {
            if (binding.year.getText().toString().trim().length() == 0) {
                Utils.showSnack(binding.parentLayout, getString(R.string.enter_year));
            } else if (Integer.parseInt(binding.year.getText().toString()) > Calendar.getInstance().get(Calendar.YEAR) || Integer.parseInt(binding.year.getText().toString()) == 0
                    || Integer.parseInt(binding.year.getText().toString()) < (Calendar.getInstance().get(Calendar.YEAR) - 100)) {
                Utils.showSnack(binding.parentLayout, getString(R.string.invalid_year));
            }
            return false;
        }

        return true;
    }

    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


}
