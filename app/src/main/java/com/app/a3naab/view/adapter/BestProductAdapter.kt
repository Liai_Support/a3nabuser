package com.app.a3naab.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.Group
import androidx.recyclerview.widget.RecyclerView
import com.app.a3naab.R
import com.app.a3naab.databinding.ChildBestProductBinding
import com.app.a3naab.interfaces.OnItemClick
import com.app.a3naab.model.DashboardResponse
import com.app.a3naab.utils.DialogUtils
import com.app.a3naab.utils.SharedHelper
import com.app.a3naab.utils.Utils
import com.app.a3naab.utils.openNewTaskActivity
import com.app.a3naab.view.activity.LoginActivity
import com.app.a3naab.view.activity.ProductDetailActivity
import java.util.*

class BestProductAdapter(
    val context: Context,
    var bestProducts: ArrayList<DashboardResponse.BestProduct>,
    var addItem: OnItemClick, var deleteItem: OnItemClick
) :
    RecyclerView.Adapter<BestProductAdapter.BestProductViewHolder>() {

    var sharedHelper: SharedHelper = SharedHelper(context)

    class BestProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildBestProductBinding = ChildBestProductBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestProductViewHolder =
        BestProductViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_best_product, parent, false)
        )


    override fun getItemCount(): Int = bestProducts.size

    override fun onBindViewHolder(holder: BestProductViewHolder, position: Int) {

        if (sharedHelper.selectedLanguage == "ar") {
            holder.binding.productName.text =
                bestProducts[position].arabicName
        } else {
            holder.binding.productName.text =
                bestProducts[position].productName
        }


        if (bestProducts[position].productImages.size != 0)
            Utils.loadImage(
                holder.binding.productImage,
                bestProducts[position].productImages[0].productImage
            )

        if (bestProducts[position].productDiscount == "" || bestProducts[position].productDiscount == "0") {
            holder.binding.discount.visibility = View.INVISIBLE
        } else {
            holder.binding.discount.visibility = View.VISIBLE
        }

        holder.binding.discount.text = bestProducts[position].productDiscount + "%"
        holder.binding.amount.text = bestProducts[position].productPrice
        holder.binding.weight.text =
            bestProducts[position].productWeight + context.getString(R.string.kg)

//        if (isProductAvailable(bestProducts[position].id)) {
//            holder.binding.addToCartText.text = context.getString(R.string.added)
//        } else {
//            holder.binding.addToCartText.text = context.getString(R.string.add_to_cart)
//        }

        fun Group.setAllOnClickListener(listener: View.OnClickListener?) {
            referencedIds.forEach { id ->
                rootView.findViewById<View>(id).setOnClickListener(listener)
            }
        }

        holder.binding.grpAddCart.setAllOnClickListener(
            View.OnClickListener {
                //            setProductAvailable(bestProducts[position].id);
//            plus(position)
                if (sharedHelper.loggedIn) {
                    addItem.onClick(position)
                } else {
                    context.openNewTaskActivity(LoginActivity::class.java)
                }
            }
        )

    /*    holder.binding.addToCartText.setOnClickListener {
//            setProductAvailable(bestProducts[position].id);
//            plus(position)
            if (sharedHelper.loggedIn) {
                addItem.onClick(position)
            } else {
                context.openNewTaskActivity(LoginActivity::class.java)
            }

        }*/

        holder.binding.add.setOnClickListener {
//            setProductAvailable(bestProducts[position].id);
//            plus(position);
            if (bestProducts[position].maxQty > bestProducts[position].count)
                addItem.onClick(position)
            else
                DialogUtils.showMaximumLimit(context)
        }

        holder.binding.minus.setOnClickListener {
//            setProductAvailable(bestProducts[position].id);
//            minus(position);
            deleteItem.onClick(position)
        }

        holder.binding.delete.setOnClickListener {
//            setProductAvailable(bestProducts[position].id);
//            minus(position);
            deleteItem.onClick(position)
        }

        if (SharedHelper(context).selectedLanguage == "en") {
            holder.binding.discount.setBackgroundResource(R.drawable.product_discount_bg)
        } else {
            holder.binding.discount.setBackgroundResource(R.drawable.product_discount_bg_arabic)
        }

        if (bestProducts[position].storeStock != null) {
            if (bestProducts[position].storeStock != 0) {
                holder.binding.disableView.visibility = View.GONE
                holder.binding.outOfStock.visibility = View.GONE
            } else {
                holder.binding.disableView.visibility = View.VISIBLE
                holder.binding.outOfStock.visibility = View.VISIBLE
            }
        } else {

            holder.binding.disableView.visibility = View.VISIBLE
            holder.binding.outOfStock.visibility = View.VISIBLE

        }


        holder.binding.disableView.setOnClickListener {

        }

        holder.binding.viewClick.setOnClickListener {
            context.startActivity(
                Intent(context, ProductDetailActivity::class.java)
                    .putExtra("data", bestProducts[position].id).putExtra("storeid",bestProducts[position].storeId)

            )
        }



        if (bestProducts[position].productDiscount != null && (bestProducts[position].productDiscount != "0") && bestProducts[position].productDiscount != "") {
            holder.binding.oldAmount.visibility = View.VISIBLE
            holder.binding.oldCurrency.visibility = View.VISIBLE
            holder.binding.amount.text =
                (bestProducts[position].productPrice.toDouble() - bestProducts[position].productPrice
                    .toDouble() * bestProducts[position].productDiscount.toDouble() / 100).toString()
            holder.binding.oldAmount.text = bestProducts[position].productPrice
        } else {
            holder.binding.oldAmount.visibility = View.GONE
            holder.binding.oldCurrency.visibility = View.GONE
            holder.binding.amount.text = bestProducts[position].productPrice
        }

        setView(holder, position)

    }

    private fun setView(holder: BestProductViewHolder, position: Int) {

        holder.binding.count.text = bestProducts[position].count.toString()

        if (bestProducts[position].count == 0) {
            holder.binding.viewCart.setBackgroundResource(R.drawable.add_to_cart_bg)
            holder.binding.grpAddCart.visibility = View.VISIBLE

            holder.binding.add.visibility = View.GONE
            holder.binding.count.visibility = View.GONE
            holder.binding.minus.visibility = View.GONE
            holder.binding.delete.visibility = View.GONE
        } else {

            if (bestProducts[position].count == 1) {
                holder.binding.delete.visibility = View.VISIBLE
                holder.binding.minus.visibility = View.GONE
            } else {
                holder.binding.delete.visibility = View.GONE
                holder.binding.minus.visibility = View.VISIBLE
            }

            holder.binding.viewCart.setBackgroundResource(R.drawable.add_to_cart_bg_selected)
            holder.binding.grpAddCart.visibility = View.GONE

            holder.binding.add.visibility = View.VISIBLE
            holder.binding.count.visibility = View.VISIBLE

        }
    }

    private fun minus(id: Int) {

        bestProducts[id].count = bestProducts[id].count - 1
        notifyDataSetChanged()


    }

    private fun plus(id: Int) {

        bestProducts[id].count = bestProducts[id].count + 1
        notifyDataSetChanged()


//        var list = sharedHelper.addedCategory
//        if (isProductAvailable(id)) {
//            list.remove(id.toString())
//        } else {
//            list.add(id.toString())
//        }
//        sharedHelper.addedCategory = list
        notifyDataSetChanged()
    }

    private fun isProductAvailable(id: Int): Boolean {
        return sharedHelper.addedCategory.contains(id.toString())
    }

    fun notifyAddData(position: Int) {
        bestProducts[position].count = bestProducts[position].count + 1
        notifyDataSetChanged()
    }

    fun notifyRemoveData(position: Int) {
        bestProducts[position].count = bestProducts[position].count - 1
        notifyDataSetChanged()
    }
}