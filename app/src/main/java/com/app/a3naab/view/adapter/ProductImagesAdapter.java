package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildAdsHomeBinding;
import com.app.a3naab.databinding.ChildProductImageBinding;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ProductImage;
import com.app.a3naab.utils.Utils;

import java.util.ArrayList;

public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.MyViewHolder> {

    Context context;
    ArrayList<DashboardResponse.ProductImages> bannerData;

    public ProductImagesAdapter(Context context, ArrayList<DashboardResponse.ProductImages> bannerData) {
        this.context = context;
        this.bannerData = bannerData;
    }

    @NonNull
    @Override
    public ProductImagesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_product_image, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ProductImagesAdapter.MyViewHolder holder, int position) {

        Utils.loadImage(holder.binding.ads, bannerData.get(position).getProductImage());
    }

    @Override
    public int getItemCount() {
        return bannerData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildProductImageBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildProductImageBinding.bind(itemView);
        }

    }
}
