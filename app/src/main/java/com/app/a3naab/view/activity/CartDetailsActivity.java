package com.app.a3naab.view.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityCartDetailsBinding;
import com.app.a3naab.databinding.DialogAddressBinding;
import com.app.a3naab.interfaces.AddressSelection;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.CouponResponse;
import com.app.a3naab.model.CurrentLocationIdResponse;
import com.app.a3naab.model.GetProfileResponse;
import com.app.a3naab.model.ListMyAddressResponse;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.AddressListingAdapter;
import com.app.a3naab.view.fragment.ShoppingFragment;
import com.app.a3naab.viewmodel.ShoppingViewModel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

public class CartDetailsActivity extends BaseActivity {

    SharedHelper sharedHelper;
    ActivityCartDetailsBinding binding;

    //address popup
    DialogAddressBinding dialogAddressBinding;
    BottomSheetDialog addressDialog;
    private ArrayList<ListMyAddressResponse.AddressData> addressList = new ArrayList<>();

    ShoppingViewModel shoppingViewModel;

    Double coupondiscountamount=0.0;
    Double availablewalletpointamount=0.0;
    Double availablewalletamount=0.0;
    Double usedwalletpointamount=0.0;
    Double usedwalletamount=0.0;
    Double subtotal=0.0;
    Double tax=0.0;
    Double total=0.0;
    Double deliverycharge=0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_cart_details);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        init();
        initView();
        initValue();
        initListener();
        setPointEnable();
    }

    private void setProgressView() {

        if (CartSessionManager.getInstance().getSelectedId().equalsIgnoreCase("")) {
            binding.imageView25.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.incompleted));
            binding.imageView31.setImageTintList(ContextCompat.getColorStateList(this, R.color.line_color));
        } else {
            binding.imageView25.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
            binding.imageView31.setImageTintList(ContextCompat.getColorStateList(this, R.color.searchcolor_green));
        }

        if (CartSessionManager.getInstance().getSelectedTime() == 0) {
            binding.imageView27.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.incompleted));
            binding.imageView36.setImageTintList(ContextCompat.getColorStateList(this, R.color.line_color));
        } else {
            binding.imageView27.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
            binding.imageView36.setImageTintList(ContextCompat.getColorStateList(this, R.color.searchcolor_green));
        }


        if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("")) {
            binding.imageView33.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.incompleted));
        } else {
            binding.imageView33.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
        }

        changePaymentStatus();

      /*  if (CartSessionManager.getInstance().getPaymentOption() == 0) {
            binding.imageView29.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.incompleted));
            if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
                binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white_ar));
            } else {
                binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white));
            }
            binding.imageView35.setImageTintList(ContextCompat.getColorStateList(this, R.color.line_color));
        }
        else {
            binding.imageView29.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
            if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
                binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_arabic));
            } else {
                binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg));
            }

            binding.imageView35.setImageTintList(ContextCompat.getColorStateList(this, R.color.searchcolor_green));

        }*/
//
//
//        if (!CartSessionManager.getInstance().isEnablePoints()) {
//            binding.imageView37.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.incompleted));
//            binding.imageView43.setImageTintList(ContextCompat.getColorStateList(this, R.color.line_color));
//        } else {
//            binding.imageView37.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
//            binding.imageView43.setImageTintList(ContextCompat.getColorStateList(this, R.color.searchcolor_green));
//        }


    }

    private void setData() {

        binding.deliverTime.setText(CartSessionManager.getInstance().getSelectedTimeStr());


    }

    private void getProfile() {

        shoppingViewModel.getProfileData(this).observe(this, new Observer<GetProfileResponse>() {
            @Override
            public void onChanged(GetProfileResponse getProfileResponse) {
                if (getProfileResponse != null) {
                    if (getProfileResponse.getError()) {
                        Utils.showSnack(binding.parent, getProfileResponse.getMessage());
                    } else {
                        if (getProfileResponse.getData() != null && getProfileResponse.getData().getProfile() != null) {
                            CartSessionManager.getInstance().setPackageValue(getProfileResponse.getData().getProfile().getPackageValue());
                            CartSessionManager.getInstance().setTrustUser(getProfileResponse.getData().getProfile().getTrustUser());

                            CartSessionManager.getInstance().setavailableWalletamount(getProfileResponse.getData().getProfile().getWalletAmount());
                            //CartSessionManager.getInstance().setavailableWalletamount("100");
                            availablewalletamount = Double.parseDouble(CartSessionManager.getInstance().getavailableWalletamount());

                            CartSessionManager.getInstance().setavailableWalletpoint(getProfileResponse.getData().getProfile().getPoints());
                            //CartSessionManager.getInstance().setavailableWalletpoint("1000");

                            binding.walletPoints.setText(getString(R.string.available_points) + " " +CartSessionManager.getInstance().getavailableWalletpoint());
                            Double dsd = Double.parseDouble(sharedHelper.getwalletpointsar())/Double.parseDouble(sharedHelper.getwalletpoint());
                            availablewalletpointamount = dsd*Double.parseDouble(CartSessionManager.getInstance().getavailableWalletpoint());
                        }
                    }
                }
            }
        });
    }

    private void changePaymentStatus() {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white_ar));
            binding.cardLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white_ar));
            binding.machineLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white_ar));


        } else {
            binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white));
            binding.cardLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white));
            binding.machineLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white));
        }

        if (CartSessionManager.getInstance().getPaymentOption() == 2) {
            binding.imageView29.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
            binding.imageView35.setImageTintList(ContextCompat.getColorStateList(this, R.color.searchcolor_green));

            binding.codLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary)));
            binding.cardLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));
            binding.machineLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));

            binding.imageView32.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));
          //  binding.imageView32card.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.edit_text_color)));
            binding.imageView33card.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.edit_text_color)));

            binding.cod.setTextColor(ContextCompat.getColor(this, R.color.white));
            binding.card.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color));
            binding.machine.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color));

        }
        else if (CartSessionManager.getInstance().getPaymentOption() == 1) {
            binding.imageView29.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
            binding.imageView35.setImageTintList(ContextCompat.getColorStateList(this, R.color.searchcolor_green));

            binding.codLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));
            binding.cardLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary)));
            binding.machineLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));

            binding.imageView32.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.edit_text_color)));
          //  binding.imageView32card.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary)));
            binding.imageView33card.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.edit_text_color)));


            binding.cod.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color));
            binding.card.setTextColor(ContextCompat.getColor(this, R.color.white));
            binding.machine.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color));

        }
        else if (CartSessionManager.getInstance().getPaymentOption() == 4) {
            binding.imageView29.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.complete));
            binding.imageView35.setImageTintList(ContextCompat.getColorStateList(this, R.color.searchcolor_green));

            binding.codLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));
            binding.machineLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary)));
            binding.cardLayout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));

            binding.imageView32.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.edit_text_color)));
        //    binding.imageView32card.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.edit_text_color)));
            binding.imageView33card.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white)));


            binding.cod.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color));
            binding.machine.setTextColor(ContextCompat.getColor(this, R.color.white));
            binding.card.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color));

        }
    }


    private void initListener() {
        CartSessionManager.getInstance().setDiscount("0");

        binding.changeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddressDialog();
            }
        });


        binding.codLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CartSessionManager.getInstance().setPaymentOption(2);
                setProgressView();
            }
        });

        binding.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CartSessionManager.getInstance().setPaymentOption(1);
                setProgressView();
            }
        });

        binding.machineLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CartSessionManager.getInstance().setPaymentOption(4);
                setProgressView();
            }
        });


        binding.enableWalletPointImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Double.parseDouble(CartSessionManager.getInstance().getavailableWalletpoint())>0){
                    CartSessionManager.getInstance().setEnablePoints(!CartSessionManager.getInstance().isEnablePoints());
                    setPointEnable();
                }
                else {
                    CartSessionManager.getInstance().setEnablePoints(false);
                    setPointEnable();
                    Utils.showSnack(binding.enableWalletPointImg,getString(R.string.walletpointislow));
                }
            }
        });


        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        binding.notAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtils.showProductNotAvailable(CartDetailsActivity.this, new OnItemClick() {
                    @Override
                    public void onClick(int position) {
                        if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("true")) {
                            binding.notAvailable.setText(getString(R.string.delete_the_item_from_the_shopping_cart));
                        } else if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("false")) {
                            binding.notAvailable.setText(getString(R.string.contact_me_to_introduce_another_product));
                        }
                        setProgressView();
                    }
                });
            }
        });


        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    if(CartSessionManager.getInstance().getIsFastDeliverySelected() == 1) {
                        deliverycharge = Double.parseDouble(sharedHelper.getFastDeliverycharge());
                        subtotal = Double.parseDouble(CartSessionManager.getInstance().getTotalAmount())+Double.parseDouble(CartSessionManager.getInstance().getcbstyleamount());
                        Double vat = Double.parseDouble(CartSessionManager.getInstance().getFlatDiscount());
                        Double subtotal1 = subtotal+deliverycharge;
                        if(coupondiscountamount!=0.0 || CartSessionManager.getInstance().isEnablePoints()==true || availablewalletamount!=0.0){
                            if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount!=0.0){
                                //all 3
                                //subtotal1 = subtotal;
                                Double new1 = subtotal1-coupondiscountamount;
                                if(new1==0){
                                    usedwalletpointamount = new1;
                                    CartSessionManager.getInstance().setEnablePoints(false);
                                    usedwalletamount = new1;
                                    CartSessionManager.getInstance().setwallet(false);
                                    tax = (new1 - (usedwalletamount-usedwalletpointamount)) * (vat / 100);
                                    total = (new1 - (usedwalletamount-usedwalletpointamount)) + tax;
                                }
                                else {
                                    if(new1 <= availablewalletamount){
                                        usedwalletamount = new1;
                                        tax = (new1 - usedwalletamount) * (vat / 100);
                                        total = (new1 - usedwalletamount) + tax;
                                        usedwalletpointamount = new1;
                                        CartSessionManager.getInstance().setEnablePoints(false);
                                    }
                                    else {
                                        Double new2 = new1-availablewalletamount;
                                        usedwalletamount = availablewalletamount;
                                        if(new2==0){
                                            usedwalletpointamount = new2;
                                            CartSessionManager.getInstance().setEnablePoints(false);
                                            tax = (new2 - usedwalletpointamount) * (vat / 100);
                                            total = (new2 - usedwalletpointamount) + tax;
                                        }
                                        else {
                                            if(new2 <= availablewalletpointamount){
                                                usedwalletpointamount = new2;
                                                tax = (new2 - usedwalletpointamount) * (vat / 100);
                                                total = (new2 - usedwalletpointamount) + tax;
                                            }
                                            else {
                                                usedwalletpointamount = availablewalletpointamount;
                                                tax = (new2 - usedwalletpointamount) * (vat / 100);
                                                total = (new2 - usedwalletpointamount) + tax;
                                            }
                                        }
                                    }
                                }
                            }
                            else if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==false && availablewalletamount==0.0){
                                // only coupon
                                //subtotal1 = subtotal;
                                tax = (subtotal1-coupondiscountamount) * (vat /100);
                                total = (subtotal1-coupondiscountamount)+tax;
                            }
                            else if(coupondiscountamount==0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount==0.0){
                                // only point
                                if(subtotal1<=availablewalletpointamount){
                                    usedwalletpointamount = subtotal1;
                                    tax = (0) * (vat / 100);
                                    total = (0) + tax;
                                }
                                else {
                                    usedwalletpointamount = availablewalletpointamount;
                                    tax = (subtotal1 - availablewalletpointamount) * (vat / 100);
                                    total = (subtotal1 - availablewalletpointamount) + tax;
                                }
                            }
                            else if(coupondiscountamount==0.0 && CartSessionManager.getInstance().isEnablePoints()==false && availablewalletamount!=0.0){
                                // only wallet amount
                                if(subtotal1<=availablewalletamount){
                                    usedwalletamount = subtotal1;
                                    tax = (0) * (vat / 100);
                                    total = (0) + tax;
                                }
                                else {
                                    usedwalletamount = availablewalletamount;
                                    tax = (subtotal1 - availablewalletamount) * (vat / 100);
                                    total = (subtotal1 - availablewalletamount) + tax;
                                }
                            }
                            else if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount==0.0){
                                // only coupon + point
                                //subtotal1 = subtotal;
                                Double new1 = subtotal1-coupondiscountamount;
                                if(new1==0){
                                    usedwalletpointamount = new1;
                                    CartSessionManager.getInstance().setEnablePoints(false);
                                    tax = (new1 - usedwalletpointamount) * (vat / 100);
                                    total = (new1 - usedwalletpointamount) + tax;
                                }
                                else {
                                    if (new1 <= availablewalletpointamount) {
                                        usedwalletpointamount = subtotal1;
                                        tax = (new1 - usedwalletpointamount) * (vat / 100);
                                        total = (new1 - usedwalletpointamount) + tax;
                                    } else {
                                        usedwalletpointamount = availablewalletpointamount;
                                        tax = (new1 - usedwalletpointamount) * (vat / 100);
                                        total = (new1 - usedwalletpointamount) + tax;
                                    }
                                }
                            }
                            else if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==false && availablewalletamount!=0.0){
                                // only coupon + wallet amount
                                //subtotal1 = subtotal;
                                Double new1 = subtotal1-coupondiscountamount;
                                if(new1==0){
                                    usedwalletamount = new1;
                                    CartSessionManager.getInstance().setwallet(false);
                                    tax = (new1 - usedwalletamount) * (vat / 100);
                                    total = (new1 - usedwalletamount) + tax;
                                }
                                else {
                                    if (new1 <= availablewalletamount) {
                                        usedwalletamount = subtotal1;
                                        tax = (new1 - usedwalletamount) * (vat / 100);
                                        total = (new1 - usedwalletamount) + tax;
                                    } else {
                                        usedwalletamount = availablewalletamount;
                                        tax = (new1 - usedwalletamount) * (vat / 100);
                                        total = (new1 - usedwalletamount) + tax;
                                    }
                                }
                            }
                            else if(coupondiscountamount==0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount!=0.0){
                                // only point + wallet amount
                                if(subtotal1 <= availablewalletamount){
                                    usedwalletamount = subtotal1;
                                    tax = (subtotal1 - usedwalletamount) * (vat / 100);
                                    total = (subtotal1 - usedwalletamount) + tax;

                                    usedwalletpointamount = 0.0;
                                    CartSessionManager.getInstance().setEnablePoints(false);
                                }
                                else {
                                    Double new1 = subtotal1-availablewalletamount;
                                    usedwalletamount = availablewalletamount;
                                    if(new1 == 0){
                                        usedwalletpointamount = 0.0;
                                        CartSessionManager.getInstance().setEnablePoints(false);
                                        tax = (new1 - usedwalletpointamount) * (vat / 100);
                                        total = (new1 - usedwalletpointamount) + tax;
                                    }
                                    else {
                                        if (new1 <= availablewalletpointamount) {
                                            usedwalletpointamount = new1;
                                            tax = (new1 - usedwalletpointamount) * (vat / 100);
                                            total = (new1 - usedwalletpointamount) + tax;
                                        } else {
                                            usedwalletpointamount = availablewalletpointamount;
                                            tax = (new1 - usedwalletpointamount) * (vat / 100);
                                            total = (new1 - usedwalletpointamount) + tax;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            coupondiscountamount=0.0;
                            usedwalletpointamount=0.0;
                            usedwalletamount=0.0;
                            tax = subtotal1 * (vat /100);
                            total = subtotal1+tax;
                        }

                        Log.d("subtotal - ",""+subtotal);
                        Log.d("delivery - ",""+deliverycharge);
                        Log.d("vat - ",""+vat);
                        Log.d("subtotal1 - ",""+subtotal1);
                        Log.d("tax - ",""+tax);
                        Log.d("total - ",""+total);
                        Log.d("availablewalletpoint - ",""+availablewalletpointamount);
                        Log.d("usedwalletpoint - ",""+usedwalletpointamount);
                        Log.d("availawalletamount - ",""+availablewalletamount);
                        Log.d("usedwalletamount - ",""+usedwalletamount);

                    }
                    else {
                        deliverycharge = Double.parseDouble(sharedHelper.getDeliverycharge());
                        subtotal = Double.parseDouble(CartSessionManager.getInstance().getTotalAmount())+Double.parseDouble(CartSessionManager.getInstance().getcbstyleamount());
                        Double vat = Double.parseDouble(CartSessionManager.getInstance().getFlatDiscount());
                        Double subtotal1 = subtotal+deliverycharge;
                        if(coupondiscountamount!=0.0 || CartSessionManager.getInstance().isEnablePoints()==true || availablewalletamount!=0.0){
                            if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount!=0.0){
                                //all 3
                                //subtotal1 = subtotal;
                                Double new1 = subtotal1-coupondiscountamount;
                                if(new1==0){
                                    usedwalletpointamount = new1;
                                    CartSessionManager.getInstance().setEnablePoints(false);
                                    usedwalletamount = new1;
                                    CartSessionManager.getInstance().setwallet(false);
                                    tax = (new1 - (usedwalletamount-usedwalletpointamount)) * (vat / 100);
                                    total = (new1 - (usedwalletamount-usedwalletpointamount)) + tax;
                                }
                                else {
                                    if(new1 <= availablewalletamount){
                                        usedwalletamount = new1;
                                        tax = (new1 - usedwalletamount) * (vat / 100);
                                        total = (new1 - usedwalletamount) + tax;
                                        usedwalletpointamount = new1;
                                        CartSessionManager.getInstance().setEnablePoints(false);
                                    }
                                    else {
                                        Double new2 = new1-availablewalletamount;
                                        usedwalletamount = availablewalletamount;
                                        if(new2==0){
                                            usedwalletpointamount = new2;
                                            CartSessionManager.getInstance().setEnablePoints(false);
                                            tax = (new2 - usedwalletpointamount) * (vat / 100);
                                            total = (new2 - usedwalletpointamount) + tax;
                                        }
                                        else {
                                            if(new2 <= availablewalletpointamount){
                                                usedwalletpointamount = new2;
                                                tax = (new2 - usedwalletpointamount) * (vat / 100);
                                                total = (new2 - usedwalletpointamount) + tax;
                                            }
                                            else {
                                                usedwalletpointamount = availablewalletpointamount;
                                                tax = (new2 - usedwalletpointamount) * (vat / 100);
                                                total = (new2 - usedwalletpointamount) + tax;
                                            }
                                        }
                                    }
                                }
                            }
                            else if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==false && availablewalletamount==0.0){
                                // only coupon
                                //subtotal1 = subtotal;
                                tax = (subtotal1-coupondiscountamount) * (vat /100);
                                total = (subtotal1-coupondiscountamount)+tax;
                            }
                            else if(coupondiscountamount==0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount==0.0){
                                // only point
                                if(subtotal1<=availablewalletpointamount){
                                    usedwalletpointamount = subtotal1;
                                    tax = (0) * (vat / 100);
                                    total = (0) + tax;
                                }
                                else {
                                    usedwalletpointamount = availablewalletpointamount;
                                    tax = (subtotal1 - availablewalletpointamount) * (vat / 100);
                                    total = (subtotal1 - availablewalletpointamount) + tax;
                                }
                            }
                            else if(coupondiscountamount==0.0 && CartSessionManager.getInstance().isEnablePoints()==false && availablewalletamount!=0.0){
                                // only wallet amount
                                if(subtotal1<=availablewalletamount){
                                    usedwalletamount = subtotal1;
                                    tax = (0) * (vat / 100);
                                    total = (0) + tax;
                                }
                                else {
                                    usedwalletamount = availablewalletamount;
                                    tax = (subtotal1 - availablewalletamount) * (vat / 100);
                                    total = (subtotal1 - availablewalletamount) + tax;
                                }
                            }
                            else if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount==0.0){
                                // only coupon + point
                                //subtotal1 = subtotal;
                                Double new1 = subtotal1-coupondiscountamount;
                                if(new1==0){
                                    usedwalletpointamount = new1;
                                    CartSessionManager.getInstance().setEnablePoints(false);
                                    tax = (new1 - usedwalletpointamount) * (vat / 100);
                                    total = (new1 - usedwalletpointamount) + tax;
                                }
                                else {
                                    if (new1 <= availablewalletpointamount) {
                                        usedwalletpointamount = subtotal1;
                                        tax = (new1 - usedwalletpointamount) * (vat / 100);
                                        total = (new1 - usedwalletpointamount) + tax;
                                    } else {
                                        usedwalletpointamount = availablewalletpointamount;
                                        tax = (new1 - usedwalletpointamount) * (vat / 100);
                                        total = (new1 - usedwalletpointamount) + tax;
                                    }
                                }
                            }
                            else if(coupondiscountamount!=0.0 && CartSessionManager.getInstance().isEnablePoints()==false && availablewalletamount!=0.0){
                                // only coupon + wallet amount
                                //subtotal1 = subtotal;
                                Double new1 = subtotal1-coupondiscountamount;
                                if(new1==0){
                                    usedwalletamount = new1;
                                    CartSessionManager.getInstance().setwallet(false);
                                    tax = (new1 - usedwalletamount) * (vat / 100);
                                    total = (new1 - usedwalletamount) + tax;
                                }
                                else {
                                    if (new1 <= availablewalletamount) {
                                        usedwalletamount = subtotal1;
                                        tax = (new1 - usedwalletamount) * (vat / 100);
                                        total = (new1 - usedwalletamount) + tax;
                                    } else {
                                        usedwalletamount = availablewalletamount;
                                        tax = (new1 - usedwalletamount) * (vat / 100);
                                        total = (new1 - usedwalletamount) + tax;
                                    }
                                }
                            }
                            else if(coupondiscountamount==0.0 && CartSessionManager.getInstance().isEnablePoints()==true && availablewalletamount!=0.0){
                                // only point + wallet amount
                                if(subtotal1 <= availablewalletamount){
                                    usedwalletamount = subtotal1;
                                    tax = (subtotal1 - usedwalletamount) * (vat / 100);
                                    total = (subtotal1 - usedwalletamount) + tax;

                                    usedwalletpointamount = 0.0;
                                    CartSessionManager.getInstance().setEnablePoints(false);
                                }
                                else {
                                    Double new1 = subtotal1-availablewalletamount;
                                    usedwalletamount = availablewalletamount;
                                    if(new1 == 0){
                                        usedwalletpointamount = 0.0;
                                        CartSessionManager.getInstance().setEnablePoints(false);
                                        tax = (new1 - usedwalletpointamount) * (vat / 100);
                                        total = (new1 - usedwalletpointamount) + tax;
                                    }
                                    else {
                                        if (new1 <= availablewalletpointamount) {
                                            usedwalletpointamount = new1;
                                            tax = (new1 - usedwalletpointamount) * (vat / 100);
                                            total = (new1 - usedwalletpointamount) + tax;
                                        } else {
                                            usedwalletpointamount = availablewalletpointamount;
                                            tax = (new1 - usedwalletpointamount) * (vat / 100);
                                            total = (new1 - usedwalletpointamount) + tax;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            coupondiscountamount=0.0;
                            usedwalletpointamount=0.0;
                            usedwalletamount=0.0;
                            tax = subtotal1 * (vat /100);
                            total = subtotal1+tax;
                        }

                        Log.d("subtotal - ",""+subtotal);
                        Log.d("delivery - ",""+deliverycharge);
                        Log.d("vat - ",""+vat);
                        Log.d("subtotal1 - ",""+subtotal1);
                        Log.d("tax - ",""+tax);
                        Log.d("total - ",""+total);
                        Log.d("availablewalletpoint - ",""+availablewalletpointamount);
                        Log.d("usedwalletpoint - ",""+usedwalletpointamount);
                        Log.d("availawalletamount - ",""+availablewalletamount);
                        Log.d("usedwalletamount - ",""+usedwalletamount);

                    }

                    startActivity(new Intent(CartDetailsActivity.this, OrderBillDetails.class)
                            .putExtra("subtotal",String.format(Locale.ENGLISH,"%.2f",subtotal))
                            .putExtra("dcharge",String.format(Locale.ENGLISH,"%.2f",deliverycharge))
                            .putExtra("vat",String.valueOf(CartSessionManager.getInstance().getFlatDiscount()))
                            .putExtra("tax",String.format(Locale.ENGLISH,"%.2f",tax))
                            .putExtra("usedcoupondiscount",String.format(Locale.ENGLISH,"%.2f",coupondiscountamount))
                            .putExtra("usedwalletpoint",String.format(Locale.ENGLISH,"%.2f",usedwalletpointamount))
                            .putExtra("usedwalletamount",String.format(Locale.ENGLISH,"%.2f",usedwalletamount))
                            .putExtra("total",String.format(Locale.ENGLISH,"%.2f",total))
                    );
                }
            }
        });


        binding.deliveryTimeChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CartDetailsActivity.this, DeliveryTimeSelectionActivity.class));
            }
        });

        binding.deliverTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CartDetailsActivity.this, DeliveryTimeSelectionActivity.class));
            }
        });

        binding.addCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!binding.coupon.getText().toString().equalsIgnoreCase("")) {
                    DialogUtils.showLoader(CartDetailsActivity.this);
                    shoppingViewModel.checkCouponCode(CartDetailsActivity.this, binding.coupon.getText().toString()).observe(CartDetailsActivity.this, new Observer<CouponResponse>() {
                        @Override
                        public void onChanged(CouponResponse commonResponse) {
                            DialogUtils.dismissLoader();
                            if (!commonResponse.getError()) {
                                if(commonResponse.getData().getCouponcode().getMinimumValue() != null){
                                    if(commonResponse.getData().getCouponcode().getUptoAmount() != null){
                                        if(Double.parseDouble(CartSessionManager.getInstance().getTotalAmount()) > Double.parseDouble(commonResponse.getData().getCouponcode().getMinimumValue())){
                                            if(commonResponse.getData().getCouponcode().getOff_types().equalsIgnoreCase("0")){
                                                if(CartSessionManager.getInstance().getIsFastDeliverySelected() == 1){
                                                    coupondiscountamount = Double.parseDouble(sharedHelper.getFastDeliverycharge());
                                                    CartSessionManager.getInstance().setDiscount(""+sharedHelper.getFastDeliverycharge());
                                                    CartSessionManager.getInstance().setOfferType("0");
                                                    CartSessionManager.getInstance().setIsFreeDeliverySelected(1);
                                                    //sharedHelper.setFastDeliverycharge("0");
                                                }
                                                else {
                                                    coupondiscountamount = Double.parseDouble(sharedHelper.getDeliverycharge());
                                                    CartSessionManager.getInstance().setDiscount(""+sharedHelper.getDeliverycharge());
                                                    CartSessionManager.getInstance().setIsFreeDeliverySelected(1);
                                                    CartSessionManager.getInstance().setOfferType("0");
                                                    //sharedHelper.setDeliverycharge("0");
                                                }
                                            }
                                            else {
                                                Double dis = Double.parseDouble(commonResponse.getData().getCouponcode().getDiscount());
                                                Double sar = (dis/100)*Double.parseDouble(CartSessionManager.getInstance().getTotalAmount());
                                                if(sar <= Double.parseDouble(commonResponse.getData().getCouponcode().getUptoAmount())){
                                                    CartSessionManager.getInstance().setDiscount(String.format(Locale.ENGLISH,"%.2f",sar));
                                                    coupondiscountamount = sar;
                                                    CartSessionManager.getInstance().setIsFreeDeliverySelected(0);
                                                    CartSessionManager.getInstance().setOfferType("1");
                                                    Log.d("xgcxvc1",""+dis+"==="+sar);
                                                }
                                                else {
                                                    CartSessionManager.getInstance().setDiscount(commonResponse.getData().getCouponcode().getUptoAmount());
                                                    coupondiscountamount = Double.parseDouble(commonResponse.getData().getCouponcode().getUptoAmount());
                                                    CartSessionManager.getInstance().setIsFreeDeliverySelected(0);
                                                    CartSessionManager.getInstance().setOfferType("1");
                                                    Log.d("xgcxvc2",""+dis+"==="+sar);
                                                }
                                            }
                                            CartSessionManager.getInstance().setCouponCode(binding.coupon.getText().toString());
                                            DialogUtils.showCouponApplied(CartDetailsActivity.this, true,"");
                                        }
                                        else {
                                            DialogUtils.showCouponApplied(CartDetailsActivity.this, false,getString(R.string.minimum_order_nvalue_required_toast));
                                            CartSessionManager.getInstance().setDiscount("0");
                                            CartSessionManager.getInstance().setCouponCode("");
                                            coupondiscountamount = 0.0;
                                            //  Utils.showSnack(binding.addCoupon,getString(R.string.productamountislessthandiscountamount));
                                        }
                                    }
                                    else {
                                        DialogUtils.showCouponApplied(CartDetailsActivity.this, false,getString(R.string.coupon_code_has_been_added_unsuccessfully));
                                        CartSessionManager.getInstance().setDiscount("0");
                                        CartSessionManager.getInstance().setCouponCode("");
                                        coupondiscountamount = 0.0;
                                        //  Utils.showSnack(binding.addCoupon,getString(R.string.productamountislessthandiscountamount));
                                    }
                                }
                                else {
                                    DialogUtils.showCouponApplied(CartDetailsActivity.this, false,getString(R.string.coupon_code_has_been_added_unsuccessfully));
                                    CartSessionManager.getInstance().setDiscount("0");
                                    CartSessionManager.getInstance().setCouponCode("");
                                    coupondiscountamount = 0.0;
                                }
                            }
                            else {
                                DialogUtils.showCouponApplied(CartDetailsActivity.this, false,commonResponse.getMessage());
                                CartSessionManager.getInstance().setDiscount("0");
                                CartSessionManager.getInstance().setCouponCode("");
                                coupondiscountamount = 0.0;
                            }
                            setProgressView();
                        }
                    });
                } else {
                    Utils.showSnack(view, getString(R.string.entervalue));
                }
            }
        });


    }

    private void setPointEnable() {

        if (CartSessionManager.getInstance().isEnablePoints()) {
            binding.enableWalletPointImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_on));
        } else {
            usedwalletpointamount = 0.0;
            binding.enableWalletPointImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_off));
        }

        setProgressView();

    }

    private boolean isValid() {

        if (CartSessionManager.getInstance().getSelectedTime() == 0) {
            Utils.showSnack(binding.parent, getString(R.string.select_time));
            return false;
        } else if (CartSessionManager.getInstance().getSelectedDate().equalsIgnoreCase("")) {
            Utils.showSnack(binding.parent, getString(R.string.select_date));
            return false;
        } else if (CartSessionManager.getInstance().getSelectedId().equalsIgnoreCase("")) {
            Utils.showSnack(binding.parent, getString(R.string.select_address));
            return false;
        } else if (CartSessionManager.getInstance().getPaymentOption() == 0) {
            Utils.showSnack(binding.parent, getString(R.string.select_payment));
            return false;
        } else if (CartSessionManager.getInstance().getDeleteProductNotAvailable().equalsIgnoreCase("")) {
            Utils.showSnack(binding.parent, getString(R.string.select_not_available));
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfile();
        setData();
        getAddressList();
        setProgressView();
    }

    private void initView() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        dialogAddressBinding = DataBindingUtil.inflate(
                LayoutInflater.from(this),
                R.layout.dialog_address,
                null,
                false);

        addressDialog = DialogUtils.getAddressDialog(this, dialogAddressBinding);

    }

    private void init() {
        sharedHelper = new SharedHelper(this);

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.imageView24.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.cart_top_2));
            binding.view11.setBackground(ContextCompat.getDrawable(this, R.drawable.card_field_bg_arabic));
            binding.view12.setBackground(ContextCompat.getDrawable(this, R.drawable.card_field_bg_arabic));
            binding.view13.setBackground(ContextCompat.getDrawable(this, R.drawable.card_field_bg_arabic));
            binding.view14.setBackground(ContextCompat.getDrawable(this, R.drawable.card_field_bg_arabic));
            binding.view15.setBackground(ContextCompat.getDrawable(this, R.drawable.card_field_bg_arabic));
            binding.view17.setBackground(ContextCompat.getDrawable(this, R.drawable.card_field_bg_arabic));
        }

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white_ar));
            binding.cardLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white_ar));

        } else {
            binding.codLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white));
            binding.cardLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.cart_cod_bg_white));
        }

    }

    private void initValue() {

        Log.d("cjvcmccx",""+sharedHelper.getSelectedDeliveryToId());
        Log.d("cjvhfvghcmccx",""+sharedHelper.getSelectedDeliveryToAddress());
        Log.d("cjvcersmccx",""+sharedHelper.getSelectedDeliveryToLat());
        Log.d("cjvctemccx",""+sharedHelper.getSelectedDeliveryToLat());

        if( CartSessionManager.getInstance().getReorder() == true){
            binding.address.setText(CartSessionManager.getInstance().getSelectedAddress());
        }
        else {
            if (CartSessionManager.getInstance().getSelectedAddress().equalsIgnoreCase("")) {
                if (!sharedHelper.getSelectedDeliveryToId().equals("")) {
                    binding.address.setText(sharedHelper.getSelectedDeliveryToAddress());

                    CartSessionManager.getInstance().setSelectedAddress(sharedHelper.getSelectedDeliveryToAddress());
                    if(sharedHelper.getSelectedDeliveryToTag().equals("")){
                        CartSessionManager.getInstance().setSelectedAddressType(getString(R.string.current_location));
                    }
                    else {
                        CartSessionManager.getInstance().setSelectedAddressType(sharedHelper.getSelectedDeliveryToTag());
                    }
                    CartSessionManager.getInstance().setSelectedLat(sharedHelper.getSelectedDeliveryToLat());
                    CartSessionManager.getInstance().setSelectedLng(sharedHelper.getSelectedDeliveryToLng());
                    CartSessionManager.getInstance().setSelectedId(sharedHelper.getSelectedDeliveryToId());
                }

            } else {
                binding.address.setText(CartSessionManager.getInstance().getSelectedAddress());
            }
        }



    }

    private void showAddressDialog() {

        dialogAddressBinding.addressView.setLayoutManager(new LinearLayoutManager(this));
        dialogAddressBinding.addressView.setAdapter(new AddressListingAdapter(false,new ShoppingFragment(), this, addressList, new AddressSelection() {
            @Override
            public void addressSelected(int position) {

                CartSessionManager.getInstance().setSelectedAddress(addressList.get(position).getAddressPinDetails());
                CartSessionManager.getInstance().setSelectedLat(addressList.get(position).getLatitude());
                CartSessionManager.getInstance().setSelectedLng(addressList.get(position).getLongitude());
                CartSessionManager.getInstance().setSelectedId(String.valueOf(addressList.get(position).getId()));
                binding.address.setText(addressList.get(position).getAddressPinDetails());

                addressDialog.dismiss();
                setProgressView();
            }

            @Override
            public void addressEdit(int position) {

                addressDialog.dismiss();

                Intent intent = new Intent(CartDetailsActivity.this, EditAddressActivity.class);
                intent.putExtra("data", (Parcelable) addressList.get(position));
                startActivity(intent);

            }

            @Override
            public void addressDelete(int position) {


            }
        }, false));


        dialogAddressBinding.currentLocationAddress.setText(sharedHelper.getLastKnownAddress());

        if (addressList.size() == 0) {
            dialogAddressBinding.addressView.setVisibility(View.GONE);
        } else {
            dialogAddressBinding.addressView.setVisibility(View.VISIBLE);
        }


        dialogAddressBinding.viewMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myLocationClicked();


            }
        });

        dialogAddressBinding.otherLocationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addressDialog.dismiss();

                startActivity(new Intent(CartDetailsActivity.this, SelectStartAddressActivity.class));
            }
        });

        addressDialog.show();
    }


    private void myLocationClicked() {

        DialogUtils.showLoader(this);
        shoppingViewModel.getCurrentAddressId(this, sharedHelper.getLastKnownAddress(),
                sharedHelper.getLastKnownLat(), sharedHelper.getLastKnownLng()).observe(this, new Observer<CurrentLocationIdResponse>() {
            @Override
            public void onChanged(CurrentLocationIdResponse currentLocationIdResponse) {
                DialogUtils.dismissLoader();
                if (currentLocationIdResponse.getError()) {

                } else {
                    if (currentLocationIdResponse.getData().getAddressId() != null && !currentLocationIdResponse.getData().getAddressId().equalsIgnoreCase("")) {
                        CartSessionManager.getInstance().setSelectedId(String.valueOf(currentLocationIdResponse.getData().getAddressId()));

                        CartSessionManager.getInstance().setSelectedAddress(sharedHelper.getLastKnownAddress());
                        CartSessionManager.getInstance().setSelectedLat(sharedHelper.getLastKnownLat());
                        CartSessionManager.getInstance().setSelectedLng(sharedHelper.getLastKnownLng());

                        binding.address.setText(sharedHelper.getLastKnownAddress());
                        addressDialog.dismiss();

                        setProgressView();
                    }
                }
            }
        });

    }


    private void getAddressList() {

        shoppingViewModel.getAddressList(this).observe(this, listMyAddressResponse -> {

            if (!listMyAddressResponse.getError()) {
                addressList = listMyAddressResponse.getAddressData();
//                setAddress();
            }
        });
    }


}
