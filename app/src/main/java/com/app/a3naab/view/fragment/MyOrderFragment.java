package com.app.a3naab.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.FragmentMyOrderBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.OrderListResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.activity.ViewActiveOrdersActivity;
import com.app.a3naab.view.activity.ViewPastOrdersActivity;
import com.app.a3naab.view.adapter.ActiveOrderAdapter;
import com.app.a3naab.view.adapter.PastOrderAdapter;
import com.app.a3naab.viewmodel.ShoppingViewModel;

public class MyOrderFragment extends Fragment {

    FragmentMyOrderBinding binding;

    public ShoppingViewModel shoppingViewModel;

    ActiveOrderAdapter activeAdapter;
    PastOrderAdapter pastOrderAdapter;
    public String adminno="";
    public SharedHelper sharedHelper;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_my_order, container, false);
        binding = FragmentMyOrderBinding.bind(layout);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        sharedHelper = new SharedHelper(getContext());


        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActiveOrders();
    }


    private void getPastorders() {

        shoppingViewModel.getPastOrders(requireContext()).observe(getViewLifecycleOwner(), new Observer<OrderListResponse>() {
            @Override
            public void onChanged(OrderListResponse orderListResponse) {
                DialogUtils.dismissLoader();
                if (!orderListResponse.getError()) {

                    if (orderListResponse.getData().getOrderList().size() == 0) {
                        binding.textView39.setVisibility(View.GONE);
                        binding.pastOrderView.setVisibility(View.GONE);
                    } else {
                        binding.textView39.setVisibility(View.VISIBLE);
                        binding.pastOrderView.setVisibility(View.VISIBLE);
                    }

                    pastOrderAdapter = new PastOrderAdapter(MyOrderFragment.this,requireContext(), orderListResponse.getData().getOrderList(),orderListResponse.getData().getTax(), new OnItemClick() {
                        @Override
                        public void onClick(int position) {
                            startActivity(new Intent(requireContext(), ViewPastOrdersActivity.class).putExtra("orderId", orderListResponse.getData().getOrderList().get(position).getId()));
                        }
                    });
                    binding.pastOrderView.setLayoutManager(new LinearLayoutManager(requireContext()));
                    binding.pastOrderView.setAdapter(pastOrderAdapter);

                }
            }
        });

    }

    private void getActiveOrders() {

        DialogUtils.showLoader(requireContext());
        shoppingViewModel.getActiveOrders(requireContext()).observe(getViewLifecycleOwner(), new Observer<OrderListResponse>() {
            @Override
            public void onChanged(OrderListResponse orderListResponse) {
                getPastorders();
                if (!orderListResponse.getError()) {
                    Log.d("hch",""+orderListResponse.getData().getSettings().getQuickDelivery());
                    if(orderListResponse.getData().getSettings().getQuickDelivery().equals("true")){
                        sharedHelper.setIsFastDelivery("true");
                    }
                    else {
                        sharedHelper.setIsFastDelivery("false");
                    }
                    if (orderListResponse.getData().getOrderList().size() == 0) {
                        binding.textView38.setVisibility(View.GONE);
                        binding.recyclerView2.setVisibility(View.GONE);
                    } else {
                        binding.textView38.setVisibility(View.VISIBLE);
                        binding.recyclerView2.setVisibility(View.VISIBLE);
                    }
                    adminno = orderListResponse.getData().getAdminNumber();
                    activeAdapter = new ActiveOrderAdapter(MyOrderFragment.this,requireContext(), orderListResponse.getData().getOrderList(), new OnItemClick() {
                        @Override
                        public void onClick(int position) {

                            startActivity(new Intent(requireContext(), ViewActiveOrdersActivity.class)
                                    .putExtra("orderId", orderListResponse.getData().getOrderList().get(position).getId())
                                    .putExtra("adminno",adminno)
                                    .putExtra("driverno",orderListResponse.getData().getOrderList().get(position).getDriverNumber())
                            );
                        }
                    });
                    binding.recyclerView2.setLayoutManager(new LinearLayoutManager(requireContext()));
                    binding.recyclerView2.setAdapter(activeAdapter);
                }
            }
        });


    }

}
