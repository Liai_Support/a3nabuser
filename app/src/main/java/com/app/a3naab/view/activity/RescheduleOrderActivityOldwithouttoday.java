package com.app.a3naab.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivitySelectDeliveryDateBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DateSlotModel;
import com.app.a3naab.model.DeliveryTimeResponse;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.DateSlotAdapter;
import com.app.a3naab.view.adapter.TimeSlotAdapter;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.util.ArrayList;
import java.util.Calendar;

public class RescheduleOrderActivityOldwithouttoday extends BaseActivity {

    ArrayList<DateSlotModel> dateList = new ArrayList<>();
    ActivitySelectDeliveryDateBinding binding;
    Boolean isFastDeliveryEnabled = false;
    SharedHelper sharedHelper;

    ShoppingViewModel viewModel;

    public int deliveryTimeId = 0;
    public String selectedtime = "";
    String deliveryDate = "";
    int selectedDayId = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_delivery_date);
        sharedHelper = new SharedHelper(this);
        viewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        setDateList();
        initListener();

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }

        setFastDelivery();

    }

    private void setFastDelivery() {

        binding.group.setVisibility(View.GONE);
        isFastDeliveryEnabled = false;
        binding.view16.setBackgroundColor(ContextCompat.getColor(RescheduleOrderActivityOldwithouttoday.this, R.color.white));
        binding.deliveryText.setTextColor(ContextCompat.getColor(RescheduleOrderActivityOldwithouttoday.this, R.color.header_text_color));
        binding.amount.setTextColor(ContextCompat.getColor(RescheduleOrderActivityOldwithouttoday.this, R.color.header_text_color));
        binding.amountSAR.setTextColor(ContextCompat.getColor(RescheduleOrderActivityOldwithouttoday.this, R.color.header_text_color));


    }


    private void getDeliveryData() {

        DialogUtils.showLoader(this);
        viewModel.getDeliverydate(this, selectedDayId,deliveryDate).observe(this, new Observer<DeliveryTimeResponse>() {
            @Override
            public void onChanged(DeliveryTimeResponse deliveryTimeResponse) {

                DialogUtils.dismissLoader();
                if (!deliveryTimeResponse.getError()) {

                    binding.timeSlotView.setLayoutManager(new GridLayoutManager(RescheduleOrderActivityOldwithouttoday.this, 2));
                    TimeSlotAdapter adapter = new TimeSlotAdapter(RescheduleOrderActivityOldwithouttoday.this, deliveryTimeResponse.getData().getList(), new OnItemClick() {
                        @Override
                        public void onClick(int position) {

                            deliveryTimeId = deliveryTimeResponse.getData().getList().get(position).getId();
                            selectedtime = Utils.convertDate(deliveryTimeResponse.getData().getList().get(position).getFromTime(), "HH:mm:ss", "hh:mm a")
                                    + "-" +
                                    Utils.convertDate(deliveryTimeResponse.getData().getList().get(position).getToTime(), "HH:mm:ss", "hh:mm a");

                        }
                    }, false);
                    binding.timeSlotView.setAdapter(adapter);

                }

            }
        });
    }

    private void initListener() {

        if(sharedHelper.getIsFastDelivery().equals("true")){
            binding.group.setVisibility(View.VISIBLE);
            Log.d("c,xcnvmnvxc","djjvd");
        }
        else {
            binding.group.setVisibility(View.GONE);
            Log.d("c,xcnvmnvxdkvdkc","djjvd");
        }

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.view16.setOnClickListener(view -> {

            if (isFastDeliveryEnabled) {

                isFastDeliveryEnabled = false;
                binding.view16.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
                binding.deliveryText.setTextColor(ContextCompat.getColor(this, R.color.header_text_color));
                binding.amount.setTextColor(ContextCompat.getColor(this, R.color.header_text_color));
                binding.amountSAR.setTextColor(ContextCompat.getColor(this, R.color.header_text_color));

            } else {

                isFastDeliveryEnabled = true;
                binding.view16.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_corner_bg_primary));
                binding.deliveryText.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_red));
                binding.amount.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_red));
                binding.amountSAR.setTextColor(ContextCompat.getColor(this, R.color.searchcolor_red));

            }
        });


        binding.next.setOnClickListener(view -> {
            if (deliveryTimeId == 0) {
                Utils.showSnack(binding.parent, getString(R.string.select_time));
            } else if (deliveryDate.equalsIgnoreCase("")) {
                Utils.showSnack(binding.parent, getString(R.string.select_date));
            } else {

                CartSessionManager.getInstance().setSelectedDate(deliveryDate);
                CartSessionManager.getInstance().setSelectedTime(deliveryTimeId);
                CartSessionManager.getInstance().setSelectedTimeStr(selectedtime);

                if (isFastDeliveryEnabled) {
                    CartSessionManager.getInstance().setIsFastDeliverySelected(1);
                } else {
                    CartSessionManager.getInstance().setIsFastDeliverySelected(0);
                }

                DialogUtils.showLoader(this);
                viewModel.rescheduelOrder(RescheduleOrderActivityOldwithouttoday.this).observe(this, new Observer<CommonResponse>() {
                    @Override
                    public void onChanged(CommonResponse commonResponse) {
                        DialogUtils.dismissLoader();
                        if (commonResponse.getError()) {
                            Utils.showSnack(binding.parent, commonResponse.getMessage());
                        } else {
                            CartSessionManager.removeAllValues();
                            DialogUtils.showOrderSuccessDialog(RescheduleOrderActivityOldwithouttoday.this, new DialogCallback() {
                                @Override
                                public void onPositiveClick() {

                                    Intent intent = new Intent(RescheduleOrderActivityOldwithouttoday.this, DashboardActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);

                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });
                        }
                    }
                });


            }
        });
    }

    private void setDateList() {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        for (int i = 0; i < 30; i++) {

            DateSlotModel model = new DateSlotModel();
            model.setOriginalDate(Utils.convertDate(calendar.getTime(), "yyyy-MM-dd"));


            if (i == 0) {
                model.setDisplayDay(getString(R.string.tomorrow));
            } else {
                model.setDisplayDay(Utils.convertDate(calendar.getTime(), "EEEE"));
            }


            if (Utils.convertDate(calendar.getTime(), "EEEE").equalsIgnoreCase("Monday")) {
                model.setDisplayDay(getString(R.string.monday));
                model.setGeneratedDayId(1);
            } else if (Utils.convertDate(calendar.getTime(), "EEEE").equalsIgnoreCase("Tuesday")) {
                model.setDisplayDay(getString(R.string.tuesday));
                model.setGeneratedDayId(2);
            } else if (Utils.convertDate(calendar.getTime(), "EEEE").equalsIgnoreCase("Wednesday")) {
                model.setDisplayDay(getString(R.string.wednesday));
                model.setGeneratedDayId(3);
            } else if (Utils.convertDate(calendar.getTime(), "EEEE").equalsIgnoreCase("Thursday")) {
                model.setDisplayDay(getString(R.string.thursday));
                model.setGeneratedDayId(4);
            } else if (Utils.convertDate(calendar.getTime(), "EEEE").equalsIgnoreCase("Friday")) {
                model.setDisplayDay(getString(R.string.friday));
                model.setGeneratedDayId(5);
            } else if (Utils.convertDate(calendar.getTime(), "EEEE").equalsIgnoreCase("Saturday")) {
                model.setDisplayDay(getString(R.string.saturday));
                model.setGeneratedDayId(6);
            } else if (Utils.convertDate(calendar.getTime(), "EEEE").equalsIgnoreCase("Sunday")) {
                model.setDisplayDay(getString(R.string.sunday));
                model.setGeneratedDayId(7);
            }

            model.setDisplayDate(Utils.convertDate(calendar.getTime(), "dd/MM"));

            dateList.add(model);

            calendar.add(Calendar.DATE, 1);
        }

        binding.timeSlot.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        DateSlotAdapter adapter = new DateSlotAdapter(this, dateList, new OnItemClick() {
            @Override
            public void onClick(int position) {
                deliveryDate = dateList.get(position).getOriginalDate();
                selectedDayId = dateList.get(position).getGeneratedDayId();

                getDeliveryData();
            }
        });
        binding.timeSlot.setAdapter(adapter);

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            adapter.setSelected(0);
        }, 500);
    }
}
