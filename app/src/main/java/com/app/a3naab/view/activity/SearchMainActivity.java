package com.app.a3naab.view.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityMainSearchBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.SearchHomeAdapter;
import com.app.a3naab.viewmodel.CategoryViewModel;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.util.ArrayList;

public class SearchMainActivity extends BaseActivity {

    ActivityMainSearchBinding binding;
    ShoppingViewModel shoppingViewModel;
    CategoryViewModel categoryViewModel;

    ArrayList<DashboardResponse.Categories> list = new ArrayList<>();
    SearchHomeAdapter adapter;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_search);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        categoryViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        sharedHelper = new SharedHelper(this);
        initView();
        initAdapter();
        setRecentList();
    }

    private void initAdapter() {

        adapter = new SearchHomeAdapter(this, list);
        binding.searchResultView.setLayoutManager(new LinearLayoutManager(this));
        binding.searchResultView.setAdapter(adapter);
    }

    private void initView() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        binding.addRequest.setOnClickListener(view -> {
            DialogUtils.showLoader(this);
            categoryViewModel.requestTypingText(this, "maincategory", binding.searchText.getText().toString()).observe(this, new Observer<CommonResponse>() {
                @Override
                public void onChanged(CommonResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError())
                        Utils.showSnack(binding.parentLayout, commonResponse.getMessage());
                    else {
                        DialogUtils.showOrderSuccessDialog(SearchMainActivity.this, new DialogCallback() {
                            @Override
                            public void onPositiveClick() {
                                finish();
                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });
                    }
                }
            });
        });


        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().equals("")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.requestGroup.setVisibility(View.GONE);
                            setRecentList();
                        }
                    }, 500);
                } else {
                    searchText(editable.toString());
                }

            }
        });

    }

    private void searchText(String text) {

        shoppingViewModel.getDashboardSearch(this, text).observe(this, dashboardSearchResponse -> {

            if (!dashboardSearchResponse.isError()) {
                if (dashboardSearchResponse.getData().size() == 0) {
                    binding.requestGroup.setVisibility(View.VISIBLE);
                    adapter.setValues(new ArrayList<>());

                    binding.errorMessage.setText(getString(R.string.ooo_couldn_t_find_products_that_contains_the_word) + "\n" + text);

                } else {
                    binding.requestGroup.setVisibility(View.GONE);
                    adapter.setValues(dashboardSearchResponse.getData());
                }

            }
        });

    }


    private void setRecentList() {

        adapter = new SearchHomeAdapter(this, sharedHelper.getMainCategorySearch());
        binding.searchResultView.setLayoutManager(new LinearLayoutManager(this));
        binding.searchResultView.setAdapter(adapter);

    }

}
