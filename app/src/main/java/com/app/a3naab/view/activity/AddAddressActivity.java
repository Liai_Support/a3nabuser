package com.app.a3naab.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityAddAddressBinding;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.utils.Constants;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.ShoppingViewModel;

public class AddAddressActivity extends BaseActivity {

    ActivityAddAddressBinding binding;
    String selectedItem;
    ShoppingViewModel shoppingViewModel;

    String latitude = "", longitude = "";
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_address);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        selectedItem = getString(R.string.select_the_type_of_address);
        sharedHelper = new SharedHelper(this);
        getIntentValues();
        initSpinner();
        initListener();
        initSpinner();
    }

    private void getIntentValues() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            latitude = bundle.getString("latitude");
            longitude = bundle.getString("longitude");
            binding.pinDetails.setText(bundle.getString("address"));
            binding.textView12.setText(bundle.getString("address"));
        }


    }

    private void initListener() {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidInput().equals("true")) {
                    shoppingViewModel.addAddress(AddAddressActivity.this, selectedItem, latitude, longitude, binding.pinDetails.getText().toString(), binding.apartmentNumber.getText().toString(),
                            binding.nearestTeacher.getText().toString(), binding.deliveryInstruction.getText().toString(), binding.othertag.getText().toString().trim()).observe(AddAddressActivity.this, commonResponse -> {
                        if (commonResponse.getError()) {
                            Utils.showSnack(findViewById(android.R.id.content), commonResponse.getMessage());
                        } else {
                            sharedHelper.setSelectedDeliveryToLat(latitude);
                            sharedHelper.setSelectedDeliveryToLng(longitude);
                            new SharedHelper(AddAddressActivity.this).setIsAddAddress(true);
                            shoppingViewModel.getclearcartData(AddAddressActivity.this).observe(AddAddressActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse listMyAddressResponse) {
                                    if (!listMyAddressResponse.getError()) {
                                        Log.d("cxvdxv",""+listMyAddressResponse.getError());
                                    }
                                }
                            });
                            finish();
                            Intent intent = new Intent(AddAddressActivity.this,DashboardActivity.class);
                            startActivity(intent);
                        }
                    });
                } else {
                    Utils.showSnack(findViewById(android.R.id.content), isValidInput());
                }
            }
        });

        binding.selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(
                        new Intent(AddAddressActivity.this, SelectAddressActivity.class)
                                .putExtra("isEdit", false)
                                .putExtra("lat", latitude)
                                .putExtra("lng", longitude)
                        ,
                        Constants.RequestCode.ADDRESS_REQUEST);
            }
        });

        binding.identifyOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(
                        new Intent(AddAddressActivity.this, SelectAddressActivity.class)
                                .putExtra("isEdit", false)
                                .putExtra("lat", latitude)
                                .putExtra("lng", longitude)
                        ,
                        Constants.RequestCode.ADDRESS_REQUEST);
            }
        });

        binding.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSelectionListener();
            }
        });

    }

    private void initSpinner() {


        if (sharedHelper.getHomeAvailable()) {

            binding.homeView.setVisibility(View.GONE);
            binding.homeiahe.setVisibility(View.GONE);
            binding.home.setVisibility(View.GONE);

        } else {

            binding.homeView.setVisibility(View.VISIBLE);
            binding.homeiahe.setVisibility(View.VISIBLE);
            binding.home.setVisibility(View.VISIBLE);
        }


        if (sharedHelper.getOfficeAvailable()) {
            binding.officeView.setVisibility(View.GONE);
            binding.officeiahe.setVisibility(View.GONE);
            binding.office.setVisibility(View.GONE);
        } else {
            binding.officeView.setVisibility(View.VISIBLE);
            binding.officeiahe.setVisibility(View.VISIBLE);
            binding.office.setVisibility(View.VISIBLE);

        }
    }


    void showSelectionListener() {

//        ArrayList<String> value = new ArrayList<>();
//
//        value.add("Home");
//        value.add("Office");
//        value.add("Other");


        binding.cardSpinner.setVisibility(View.VISIBLE);


        binding.homeView.setOnClickListener(view -> {

            selectedItem = getString(R.string.home);
            binding.addressType.setText(selectedItem);
            binding.cardView8.setVisibility(View.GONE);
            binding.cardSpinner.setVisibility(View.GONE);
        });

        binding.officeView.setOnClickListener(view -> {

            selectedItem = getString(R.string.office);
            binding.addressType.setText(selectedItem);
            binding.cardView8.setVisibility(View.GONE);
            binding.cardSpinner.setVisibility(View.GONE);
        });

        binding.othersView.setOnClickListener(view -> {

            selectedItem = getString(R.string.other);
            binding.addressType.setText(selectedItem);
            binding.cardView8.setVisibility(View.VISIBLE);
            binding.cardSpinner.setVisibility(View.GONE);
        });


//        DialogUtils.getSelectAddressTypeDialog(this, value, new OnItemClick() {
//            @Override
//            public void onClick(int position) {
//                selectedItem = value.get(position);
//                binding.addressType.setText(selectedItem);
//                if (selectedItem.equals("Other")) {
//                    binding.cardView8.setVisibility(View.VISIBLE);
//                } else {
//                    binding.cardView8.setVisibility(View.GONE);
//                }
//            }
//        });
    }

    private String isValidInput() {

        if (selectedItem.equals("") || selectedItem.equals(getString(R.string.select_the_type_of_address))) {
            return getString(R.string.select_the_type_of_address);
        } else if (binding.pinDetails.getText().toString().trim().equals("")) {
            return getString(R.string.select_address_pin_details);
        } else if (latitude.equals("")) {
            return getString(R.string.pick_location);
        } else if (selectedItem.equals(getString(R.string.other)) && binding.othertag.getText().toString().trim().equals("")) {
            return getString(R.string.add_tag);
        } else {
            return "true";
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Constants.RequestCode.ADDRESS_REQUEST == requestCode) {
            if (resultCode == Activity.RESULT_OK) {
                latitude = data.getStringExtra("latitude");
                longitude = data.getStringExtra("longitude");
                binding.pinDetails.setText(data.getStringExtra("address"));
                binding.textView12.setText(data.getStringExtra("address"));
            }
        }
    }
}
