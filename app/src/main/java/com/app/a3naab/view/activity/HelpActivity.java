package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.FragmentHelpBinding;
import com.app.a3naab.model.ListFaqResponse;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.FaqAdapter;
import com.app.a3naab.viewmodel.ProductViewModel;
import com.app.a3naab.viewmodel.ShoppingViewModel;

public class HelpActivity extends BaseActivity {

    ProductViewModel productViewModel;
    FragmentHelpBinding binding;
    ShoppingViewModel shoppingViewModel;
    SharedHelper sharedHelper;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_help);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        //faqList();
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

        binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        if (sharedHelper.getCartCount() == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setVisibility(View.VISIBLE);
            binding.foot.badge.setText(""+sharedHelper.getCartCount());
        }
    }


    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }



    @Override
    protected void onResume() {
        super.onResume();
        faqList();
    }

    private void faqList() {
        shoppingViewModel.getfaqList(this).observe(this, new Observer<ListFaqResponse>() {
            @Override
            public void onChanged(ListFaqResponse listMyAddressResponse) {
                Log.d("cdxc",""+listMyAddressResponse.getData().getMyCart().size());
                if (!listMyAddressResponse.getError()) {
                    binding.faqview.setLayoutManager(new LinearLayoutManager(HelpActivity.this));
                    binding.faqview.setAdapter(new FaqAdapter(HelpActivity.this,HelpActivity.this, listMyAddressResponse.getData().getMyCart()));
                }
            }
        });
    }

}
