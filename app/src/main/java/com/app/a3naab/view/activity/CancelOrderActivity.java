package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityCancelOrderBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.PaymentRefundResponse;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.ShoppingViewModel;

public class CancelOrderActivity extends BaseActivity {

    ActivityCancelOrderBinding binding;
    ShoppingViewModel shoppingViewModel;
    String orderId = "0";
    String amount = "",noonorderid = "",paytype="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cancel_order);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);

        initView();
        getIntentDetails();

    }

    private void getIntentDetails() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            orderId = String.valueOf(bundle.getInt("orderId", 0));
            amount = bundle.getString("amount");
            noonorderid = bundle.getString("noonorderid");
            paytype = bundle.getString("paytype");
        }
    }

    private void initView() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.reason1Radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (binding.reason1Radio.isChecked()) {
                    binding.reason2Radio.setChecked(false);
                    binding.reason3Radio.setChecked(false);
                    binding.reasonothersRadio.setChecked(false);
                    binding.reason.setVisibility(View.GONE);

                }
            }
        });

        binding.reason2Radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (binding.reason2Radio.isChecked()) {
                    binding.reason1Radio.setChecked(false);
                    binding.reason3Radio.setChecked(false);
                    binding.reasonothersRadio.setChecked(false);
                    binding.reason.setVisibility(View.GONE);
                }
            }
        });

        binding.reason3Radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (binding.reason3Radio.isChecked()) {
                    binding.reason2Radio.setChecked(false);
                    binding.reason1Radio.setChecked(false);
                    binding.reasonothersRadio.setChecked(false);
                    binding.reason.setVisibility(View.GONE);
                }
            }
        });

        binding.reasonothersRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (binding.reasonothersRadio.isChecked()) {
                    binding.reason2Radio.setChecked(false);
                    binding.reason3Radio.setChecked(false);
                    binding.reason1Radio.setChecked(false);
                    binding.reason.setVisibility(View.VISIBLE);
                }
            }
        });

        binding.reason1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.reason1Radio.setChecked(true);
                binding.reason2Radio.setChecked(false);
                binding.reason3Radio.setChecked(false);
                binding.reasonothersRadio.setChecked(false);
                binding.reason.setVisibility(View.GONE);
            }
        });

        binding.reason2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.reason2Radio.setChecked(true);
                binding.reason1Radio.setChecked(false);
                binding.reason3Radio.setChecked(false);
                binding.reasonothersRadio.setChecked(false);
                binding.reason.setVisibility(View.GONE);
            }
        });

        binding.reason3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.reason3Radio.setChecked(true);
                binding.reason2Radio.setChecked(false);
                binding.reason1Radio.setChecked(false);
                binding.reasonothersRadio.setChecked(false);
                binding.reason.setVisibility(View.GONE);
            }
        });

        binding.others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.reason2Radio.setChecked(false);
                binding.reason3Radio.setChecked(false);
                binding.reason1Radio.setChecked(false);
                binding.reasonothersRadio.setChecked(true);
                binding.reason.setVisibility(View.VISIBLE);
            }
        });

        binding.back.setOnClickListener(view -> {
            finish();
        });


        binding.cancelOrder.setOnClickListener((view) -> {

            String reason = "";
            if (binding.reason1Radio.isChecked()) {
                reason = binding.reason1.getText().toString();
            } else if (binding.reason2Radio.isChecked()) {
                reason = binding.reason2.getText().toString();
            } else if (binding.reason3Radio.isChecked()) {
                reason = binding.reason3.getText().toString();
            } else if (binding.reasonothersRadio.isChecked()) {
                reason = binding.reason.getText().toString();
            }

            if (reason.equalsIgnoreCase("")) {
                Utils.showSnack(binding.parent, getString(R.string.select_reason));
                return;
            }
            else {
                DialogUtils.showLoader(this);
                shoppingViewModel.cancelOrder(this, orderId, reason).observe(this, new Observer<CommonResponse>() {
                    @Override
                    public void onChanged(CommonResponse commonResponse) {
                        DialogUtils.dismissLoader();
                        if (commonResponse.getError()) {
                            Utils.showSnack(binding.parent, commonResponse.getMessage());
                        } else {
                       /* if(paytype.equalsIgnoreCase("card")){
                            shoppingViewModel.refundpayment(CancelOrderActivity.this,amount,noonorderid).observe(CancelOrderActivity.this, new Observer<PaymentRefundResponse>() {
                                @Override
                                public void onChanged(PaymentRefundResponse paymentRefundResponse) {
                                    if(paymentRefundResponse.getRcode() == 0){
                                        Utils.showSnack(binding.parent, getString(R.string.refundsuccess));
                                    }
                                    else {
                                        Utils.showSnack(binding.parent, getString(R.string.refundfailure));
                                    }
                                }
                            });
                        }*/
                            DialogUtils.showOrderCancelledDialog(CancelOrderActivity.this, new DialogCallback() {
                                @Override
                                public void onPositiveClick() {

                                    Intent intent = new Intent(CancelOrderActivity.this, DashboardActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);

                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });

                        }
                    }
                });
            }
        });

    }
}
