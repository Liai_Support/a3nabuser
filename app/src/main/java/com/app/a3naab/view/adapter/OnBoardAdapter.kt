package com.app.a3naab.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.a3naab.R
import com.app.a3naab.databinding.ChildOnboardBinding
import com.app.a3naab.utils.loadImage

class OnBoardAdapter(
    var context: Context,
    var description: ArrayList<String>,
    var header: ArrayList<String>,
    var image: ArrayList<Int>,
    var next: (Int) -> Unit
) :
    RecyclerView.Adapter<OnBoardAdapter.ViewHolder>() {


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var childOnboardBinding: ChildOnboardBinding = ChildOnboardBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_onboard, parent, false)
        )
    }

    override fun getItemCount(): Int = header.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position == 0) {
            holder.childOnboardBinding.next2.visibility = View.VISIBLE
            holder.childOnboardBinding.header.visibility = View.GONE
        } else {
            holder.childOnboardBinding.next2.visibility = View.GONE
            holder.childOnboardBinding.header.visibility = View.VISIBLE
        }

        //holder.childOnboardBinding.onboardImage.loadImage(image[position])
        holder.childOnboardBinding.onboardImage.setImageResource(image[position])

        holder.childOnboardBinding.header.text = header[position]
        holder.childOnboardBinding.desc.text = description[position]


    }
}