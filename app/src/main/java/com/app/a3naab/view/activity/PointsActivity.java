package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityEditProfileBinding;
import com.app.a3naab.databinding.ActivityPoinetsBinding;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.model.ProfileDatas;
import com.app.a3naab.model.WalletAmountTransactionResponse;
import com.app.a3naab.model.WalletPointsTransactionResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.WalletHistoryAdapter;
import com.app.a3naab.view.adapter.WalletPointsHistoryAdapter;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.Calendar;

public class PointsActivity extends BaseActivity {

    ActivityPoinetsBinding binding;
    ProductViewModel viewModel;
    ProductViewModel productViewModel;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_poinets);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        initListener();
        binding.count.setText(""+sharedHelper.getAvaliableWalletpoints());
    }

    private void initListener() {
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

        binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        if (sharedHelper.getCartCount() == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setVisibility(View.VISIBLE);
            binding.foot.badge.setText(""+sharedHelper.getCartCount());
        }


        viewModel.getWalletPointTransaction(this, 1).observe(this, new Observer<WalletPointsTransactionResponse>() {
            @Override
            public void onChanged(WalletPointsTransactionResponse walletPointsTransactionResponse) {
                if (walletPointsTransactionResponse.getError()==false) {
                    binding.pointsList.setVisibility(View.VISIBLE);
                    binding.pointsList.setLayoutManager(new LinearLayoutManager(PointsActivity.this));
                    binding.pointsList.setAdapter(new WalletPointsHistoryAdapter( PointsActivity.this, walletPointsTransactionResponse.getData().getList()));
                }
                else {
                    binding.pointsList.setVisibility(View.GONE);
                }
            }
        });
    }

    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
