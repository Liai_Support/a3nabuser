package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityDashboardBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.utils.Constants;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.HomeFragmentAdapter;
import com.app.a3naab.view.fragment.MyOrderFragment;
import com.app.a3naab.view.fragment.ProfileFragment;
import com.app.a3naab.view.fragment.ShoppingCartFragment;
import com.app.a3naab.view.fragment.ShoppingFragment;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.util.ArrayList;

public class DashboardActivity extends BaseActivity {

    public ActivityDashboardBinding binding;
    ArrayList<Fragment> listOfFragment = new ArrayList<>();
    ShoppingFragment shoppingFragment = new ShoppingFragment();
    MyOrderFragment myOrderFragment = new MyOrderFragment();
    ShoppingCartFragment shoppingCartFragment = new ShoppingCartFragment();
    ProfileFragment profileFragment = new ProfileFragment();
  /*  ListAddressFragment listAddressFragment =  new ListAddressFragment();
    HelpFragment helpFragment = new HelpFragment();
    ReferandEarnFragment referandEarnFragment = new ReferandEarnFragment();
    FavouriteProductListFragment favouriteProductListFragment = new FavouriteProductListFragment();*/
    public int exitcount = 0;
    boolean doubleBackToExitPressedOnce = false;
    HomeFragmentAdapter adapter;
    ShoppingViewModel shoppingViewModel;
    SharedHelper sharedHelper;
    public String fvalue= "home";
    public String rfcode = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        sharedHelper = new SharedHelper(this);
        initView();
        getIntentValues();
       /* if(Constants.Value.first == true){
            Constants.Value.first = false;
            Intent intent = new Intent(this, FlexibleUpdateActivity.class);
            startActivity(intent);
        }*/
    }

    private void getIntentValues() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            binding.container.setCurrentItem(bundle.getInt("page", 0));
        }
        else {
            binding.container.setCurrentItem(0);
        }
    }

    private void setListeners() {

        binding.foot.shopping.setOnClickListener(view -> {
            binding.container.setCurrentItem(0);
            fvalue = "home";
        });

        binding.foot.order.setOnClickListener(view -> {
            if (sharedHelper.getLoggedIn()){
                binding.container.setCurrentItem(2);
                fvalue="order";
            }
            else
                DialogUtils.showAlertDialogWithCancel(this, getString(R.string.login_feature), getString(R.string.please_login), getString(R.string.ok),getString(R.string.cancel),
                        new DialogCallback() {
                            @Override
                            public void onPositiveClick() {
                               // SessionManager.getInstance().setRegisterLaterFlow(true);
                                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });


        });

        binding.foot.cart.setOnClickListener(view -> {
            if (sharedHelper.getLoggedIn()){
                binding.container.setCurrentItem(1);
            fvalue = "cart";
            }
            else
                DialogUtils.showAlertDialogWithCancel(this, getString(R.string.login_feature), getString(R.string.please_login), getString(R.string.ok),getString(R.string.cancel),
                        new DialogCallback() {
                            @Override
                            public void onPositiveClick() {
                                //SessionManager.getInstance().setRegisterLaterFlow(true);
                                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });

        });

        binding.foot.profile.setOnClickListener(view -> {
            if (sharedHelper.getLoggedIn()){
                binding.container.setCurrentItem(3);
                fvalue = "profile";
            }
            else
                DialogUtils.showAlertDialogWithCancel(this, getString(R.string.login_feature), getString(R.string.please_login), getString(R.string.ok),getString(R.string.cancel),
                        new DialogCallback() {
                            @Override
                            public void onPositiveClick() {
                               // SessionManager.getInstance().setRegisterLaterFlow(true);
                                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (new SharedHelper(this).getLoggedIn()) {
            shoppingViewModel.updateToken(this);
        }
    }

    private void initView() {
        listOfFragment.add(shoppingFragment);
        listOfFragment.add(shoppingCartFragment);
        listOfFragment.add(myOrderFragment);
        listOfFragment.add(profileFragment);
        initAdapter();
    }

    private void initAdapter() {

        Log.d("dcdsc",""+binding.container.getChildCount());
        adapter = new HomeFragmentAdapter(getSupportFragmentManager(), listOfFragment,getLifecycle());
        binding.container.setUserInputEnabled(false);
        binding.container.setAdapter(adapter);

        binding.container.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                setNavBar(position);
            }
            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        setListeners();

/*
        binding.container.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setNavBar(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
*/

//        binding.bottomNavigationView.setOnNavigationItemSelectedListener((item) -> {
//
//            switch (item.getItemId()) {
//
//                case R.id.shopping: {
//                    binding.container.setCurrentItem(0);
//                    break;
//                }
//
//                case R.id.order: {
//                    binding.container.setCurrentItem(1);
//                    break;
//                }
//
//                case R.id.shoppingCart: {
//                    binding.container.setCurrentItem(2);
//                    break;
//                }
//
//                case R.id.profile: {
//                    binding.container.setCurrentItem(3);
//                    break;
//                }
//
//            }
//            return true;
//        });

    }

    public void setCount(int count) {

        if (count == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setText(count + "");
            binding.foot.badge.setVisibility(View.VISIBLE);
        }

    }

    private void setNavBar(int position) {

        if (position == 0) {
            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));


        } else if (position == 1) {

            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
        } else if (position == 2) {


            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));






        } else if (position == 3) {
            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));


        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (shoppingFragment != null) {
            shoppingFragment.onFragmentResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
            if(fvalue.equals("home")){
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
            }
        else if(fvalue.equals("address") || fvalue.equals("help") || fvalue.equals("refer") || fvalue.equals("favourite")) {
                binding.container.setCurrentItem(3);
                fvalue = "profile";
            }
        else {
                binding.container.setCurrentItem(0);
                fvalue = "home";
            }
    }
}
