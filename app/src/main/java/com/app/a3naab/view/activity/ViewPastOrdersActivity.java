package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityViewPastOrdersBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.CheckReorderResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.OrderDetails;
import com.app.a3naab.model.OrderList;
import com.app.a3naab.model.ViewOrdersResponse;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.CartViewcartProductAdapter;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.util.ArrayList;
import java.util.Locale;

public class ViewPastOrdersActivity extends BaseActivity {

    ActivityViewPastOrdersBinding binding;
    ShoppingViewModel shoppingViewModel;
    SharedHelper sharedHelper;
    Double cbstyleamount =0.0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_past_orders);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        sharedHelper = new SharedHelper(this);
        getOrderDetails();
        initListeners();
    }

    private void initListeners() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.back.setOnClickListener((view) -> {
            finish();
        });
    }


    private void getOrderDetails() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            int orderId = bundle.getInt("orderId");

            DialogUtils.showLoader(this);
            shoppingViewModel.getOrderDetails(this, orderId).observe(this, new Observer<ViewOrdersResponse>() {
                @Override
                public void onChanged(ViewOrdersResponse viewOrdersResponse) {
                    DialogUtils.dismissLoader();
                    if (!viewOrdersResponse.getError()) {
                        loadData(viewOrdersResponse.getData());
                    }
                }
            });
        }

    }

    private void loadData(OrderDetails data) {

        binding.orderId.setText(getString(R.string.order_id) + " " + data.getOrderInfo().getOrderIDs());

        if (data.getOrderInfo().getOrderStatus().equalsIgnoreCase("CANCELLED")) {
            if (data.getOrderInfo().getOrderOn() != null) {
                binding.orderDate.setText(getString(R.string.order_placed) + "\n" + Utils.getConvertedTime(data.getOrderInfo().getOrderOn())+ "\n" +Utils.convertDateutc(data.getOrderInfo().getOrderOn(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
            }
            if (data.getOrderInfo().getCancelDate() != null) {
                binding.deliveryDate.setVisibility(View.VISIBLE);
                binding.deliveryDateImage.setVisibility(View.VISIBLE);
                binding.deliveryDate.setText(getString(R.string.order_canceled) + "\n" + Utils.getConvertedTime(data.getOrderInfo().getCancelDate())+ "\n" +Utils.convertDateutc(data.getOrderInfo().getCancelDate(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
            }
            else {
                binding.deliveryDate.setVisibility(View.GONE);
                binding.deliveryDateImage.setVisibility(View.GONE);
            }

        }
        else {
            if (data.getOrderInfo().getOrderOn() != null) {
                binding.orderDate.setText(getString(R.string.order_placed) + "\n" + Utils.getConvertedTime(data.getOrderInfo().getOrderOn())+ "\n" +Utils.convertDateutc(data.getOrderInfo().getOrderOn(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
            }
            if (data.getOrderInfo().getDeliveryOn() != null || data.getOrderInfo().getDeliveryDate() != null) {
                binding.deliveryDate.setVisibility(View.VISIBLE);
                binding.deliveryDateImage.setVisibility(View.VISIBLE);
                String selectedtime = Utils.convertDate(data.getOrderInfo().getDeliveryOn_fromTime(), "HH:mm:ss", "hh:mm a")
                        + "-" +
                        Utils.convertDate(data.getOrderInfo().getDeliveryOn_toTime(), "HH:mm:ss", "hh:mm a");
                binding.deliveryDate.setText(getString(R.string.delivery_by) + "\n" + Utils.getConvertedTime(data.getOrderInfo().getDeliveryDate()) +"\n" +selectedtime);
            }
            else {
                binding.deliveryDate.setVisibility(View.GONE);
                binding.deliveryDateImage.setVisibility(View.GONE);
            }
        }


        binding.address.setText(data.getOrderInfo().getAddressType() + "," + data.getOrderInfo().getAddressPinDetails());

        cbstyleamount =0.0;
        for (int i=0;i<data.getOrderList().size();i++){
            Double v = Integer.parseInt(data.getOrderList().get(i).getQuantity()) * (Double.parseDouble(data.getOrderList().get(i).getBoxPrice()) + Double.parseDouble(data.getOrderList().get(i).getCuttingPrice()));
            cbstyleamount = cbstyleamount+v;
        }

        Double subtotal = (cbstyleamount)+(Double.parseDouble(data.getOrderInfo().getTotalAmount()) - Double.parseDouble(data.getOrderInfo().getDiscountAmount()));
        Double delivery = Double.parseDouble(data.getOrderInfo().getFastDelievryCharge());
       // Double subtotal1 = (subtotal-Double.parseDouble(data.getOrderInfo().getTakeToCustomer()))+Double.parseDouble(data.getOrderInfo().getGiveToCustomer())+delivery;
        if(data.getOrderInfo().getOff_types().equalsIgnoreCase("0")){
            delivery = Double.parseDouble(data.getOrderInfo().getCouponDiscount());
        }

        Double subtotal1 = subtotal+delivery;
        Double coupondiscount = Double.parseDouble(data.getOrderInfo().getCouponDiscount());
        Double pointamount = Double.parseDouble(data.getOrderInfo().getPointsAmount());
        Double walletamount = Double.parseDouble(data.getOrderInfo().getPaidByWallet());
        Double vat = ((subtotal1-(coupondiscount+pointamount+walletamount)) * (Double.parseDouble(data.getOrderInfo().getTaxvalue()) / 100));
        Double total = ((subtotal1-(coupondiscount+pointamount+walletamount))+vat);
        if(total < 0){
            total = 0.0;
        }

        if(coupondiscount==0.0){
            binding.discountVal.setVisibility(View.GONE);
            binding.disTxt.setVisibility(View.GONE);
        }
        if(pointamount==0.0){
            binding.walletpointVal.setVisibility(View.GONE);
            binding.walletpointTxt.setVisibility(View.GONE);
        }
        if(walletamount==0.0){
            binding.walletamountTxt.setVisibility(View.GONE);
            binding.walletamountVal.setVisibility(View.GONE);
        }

        //binding.textView33.setText("SAR "+Double.parseDouble(data.getOrderInfo().getTaxvalue()));
        binding.textView29.setText(getString(R.string.vat)+"("+data.getOrderInfo().getTaxvalue()+" %)");
        binding.totalAmount.setText("SAR " +String.format(Locale.ENGLISH,"%.2f",total));
        binding.totalAmountTop.setText("SAR " +String.format(Locale.ENGLISH,"%.2f",total));
        binding.subtotal.setText("SAR " + String.format(Locale.ENGLISH,"%.2f",subtotal));
        binding.textView33.setText("SAR " + String.format(Locale.ENGLISH,"%.2f",vat));
        binding.discountVal.setText("SAR "+"-"+String.format(Locale.ENGLISH,"%.2f",Double.parseDouble(data.getOrderInfo().getCouponDiscount())));
        binding.walletpointVal.setText("SAR "+String.format(Locale.ENGLISH,"%.2f",pointamount));
        binding.walletamountVal.setText("SAR "+String.format(Locale.ENGLISH,"%.2f",walletamount));
        binding.deliveryfee.setText("SAR "+String.format(Locale.ENGLISH,"%.2f",delivery));
        binding.amounttobepaid.setText("SAR "+data.getOrderInfo().getTakeToCustomer());
        binding.amounttobereceived.setText("SAR "+data.getOrderInfo().getGiveToCustomer());

        if(data.getOrderInfo().getPaytype().equalsIgnoreCase("CASH")){
            binding.paymentType.setText(""+getString(R.string.codmachine));
        }
        else {
            binding.paymentType.setText(data.getOrderInfo().getPaytype());
        }
       /* if(data.getOrderInfo().getFastDelivery().equals("1") || data.getOrderInfo().getFastDelivery().equals("0")){
            binding.deliveryfee.setText("SAR "+data.getOrderInfo().getFastDelievryCharge());
        }*/

        binding.orderedItems.setLayoutManager(new LinearLayoutManager(this));
        binding.orderedItems.setAdapter(new CartViewcartProductAdapter(this, data.getOrderList(), position -> {

        }, position -> {

        }));


        binding.reOrder.setOnClickListener(view -> {
           /* CartSessionManager.removeAllValues();
            CartSessionManager.getInstance().setReorder(true);
            CartSessionManager.getInstance().setOrderId(String.valueOf(data.getOrderInfo().getId()));
            CartSessionManager.getInstance().setTotalAmount(String.valueOf(data.getOrderInfo().getTotalAmount()));

            CartSessionManager.getInstance().setFlatDiscount(String.valueOf(data.getTax()));

            setMaxTime(data.getOrderList());
            startActivity(new Intent(ViewPastOrdersActivity.this, CartDetailsActivity.class));*/
            DialogUtils.showLoader(this);
            shoppingViewModel.checkreorder(this, data.getOrderInfo().getId()).observe(this, new Observer<CheckReorderResponse>() {
                @Override
                public void onChanged(CheckReorderResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (!commonResponse.getError()) {
                        //loadData(commonResponse.getData());
                        CartSessionManager.removeAllValues();
                        CartSessionManager.getInstance().setReorder(true);
                        CartSessionManager.getInstance().setOrderId(String.valueOf(data.getOrderInfo().getId()));

                        CartSessionManager.getInstance().setTotalAmount(String.valueOf((Double.parseDouble(data.getOrderInfo().getTotalAmount()))-(Double.parseDouble(data.getOrderInfo().getDiscountAmount()))));
                        CartSessionManager.getInstance().setcbstyleamount(String.valueOf(cbstyleamount));
                        CartSessionManager.getInstance().setSelectedAddressType(String.valueOf(data.getOrderInfo().getAddressType()));
                        CartSessionManager.getInstance().setSelectedAddress(data.getOrderInfo().getAddressPinDetails());
                        CartSessionManager.getInstance().setSelectedId(data.getOrderInfo().getAddressId());

                        // CartSessionManager.getInstance().setTotalAmountcpy(String.valueOf((Double.parseDouble(data.getOrderInfo().getTotalAmount()))-(Double.parseDouble(data.getOrderInfo().getDiscountAmount()))));

                        CartSessionManager.getInstance().setFlatDiscount(String.valueOf(data.getTax()));
                        setMaxTime(data.getOrderList());
                        //hari

                        Double tot = Double.parseDouble(CartSessionManager.getInstance().getTotalAmount())+Double.parseDouble(CartSessionManager.getInstance().getcbstyleamount());
                        if(Double.parseDouble(commonResponse.getData().getFreeDeliveryAmt()) <= tot){
                            sharedHelper.setDeliverycharge("0");
                            sharedHelper.setFastDeliverycharge(commonResponse.getData().getOverAllFastDeliveryAmount());
                        }
                        else {
                            sharedHelper.setFastDeliverycharge(commonResponse.getData().getOverAllFastDeliveryAmount());
                            sharedHelper.setDeliverycharge(commonResponse.getData().getOverAllDeliveryAmount());
                        }
                        startActivity(new Intent(ViewPastOrdersActivity.this, CartDetailsActivity.class));
                    }
                    else {
                        DialogUtils.showAlertDialog(ViewPastOrdersActivity.this,  getString(R.string.storenotavailable),getString(R.string.oops), getString(R.string.ok), new DialogCallback() {
                            @Override
                            public void onPositiveClick() {

                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });                        //commonResponse.getMessage()
                    }
                }
            });



        });

        binding.reschedule.setOnClickListener(view -> {

            DialogUtils.showLoader(this);
            shoppingViewModel.checkreorder(this, data.getOrderInfo().getId()).observe(this, new Observer<CheckReorderResponse>() {
                @Override
                public void onChanged(CheckReorderResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (!commonResponse.getError()) {
                        //loadData(commonResponse.getData());
                        CartSessionManager.removeAllValues();
                        CartSessionManager.getInstance().setReorder(true);
                        CartSessionManager.getInstance().setOrderId(String.valueOf(data.getOrderInfo().getId()));
                        CartSessionManager.getInstance().setTotalAmount(String.valueOf((Double.parseDouble(data.getOrderInfo().getTotalAmount()))-(Double.parseDouble(data.getOrderInfo().getDiscountAmount()))));
                        CartSessionManager.getInstance().setcbstyleamount(String.valueOf(cbstyleamount));
                       // CartSessionManager.getInstance().setTotalAmountcpy(String.valueOf((Double.parseDouble(data.getOrderInfo().getTotalAmount()))-(Double.parseDouble(data.getOrderInfo().getDiscountAmount()))));
                        CartSessionManager.getInstance().setSelectedAddressType(String.valueOf(data.getOrderInfo().getAddressType()));
                        CartSessionManager.getInstance().setSelectedAddress(data.getOrderInfo().getAddressPinDetails());
                        CartSessionManager.getInstance().setSelectedId(data.getOrderInfo().getAddressId());

                        CartSessionManager.getInstance().setFlatDiscount(String.valueOf(data.getTax()));

                        setMaxTime(data.getOrderList());
                        //hari
                        Double tot = Double.parseDouble(CartSessionManager.getInstance().getTotalAmount())+Double.parseDouble(CartSessionManager.getInstance().getcbstyleamount());
                        if(Double.parseDouble(commonResponse.getData().getFreeDeliveryAmt()) <= tot){
                            sharedHelper.setDeliverycharge("0");
                            sharedHelper.setFastDeliverycharge(commonResponse.getData().getOverAllFastDeliveryAmount());
                        }
                        else {
                            sharedHelper.setFastDeliverycharge(commonResponse.getData().getOverAllFastDeliveryAmount());
                            sharedHelper.setDeliverycharge(commonResponse.getData().getOverAllDeliveryAmount());
                        }
                        startActivity(new Intent(ViewPastOrdersActivity.this, CartDetailsActivity.class));
                    }
                    else {
                        DialogUtils.showAlertDialog(ViewPastOrdersActivity.this,  getString(R.string.storenotavailable),getString(R.string.oops), getString(R.string.ok), new DialogCallback() {
                            @Override
                            public void onPositiveClick() {

                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });                        //commonResponse.getMessage()
                    }
                }
            });


          /*  CartSessionManager.removeAllValues();
            CartSessionManager.getInstance().setReorder(true);
            CartSessionManager.getInstance().setOrderId(String.valueOf(data.getOrderInfo().getId()));
            CartSessionManager.getInstance().setTotalAmount(String.valueOf(data.getOrderInfo().getTotalAmount()));
            CartSessionManager.getInstance().setFlatDiscount(String.valueOf(data.getTax()));


            setMaxTime(data.getOrderList());
            startActivity(new Intent(this, CartDetailsActivity.class));*/

        });
    }


    private void setMaxTime(ArrayList<OrderList> data) {

        int maxTime = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getSub_processingMin() == null || data.get(i).getSub_processingMin() == 0) {
                if (data.get(i).getCate_processingMin() == null || data.get(i).getCate_processingMin() == 0) {

                } else {
                    if (maxTime < data.get(i).getCate_processingMin()) {
                        maxTime = data.get(i).getCate_processingMin();
                    }
                }
            } else {
                if (maxTime < data.get(i).getSub_processingMin()) {
                    maxTime = data.get(i).getSub_processingMin();
                }
            }
        }
        CartSessionManager.getInstance().setOrderMinimumMins(maxTime);

    }
}
