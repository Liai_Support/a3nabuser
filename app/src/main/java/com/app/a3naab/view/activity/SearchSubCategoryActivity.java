package com.app.a3naab.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivitySearchCategoryBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ProducSearchtListResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.SearchProductAdapter;
import com.app.a3naab.viewmodel.CategoryViewModel;

import java.util.ArrayList;

public class SearchSubCategoryActivity extends BaseActivity {

    ActivitySearchCategoryBinding binding;
    int categoryId, subCategoryId;
    int isHavingSub = 0;
    CategoryViewModel categoryViewModel;
    SearchProductAdapter adapter;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeblue);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_category);
        categoryViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);

        getCategoryId();
        setAdapter();
        getReadyForSearch();
        binding.textView6.setText("Product");
        setRecentList();
    }

    private void setAdapter() {
        adapter = new SearchProductAdapter(this, new ArrayList<>());
        binding.adsView.setLayoutManager(new LinearLayoutManager((this)));
        binding.adsView.setAdapter(adapter);
    }

    private void getCategoryId() {

        if (getIntent().getExtras() != null) {
            categoryId = getIntent().getExtras().getInt("categoryId", 0);
            subCategoryId = getIntent().getExtras().getInt("subCategoryId", 0);
            isHavingSub = getIntent().getExtras().getInt("havingSub", 0);
        }

    }

    private void getReadyForSearch() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().equals("")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setRecentList();
                            binding.requestGroup.setVisibility(View.GONE);
                        }
                    }, 500);

                } else {
                    searchCategory(editable.toString());
                }
            }


        });


        binding.addRequest.setOnClickListener(view -> {
            DialogUtils.showLoader(this);
            categoryViewModel.requestTypingText(this, "product", binding.searchText.getText().toString()).observe(this, new Observer<CommonResponse>() {
                @Override
                public void onChanged(CommonResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError())
                        Utils.showSnack(binding.parentLayout, commonResponse.getMessage());
                    else {
                        DialogUtils.showOrderSuccessDialog(SearchSubCategoryActivity.this, new DialogCallback() {
                            @Override
                            public void onPositiveClick() {
                                finish();
                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });
                    }
                }
            });
        });

    }

    private void searchCategory(String str) {

        int id = 0;
        if (isHavingSub == 0) {
            id = categoryId;
        } else {
            id = subCategoryId;
        }
        categoryViewModel.getProductSearch(this, id, str, isHavingSub).observe(this, new Observer<ProducSearchtListResponse>() {
            @Override
            public void onChanged(ProducSearchtListResponse searchCategoryDataResponse) {
                if (!searchCategoryDataResponse.getError()) {
                    if (searchCategoryDataResponse.getData().size() == 0) {
                        binding.requestGroup.setVisibility(View.VISIBLE);
                        adapter.setValues(new ArrayList<>());

                        binding.errorMessage.setText(getString(R.string.ooo_couldn_t_find_products_that_contains_the_word) + "\n" + str);

                    } else {
                        binding.requestGroup.setVisibility(View.GONE);
                        adapter.setValues(searchCategoryDataResponse.getData());
                    }
                }
            }
        });

    }


    private void setRecentList() {

        adapter = new SearchProductAdapter(this, new SharedHelper(this).getProductSearchRecent());
        binding.adsView.setLayoutManager(new LinearLayoutManager((this)));
        binding.adsView.setAdapter(adapter);

    }


}
