package com.app.a3naab.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.FragmentProfileBinding;
import com.app.a3naab.interfaces.OnRatingSaved;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.model.ProfileDatas;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.DashboardActivity;
import com.app.a3naab.view.activity.FavouriteProductListActivity;
import com.app.a3naab.view.activity.HelpActivity;
import com.app.a3naab.view.activity.ListAddressActivity;
import com.app.a3naab.view.activity.LoginActivity;
import com.app.a3naab.view.activity.PointsActivity;
import com.app.a3naab.view.activity.ProfileActivity;
import com.app.a3naab.view.activity.ReferAndEarnActivity;
import com.app.a3naab.view.activity.SettingsActivity;
import com.app.a3naab.view.activity.WalletBalanceActivity;
import com.app.a3naab.viewmodel.ProductViewModel;
import com.app.a3naab.viewmodel.ShoppingViewModel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

public class ProfileFragment extends Fragment {

    FragmentProfileBinding binding;

    SharedHelper sharedHelper;
    ShoppingViewModel shoppingViewModel;
    ProductViewModel viewModel;
    BottomSheetDialog feedDialog = null;
    String rfcode;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_profile, container, false);
        binding = FragmentProfileBinding.bind(view);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        sharedHelper = new SharedHelper(requireContext());
        initListener();
        return binding.getRoot();
    }

    private void initListener() {

        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                logout();
            }
        });

        binding.pointsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("scvsgh","scsd");
                startActivity(new Intent(requireContext(), PointsActivity.class));
            }
        });

        binding.balanceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(requireContext(), WalletBalanceActivity.class));
            }
        });

        binding.ordersView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity)getActivity()).binding.container.setCurrentItem(2);
            }
        });

        binding.edit.setOnClickListener(view -> {

            startActivity(new Intent(requireContext(), ProfileActivity.class));
        });

        binding.shareExp.setOnClickListener(view -> {

            feedDialog = DialogUtils.getFeedback(requireContext(), new OnRatingSaved() {
                @Override
                public void onSaved(Float app, Float driver, Float product, String comments) {
                    DialogUtils.showLoader(requireContext());
                    viewModel.setFeedBack(requireContext(), app, comments).observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                        @Override
                        public void onChanged(CommonResponse commonResponse) {
                            DialogUtils.dismissLoader();
                            if (!commonResponse.getError()) {
                                feedDialog.dismiss();
                            }
                        }
                    });
                }
            });

        });

        binding.imageView37.setOnClickListener(view -> {

            startActivity(new Intent(requireContext(), ListAddressActivity.class));
        });


        binding.address.setOnClickListener(view -> {
            ((DashboardActivity)getActivity()).fvalue = "address";
           // ((DashboardActivity)getActivity()).binding.container.setCurrentItem(4);
            startActivity(new Intent(requireContext(), ListAddressActivity.class));
        });

        binding.imageView40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(requireContext(), SettingsActivity.class));
            }
        });

        binding.shareApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                String body = getString(R.string.sharecont) + " : "+"https://www.a3nab.com/";
//                String body = getString(R.string.sharecont) + " : "+"https://play.google.com/store/apps/details?id=com.app.a3naab";
                /* String sub = "Your Subject";
                myIntent.putExtra(Intent.EXTRA_SUBJECT,sub);*/
                myIntent.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(myIntent, "Share Using"));
            }
        });

        binding.settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(requireContext(), SettingsActivity.class));
            }
        });

        binding.imageView39.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  ((DashboardActivity)getActivity()).fvalue = "favourite";
               // ((DashboardActivity)getActivity()).binding.container.setCurrentItem(7);
                startActivity(new Intent(requireContext(), FavouriteProductListActivity.class));
            }
        });

        binding.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  ((DashboardActivity)getActivity()).fvalue = "favourite";
                ((DashboardActivity)getActivity()).binding.container.setCurrentItem(7);*/
                startActivity(new Intent(requireContext(), FavouriteProductListActivity.class));
            }
        });


        binding.textView53.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity)getActivity()).fvalue = "refer";
              //  ((DashboardActivity)getActivity()).binding.container.setCurrentItem(6);
                startActivity(new Intent(requireContext(), ReferAndEarnActivity.class));
            }
        });

        binding.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity)getActivity()).fvalue = "refer";
                //  ((DashboardActivity)getActivity()).binding.container.setCurrentItem(6);
                startActivity(new Intent(requireContext(), ReferAndEarnActivity.class));
            }
        });

        binding.privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://a3nab.com/privacy.php"));
                startActivity(browserIntent);
            }
        });

        binding.rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.app.a3naab"));
                startActivity(browserIntent);
            }
        });


        binding.help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity)getActivity()).fvalue = "help";
               // ((DashboardActivity)getActivity()).binding.container.setCurrentItem(5);
                startActivity(new Intent(requireContext(), HelpActivity.class));
            }
        });

        binding.changeLanguage.setOnClickListener(view -> {

            if (sharedHelper.getSelectedLanguage().equals("en")) {
                sharedHelper.setSelectedLanguage("ar");
            } else {
                sharedHelper.setSelectedLanguage("en");
            }

            Intent intent = new Intent(requireContext(), DashboardActivity.class);
            intent.putExtra("page", 3);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);


        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getProfileDetails();
    }

    private void getProfileDetails() {
        DialogUtils.showLoader(getContext());
        viewModel.getProfileDetails(requireContext()).observe(getViewLifecycleOwner(), new Observer<ProfileDataResponse>() {
            @Override
            public void onChanged(ProfileDataResponse profileDataResponse) {
                DialogUtils.dismissLoader();
                if (profileDataResponse.getError()) {
                    Utils.showSnack(binding.parentLayout, profileDataResponse.getMessage());
                } else {
                    setData(profileDataResponse.getData().getProfile());
                }
            }
        });
    }

    private void setData(ProfileDatas profile) {
        if (profile != null) {
            binding.name.setText(profile.getFirstName()
                    + " " + profile.getLastName());

            binding.phoneNumber.setText("+"+profile.getCountryCode()+profile.getMobileNumber());
            binding.orders.setText(profile.getOrders());
            binding.points1.setText(profile.getPoints());
            binding.balance.setText(profile.getWalletAmount());
            sharedHelper.setAvaliableWalletamount(profile.getWalletAmount());
            sharedHelper.setAvaliableWalletpoints(profile.getPoints());
            rfcode = profile.getReferralCode();
            ((DashboardActivity)getActivity()).rfcode = profile.getReferralCode();
        }
    }


    private void logout() {


        sharedHelper.setUserId("");
        sharedHelper.setFirstName("");
        sharedHelper.setLastName("");
        sharedHelper.setEmail("");
        sharedHelper.setLatitude("");
        sharedHelper.setLongitude("");
        sharedHelper.setCountryCode("");
        sharedHelper.setMobileNumber("");
        sharedHelper.setProfilePic("");
        sharedHelper.setAuthToken("");
        sharedHelper.setSelectedDeliveryToId("");
        sharedHelper.setSelectedDeliveryToTag("");
        sharedHelper.setSelectedDeliveryToAddress("");
        sharedHelper.setSelectedDeliveryToLat("");
        sharedHelper.setSelectedDeliveryToLng("");
        sharedHelper.setAvaliableWalletpoints("");
        sharedHelper.setAvaliableWalletamount("");
        sharedHelper.setLoggedIn(false);

        Intent intent = new Intent(requireContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
