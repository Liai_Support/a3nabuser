package com.app.a3naab.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.DialogAddressBinding;
import com.app.a3naab.databinding.FragmentShoppingBinding;
import com.app.a3naab.interfaces.AddressSelection;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.interfaces.OnRatingSaved;
import com.app.a3naab.model.AdsResponse;
import com.app.a3naab.model.AutoCompleteResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.CurrentLocationIdResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ListMyAddressResponse;
import com.app.a3naab.model.Prediction;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.model.ProfileDatas;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.network.InputForAPI;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.utils.Constants;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.GpsUtils;
import com.app.a3naab.utils.SessionManager;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.AdsActivity;
import com.app.a3naab.view.activity.BestProductListActivity;
import com.app.a3naab.view.activity.DashboardActivity;
import com.app.a3naab.view.activity.EditAddressActivity;
import com.app.a3naab.view.activity.FlexibleUpdateActivity;
import com.app.a3naab.view.activity.LoginActivity;
import com.app.a3naab.view.activity.SearchMainActivity;
import com.app.a3naab.view.activity.SelectStartAddressActivity;
import com.app.a3naab.view.activity.SubCategoryActivity;
import com.app.a3naab.view.adapter.AddressListingAdapter;
import com.app.a3naab.view.adapter.AdsHomeAdapter;
import com.app.a3naab.view.adapter.BestProductAdapter;
import com.app.a3naab.view.adapter.CategoryHomeAdapter;
import com.app.a3naab.viewmodel.MapViewModel;
import com.app.a3naab.viewmodel.ProductViewModel;
import com.app.a3naab.viewmodel.ShoppingViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

public class ShoppingFragment extends Fragment {

    FragmentShoppingBinding binding;
    public BottomSheetDialog addressDialog;
    public DialogAddressBinding dialogAddressBinding;
    ShoppingViewModel shoppingViewModel;
    SharedHelper sharedHelper;
    public boolean iscartempty;
    ProductViewModel viewModel;
    ArrayList<ViewCartResponse.CartDetails> cartlist = new ArrayList<>();

    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;

    private String currentLat, currentLng, currentAddress = "";
    private MapViewModel mapViewModel;
    private ArrayList<ListMyAddressResponse.AddressData> addressList = new ArrayList<>();

    ProductViewModel productViewModel;
    BestProductAdapter bestAdapter;
    AddressListingAdapter adapter;
    BottomSheetDialog ratingDialog;
    private Boolean callApiFromMyLocation = false;
    public String fvalue;
    public String avalue = "";
    public String avalue1 ="";
    String boxid = "0";
    String cutid = "0";
    String sinstruction = "";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_shopping, container, false);
        binding = FragmentShoppingBinding.bind(layout);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext());
        sharedHelper = new SharedHelper(requireContext());
        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        //getprofile();
        getCartDetails();
        locationListener();
        askLocationPermission();
        initListener();
        return binding.getRoot();
    }

    public void getprofile(){
        DialogUtils.showLoader(getContext());
        viewModel.getProfileDetails(getContext()).observe(getViewLifecycleOwner(), new Observer<ProfileDataResponse>() {
            @Override
            public void onChanged(ProfileDataResponse profileDataResponse) {
                DialogUtils.dismissLoader();
                if (profileDataResponse.getError()) {
                    Utils.showSnack(binding.parentLayout, profileDataResponse.getMessage());
                } else {
                   // setData(profileDataResponse.getData().getProfile());
                }
            }
        });
    }

   /* private void setData(ProfileDatas profile) {
        avalue1 = profile.getCaddr();
        sharedHelper.setSelectedDeliveryToTag(profile.getCaddr());

    }*/


    @Override
    public void onResume() {
        super.onResume();
        initView();
        if (sharedHelper.getLoggedIn()) {
            getCartDetails();
            getAddressList();
            getLastKnownLocation();
            getDashBoardDetails();
             /* if (!sharedHelper.getSelectedDeliveryToTag().equals("")) {
            binding.address.setText(sharedHelper.getSelectedDeliveryToTag());
            }*/
        }
    }

    private void getCartDetails() {

        DialogUtils.showLoader(requireContext());
        viewModel.getCartDetails(requireContext()).observe(getViewLifecycleOwner(), it -> {
            DialogUtils.dismissLoader();
            if (!it.getError()) {
                cartlist = it.getData().getMyCart();
                if(cartlist.isEmpty()){
                   iscartempty = true;
                    ((DashboardActivity) getActivity()).setCount(cartlist.size());
                    sharedHelper.setCartCount(cartlist.size());
                }
                else {
                  iscartempty = false;
                  ((DashboardActivity) getActivity()).setCount(cartlist.size());
                  sharedHelper.setCartCount(cartlist.size());
                }
                sharedHelper.setwalletpointsar(it.getData().getSettings().getWalletSAR());
                sharedHelper.setwalletpoint(it.getData().getSettings().getWalletPoints());
            }
        });

    }

    private Boolean checkiscart(int productid,int storeid){
        boxid = "0";
        cutid = "0";
        sinstruction = "";
        for (int i=0;i<cartlist.size();i++){
            if(cartlist.get(i).getProductId() == productid && cartlist.get(i).getStoreId() == storeid){
                if(cartlist.get(i).getBoxStyle()!=null){
                    boxid = cartlist.get(i).getBoxStyle();
                }
                if(cartlist.get(i).getBoxStyle()!=null){
                    cutid = cartlist.get(i).getCuttingStyle();
                }
                if(cartlist.get(i).getSpecialInstructions()!=null){
                    sinstruction = cartlist.get(i).getSpecialInstructions();
                }
                return true;
            }
        }
        return false;
    }

    private void getAddressList() {

        shoppingViewModel.getAddressList(requireContext()).observe(getViewLifecycleOwner(), new Observer<ListMyAddressResponse>() {
            @Override
            public void onChanged(ListMyAddressResponse listMyAddressResponse) {

                if (!listMyAddressResponse.getError()) {
                    addressList = listMyAddressResponse.getAddressData();
                    setAddress();
                    setAvailable();
                }
            }
        });
    }

    private void setAvailable() {

        sharedHelper.setHomeAvailable(false);
        sharedHelper.setOfficeAvailable(false);

        for (int i = 0; i < addressList.size(); i++) {
            if (addressList.get(i).getAddressType().equalsIgnoreCase("Home") || addressList.get(i).getAddressType().equalsIgnoreCase("الصفحة الرئيسية")) {
                sharedHelper.setHomeAvailable(true);
            } else if (addressList.get(i).getAddressType().equalsIgnoreCase("Office") || addressList.get(i).getAddressType().equalsIgnoreCase("مقرنا")) {
                sharedHelper.setOfficeAvailable(true);
            }
        }
    }

    private void initListener() {

        binding.showAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(requireContext(), BestProductListActivity.class));
            }
        });

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedHelper.getLoggedIn()) {
                   // getCartDetails();
                    showAddressDialog();
                }
                else {
                    DialogUtils.showAlertDialogWithCancel(requireContext(), getString(R.string.login_feature), getString(R.string.please_login), getString(R.string.ok),getString(R.string.cancel),
                            new DialogCallback() {
                                @Override
                                public void onPositiveClick() {
                                  //  SessionManager.getInstance().setRegisterLaterFlow(true);
                                    Intent intent = new Intent(requireContext(), LoginActivity.class);
                                    startActivity(intent);
                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });
                }
               // startActivity(new Intent(requireContext(), SelectStartAddressActivity.class));
            }
        });

        binding.chooseAddress.setOnClickListener((view) -> {

            if (sharedHelper.getLoggedIn()) {
                getCartDetails();
                showAddressDialog();
            } else {
                DialogUtils.showAlertDialogWithCancel(requireContext(), getString(R.string.login_feature), getString(R.string.please_login), getString(R.string.ok),getString(R.string.cancel),
                        new DialogCallback() {
                            @Override
                            public void onPositiveClick() {
                               // SessionManager.getInstance().setRegisterLaterFlow(true);
                                Intent intent = new Intent(requireContext(), LoginActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });
            }


        });

        binding.showMore.setOnClickListener((view) -> {
            startActivity(new Intent(requireContext(), AdsActivity.class));
        });

        binding.imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(requireContext(), SearchMainActivity.class));
            }
        });
    }

    private void showAddressDialog() {

        dialogAddressBinding.addressView.setLayoutManager(new LinearLayoutManager(requireContext()));

        adapter = new AddressListingAdapter(true,ShoppingFragment.this,requireContext(), addressList, new AddressSelection() {
            @Override
            public void addressSelected(int position) {
                if(iscartempty == true){
                    sharedHelper.setSelectedDeliveryToAddress(addressList.get(position).getAddressPinDetails());
                    sharedHelper.setSelectedDeliveryToTag(addressList.get(position).getAddressType());
                    sharedHelper.setSelectedDeliveryToLat(addressList.get(position).getLatitude());
                    sharedHelper.setSelectedDeliveryToLng(addressList.get(position).getLongitude());
                    sharedHelper.setSelectedDeliveryToId(String.valueOf(addressList.get(position).getId()));
                    dialogAddressBinding.viewMyLocation.setBackground(getResources().getDrawable(R.drawable.address_unselected_bg));
                    binding.address.setText(addressList.get(position).getAddressType());
                    avalue = addressList.get(position).getAddressType();
                    callApiFromMyLocation = false;
                    addressDialog.dismiss();
                    getDashBoardDetails();
                }
                else {
                    DialogUtils.showAlertDialogWithCancel(getContext(), getString(R.string.cartempty), getString(R.string.alert), getString(R.string.ok),getString(R.string.cancel),
                            new DialogCallback() {
                                @Override
                                public void onPositiveClick() {
                                    shoppingViewModel.getclearcartData(requireContext()).observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                        @Override
                                        public void onChanged(CommonResponse listMyAddressResponse) {
                                            if (!listMyAddressResponse.getError()) {
                                                Log.d("cxvdxv",""+listMyAddressResponse.getError());
                                            }
                                        }
                                    });
                                    callApiFromMyLocation = false;
                                    sharedHelper.setSelectedDeliveryToAddress(addressList.get(position).getAddressPinDetails());
                                    sharedHelper.setSelectedDeliveryToTag(addressList.get(position).getAddressType());
                                    sharedHelper.setSelectedDeliveryToLat(addressList.get(position).getLatitude());
                                    sharedHelper.setSelectedDeliveryToLng(addressList.get(position).getLongitude());
                                    sharedHelper.setSelectedDeliveryToId(String.valueOf(addressList.get(position).getId()));
                                    dialogAddressBinding.viewMyLocation.setBackground(getResources().getDrawable(R.drawable.address_unselected_bg));
                                    binding.address.setText(addressList.get(position).getAddressType());
                                    avalue = addressList.get(position).getAddressType();
                                    addressDialog.dismiss();
                                    getDashBoardDetails();
                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });
                }

            }

            @Override
            public void addressEdit(int position) {

                addressDialog.dismiss();

                Intent intent = new Intent(requireContext(), EditAddressActivity.class);
                intent.putExtra("data", (Parcelable) addressList.get(position));
                requireContext().startActivity(intent);

            }

            @Override
            public void addressDelete(int position) {

                Log.d("dcbxhdj",""+sharedHelper.getSelectedDeliveryToId());
                Log.d("dcxjcbxhdj",""+String.valueOf(addressList.get(position).getId()));
                if (sharedHelper.getSelectedDeliveryToId().equalsIgnoreCase(String.valueOf(addressList.get(position).getId()))) {
                    DialogUtils.showAlertDialog(requireContext(), getString(R.string.cur_Ads), getString(R.string.oops), getString(R.string.ok),
                            new DialogCallback() {
                                @Override
                                public void onPositiveClick() {
                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });
                } else {
                    DialogUtils.showLoader(requireContext());
                    shoppingViewModel.deleteAddress(requireContext(), String.valueOf(addressList.get(position).getId())).observe(getViewLifecycleOwner(), commonResponse -> {
                        DialogUtils.dismissLoader();
                        if (commonResponse.getError()) {
                            Toast.makeText(requireContext(), commonResponse.getMessage(), Toast.LENGTH_LONG).show();
//                            Utils.showSnack(dialogAddressBinding.parent, commonResponse.getMessage());
                        } else {
                            addressList.remove(position);
                            adapter.notifyDataChanged(addressList);
                            setAvailable();
                            showAddressDialog();
                        }
                    });
                }

            }
        }, true);


        dialogAddressBinding.addressView.setAdapter(adapter);

        if (currentAddress.equals("")) {
            dialogAddressBinding.currentLocationAddress.setText(getString(R.string.enable_location));
        } else {
            dialogAddressBinding.currentLocationAddress.setText(currentAddress);
        }

        if (addressList.size() == 0) {
            dialogAddressBinding.addressView.setVisibility(View.GONE);
        } else {
            dialogAddressBinding.addressView.setVisibility(View.VISIBLE);
        }

        if(sharedHelper.getSelectedDeliveryToTag().equals("")){
            dialogAddressBinding.viewMyLocation.setBackground(getResources().getDrawable(R.drawable.my_address_bg));
        }

        dialogAddressBinding.viewMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("xvbxcv",""+sharedHelper.getSelectedDeliveryToTag());
                if(!sharedHelper.getSelectedDeliveryToTag().equals("")){
                    if (iscartempty == true) {
                        callApiFromMyLocation = true;
                        sharedHelper.setSelectedDeliveryToAddress("");
                        sharedHelper.setSelectedDeliveryToTag("");
                        sharedHelper.setSelectedDeliveryToId("");
                        avalue = "mylocation";
                        askLocationPermission();
                        dialogAddressBinding.viewMyLocation.setBackground(getResources().getDrawable(R.drawable.my_address_bg));
                        addressDialog.dismiss();
                    }
                    else {
                        DialogUtils.showAlertDialogWithCancel(getContext(), getString(R.string.cartempty), getString(R.string.alert), getString(R.string.ok), getString(R.string.cancel),
                                new DialogCallback() {
                                    @Override
                                    public void onPositiveClick() {
                                        shoppingViewModel.getclearcartData(requireContext()).observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                            @Override
                                            public void onChanged(CommonResponse listMyAddressResponse) {
                                                if (!listMyAddressResponse.getError()) {
                                                    Log.d("cxvdxv", "" + listMyAddressResponse.getError());
                                                }
                                            }
                                        });
                                        callApiFromMyLocation = true;
                                        sharedHelper.setSelectedDeliveryToAddress("");
                                        sharedHelper.setSelectedDeliveryToTag("");
                                        sharedHelper.setSelectedDeliveryToId("");
                                        avalue = "mylocation";
                                        askLocationPermission();
                                        dialogAddressBinding.viewMyLocation.setBackground(getResources().getDrawable(R.drawable.my_address_bg));
                                        addressDialog.dismiss();
                                    }

                                    @Override
                                    public void onNegativeClick() {

                                    }
                                });

                    }
                }
                else {
                    addressDialog.dismiss();
                }
            }
        });

        dialogAddressBinding.otherLocationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addressDialog.dismiss();

                startActivity(new Intent(requireContext(), SelectStartAddressActivity.class));
            }
        });

        addressDialog.show();
    }

    private void initView() {

        dialogAddressBinding = DataBindingUtil.inflate(
                LayoutInflater.from(requireContext()),
                R.layout.dialog_address,
                null,
                false);

        addressDialog = DialogUtils.getAddressDialog(requireContext(), dialogAddressBinding);

    }

    private void getDashBoardDetails() {

       // DialogUtils.showLoader(requireContext());
        shoppingViewModel.getDashBoardDetails(requireContext(),sharedHelper.getSelectedDeliveryToTag()).observe(getViewLifecycleOwner(), new Observer<DashboardResponse>() {
            @Override
            public void onChanged(DashboardResponse dashboardResponse) {
               // DialogUtils.dismissLoader();
                if (dashboardResponse.getError()) {
                    //Utils.showSnack(binding.parentLayout, dashboardResponse.getMessage());
                } else {
                    if(Constants.Value.first == true && dashboardResponse.getData().getApp_version().get(0).getAndroid_ver() != null){
                        Constants.Value.first = false;
                        Constants.Value.api_appversion = dashboardResponse.getData().getApp_version().get(0).getAndroid_ver();
                        String currentVersion = "";
                        try {
                            currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        if(!Constants.Value.api_appversion.equals("0")){
                            if(Float.valueOf(currentVersion) < Float.valueOf(Constants.Value.api_appversion)) {
                                versiondialog();
                            }
                        }
                    }

                    initAdapter(dashboardResponse.getData());

                    if (dashboardResponse.getData().getStores().size() == 0) {
                        callUnavailableProduct();
                        binding.group2.setVisibility(View.VISIBLE);
                    } else {
                        binding.group2.setVisibility(View.GONE);
                    }

                    checkForRating(dashboardResponse.getData().getRating());
                }

            }

        });
    }

    public void versiondialog(){
        //show anything
        //Uncomment the below code to Set the message and title from the strings.xml file
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //Setting message manually and performing action on button click
        builder.setMessage(getString(R.string.newversionisavailable))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                        dialog.cancel();
                        Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                        myWebLink.setData(Uri.parse("https://www.a3nab.com/"));
//                        myWebLink.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.app.a3naab"));
                        startActivity(myWebLink);
                        getActivity().finish();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("");
        alert.show();
    }


    private void checkForRating(ArrayList<DashboardResponse.Rating> rating) {

        if (rating.size() != 0) {
            ratingDialog = DialogUtils.getRating(requireContext(),rating.get(0).getOrderIds(), new OnRatingSaved() {
                @Override
                public void onSaved(Float app, Float driver, Float product, String comments) {
                    DialogUtils.showLoader(requireContext());
                    shoppingViewModel.setRating(requireContext(), app, driver, product, comments, rating.get(0).getId()).observe(getViewLifecycleOwner(), commonResponse -> {
                        DialogUtils.dismissLoader();
                        if (!commonResponse.getError()) {
                            ratingDialog.dismiss();
                        }
                    });
                }
            });
        }

    }


    private void callUnavailableProduct() {

        shoppingViewModel.updateUnAvailableProduct(requireContext(), sharedHelper.getLastKnownLat(), sharedHelper.getLastKnownLng(), sharedHelper.getLastKnownAddress(), sharedHelper.getUserId());
    }

    private void initAdapter(DashboardResponse.DashboardData data) {


        if (data.getCartDetails() != null && !data.getCartDetails().isError()) {
            ((DashboardActivity) getActivity()).setCount(data.getCartDetails().getCartCount());
        }

        ArrayList<DashboardResponse.BannerData> list = new ArrayList<>();
        for (int i=0;i<data.getBannerData().size();i++){
            if(!data.getBannerData().get(i).getImage().equals("")){
                list.add(data.getBannerData().get(i));
            }
        }

        binding.adsView.setAdapter(new AdsHomeAdapter(requireContext(), list));
        binding.indicator2.setViewPager2(binding.adsView);

//        SessionManager.getInstance().setBestProduct(data.getBestProducts());
        SessionManager.getInstance().setCategoryStore(data.getStores());
     //   SessionManager.getInstance().setBannerData(data.getBannerData());

        ArrayList<ArrayList<DashboardResponse.Categories>> adapterList = new ArrayList<>();
        ArrayList<DashboardResponse.Categories> nestedList = null;


        if (data.getStores().size() == 1) {
            nestedList = new ArrayList<>();
            nestedList.add(data.getStores().get(0));
            adapterList.add(nestedList);
        } else {
            for (int i = 0; i < data.getStores().size(); i++) {
                if (i == 0 || i == 1) {
                    if (nestedList == null) {
                        nestedList = new ArrayList<>();
                    }
                    nestedList.add(data.getStores().get(i));
                    if (nestedList.size() == 2) {
                        adapterList.add(nestedList);
                        nestedList = new ArrayList<>();
                    }
                } else {

                    if (nestedList.size() == 3) {
                        nestedList = new ArrayList<>();
                    }
                    nestedList.add(data.getStores().get(i));

                    if (nestedList.size() == 3 || i == data.getStores().size() - 1) {
                        adapterList.add(nestedList);
                    }

                }

            }
        }

        binding.categoryView.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.categoryView.setAdapter(new CategoryHomeAdapter(requireContext(), adapterList));

        bestAdapter = new BestProductAdapter(requireContext(), data.getBestProducts(), new OnItemClick() {
            @Override
            public void onClick(int position) {
                if(data.getBestProducts().get(position).getBoxStyle().size()!=0 || data.getBestProducts().get(position).getCuttingStyle().size()!=0){
                    if(checkiscart(data.getBestProducts().get(position).getId(),Integer.parseInt(data.getBestProducts().get(position).getStoreId()))){
                        DialogUtils.showLoader(requireContext());
                        productViewModel.addtoCartWithStyle(requireContext(), data.getBestProducts().get(position).getId(), data.getBestProducts().get(position).getStoreId(), cutid, boxid, sinstruction)
                                .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyAddData(position);
                                            getCartDetails();
                                        }
                                    }
                                });
                    }
                    else {
                        DialogUtils.showLoader(requireContext());
                        productViewModel.addItemToCart(requireContext(), data.getBestProducts().get(position).getId(), data.getBestProducts().get(position).getStoreId())
                                .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyAddData(position);
                                            getCartDetails();
                                        }
                                    }
                                });
                    }
                }
                else {
                    DialogUtils.showLoader(requireContext());
                    productViewModel.addItemToCart(requireContext(), data.getBestProducts().get(position).getId(), data.getBestProducts().get(position).getStoreId())
                            .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        bestAdapter.notifyAddData(position);
                                        getCartDetails();
                                    }
                                }
                            });
                }
            }
        }, new OnItemClick() {
            @Override
            public void onClick(int position) {
                if(data.getBestProducts().get(position).getBoxStyle().size()!=0 || data.getBestProducts().get(position).getCuttingStyle().size()!=0){
                    if(checkiscart(data.getBestProducts().get(position).getId(),Integer.parseInt(data.getBestProducts().get(position).getStoreId()))){

                        DialogUtils.showLoader(requireContext());
                        productViewModel.removeItemToCartwithstyle(requireContext(),  data.getBestProducts().get(position).getId(), data.getBestProducts().get(position).getStoreId(), cutid, boxid, sinstruction)
                                .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyRemoveData(position);
                                            getCartDetails();
                                        }
                                    }
                                });
                    }
                    else {
                        DialogUtils.showLoader(requireContext());
                        productViewModel.removeItemToCart(requireContext(), data.getBestProducts().get(position).getId(), data.getBestProducts().get(position).getStoreId())
                                .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyRemoveData(position);
                                            getCartDetails();
                                        }

                                    }
                                });
                    }
                }
                else {
                    DialogUtils.showLoader(requireContext());
                    productViewModel.removeItemToCart(requireContext(), data.getBestProducts().get(position).getId(), data.getBestProducts().get(position).getStoreId())
                            .observe(getViewLifecycleOwner(), new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        bestAdapter.notifyRemoveData(position);
                                        getCartDetails();
                                    }

                                }
                            });
                }
            }
        });

        binding.bestProduct1.setLayoutManager(new LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false));
        binding.bestProduct1.setAdapter(bestAdapter);


    }


    private void locationListener() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval((20 * 1000));

        locationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        getLastKnownLocation();
                    }
                }

            }

        };


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void getLastKnownLocation() {


        if ((!isDetached()) && isVisible()) {
            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            fusedLocationClient.getLastLocation().addOnSuccessListener(requireActivity(), new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if (location != null) {
                        currentLat = String.valueOf(location.getLatitude());
                        currentLng = String.valueOf(location.getLongitude());

                        sharedHelper.setLastKnownLat(currentLat);
                        sharedHelper.setLastKnownLng(currentLng);

                        if (sharedHelper.getLastKnownLat().equalsIgnoreCase("")) {
                            Log.d("zchjhzbcz","lastlng null");
                            getDashBoardDetails();

                        }


                        searchPlaces();

                    } else {
                        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        fusedLocationClient.requestLocationUpdates(
                                locationRequest,
                                locationCallback,
                                Looper.getMainLooper()
                        );
                    }

                }
            });
        }


    }


    private void askLocationPermission() {
      /*  ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CALL_PHONE},
                100);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                        Constants.Permissions.LOCATIONPERMISSION_LIST,
                        Constants.Permissions.LOCATIONREQUEST_CODE
                );

            } else {
                getUserLocation();
            }
        } else {
            getUserLocation();
        }
    }

    private void getUserLocation() {
        new GpsUtils(requireActivity()).turnGPSOn(isGPSEnable -> {
            if (isGPSEnable) {
                getLastKnownLocation();
            }
        });
    }

    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                getLastKnownLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.Permissions.LOCATIONREQUEST_CODE) {
            if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation();
            }
        }

    }

    private void searchPlaces() {
        String url = URLHelper.getAddress(currentLat, currentLng);
        InputForAPI inputForAPI = new InputForAPI(requireContext(),url);
        inputForAPI.setUrl(url);

        if (!isDetached())
            mapViewModel.getAddress(requireContext(), inputForAPI).observe(getViewLifecycleOwner(), new Observer<AutoCompleteResponse>() {
                @Override
                public void onChanged(AutoCompleteResponse autoCompleteResponse) {
                    if (!autoCompleteResponse.getError()) {
                        getAddressFromResponse(autoCompleteResponse.getPredictions());
                        Log.d("xnvbxncvxccv","callapifromlocation not call");
                        if (callApiFromMyLocation == true || sharedHelper.getSelectedDeliveryToTag().equals("")) {
                            Log.d("xnvbxncv","callapifromlocation true");
                            /*DialogUtils.showLoader(getContext());
                            shoppingViewModel.getCurrentAddressId(getContext(), sharedHelper.getLastKnownAddress(),
                                    sharedHelper.getLastKnownLat(), sharedHelper.getLastKnownLng()).observe(getViewLifecycleOwner(), new Observer<CurrentLocationIdResponse>() {
                                @Override
                                public void onChanged(CurrentLocationIdResponse currentLocationIdResponse) {
                                    DialogUtils.dismissLoader();
                                    if (currentLocationIdResponse.getError()) {

                                    } else {
                                        if (currentLocationIdResponse.getData().getAddressId() != null && !currentLocationIdResponse.getData().getAddressId().equalsIgnoreCase("")) {
                                            sharedHelper.setSelectedDeliveryToAddress(sharedHelper.getLastKnownAddress());
                                            sharedHelper.setSelectedDeliveryToId(String.valueOf(currentLocationIdResponse.getData().getAddressId()));
                                        }
                                    }
                                }
                            });*/
                            getDashBoardDetails();
                        }
                    }
                }
            });

    }

    private void getAddressFromResponse(List<Prediction> value) {
        if (value.size() != 0) {

            if (value.get(0).getAddressComponents() != null && value.get(0).getAddressComponents().size() != 0) {

                if (value.get(0).getAddressComponents().size() >= 2) {
                    currentAddress = value.get(0).getAddressComponents().get(0).getLong_name()
                            + "," + value.get(0).getAddressComponents().get(1).getLong_name();
                } else if (value.get(0).getAddressComponents().size() >= 1) {
                    currentAddress = value.get(0).getAddressComponents().get(0).getLong_name();
                }

                sharedHelper.setLastKnownAddress(currentAddress);

                Log.d("cvgxbc",""+sharedHelper.getSelectedDeliveryToTag());
                if (sharedHelper.getSelectedDeliveryToTag().equals("")) {
                    Log.d("xdlkkclnvbxncv","get current locaion id generate true");
                    binding.address.setText(currentAddress);
                    sharedHelper.setSelectedDeliveryToLat(currentLat);
                    sharedHelper.setSelectedDeliveryToLng(currentLng);
                    DialogUtils.showLoader(getContext());
                    shoppingViewModel.getCurrentAddressId(getContext(), sharedHelper.getLastKnownAddress(),
                            sharedHelper.getLastKnownLat(), sharedHelper.getLastKnownLng()).observe(getViewLifecycleOwner(), new Observer<CurrentLocationIdResponse>() {
                        @Override
                        public void onChanged(CurrentLocationIdResponse currentLocationIdResponse) {
                            DialogUtils.dismissLoader();
                            if (currentLocationIdResponse.getError()) {

                            } else {
                                if (currentLocationIdResponse.getData().getAddressId() != null && !currentLocationIdResponse.getData().getAddressId().equalsIgnoreCase("")) {
                                    sharedHelper.setSelectedDeliveryToAddress(sharedHelper.getLastKnownAddress());
                                    sharedHelper.setSelectedDeliveryToId(String.valueOf(currentLocationIdResponse.getData().getAddressId()));
                                }
                            }
                        }
                    });
                }

            } else {
                Log.d("xdlkkclnvbxncv","get current locaion id generate false1");

                if (sharedHelper.getSelectedDeliveryToTag().equals("")) {
                    Log.d("xdlkkclnvbxncv","get current locaion id generate false01");
                    binding.address.setText(getString(R.string.current_location));
                    sharedHelper.setSelectedDeliveryToLat(currentLat);
                    sharedHelper.setSelectedDeliveryToLng(currentLng);
                }
            }

        } else {
            Log.d("xdlkkclnvbxncv","get current locaion id generate false2");

            if (sharedHelper.getSelectedDeliveryToTag().equals("")) {
                Log.d("xdlkkclnvbxncv","get current locaion id generate false02");
                binding.address.setText(getString(R.string.current_location));
                sharedHelper.setSelectedDeliveryToLat(currentLat);
                sharedHelper.setSelectedDeliveryToLng(currentLng);
            }
        }
    }

    private void setAddress() {

        if (addressList != null && addressList.size() != 0) {
            if (sharedHelper.getIsAddAddress()) {
                sharedHelper.setIsAddAddress(false);
                sharedHelper.setSelectedDeliveryToId(String.valueOf(addressList.get(0).getId()));
                sharedHelper.setSelectedDeliveryToTag(addressList.get(0).getAddressType());
                sharedHelper.setSelectedDeliveryToAddress(addressList.get(0).getAddressPinDetails());
                sharedHelper.setSelectedDeliveryToLat(addressList.get(0).getLatitude());
                sharedHelper.setSelectedDeliveryToLng(addressList.get(0).getLongitude());
                binding.address.setText(sharedHelper.getSelectedDeliveryToTag());
            } else {
                for (int i = 0; i < addressList.size(); i++) {
                    if (sharedHelper.getSelectedDeliveryToId().equals(String.valueOf(addressList.get(i).getId()))) {
                        sharedHelper.setSelectedDeliveryToAddress(addressList.get(i).getAddressPinDetails());
                        sharedHelper.setSelectedDeliveryToTag(addressList.get(i).getAddressType());
                        sharedHelper.setSelectedDeliveryToLat(addressList.get(i).getLatitude());
                        sharedHelper.setSelectedDeliveryToLng(addressList.get(i).getLongitude());
                        binding.address.setText(sharedHelper.getSelectedDeliveryToTag());
                    }
                }
            }


           /* if (!sharedHelper.getSelectedDeliveryToTag().equals(""))
                binding.address.setText(sharedHelper.getSelectedDeliveryToTag());*/
        }
        else {
           // askLocationPermission();
        }

    }

}
