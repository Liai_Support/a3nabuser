package com.app.a3naab.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivitySelectAddressBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.AutoCompleteResponse;
import com.app.a3naab.model.Prediction;
import com.app.a3naab.network.InputForAPI;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.utils.Constants;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.GpsUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.viewmodel.MapViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;

public class SelectAddressActivity extends BaseActivity implements OnMapReadyCallback {

    ActivitySelectAddressBinding binding;
    MapViewModel mapViewModel;

    GoogleMap map;
    MapFragment mapFragment;

    Double selectedLat = 0.0, selectedLng = 0.0;
    Double oldLat = 0.0, oldLng = 0.0;


    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;

    private boolean isEdit = false;
    private Boolean isSaudiArea = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_address);
        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getIntentValues();
        initListener();
        locationListener();
        askLocationPermission();

        moveToCurrentLocation();
        setCurrentLocation();
    }

    private void getIntentValues() {

        if (getIntent().getExtras() != null) {

            isEdit = getIntent().getExtras().getBoolean("isEdit");

            if (getIntent().getExtras().getString("lat") != null && getIntent().getExtras().getString("lng") != null) {
                if (!getIntent().getExtras().getString("lat").equalsIgnoreCase("")) {
                    oldLat = Double.parseDouble(getIntent().getExtras().getString("lat"));
                }

                if (!getIntent().getExtras().getString("lng").equalsIgnoreCase("")) {
                    oldLng = Double.parseDouble(getIntent().getExtras().getString("lng"));
                }
            }

        }


    }

    private void initListener() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLastKnownLocation();
            }
        });

        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isSaudiArea) {
                    if (selectedLat != 0.0 && selectedLng != 0.0
                            && (!binding.textView12.getText().toString().equals(getString(R.string.select_pin_address_details)))) {
                        Intent intent = new Intent();
                        intent.putExtra("latitude", String.valueOf(selectedLat));
                        intent.putExtra("longitude", String.valueOf(selectedLng));
                        intent.putExtra("address", binding.textView12.getText().toString());
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                }else {
                    DialogUtils.showAlertDialog(SelectAddressActivity.this,  getString(R.string.no_service),getString(R.string.oops), getString(R.string.ok), new DialogCallback() {
                        @Override
                        public void onPositiveClick() {

                        }

                        @Override
                        public void onNegativeClick() {

                        }
                    });
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;


        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                selectedLat = map.getCameraPosition().target.latitude;
                selectedLng = map.getCameraPosition().target.longitude;

                searchPlaces(String.valueOf(selectedLat), String.valueOf(selectedLng));
            }
        });
        moveToCurrentLocation();

        setCurrentLocation();

    }

    private void setCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (map != null)
            try {
                map.setMyLocationEnabled(true);
            } catch (SecurityException e) {
                Log.e("Exception: %s", e.getMessage());
            }
    }

    private void moveToCurrentLocation() {
        setCurrentLocation();
        if (isEdit) {
            if (map != null && oldLat != 0.0 && oldLng != 0.0) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(oldLat, oldLng), 15.0f));
            }
        } else {
            if (map != null && oldLat != 0.0 && oldLng != 0.0) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(oldLat, oldLng), 15.0f));
            } else if (map != null && selectedLat != 0.0 && selectedLng != 0.0) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(selectedLat, selectedLng), 15.0f));
            }
        }


    }

    private void searchPlaces(String lat, String lng) {
        String url = URLHelper.getAddress(lat, lng);
        InputForAPI inputForAPI = new InputForAPI(this,url);
        inputForAPI.setUrl(url);

        mapViewModel.getAddress(this, inputForAPI).observe(this, new Observer<AutoCompleteResponse>() {
            @Override
            public void onChanged(AutoCompleteResponse autoCompleteResponse) {
                if (!autoCompleteResponse.getError()) {
                    getAddressFromResponse(autoCompleteResponse.getPredictions());
                }
            }
        });

    }

    private void getAddressFromResponse(List<Prediction> value) {


        if (value.size() != 0) {
            binding.textView12.setText(value.get(0).getFormatted_address());

            for (int i = 0; i < value.get(0).getAddressComponents().size(); i++) {
                if (value.get(0).getAddressComponents().get(i).getLong_name().equalsIgnoreCase("Saudi Arabia")) {
                    isSaudiArea = true;
                    break;
                } else {
                    isSaudiArea = false;
                }
            }
        }

//        if (value.size() != 0) {
//
//            if (value.get(0).getAddressComponents() != null && value.get(0).getAddressComponents().size() != 0) {

//                currentAddress = value.get(0).getAddressComponents().get(0).getLong_name();
//                if (sharedHelper.getSelectedDeliveryToTag().equals(""))
//                    binding.address.setText(currentAddress);

//            } else {
//                binding.address.setText(getString(R.string.current_location));
//            }

//        } else {
//            binding.address.setText(getString(R.string.current_location));
//        }
    }


    private void locationListener() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval((20 * 1000));

        locationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        getLastKnownLocation();
                    }
                }

            }

        };


    }


    private void getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {

                    selectedLat = location.getLatitude();
                    selectedLng = location.getLongitude();

                    moveToCurrentLocation();
//                    currentLat = String.valueOf(location.getLatitude());
//                    currentLng = String.valueOf(location.getLongitude());
//
//                    sharedHelper.setLastKnownLat(currentLat);
//                    sharedHelper.setLastKnownLng(currentLng);
//
//                    searchPlaces(currentLat, currentLng);

                } else {
                    if (ActivityCompat.checkSelfPermission(SelectAddressActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SelectAddressActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    fusedLocationClient.requestLocationUpdates(
                            locationRequest,
                            locationCallback,
                            Looper.getMainLooper()
                    );
                }

            }
        });

    }


    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                        Constants.Permissions.LOCATIONPERMISSION_LIST,
                        Constants.Permissions.LOCATIONREQUEST_CODE
                );

            } else {
                getUserLocation();
            }
        } else {
            getUserLocation();
        }
    }

    private void getUserLocation() {
        new GpsUtils(this).turnGPSOn(isGPSEnable -> {
            if (isGPSEnable) {
                getLastKnownLocation();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                getLastKnownLocation();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.Permissions.LOCATIONREQUEST_CODE) {
            if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation();
            }
        }

    }

}
