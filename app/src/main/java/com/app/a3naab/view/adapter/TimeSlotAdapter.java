package com.app.a3naab.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildTimeSlotBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.DeliveryTimeData;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.DeliveryTimeSelectionActivity;
import com.app.a3naab.view.activity.RescheduleOrderActivity;

import java.util.ArrayList;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.MyViewHolder> {

    Context context;
    //DeliveryTimeSelectionActivity activity;
    ArrayList<DeliveryTimeData> slotData;
    OnItemClick onItemClick;
    Boolean isToday;

    public TimeSlotAdapter(Context context, ArrayList<DeliveryTimeData> slotData, OnItemClick onItemClick, Boolean isToday) {
        this.context = context;
        this.slotData = slotData;
        this.onItemClick = onItemClick;
        this.isToday = isToday;
    }

    @NonNull
    @Override
    public TimeSlotAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_time_slot, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull TimeSlotAdapter.MyViewHolder holder, int position) {

        holder.binding.time.setText(Utils.convertDate(slotData.get(position).getFromTime(), "HH:mm:ss", "hh:mm a") + "-" + Utils.convertDate(slotData.get(position).getToTime(), "HH:mm:ss", "hh:mm a"));


        if (slotData.get(position).isSelected()) {

            holder.binding.time.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_red));
            holder.binding.amount.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_red));

            holder.binding.view18.setBackground(ContextCompat.getDrawable(context, R.drawable.rectangle_corner_bg_primary));

        } else {

            holder.binding.time.setTextColor(ContextCompat.getColor(context, R.color.header_text_color));
            holder.binding.amount.setTextColor(ContextCompat.getColor(context, R.color.header_text_color));

            holder.binding.view18.setBackground(ContextCompat.getDrawable(context, R.drawable.rectangle_corner_not_selected_bg));

        }

        if (isToday) {
            if (Utils.isTimeEnabled(slotData.get(position).getFromTime()) && (Integer.parseInt(slotData.get(position).getMaxOrder()) > Integer.parseInt(slotData.get(position).getUsedCount()))) {
                holder.binding.time.setAlpha(1f);
                holder.binding.amount.setAlpha(1f);
                //hari
               /* activity.isfastdeliverytimeavailable++;
                if(activity.isfastdeliverytimeavailable == 1){
                    activity.deliveryTimeId = slotData.get(position).getId();
                    activity.selectedtime =
                            Utils.convertDate(slotData.get(position).getFromTime(), "HH:mm:ss", "hh:mm a")
                                    + "-" +
                                    Utils.convertDate(slotData.get(position).getToTime(), "HH:mm:ss", "hh:mm a");
                }*/

            } else {

                holder.binding.time.setAlpha(0.3f);
                holder.binding.amount.setAlpha(0.3f);

            }
        }
        else {
            if ((Integer.parseInt(slotData.get(position).getMaxOrder()) > Integer.parseInt(slotData.get(position).getUsedCount()))) {
                holder.binding.time.setAlpha(1f);
                holder.binding.amount.setAlpha(1f);
            } else {
                holder.binding.time.setAlpha(0.3f);
                holder.binding.amount.setAlpha(0.3f);
            }
        }

        holder.binding.view18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isToday == true) {
                    if (Utils.isTimeEnabled(slotData.get(position).getFromTime())) {
                        if(Integer.parseInt(slotData.get(position).getMaxOrder()) > Integer.parseInt(slotData.get(position).getUsedCount())) {
                            setSelected(position);
                        }
                        else {
                            Utils.showSnack(holder.binding.view18, context.getString(R.string.ordercountisfull));
                        }
                    }
                    else {
                        Utils.showSnack(holder.binding.view18, context.getString(R.string.timeexpired));
                    }
                }
                else {
                    if(Integer.parseInt(slotData.get(position).getMaxOrder()) > Integer.parseInt(slotData.get(position).getUsedCount())) {
                        setSelected(position);
                    }
                    else {
                        Utils.showSnack(holder.binding.view18, context.getString(R.string.ordercountisfull));
                    }
                }
            }
        });

    }

    public void setSelected(int position) {
        onItemClick.onClick(position);
        for (int i = 0; i < slotData.size(); i++) {
            slotData.get(i).setSelected(position == i);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return slotData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildTimeSlotBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildTimeSlotBinding.bind(itemView);
        }

    }

    private void setTimeDisabled() {

    }
}
