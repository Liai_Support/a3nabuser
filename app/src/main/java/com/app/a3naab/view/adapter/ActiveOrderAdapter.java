package com.app.a3naab.view.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildActiveOrdersBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.CheckReorderResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.OrderListResponse;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.RescheduleOrderActivity;
import com.app.a3naab.view.fragment.MyOrderFragment;

import java.util.ArrayList;
import java.util.Locale;

public class ActiveOrderAdapter extends RecyclerView.Adapter<ActiveOrderAdapter.MyViewHolder> {

    Context context;
    ArrayList<OrderListResponse.OrderListData> slotData;
    OnItemClick onItemClick;
    MyOrderFragment myOrderFragment;
    SharedHelper sharedHelper;



    public ActiveOrderAdapter(MyOrderFragment myOrderFragment,Context context, ArrayList<OrderListResponse.OrderListData> slotData, OnItemClick onItemClick) {
        this.context = context;
        this.slotData = slotData;
        this.onItemClick = onItemClick;
        this.myOrderFragment = myOrderFragment;

    }

    @NonNull
    @Override
    public ActiveOrderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        sharedHelper = new SharedHelper(context);
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_active_orders, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ActiveOrderAdapter.MyViewHolder holder, int position) {


        holder.binding.orderId.setText(context.getString(R.string.order_id) + " " + slotData.get(position).getOrderIDs());

        if (slotData.get(position).getOrderOn() != null) {
            holder.binding.orderDate.setText(context.getString(R.string.order_placed) + "\n" + Utils.getConvertedTime(slotData.get(position).getOrderOn()) + "\n" +Utils.convertDateutc(slotData.get(position).getOrderOn(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
        }

        if (slotData.get(position).getDeliveryDate() != null) {
            holder.binding.deliveryDate.setVisibility(View.VISIBLE);
            holder.binding.deliveryDateImage.setVisibility(View.VISIBLE);
            if(slotData.get(position).getFastDelivery().equals("1")){
                holder.binding.deliveryDate.setText(context.getString(R.string.delivery_by) + "\n" + context.getString(R.string.fast_delivery));
            }
            else {
                String selectedtime = Utils.convertDate(slotData.get(position).getDeliveryOn_fromTime(), "HH:mm:ss", "hh:mm a")
                        + "-" +
                        Utils.convertDate(slotData.get(position).getDeliveryOn_toTime(), "HH:mm:ss", "hh:mm a");
                holder.binding.deliveryDate.setText(context.getString(R.string.delivery_by) + "\n" + Utils.getConvertedTime(slotData.get(position).getDeliveryDate()) + "\n" + selectedtime);
            }

        } else {
            holder.binding.deliveryDate.setVisibility(View.GONE);
            holder.binding.deliveryDateImage.setVisibility(View.GONE);
        }

        //setOrderView(slotData.get(position).getOrderStatus(), holder);
        Double cbstyleamount = Double.parseDouble(slotData.get(position).getOtherTotal());
        Double subtotal = (cbstyleamount)+(Double.parseDouble(slotData.get(position).getTotalAmount()) - Double.parseDouble(slotData.get(position).getDiscountAmount()));
        Double delivery = Double.parseDouble(slotData.get(position).getFastDelievryCharge());
        //Double subtotal1 = (subtotal-Double.parseDouble(slotData.get(position).getTakeToCustomer()))+Double.parseDouble(slotData.get(position).getGiveToCustomer())+delivery;
        if(slotData.get(position).getOff_types().equalsIgnoreCase("0")){
            delivery = Double.parseDouble(slotData.get(position).getCouponDiscount());
        }
        Double subtotal1 = subtotal+delivery;
        Double coupondiscount = Double.parseDouble(slotData.get(position).getCouponDiscount());
        Double pointamount = Double.parseDouble(slotData.get(position).getPointsAmount());
        Double walletamount = Double.parseDouble(slotData.get(position).getPaidByWallet());
        Double vat = ((subtotal1-(coupondiscount+pointamount+walletamount)) * (Double.parseDouble(slotData.get(position).getTaxvalue()) / 100));
        Double total = ((subtotal1-(coupondiscount+pointamount+walletamount))+vat);
        if(total < 0) {
            total = 0.0;
        }

        holder.binding.totalAmount.setText("SAR " +String.format(Locale.ENGLISH,"%.2f",total));


        if(slotData.get(position).getType().equalsIgnoreCase("CASH")){
            holder.binding.paymentType.setText(""+context.getString(R.string.codmachine));

        }
        else {
            holder.binding.paymentType.setText(slotData.get(position).getType());
        }

        holder.binding.reschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogUtils.showLoader(context);
                myOrderFragment.shoppingViewModel.checkreorder(context, slotData.get(position).getId()).observe(myOrderFragment, new Observer<CheckReorderResponse>() {
                    @Override
                    public void onChanged(CheckReorderResponse commonResponse) {
                        DialogUtils.dismissLoader();
                        if (!commonResponse.getError()) {
                            CartSessionManager.removeAllValues();
                            CartSessionManager.getInstance().setOrderId(String.valueOf(slotData.get(position).getId()));

                            context.startActivity(new Intent(context, RescheduleOrderActivity.class));
                        }
                        else {
                            DialogUtils.showAlertDialog(context,  context.getString(R.string.storenotavailable),context.getString(R.string.oops), context.getString(R.string.ok), new DialogCallback() {
                                @Override
                                public void onPositiveClick() {

                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });                        //commonResponse.getMessage()
                        }
                    }
                });




            }
        });

        holder.binding.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onClick(position);
            }
        });


        if(slotData.get(position).getIsApproved().equalsIgnoreCase("ACCEPTED")){
            holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirmed));

            holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag));

            holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck));
            if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_truck_arabic_white));
            }
            holder.binding.reschedule.setVisibility(View.VISIBLE);
            holder.binding.linear.setVisibility(View.VISIBLE);
            holder.binding.groupLoad.setVisibility(View.VISIBLE);
            holder.binding.linear1.setVisibility(View.VISIBLE);
            holder.binding.rela1.setVisibility(View.GONE);
            if(slotData.get(position).getAcceptByStore().equalsIgnoreCase("1")){

                holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirmed));

                holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag_green));

                holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
                holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
                holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck));
                if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                    holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_truck_arabic_white));
                }
                holder.binding.reschedule.setVisibility(View.GONE);
                holder.binding.linear.setVisibility(View.VISIBLE);
                holder.binding.linear1.setVisibility(View.GONE);
                holder.binding.rela1.setVisibility(View.VISIBLE);
                holder.binding.txt1.setText(""+context.getString(R.string.wearenowshoppingyourorder));
                holder.binding.call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("dvdv","check1");
                        if (ContextCompat.checkSelfPermission(context,
                                Manifest.permission.CALL_PHONE)
                                != PackageManager.PERMISSION_GRANTED) {

                            Log.d("dvdv","check2");

                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    100);

                            // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        } else {
                            //You already have permission
                            Log.d("dvdv","check3");
                            try {
                                Intent intent = new Intent(Intent.ACTION_CALL,
                                        Uri.parse("tel:" +myOrderFragment.adminno));
                                context.startActivity(intent);
                            } catch (SecurityException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                if(slotData.get(position).getPackedByDriver().equalsIgnoreCase("1")){
                    holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                    holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                    holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirmed));

                    holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                    holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                    holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag_green));

                    holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                    holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
                    holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck_green));
                    if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                        holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_truck_arabic_green));
                    }
                    holder.binding.reschedule.setVisibility(View.GONE);
                    holder.binding.linear.setVisibility(View.VISIBLE);
                    holder.binding.linear1.setVisibility(View.GONE);
                    holder.binding.rela1.setVisibility(View.VISIBLE);
                    holder.binding.txt1.setText(""+context.getString(R.string.theorderisonyourway));
                    holder.binding.call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (ContextCompat.checkSelfPermission(context,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) context,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        100);

                                // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            } else {
                                //You already have permission
                                try {
                                    Intent intent = new Intent(Intent.ACTION_CALL,
                                            Uri.parse("tel:" +"+966"+slotData.get(position).getDriverNumber()));
                                    context.startActivity(intent);
                                } catch (SecurityException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        }
        else {
            holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirm_gray));

            holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag));

            holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck));
            if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
                holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_truck_arabic_white));
            }
            holder.binding.reschedule.setVisibility(View.VISIBLE);
            holder.binding.linear.setVisibility(View.GONE);
            holder.binding.groupLoad.setVisibility(View.GONE);
        }

        if(slotData.get(position).getFastDelivery().equals("1")) {
            holder.binding.reschedule.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return slotData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildActiveOrdersBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildActiveOrdersBinding.bind(itemView);
        }

    }

    private void setOrderView(String status, ActiveOrderAdapter.MyViewHolder holder) {


        if (status.equalsIgnoreCase("PENDING")) {


            holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirm_gray));

            holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag));

            holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck));

            holder.binding.reschedule.setVisibility(View.VISIBLE);
            holder.binding.linear.setVisibility(View.GONE);
            Log.d("scscsc","xccc");


        }
        else if (status.equalsIgnoreCase("ACCEPTED")) {


            holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirmed));

            holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag));

            holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck));

            holder.binding.reschedule.setVisibility(View.GONE);
            holder.binding.linear.setVisibility(View.VISIBLE);
            holder.binding.linear1.setVisibility(View.VISIBLE);
            holder.binding.rela1.setVisibility(View.GONE);

        }
        else if (status.equalsIgnoreCase("ONGOING")) {


            holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirmed));

            holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag_green));

            holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.view_line_color_2));
            holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck));

            holder.binding.reschedule.setVisibility(View.GONE);
            holder.binding.linear.setVisibility(View.VISIBLE);
            holder.binding.linear1.setVisibility(View.GONE);
            holder.binding.rela1.setVisibility(View.VISIBLE);
            holder.binding.txt1.setText(""+context.getString(R.string.wearenowshoppingyourorder));
            holder.binding.call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("dvdv","check1");
                    if (ContextCompat.checkSelfPermission(context,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        Log.d("dvdv","check2");

                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{Manifest.permission.CALL_PHONE},
                                100);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    } else {
                        //You already have permission
                        Log.d("dvdv","check3");
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL,
                                    Uri.parse("tel:" + "+" + "966598765570"));
                            context.startActivity(intent);
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
        else if (status.equalsIgnoreCase("PICKUP")) {

            holder.binding.confirmed.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.confirmedImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.confirmed));

            holder.binding.shopping.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.shoppingView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.shoppingImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.bag_green));

            holder.binding.delivery.setTextColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.deliveryView.setBackgroundColor(ContextCompat.getColor(context, R.color.searchcolor_green));
            holder.binding.deliveryImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.truck_green));

            holder.binding.reschedule.setVisibility(View.GONE);
            holder.binding.linear.setVisibility(View.VISIBLE);
            holder.binding.linear1.setVisibility(View.GONE);
            holder.binding.rela1.setVisibility(View.VISIBLE);
            holder.binding.txt1.setText(""+context.getString(R.string.theorderisonyourway));
            holder.binding.call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(context,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{Manifest.permission.CALL_PHONE},
                                100);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    } else {
                        //You already have permission
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL,
                                    Uri.parse("tel:" + "+" + "966598765570"));
                            context.startActivity(intent);
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
        else {
            holder.binding.linear.setVisibility(View.GONE);
        }
    }
}
