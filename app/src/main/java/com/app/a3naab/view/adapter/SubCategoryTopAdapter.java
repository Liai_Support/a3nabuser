package com.app.a3naab.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildSubcategoryTopBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.ProductSubCategoryData;
import com.app.a3naab.utils.SharedHelper;

import java.util.ArrayList;

public class SubCategoryTopAdapter extends RecyclerView.Adapter<SubCategoryTopAdapter.MyViewHolder> {

    Context context;
    ArrayList<ProductSubCategoryData> list;
    OnItemClick onItemClick;
    SharedHelper sharedHelper;

    public SubCategoryTopAdapter(Context context, ArrayList<ProductSubCategoryData> list, OnItemClick onItemClick) {
        this.context = context;
        this.list = list;
        this.onItemClick = onItemClick;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public SubCategoryTopAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_subcategory_top, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryTopAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") int position) {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            holder.binding.name.setText(""+list.get(position).getArabicName());
        }
        else {
            holder.binding.name.setText(""+list.get(position).getProductSubCategoryName());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (list.get(position).isComingSoon().equals("false")) {
                setSelected(position);
                onItemClick.onClick(position);
//                }
            }
        });


//        if (list.get(position).isComingSoon().equals("true")) {
//            holder.binding.name.setAlpha(0.4f);
//        } else {
//            holder.binding.name.setAlpha(1f);
//        }

        if (list.get(position).isSelected()) {
            holder.binding.name.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.binding.name.setBackground(ContextCompat.getDrawable(context, R.drawable.selected_sub_category_bg));
        } else {
            holder.binding.name.setTextColor(ContextCompat.getColor(context, R.color.edit_text_color_disabled));
            holder.binding.name.setBackground(ContextCompat.getDrawable(context, R.drawable.unselected_sub_category_bg));
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void notifySetChanged() {

        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildSubcategoryTopBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildSubcategoryTopBinding.bind(itemView);
        }

    }

    private void setSelected(int position) {

        for (int i = 0; i < list.size(); i++) {
            list.get(i).setSelected(position == i);
        }
        notifyDataSetChanged();
    }

    public void allSelected(){
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setSelected(false);
        }
        notifyDataSetChanged();
    }
}
