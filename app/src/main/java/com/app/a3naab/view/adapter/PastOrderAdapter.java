package com.app.a3naab.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildPastOrdersBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.CheckReorderResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.OrderDetails;
import com.app.a3naab.model.OrderList;
import com.app.a3naab.model.OrderListResponse;
import com.app.a3naab.model.ViewOrdersResponse;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.CartDetailsActivity;
import com.app.a3naab.view.fragment.MyOrderFragment;

import java.util.ArrayList;
import java.util.Locale;

public class PastOrderAdapter extends RecyclerView.Adapter<PastOrderAdapter.MyViewHolder> {

    Context context;
    MyOrderFragment myOrderFragment;
    ArrayList<OrderListResponse.OrderListData> slotData;
    OnItemClick onItemClick;
    String tax;
    Double cbstyleamount = 0.0;

    public PastOrderAdapter(MyOrderFragment myOrderFragment,Context context, ArrayList<OrderListResponse.OrderListData> slotData, String tax, OnItemClick onItemClick) {
        this.context = context;
        this.slotData = slotData;
        this.onItemClick = onItemClick;
        this.tax = tax;
        this.myOrderFragment = myOrderFragment;
    }

    @NonNull
    @Override
    public PastOrderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_past_orders, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull PastOrderAdapter.MyViewHolder holder, int position) {


        holder.binding.orderId.setText(context.getString(R.string.order_id) + " " + slotData.get(position).getOrderIDs());

        if (slotData.get(position).getOrderStatus().equalsIgnoreCase("CANCELLED")) {
            if (slotData.get(position).getOrderOn() != null) {
                holder.binding.orderDate.setText(context.getString(R.string.order_placed) + "\n" + Utils.getConvertedTime(slotData.get(position).getOrderOn()) + "\n" +Utils.convertDateutc(slotData.get(position).getOrderOn(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
            }

            if (slotData.get(position).getCancelDate() != null) {
                holder.binding.deliveryDate.setVisibility(View.VISIBLE);
                holder.binding.deliveryDateImage.setVisibility(View.VISIBLE);
                holder.binding.deliveryDate.setText(context.getString(R.string.order_canceled) + "\n" + Utils.getConvertedTime(slotData.get(position).getCancelDate()) + "\n" +Utils.convertDateutc(slotData.get(position).getCancelDate(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
            }
            else {
                holder.binding.deliveryDate.setVisibility(View.GONE);
                holder.binding.deliveryDateImage.setVisibility(View.GONE);
            }
        }
        else {
            if (slotData.get(position).getOrderOn() != null) {
                holder.binding.orderDate.setText(context.getString(R.string.order_placed) + "\n" + Utils.getConvertedTime(slotData.get(position).getOrderOn()) + "\n" +Utils.convertDateutc(slotData.get(position).getOrderOn(), "yyyy-MM-dd'T'HH:mm:ss.sss'Z'", "hh:mm a"));
            }

            if (slotData.get(position).getDeliveryOn() != null || slotData.get(position).getDeliveryDate() != null) {
                holder.binding.deliveryDate.setVisibility(View.VISIBLE);
                holder.binding.deliveryDateImage.setVisibility(View.VISIBLE);
                String selectedtime = Utils.convertDate(slotData.get(position).getDeliveryOn_fromTime(), "HH:mm:ss", "hh:mm a")
                        + "-" +
                        Utils.convertDate(slotData.get(position).getDeliveryOn_toTime(), "HH:mm:ss", "hh:mm a");
                holder.binding.deliveryDate.setText(context.getString(R.string.delivery_by) + "\n" + Utils.getConvertedTime(slotData.get(position).getDeliveryDate())+"\n"+selectedtime);
            }
            else {
                holder.binding.deliveryDate.setVisibility(View.GONE);
                holder.binding.deliveryDateImage.setVisibility(View.GONE);
            }
        }





       // holder.binding.totalAmount.setText("SAR " + slotData.get(position).getGrandTotal());

        cbstyleamount = Double.parseDouble(slotData.get(position).getOtherTotal());
        Double subtotal = (cbstyleamount)+(Double.parseDouble(slotData.get(position).getTotalAmount()) - Double.parseDouble(slotData.get(position).getDiscountAmount()));
        Double delivery = Double.parseDouble(slotData.get(position).getFastDelievryCharge());
        //Double subtotal1 = (subtotal-Double.parseDouble(slotData.get(position).getTakeToCustomer()))+Double.parseDouble(slotData.get(position).getGiveToCustomer())+delivery;
        if(slotData.get(position).getOff_types().equalsIgnoreCase("0")){
            delivery = Double.parseDouble(slotData.get(position).getCouponDiscount());
        }
        Double subtotal1 = subtotal+delivery;
        Double coupondiscount = Double.parseDouble(slotData.get(position).getCouponDiscount());
        Double pointamount = Double.parseDouble(slotData.get(position).getPointsAmount());
        Double walletamount = Double.parseDouble(slotData.get(position).getPaidByWallet());
        Double vat = ((subtotal1-(coupondiscount+pointamount+walletamount)) * (Double.parseDouble(slotData.get(position).getTaxvalue()) / 100));
        Double total = ((subtotal1-(coupondiscount+pointamount+walletamount))+vat);
        if(total < 0) {
            total = 0.0;
        }
        holder.binding.totalAmount.setText("SAR " +String.format(Locale.ENGLISH,"%.2f",total));

        if(slotData.get(position).getType().equalsIgnoreCase("CASH")){
            holder.binding.paymentType.setText(""+context.getString(R.string.codmachine));
        }
        else {
            holder.binding.paymentType.setText(slotData.get(position).getType());
        }

        holder.binding.reorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  CartSessionManager.removeAllValues();
                CartSessionManager.getInstance().setReorder(true);
                CartSessionManager.getInstance().setOrderId(String.valueOf(slotData.get(position).getId()));
                CartSessionManager.getInstance().setTotalAmount(String.valueOf(slotData.get(position).getTotalAmount()));
                CartSessionManager.getInstance().setOrderMinimumMins(0);
                CartSessionManager.getInstance().setFlatDiscount(tax);
                context.startActivity(new Intent(context, CartDetailsActivity.class));*/

                DialogUtils.showLoader(context);
                myOrderFragment.shoppingViewModel.getOrderDetails(context, slotData.get(position).getId()).observe(myOrderFragment, new Observer<ViewOrdersResponse>() {
                    @Override
                    public void onChanged(ViewOrdersResponse viewOrdersResponse) {
                        DialogUtils.dismissLoader();
                        if (!viewOrdersResponse.getError()) {
                            loadData(viewOrdersResponse.getData());
                        }
                    }
                });
            }
        });

        holder.binding.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onClick(position);
            }
        });

    }

    private void loadData(OrderDetails data) {

            DialogUtils.showLoader(context);
            myOrderFragment.shoppingViewModel.checkreorder(context, data.getOrderInfo().getId()).observe(myOrderFragment, new Observer<CheckReorderResponse>() {
                @Override
                public void onChanged(CheckReorderResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (!commonResponse.getError()) {
                        //loadData(commonResponse.getData());
                        CartSessionManager.removeAllValues();
                        CartSessionManager.getInstance().setReorder(true);
                        CartSessionManager.getInstance().setOrderId(String.valueOf(data.getOrderInfo().getId()));
                        CartSessionManager.getInstance().setTotalAmount(String.valueOf((Double.parseDouble(data.getOrderInfo().getTotalAmount()))-(Double.parseDouble(data.getOrderInfo().getDiscountAmount()))));
                       // CartSessionManager.getInstance().setTotalAmountcpy(String.valueOf((Double.parseDouble(data.getOrderInfo().getTotalAmount()))-(Double.parseDouble(data.getOrderInfo().getDiscountAmount()))));
                        CartSessionManager.getInstance().setcbstyleamount(String.valueOf(cbstyleamount));
                        CartSessionManager.getInstance().setSelectedAddressType(String.valueOf(data.getOrderInfo().getAddressType()));
                        CartSessionManager.getInstance().setSelectedAddress(data.getOrderInfo().getAddressPinDetails());
                        CartSessionManager.getInstance().setSelectedId(data.getOrderInfo().getAddressId());

                        CartSessionManager.getInstance().setFlatDiscount(String.valueOf(data.getTax()));

                        setMaxTime(data.getOrderList());
                        //hari
                        Double tot = Double.parseDouble(CartSessionManager.getInstance().getTotalAmount())+Double.parseDouble(CartSessionManager.getInstance().getcbstyleamount());
                        if(Double.parseDouble(commonResponse.getData().getFreeDeliveryAmt()) <= tot){
                            myOrderFragment.sharedHelper.setDeliverycharge("0");
                            myOrderFragment.sharedHelper.setFastDeliverycharge(commonResponse.getData().getOverAllFastDeliveryAmount());
                        }
                        else {
                            myOrderFragment.sharedHelper.setFastDeliverycharge(commonResponse.getData().getOverAllFastDeliveryAmount());
                            myOrderFragment.sharedHelper.setDeliverycharge(commonResponse.getData().getOverAllDeliveryAmount());
                        }
                        context.startActivity(new Intent(context, CartDetailsActivity.class));
                    }
                    else {
                        DialogUtils.showAlertDialog(context,  context.getString(R.string.storenotavailable),context.getString(R.string.oops), context.getString(R.string.ok), new DialogCallback() {
                            @Override
                            public void onPositiveClick() {

                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });                        //commonResponse.getMessage()
                    }
                }
            });

    }



    @Override
    public int getItemCount() {
        return slotData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildPastOrdersBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildPastOrdersBinding.bind(itemView);
        }

    }

   /* private void setMaxTime(ArrayList<OrderListResponse.OrderListData> data) {

//        int maxTime = 0;
//        for (int i = 0; i < data.size(); i++) {
//            if (data.get(i).getSub_processingMin() == null || data.get(i).getSub_processingMin() == 0) {
//                if (data.get(i).getCate_processingMin() == null || data.get(i).getCate_processingMin() == 0) {
//
//                } else {
//                    if (maxTime < data.get(i).getCate_processingMin()) {
//                        maxTime = data.get(i).getCate_processingMin();
//                    }
//                }
//            } else {
//                if (maxTime < data.get(i).getSub_processingMin()) {
//                    maxTime = data.get(i).getSub_processingMin();
//                }
//            }
//        }
//        CartSessionManager.getInstance().setOrderMinimumMins(maxTime);

    }*/
   private void setMaxTime(ArrayList<OrderList> data) {

       int maxTime = 0;
       for (int i = 0; i < data.size(); i++) {
           if (data.get(i).getSub_processingMin() == null || data.get(i).getSub_processingMin() == 0) {
               if (data.get(i).getCate_processingMin() == null || data.get(i).getCate_processingMin() == 0) {

               } else {
                   if (maxTime < data.get(i).getCate_processingMin()) {
                       maxTime = data.get(i).getCate_processingMin();
                   }
               }
           } else {
               if (maxTime < data.get(i).getSub_processingMin()) {
                   maxTime = data.get(i).getSub_processingMin();
               }
           }
       }
       CartSessionManager.getInstance().setOrderMinimumMins(maxTime);

   }


}
