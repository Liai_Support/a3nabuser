package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivitySettingsBinding;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.model.ProfileDatas;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.ProductViewModel;

public class SettingsActivity extends BaseActivity {

    ActivitySettingsBinding binding;
    SharedHelper sharedHelper;
    private Boolean offersNotify;
    private Boolean ordersNotify;
    private Boolean announcementNotify;
    private Boolean othersNotify;
    private ProductViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        sharedHelper = new SharedHelper(this);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        initListener();
        initObservers();
        getProfileDetails();
        binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        if (sharedHelper.getCartCount() == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setVisibility(View.VISIBLE);
            binding.foot.badge.setText(""+sharedHelper.getCartCount());
        }

        if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")){
            binding.back.setRotationY(180);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initListener();
        initObservers();
        getProfileDetails();
    }

    private void setUI() {
        if (ordersNotify) {
            binding.toggleUpdate.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_on));
        } else {
            binding.toggleUpdate.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_off));
        }

        if (announcementNotify) {
            binding.toggleAnnouncement.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_on));
        } else {
            binding.toggleAnnouncement.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_off));
        }

        if (othersNotify) {
            binding.toggleOthers.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_on));
        } else {
            binding.toggleOthers.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_off));
        }

        if (offersNotify) {
            binding.toggleOffer.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_on));
        } else {
            binding.toggleOffer.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.toggle_off));
        }

    }

    private void initListener() {
        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.rdtEnglish.setChecked(false);
            binding.rdtArabic.setChecked(true);
        } else {
            binding.rdtEnglish.setChecked(true);
            binding.rdtArabic.setChecked(false);
        }


        binding.toggleOffer.setOnClickListener((view) -> {
            offersNotify = !offersNotify;
            updateSettings(1);
        });

        binding.toggleUpdate.setOnClickListener((view) -> {
            ordersNotify = !ordersNotify;
            updateSettings(2);
        });

        binding.toggleAnnouncement.setOnClickListener((view) -> {
            announcementNotify = !announcementNotify;
            updateSettings(3);
        });

        binding.toggleOthers.setOnClickListener((view) -> {
            othersNotify = !othersNotify;
            updateSettings(4);
        });

    }

    private void initObservers() {
        binding.rdtEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
                        changeLanguage();
                    }
            }
        });

        binding.rdtArabic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("en")) {
                        changeLanguage();
                    }
            }
        });

        binding.textView61.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("en")) {
                    changeLanguage();
                }
            }
        });

        binding.textView60.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
                    changeLanguage();
                }
            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });
        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });
        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });
        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });



    }

    private void moveToActivity(int position) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void changeLanguage() {
        if (sharedHelper.getSelectedLanguage().equals("en")) {
            sharedHelper.setSelectedLanguage("ar");
        } else {
            sharedHelper.setSelectedLanguage("en");
        }

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", 3);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void updateSettings(int type) {

        DialogUtils.showLoader(this);
        viewModel.updateSettings(this,
                offersNotify, //1
                ordersNotify,  //2
                announcementNotify,  //33
                othersNotify  //4
                , type).observe(this, new Observer<CommonResponse>() {
            @Override
            public void onChanged(CommonResponse commonResponse) {

                DialogUtils.dismissLoader();
                if (commonResponse.getError()) {
                    Utils.showSnack(binding.parentLayout, commonResponse.getMessage());
                }
                getProfileDetails();

            }
        });

    }


    private void getProfileDetails() {


        viewModel.getProfileDetails(this).observe(this, new Observer<ProfileDataResponse>() {
            @Override
            public void onChanged(ProfileDataResponse profileDataResponse) {
                if (profileDataResponse.getError()) {
                    Utils.showSnack(binding.parentLayout, profileDataResponse.getMessage());
                } else {
                    setData(profileDataResponse.getData().getProfile());
                }
            }
        });
    }

    private void setData(ProfileDatas profile) {
        if (profile != null) {

            othersNotify = profile.getOthersNotify();
            ordersNotify = profile.getOrdersNotify();
            announcementNotify = profile.getAnnouncementNotify();
            offersNotify = profile.getOffersNotify();

            setUI();

        }
    }

}
