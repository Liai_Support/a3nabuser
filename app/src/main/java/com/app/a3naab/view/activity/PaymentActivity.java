package com.app.a3naab.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityPaymentBinding;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.net.URISyntaxException;

public class PaymentActivity extends BaseActivity {

    ShoppingViewModel shoppingViewModel;
    ActivityPaymentBinding binding;
    SharedHelper sharedHelper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        sharedHelper = new SharedHelper(this);

        getIntentValues();
    }

    private void getIntentValues() {


        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String amount = bundle.getString("amount", "");
            if (!amount.equalsIgnoreCase(""))
                startPayment(amount);
            else
                Utils.showSnack(findViewById(android.R.id.content), "Invalid Amount");
        }
    }

    private void startPayment(String amount) {

        shoppingViewModel.startPayment(this, amount).observe(this, paymentResponse -> {
            if (paymentResponse.getResultCode().equalsIgnoreCase("0"))
                if (paymentResponse.getResult().getCheckoutData() != null) {
                    loadUrl(paymentResponse.getResult().getCheckoutData().getPostUrl());
                }
        });

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadUrl(String url) {


        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setLoadWithOverviewMode(true);
        binding.webView.getSettings().setUseWideViewPort(true);
        binding.webView.getSettings().setBuiltInZoomControls(true);

        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            @Override
            public void onLoadResource(WebView view, String url) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                try {
                    Log.d("URLLOADING ", url);
                    String noonorderid = Uri.parse(url).getQueryParameter("orderId");
                    Log.d("torderid:",""+noonorderid);
                    if (url.contains("https://a3naab.com")) {
                        if(noonorderid != null){
                            shoppingViewModel.getorder(PaymentActivity.this,noonorderid).observe(PaymentActivity.this, paymentResponse -> {
                                if (paymentResponse.getResultCode().equalsIgnoreCase("0") && (!paymentResponse.getResult().getOrder().getStatus().equalsIgnoreCase("CANCELLED")) && (!paymentResponse.getResult().getOrder().getStatus().equalsIgnoreCase("REJECTED")) && (paymentResponse.getResult().getOrder().getStatus().equalsIgnoreCase("INITIATED") || paymentResponse.getResult().getOrder().getStatus().equalsIgnoreCase("AUTHORIZED"))){
                                    shoppingViewModel.sale(PaymentActivity.this,noonorderid).observe(PaymentActivity.this, paymentResponse1 -> {
                                        if (paymentResponse1.getResultCode().equalsIgnoreCase("0")){
                                            CartSessionManager.getInstance().setpaymentorderid(noonorderid);
                                            CartSessionManager.getInstance().setpaymenttransactionid(paymentResponse1.getResult().getTransaction().getId());
                                            setResult(RESULT_OK);
                                            finish();
                                        }
                                    });
                                }
                                else {
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            });
                        }
                        else {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        //Load url in webView
        binding.webView.loadUrl(url);

       /* binding.webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });*/
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finish();
    }
}
