package com.app.a3naab.view.fragment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.FragmentRegisterBinding;
import com.app.a3naab.model.RegisterResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SessionManager;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.DashboardActivity;
import com.app.a3naab.view.activity.LoginActivity;
import com.app.a3naab.view.activity.SplashActivity;
import com.app.a3naab.viewmodel.SignUpViewModel;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterUserFragment extends Fragment {

    SignUpViewModel signUpViewModel;


    FragmentRegisterBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_register, container, false);
        binding = FragmentRegisterBinding.bind(view);
        signUpViewModel = new ViewModelProvider(this).get(SignUpViewModel.class);

        initListener();
        setTAC();

        ((LoginActivity)getActivity()).activityLoginBinding.imageView3.setEnabled(false);
        ((LoginActivity)getActivity()).activityLoginBinding.registerLater.setEnabled(false);
        ((LoginActivity)getActivity()).activityLoginBinding.next.setEnabled(false);
        ((LoginActivity)getActivity()).activityLoginBinding.changeLanguage.setEnabled(false);
        ((LoginActivity)getActivity()).activityLoginBinding.mobileNumber.setEnabled(false);
        ((LoginActivity)getActivity()).activityLoginBinding.countryCodePicker.setClickable(false);
        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(false);
                    Log.d("xcx", "backsucess");
                    ((LoginActivity)getActivity()).activityLoginBinding.imageView3.setEnabled(true);
                    ((LoginActivity)getActivity()).activityLoginBinding.registerLater.setEnabled(true);
                    ((LoginActivity)getActivity()).activityLoginBinding.next.setEnabled(true);
                    ((LoginActivity)getActivity()).activityLoginBinding.changeLanguage.setEnabled(true);
                    ((LoginActivity)getActivity()).activityLoginBinding.mobileNumber.setEnabled(true);
                    ((LoginActivity)getActivity()).activityLoginBinding.countryCodePicker.setClickable(true);
                    removeCurrentFrag();
                }
            }));
        }

        return binding.getRoot();
    }

    private void initListener() {

        binding.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String IsValid = isValidInput();
                if (IsValid.equals("true")) {
                    DialogUtils.showLoader(requireContext());
                    signUpViewModel.createAccount(requireContext(), binding.firstName.getText().toString().trim(),
                            binding.lastName.getText().toString().trim(),
                            getGender(), SessionManager.getInstance().getCountryCode(), SessionManager.getInstance().getMobileNumber(), binding.referalCode.getText().toString().trim()).observe(getViewLifecycleOwner(), new Observer<RegisterResponse>() {
                        @Override
                        public void onChanged(RegisterResponse registerResponse) {
                            DialogUtils.dismissLoader();
                            if (registerResponse.getError()) {
                                Utils.showSnack(binding.parentLayout, registerResponse.getMessage());
                            } else {
                                registerSuccess(registerResponse.getData());
                            }
                        }

                    });
                } else {
                    Utils.showSnack(binding.parentLayout, IsValid);
                }
            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeCurrentFrag();
            }
        });
    }

    private void registerSuccess(RegisterResponse.RegisterData data) {
        SharedHelper sharedHelper = new SharedHelper(requireContext());

        sharedHelper.setUserId(data.getId());
        sharedHelper.setFirstName(data.getFirstName());
        sharedHelper.setLastName(data.getLastName());
        sharedHelper.setAuthToken(data.getToken());

        sharedHelper.setCountryCode(SessionManager.getInstance().getCountryCode());
        sharedHelper.setMobileNumber(SessionManager.getInstance().getMobileNumber());
        sharedHelper.setProfilePic("");
        sharedHelper.setLoggedIn(true);

        if (SessionManager.getInstance().isRegisterLaterFlow()) {
            SessionManager.getInstance().setRegisterLaterFlow(false);
            requireActivity().finish();
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("title",data.getNotify().getNotifyTitle());
                jsonObject.put("body",data.getNotify().getNotifyMessage());
                sendNotification(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendNotification(jsonObject);
            Intent intent = new Intent(requireContext(), DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    private void sendNotification(JSONObject jsonObject) {
      /*  Intent intent = new Intent(getActivity(), SplashActivity.class);
       // intent.putExtra(Constants.LOGIN_TYPE, Constants.HOME_TAB);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);*/

        Log.d("sendNotification", jsonObject.toString());
        if ((!jsonObject.has("title")) || (!jsonObject.has("body"))) {
            return;
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getActivity(), "Notification")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(jsonObject.optString("title"))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX);

        if (jsonObject.has("body")) {
            notificationBuilder.setContentText(jsonObject.optString("body"));
        } else if(jsonObject.has("message")){
            notificationBuilder.setContentText(jsonObject.optString("message"));

        }

        NotificationManager notificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    private String getGender() {
        if (binding.male.isChecked()) {
            return "male";
        } else {
            return "female";
        }
    }

    private String isValidInput() {
        if (binding.firstName.getText().toString().trim().equals("")) {
            return getString(R.string.first_name_not_empty);
        } else if (binding.lastName.getText().toString().trim().equals("")) {
            return getString(R.string.last_name_not_empty);
        } else if (!(binding.male.isChecked() || binding.female.isChecked())) {
            return getString(R.string.select_gender);
        } else {
            return "true";
        }
    }

    private void setTAC() {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append(getString(R.string.if_you_click_register_you_agree_to_our_terms1)).append("  ").append(
                Utils.getColoredAndBoldString(requireContext(), getString(R.string.if_you_click_register_you_agree_to_our_terms2))).append(" ")
                .append(getString(R.string.if_you_click_register_you_agree_to_our_terms3));
        binding.agreeTerms.setText(stringBuilder);
    }

    private void removeCurrentFrag() {

        ((LoginActivity)getActivity()).activityLoginBinding.imageView3.setEnabled(true);
        ((LoginActivity)getActivity()).activityLoginBinding.registerLater.setEnabled(true);
        ((LoginActivity)getActivity()).activityLoginBinding.next.setEnabled(true);
        ((LoginActivity)getActivity()).activityLoginBinding.changeLanguage.setEnabled(true);
        ((LoginActivity)getActivity()).activityLoginBinding.mobileNumber.setEnabled(true);
        ((LoginActivity)getActivity()).activityLoginBinding.countryCodePicker.setClickable(true);
        FragmentTransaction trans = getFragmentManager().beginTransaction();
        trans.remove(this);
        trans.commit();
        getFragmentManager().popBackStack();



    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

    }
}
