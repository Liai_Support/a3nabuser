package com.app.a3naab.view.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityLoginBinding;
import com.app.a3naab.interfaces.NavigationInterface;
import com.app.a3naab.model.FireBaseVerificationResponse;
import com.app.a3naab.model.LoginResponse;
import com.app.a3naab.utils.Constants;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SessionManager;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.fragment.OTPVerificationFragment;
import com.app.a3naab.view.fragment.RegisterUserFragment;
import com.app.a3naab.viewmodel.SignUpViewModel;

public class LoginActivity extends BaseActivity {


    SignUpViewModel viewModel;
    public ActivityLoginBinding activityLoginBinding;
    OTPVerificationFragment otpVerificationFragment;
    SharedHelper sharedHelper;
    Handler handler = new Handler();
    Runnable runnable;
    public String tuserid = "";
    public String tfirstname = "";
    public String tlastname = "";
    public String tmailid = "";
    public String tlatitude = "";
    public String tlongitude = "";
    public String tcountrycode = "";
    public String tmobilenumber = "";
    public String tprofilepic = "";
    public String tauthtoken = "";
    public Boolean tloggedin = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
        sharedHelper = new SharedHelper(this);
        initListener();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                        Constants.Permissions.LOCATIONPERMISSION_LIST,
                        Constants.Permissions.LOCATIONREQUEST_CODE
                );

            }
            else {
                requestPermissions(
                        Constants.Permissions.LOCATIONPERMISSION_LIST,
                        Constants.Permissions.LOCATIONREQUEST_CODE
                );
            }
        }
       /* ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE},
                100);*/
    }


    private void initListener() {

        activityLoginBinding.imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        activityLoginBinding.registerLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SessionManager.getInstance().setRegisterLaterFlow(false);
                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });
        activityLoginBinding.next.setOnClickListener((view) -> {


            if (activityLoginBinding.mobileNumber.getText().toString().length() >= 6) {
                SessionManager.getInstance().setCountryCode(activityLoginBinding.countryCodePicker.getSelectedCountryCode());
                SessionManager.getInstance().setMobileNumber(activityLoginBinding.mobileNumber.getText().toString());

                otpVerified();

               /* DialogUtils.showLoader(this);
                viewModel.requestVerificationCode(
                        this,
                        "+" + activityLoginBinding.countryCodePicker.getSelectedCountryCode() +
                                activityLoginBinding.mobileNumber.getText().toString()
                ).observe(this, new Observer<FireBaseVerificationResponse>() {
                    @Override
                    public void onChanged(FireBaseVerificationResponse fireBaseVerificationResponse) {

                        if (fireBaseVerificationResponse.getError().equalsIgnoreCase("true")) {
                            DialogUtils.dismissLoader();
                            Utils.showSnack(activityLoginBinding.parentLayout, fireBaseVerificationResponse.getMessage());
                        } else {
                            SessionManager.getInstance().setCountryCode(activityLoginBinding.countryCodePicker.getSelectedCountryCode());
                            SessionManager.getInstance().setMobileNumber(activityLoginBinding.mobileNumber.getText().toString());
                            if (fireBaseVerificationResponse.getVerificationCode().equalsIgnoreCase("verified")) {
                                Log.d("fddf","123");
                                otpVerified();
                            } else {
                                Log.d("fddf","1234");
                                navigateToVerification(fireBaseVerificationResponse.getVerificationCode());
                            }
                        }
                    }
                });*/
            } else {
                Utils.showSnack(activityLoginBinding.parentLayout, getString(R.string.enter_mobile_number));
            }

        });

        activityLoginBinding.changeLanguage.setOnClickListener(view -> {

            if (sharedHelper.getSelectedLanguage().equals("en")) {
                sharedHelper.setSelectedLanguage("ar");
            } else {
                sharedHelper.setSelectedLanguage("en");
            }
            Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);


        });
    }

    private void otpVerified() {
        DialogUtils.showLoader(this);
        viewModel.login(this, activityLoginBinding.countryCodePicker.getSelectedCountryCode(),
                activityLoginBinding.mobileNumber.getText().toString()).observe(this, loginResponse -> {
            DialogUtils.dismissLoader();
            if (loginResponse.getError()) {
                Utils.showSnack(findViewById(android.R.id.content), loginResponse.getMessage());
            } else {
                if (loginResponse.getData().getUserExists()) {
                    SessionManager.getInstance().setUserexists(true);
                    DialogUtils.showLoader(this);
                    userExists(loginResponse.getData());
                } else {
                    if (runnable != null)
                        handler.removeCallbacks(runnable);
                    //navigateToRegisterActivity();
                    SessionManager.getInstance().setUserexists(false);
                    DialogUtils.showLoader(this);
                    navigateToVerification(""+loginResponse.getData().getOtp());
                }
            }
        });
    }

    private void userExists(LoginResponse.LoginData data) {

       /* sharedHelper.setUserId(data.getId());
        sharedHelper.setFirstName(data.getFirstName());
        sharedHelper.setLastName(data.getLastName());
        sharedHelper.setEmail(data.getEmail());
        sharedHelper.setLatitude(data.getLatitude());
        sharedHelper.setLongitude(data.getLongitude());
        sharedHelper.setCountryCode(data.getCountryCode());
        sharedHelper.setMobileNumber(data.getMobileNumber());
        sharedHelper.setProfilePic(data.getProfilePic());
        sharedHelper.setAuthToken(data.getToken());
        sharedHelper.setLoggedIn(true);
*/
        tuserid = data.getId();
        tfirstname = data.getFirstName();
        tlastname = data.getLastName();
        tmailid = data.getEmail();
        tlatitude = data.getLatitude();
        tlongitude = data.getLongitude();
        tcountrycode = data.getCountryCode();
        tmobilenumber = data.getMobileNumber();
        tprofilepic = data.getProfilePic();
        tauthtoken = data.getToken();
        tloggedin = false;

        if (SessionManager.getInstance().isRegisterLaterFlow()) {
            Log.d("xcv13","djdcvd");
            SessionManager.getInstance().setRegisterLaterFlow(false);
            finish();
        } else {
            Log.d("xcv23","djdcvd");
            navigateToVerification(""+data.getOtp());

          /*  Intent intent = new Intent(this, DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);*/
        }

    }

    private void navigateToVerification(String verificationCode) {

        runnable = new Runnable() {
            @Override
            public void run() {
                DialogUtils.dismissLoader();
                SessionManager.getInstance().setVerificationCode(verificationCode);
                otpVerificationFragment = new OTPVerificationFragment();

                Utils.navigateToFragment(
                        getSupportFragmentManager(),
                        R.id.container,
                        otpVerificationFragment
                );

                NavigationInterface navigationInterface = (countryCode, mobile) -> navigateToRegisterActivity();
                otpVerificationFragment.setNavigation(navigationInterface);
            }
        };

        handler.postDelayed(runnable, 3000);


    }

    void navigateToRegisterActivity() {

        Utils.navigateToFragment(
                getSupportFragmentManager(),
                R.id.container,
                new RegisterUserFragment()
        );


    }

    @Override
    protected void onResume() {
        super.onResume();
       /* Log.d("dcbhdvd","sdcbhdbc");
        if(!SessionManager.getInstance().getVerificationCode().equals("")){
            Toast.makeText(this,""+SessionManager.getInstance().getVerificationCode(),Toast.LENGTH_SHORT).show();
        }*/
    }


}
