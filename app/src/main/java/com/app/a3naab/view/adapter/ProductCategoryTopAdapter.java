package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildProductCategoryTopBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.ProductCategoryData;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;

import java.util.ArrayList;

public class ProductCategoryTopAdapter extends RecyclerView.Adapter<ProductCategoryTopAdapter.MyViewHolder> {

    Context context;
    ArrayList<ProductCategoryData> bannerData;
    OnItemClick onItemClick;
    SharedHelper sharedHelper;

    public ProductCategoryTopAdapter(Context context, ArrayList<ProductCategoryData> bannerData, OnItemClick onItemClick) {
        this.context = context;
        this.bannerData = bannerData;
        this.onItemClick = onItemClick;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public ProductCategoryTopAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_product_category_top, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ProductCategoryTopAdapter.MyViewHolder holder, int position) {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
            holder.binding.name.setText(bannerData.get(position).getArabicName());
        else
            holder.binding.name.setText(bannerData.get(position).getProductCategoryName());

        Utils.loadImage(holder.binding.productImage, bannerData.get(position).getProductCategoryImage());

        if (bannerData.get(position).isSelected()) {
            holder.binding.view.setVisibility(View.VISIBLE);
            holder.binding.imageView21.setAlpha(1f);
            holder.binding.name.setAlpha(1f);
        } else {
            holder.binding.view.setVisibility(View.INVISIBLE);
            holder.binding.imageView21.setAlpha(0.5f);
            holder.binding.name.setAlpha(0.5f);
        }

        holder.itemView.setOnClickListener(view -> {
            setSelection(position);
            onItemClick.onClick(position);
        });
    }

    private void setSelection(int position) {
        for (int i = 0; i < bannerData.size(); i++) {
            if (position == i) {
                bannerData.get(i).setSelected(true);
            } else {
                bannerData.get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return bannerData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildProductCategoryTopBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildProductCategoryTopBinding.bind(itemView);
        }

    }
}
