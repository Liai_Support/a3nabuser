package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivitySubcategoryProductsBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ProductCategoryData;
import com.app.a3naab.model.ProductListResponse;
import com.app.a3naab.model.ProductSubCategoryData;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SessionManager;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.ListProductAdapterStatic;
import com.app.a3naab.view.adapter.ProductCategoryTopAdapter;
import com.app.a3naab.view.adapter.SubCategoryTopAdapter;
import com.app.a3naab.viewmodel.CategoryViewModel;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.ArrayList;

public class SubCategoryActivity extends BaseActivity {

    ActivitySubcategoryProductsBinding binding;
    CategoryViewModel viewModel;
    ProductViewModel productViewModel;
    int selectedCategoryId, subCategoryId;
    int havingSubcategory = 0;
    ListProductAdapterStatic adapter;
    boolean fromSearch = false;
    SubCategoryTopAdapter subCategoryAdapter;
    ArrayList<DashboardResponse.BestProduct> productList = new ArrayList<>();
    ArrayList<ProductSubCategoryData> subCategoryResponse = new ArrayList<>();
    ArrayList<ViewCartResponse.CartDetails> cartlist = new ArrayList<>();
    private int gettingPosition = 0;
    SharedHelper sharedHelper;
    String boxid = "0";
    String cutid = "0";
    String sinstruction = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeblue);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_subcategory_products);
        viewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        getIntextValues();
        initAdapter();
        initObserver();
        initListener();
        setNavBar(0);
    }

    private void setNavBar(int position) {

        if (position == 0) {
            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));


        } else if (position == 1) {

            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
        } else if (position == 2) {


            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));






        } else if (position == 3) {
            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));


        }

    }

    private void initListener() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.searchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SubCategoryActivity.this, SearchSubCategoryActivity.class)
                        .putExtra("categoryId", viewModel.selectedCategoryId)
                        .putExtra("subCategoryId", viewModel.selectedSubCategoryId)
                        .putExtra("havingSub", viewModel.havingSubCategory)
                );
            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAllSelected();
            }
        });

        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });
    }

    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void getCartCount() {

        productViewModel.getCartDetails(this).observe(this, response -> {
            if (!response.getError()) {
                if (response.getData().getMyCart().size() == 0) {
                    binding.foot.badge.setVisibility(View.GONE);
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                } else {
                    binding.foot.badge.setVisibility(View.VISIBLE);
                    binding.foot.badge.setText(response.getData().getMyCart().size() + "");
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                    cartlist = response.getData().getMyCart();
                }
            }
        });

    }

    private Boolean checkiscart(int productid,int storeid){
        boxid = "0";
        cutid = "0";
        sinstruction = "";
        for (int i=0;i<cartlist.size();i++){
            if(cartlist.get(i).getProductId() == productid && cartlist.get(i).getStoreId() == storeid){
                if(cartlist.get(i).getBoxStyle()!=null){
                    boxid = cartlist.get(i).getBoxStyle();
                }
                if(cartlist.get(i).getBoxStyle()!=null){
                    cutid = cartlist.get(i).getCuttingStyle();
                }
                if(cartlist.get(i).getSpecialInstructions()!=null){
                    sinstruction = cartlist.get(i).getSpecialInstructions();
                }
                return true;
            }
        }
        return false;
    }

    private void initObserver() {

//        viewModel.itemPagedList.observe(this, new Observer<PagedList<ProductListData>>() {
//            @Override
//            public void onChanged(PagedList<ProductListData> productListData) {
//                adapter.submitList(productListData);
//            }
//        });
    }

    private void initAdapter() {

        adapter = new ListProductAdapterStatic(this, productList, new OnItemClick() {
            @Override
            public void onClick(int position) {
                if(productList.get(position).getBoxStyle().size()!=0 || productList.get(position).getCuttingStyle().size()!=0){
                    if(checkiscart(productList.get(position).getId(),Integer.parseInt(productList.get(position).getStoreId()))){
                        DialogUtils.showLoader(SubCategoryActivity.this);
                        productViewModel.addtoCartWithStyle(SubCategoryActivity.this, productList.get(position).getId(), productList.get(position).getStoreId(), cutid, boxid, sinstruction)
                                .observe(SubCategoryActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            adapter.notifyAddData(position);
                                            getCartCount();
                                        }
                                    }
                                });
                    }
                    else {
                        DialogUtils.showLoader(SubCategoryActivity.this);
                        productViewModel.addItemToCart(SubCategoryActivity.this, productList.get(position).getId(), productList.get(position).getStoreId())
                                .observe(SubCategoryActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            adapter.notifyAddData(position);
                                            getCartCount();
                                        }
                                    }
                                });
                    }
                }
                else {
                    DialogUtils.showLoader(SubCategoryActivity.this);
                    productViewModel.addItemToCart(SubCategoryActivity.this, productList.get(position).getId(), productList.get(position).getStoreId())
                            .observe(SubCategoryActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        adapter.notifyAddData(position);
                                        getCartCount();
                                    }
                                }
                            });
                }
            }
        }, new OnItemClick() {
            @Override
            public void onClick(int position) {
                if(productList.get(position).getBoxStyle().size()!=0 || productList.get(position).getCuttingStyle().size()!=0){
                    if(checkiscart(productList.get(position).getId(),Integer.parseInt(productList.get(position).getStoreId()))){
                        DialogUtils.showLoader(SubCategoryActivity.this);
                        productViewModel.removeItemToCartwithstyle(SubCategoryActivity.this,  productList.get(position).getId(), productList.get(position).getStoreId(), cutid, boxid, sinstruction)
                                .observe(SubCategoryActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            adapter.notifyRemoveData(position);
                                            getCartCount();
                                        }
                                    }
                                });
                    }
                    else {
                        DialogUtils.showLoader(SubCategoryActivity.this);
                        productViewModel.removeItemToCart(SubCategoryActivity.this, productList.get(position).getId(), productList.get(position).getStoreId())
                                .observe(SubCategoryActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            adapter.notifyRemoveData(position);
                                            getCartCount();
                                        }
                                    }
                                });
                    }
                }
                else {
                    DialogUtils.showLoader(SubCategoryActivity.this);
                    productViewModel.removeItemToCart(SubCategoryActivity.this, productList.get(position).getId(), productList.get(position).getStoreId())
                            .observe(SubCategoryActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        adapter.notifyRemoveData(position);
                                        getCartCount();
                                    }
                                }
                            });
                }
            }
        });
        binding.productList.setLayoutManager(new GridLayoutManager(this, 2));
        binding.productList.setAdapter(adapter);

        ArrayList<ProductCategoryData> list = SessionManager.getInstance().getProductCategoryList();

        if (fromSearch) {
            list = new ArrayList<>();

            for (int i = 0; i < SessionManager.getInstance().getProductCategoryList().size(); i++) {
                if (SessionManager.getInstance().getProductCategoryList().get(i).getId() == selectedCategoryId) {
                    list.add(SessionManager.getInstance().getProductCategoryList().get(i));
                    list.get(0).setSelected(true);
                }
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setSelected(list.get(i).getId() == selectedCategoryId);
            }
        }


        binding.categoryList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        ArrayList<ProductCategoryData> finalList = list;
        binding.categoryList.setAdapter(new ProductCategoryTopAdapter(this, list, position -> {
            viewModel.selectedCategoryId = finalList.get(position).getId();
            viewModel.havingSubCategory = finalList.get(position).isSubcate();
            selectedCategoryId = finalList.get(position).getId();

            if (adapter != null) {
                productList = new ArrayList<>();
                adapter.notifySetChanged();
            }


            if (finalList.get(position).isSubcate() == 1) {
                subCategoryResponse = new ArrayList<>();
                getSubCategory();
            } else {
//                viewModel.configDataSource(this);
                setAdapterEmpty();
                getProductData();
            }
        }));

        for (int i = 0; i < finalList.size(); i++) {
            if (finalList.get(i).getId() == selectedCategoryId) {
                viewModel.havingSubCategory = finalList.get(i).isSubcate();
                selectedCategoryId = finalList.get(i).getId();

                havingSubcategory = finalList.get(i).isSubcate();
//                if ( == 1) {
//                    getSubCategory();
//                } else {
//                    setAdapterEmpty();
//                    getProductData();
//                    setEmptyAllSelect();
//                }
            }
        }

    }

    private void getSubCategory() {

        viewModel.getSubCategory(this).observe(this, subCategoryResponse -> {
            if (!subCategoryResponse.getError()) {
                this.subCategoryResponse = subCategoryResponse.getData().getProductCategory();
                if (subCategoryResponse.getData().getProductCategory() != null && subCategoryResponse.getData().getProductCategory().size() != 0) {
//                    subCategoryResponse.getData().getProductCategory().get(0).setSelected(true);
                    binding.bottomSubView.setVisibility(View.VISIBLE);
//                    viewModel.selectedSubCategoryId = subCategoryResponse.getData().getProductCategory().get(0).getId();
//                    getProductData();
                } else {
//                    binding.bottomSubView.setVisibility(View.GONE);
                }


                binding.subcategoryList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

                subCategoryAdapter = new SubCategoryTopAdapter(this, subCategoryResponse.getData().getProductCategory(),
                        position -> {
                            viewModel.selectedSubCategoryId = subCategoryResponse.getData().getProductCategory().get(position).getId();
                            binding.all.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
                            binding.all.setBackground(ContextCompat.getDrawable(this, R.drawable.unselected_sub_category_bg));
//                    viewModel.configDataSource(this);
                            getProductData();

                        });
                binding.subcategoryList.setAdapter(subCategoryAdapter);
                viewModel.selectedSubCategoryId = -1;
                setAllSelected();

            }
        });
    }

    private void getIntextValues() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedCategoryId = bundle.getInt("categoryId", 0);
            viewModel.selectedCategoryId = selectedCategoryId;

            fromSearch = bundle.getBoolean("fromSearch", false);

            havingSubcategory = bundle.getInt("havingSubCategory", 0);
            viewModel.havingSubCategory = havingSubcategory;


        }

    }

    private void getProductData() {

        DialogUtils.showLoader(this);
        viewModel.getProductList(this).observe(this, new Observer<ProductListResponse>() {
            @Override
            public void onChanged(ProductListResponse productListResponse) {
                DialogUtils.dismissLoader();
                if (!productListResponse.getError()) {
                    productList = productListResponse.getProductListDataData().getProducts();
                    adapter.notifyData(productListResponse.getProductListDataData().getProducts());
                }
            }
        });

    }

    private void setAdapterEmpty() {
//        binding.bottomSubView.setVisibility(View.GONE);
        binding.subcategoryList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        binding.subcategoryList.setAdapter(new SubCategoryTopAdapter(this, new ArrayList<>(), position -> {
        }));
    }

    private void setAllSelected() {

        if (subCategoryAdapter != null) {
            subCategoryAdapter.allSelected();
        }

        viewModel.selectedSubCategoryId = -1;
        gettingPosition = 0;
        DialogUtils.showLoader(this);
        getProductAllData();

        binding.all.setTextColor(ContextCompat.getColor(this, R.color.white));
        binding.all.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_sub_category_bg));

    }

    private void setEmptyAllSelect() {
        binding.all.setTextColor(ContextCompat.getColor(this, R.color.white));
        binding.all.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_sub_category_bg));

    }

    private void getProductAllData() {

        if (subCategoryResponse.size() != 0 && gettingPosition < subCategoryResponse.size()) {
            if (gettingPosition == 0) {
                productList = new ArrayList<>();
            }

            viewModel.getAllProductData(this, subCategoryResponse.get(gettingPosition).getId()).observe(
                    this, productListResponse -> {
                        if (!productListResponse.getError())
                            if (productListResponse.getProductListDataData().getProducts().size() != 0) {
                                productList.addAll(productListResponse.getProductListDataData().getProducts());
                            }
                        gettingPosition = gettingPosition + 1;
                        getProductAllData();
                    });

        } else {
            DialogUtils.dismissLoader();
            adapter.notifyData(productList);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        initListener();
        if (havingSubcategory == 1) {
            getSubCategory();
        }
        else {
//                viewModel.configDataSource(this);
            setAdapterEmpty();
            getProductData();
            setEmptyAllSelect();
        }


        getCartCount();
        if (adapter != null) {
            adapter.notifySetChanged();
        }
    }

}
