package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildAdsHomeBinding;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.utils.Utils;

import java.util.ArrayList;

public class AdsHomeAdapter extends RecyclerView.Adapter<AdsHomeAdapter.MyViewHolder> {

    Context context;
    ArrayList<DashboardResponse.BannerData> bannerData;

    public AdsHomeAdapter(Context context, ArrayList<DashboardResponse.BannerData> bannerData) {
        this.context = context;
        this.bannerData = bannerData;
    }

    @NonNull
    @Override
    public AdsHomeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_ads_home, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AdsHomeAdapter.MyViewHolder holder, int position) {
        Utils.loadImageBanner(holder.binding.ads, bannerData.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return bannerData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildAdsHomeBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildAdsHomeBinding.bind(itemView);
        }

    }
}
