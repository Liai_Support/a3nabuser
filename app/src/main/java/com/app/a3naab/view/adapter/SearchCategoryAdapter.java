package com.app.a3naab.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildHomeSearchBinding;
import com.app.a3naab.model.SearchCategoryData;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.activity.SubCategoryActivity;

import java.util.ArrayList;

public class SearchCategoryAdapter extends RecyclerView.Adapter<SearchCategoryAdapter.MyViewHolder> {

    Activity context;
    ArrayList<SearchCategoryData> bannerData;
    SharedHelper sharedHelper;

    public SearchCategoryAdapter(Activity context, ArrayList<SearchCategoryData> bannerData) {
        this.context = context;
        this.bannerData = bannerData;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public SearchCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_home_search, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull SearchCategoryAdapter.MyViewHolder holder, int position) {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
            holder.binding.name.setText(bannerData.get(position).getArabicName());
        else
            holder.binding.name.setText(bannerData.get(position).getProductCategoryName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToPreference(bannerData.get(position));
                context.startActivity(new Intent(context, SubCategoryActivity.class)
                        .putExtra("categoryId", bannerData.get(position).getId())
                        .putExtra("fromSearch", true));
                context.finish();
            }
        });


        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            holder.binding.imageGo.setRotation(0f);
        }
    }

    @Override
    public int getItemCount() {
        return bannerData.size();
    }

    public void setValues(ArrayList<SearchCategoryData> list) {
        bannerData = list;
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildHomeSearchBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildHomeSearchBinding.bind(itemView);
        }

    }


    private void addToPreference(SearchCategoryData data) {

        ArrayList<SearchCategoryData> recent = new SharedHelper(context).getSubCategoryRecent();

        int position = -1;
        for (int i = 0; i < recent.size(); i++) {
            if (data.getId() == recent.get(i).getId()) {
                position = i;
                break;
            }
        }

        if (position != -1) {
            recent.remove(position);
        }
        recent.add(0, data);
        new SharedHelper(context).setSubCategoryRecent(recent);
    }

}
