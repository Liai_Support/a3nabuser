package com.app.a3naab.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class HomeFragmentAdapter(fm: FragmentManager, var fragment: ArrayList<Fragment>,lifecycle: Lifecycle) :
    FragmentStateAdapter(fm, lifecycle) {

  /*  override fun getItem(position: Int): Fragment {
        fragment[position].let { frag ->
            return frag
        }
    }

    override fun getCount(): Int {
        return fragment.size
    }*/

    override fun getItemCount(): Int {
        return fragment.size
    }

    override fun createFragment(position: Int): Fragment {
        fragment[position].let { frag ->
            return frag
        }
    }
}