package com.app.a3naab.view.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityAdsBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.AdsResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.adapter.AdsSearchAdapter;
import com.app.a3naab.viewmodel.CategoryViewModel;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.util.ArrayList;
import java.util.Locale;

public class AdsActivity extends BaseActivity {

    ActivityAdsBinding binding;
    ShoppingViewModel shoppingViewModel;
    CategoryViewModel categoryViewModel;
    ArrayList<AdsResponse.AdsData> list = new ArrayList<>();
    AdsSearchAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ads);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        categoryViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        initAdapter();
        getAds();
        initListener();
    }

    private void initListener() {

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }

        binding.searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().equals("")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.adsView.setVisibility(View.GONE);
                            binding.requestGroup.setVisibility(View.GONE);
                            getAds();
                        }
                    }, 1000);

                } else {
                    searchAds(editable.toString());
                }
            }
        });


        binding.addRequest.setOnClickListener(view -> {
            DialogUtils.showLoader(this);
            categoryViewModel.requestTypingText(this, "ads", binding.searchText.getText().toString()).observe(this, new Observer<CommonResponse>() {
                @Override
                public void onChanged(CommonResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError())
                        Utils.showSnack(binding.parentLayout, commonResponse.getMessage());
                    else {
                        DialogUtils.showOrderSuccessDialog(AdsActivity.this, new DialogCallback() {
                            @Override
                            public void onPositiveClick() {
                                finish();
                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });
                    }
                }
            });
        });



    }

    private void getAds() {
        shoppingViewModel.getAds(this).observe(this, new Observer<AdsResponse>() {
            @Override
            public void onChanged(AdsResponse adsResponse) {
                if (!adsResponse.getError()) {
                    list = new ArrayList<>();
                    for (int i=0;i<adsResponse.getAdsData().size();i++){
                        if(!adsResponse.getAdsData().get(i).getImage().equals("")){
                            list.add(adsResponse.getAdsData().get(i));
                        }
                    }
                    initAdapter();
                }
            }
        });

    }

    private void searchAds(String title) {
        shoppingViewModel.searchAds(this, title).observe(this, new Observer<AdsResponse>() {
            @Override
            public void onChanged(AdsResponse adsResponse) {
                if (!adsResponse.getError()) {
                    for (int i=0;i<adsResponse.getAdsData().size();i++){
                        if(adsResponse.getAdsData().get(i).getImage() == null || adsResponse.getAdsData().get(i).getImage().equals("")){
                            adsResponse.getAdsData().remove(i);
                        }
                    }
                    list = adsResponse.getAdsData();
                    initAdapter();
                }
            }
        });


    }

    private void initAdapter() {
        binding.adsView.setVisibility(View.VISIBLE);
        adapter = new AdsSearchAdapter(this, list);
        binding.adsView.setLayoutManager(new LinearLayoutManager(this));
        binding.adsView.setAdapter(adapter);

    }
}
