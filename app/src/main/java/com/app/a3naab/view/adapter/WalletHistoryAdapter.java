package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;

import com.app.a3naab.databinding.ChildWalletBalanceBinding;
import com.app.a3naab.model.WalletList;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.MyViewHolder> {

    Context context;
    ArrayList<WalletList> walletdata;
    SharedHelper sharedHelper;


    public WalletHistoryAdapter(Context context, ArrayList<WalletList> walletdata) {
        this.context = context;
        this.walletdata = walletdata;
    }

    @NonNull
    @Override
    public WalletHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        sharedHelper = new SharedHelper(context);
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_wallet_balance, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MyViewHolder holder, int position) {
        holder.binding.textView70.setText(""+walletdata.get(position).getAmount()+" "+context.getString(R.string.sar));
        holder.binding.time.setText(""+Utils.getConvertedTime(walletdata.get(position).getUpdateAt()));

        if(walletdata.get(position).getTypeOfTrans().equalsIgnoreCase("DEBIT")){
            holder.binding.walletType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.binding.walletType.setText(""+context.getString(R.string.debited_order_purchase));
        }
        else {
            holder.binding.walletType.setTextColor(context.getResources().getColor(R.color.amount_clr));
            holder.binding.walletType.setText(""+context.getString(R.string.credited_wallet_balance));
        }
    }

    @Override
    public int getItemCount() {
        return walletdata.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildWalletBalanceBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildWalletBalanceBinding.bind(itemView);
        }

    }
}
