package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityCategoryBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SessionManager;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.AdsHomeAdapter;
import com.app.a3naab.view.adapter.BestProductAdapter;
import com.app.a3naab.view.adapter.CategoryTopAdapter;
import com.app.a3naab.view.adapter.ProductCategoryAdapter;
import com.app.a3naab.viewmodel.CategoryViewModel;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.ArrayList;

public class CategoryActivity extends BaseActivity {

    int selectedCategoryId = 0;
    boolean fromSearch = false;
    CategoryViewModel viewModel;
    ActivityCategoryBinding binding;

    SharedHelper sharedHelper;
    ProductViewModel productViewModel;
    BestProductAdapter bestAdapter;
    ArrayList<ViewCartResponse.CartDetails> cartlist = new ArrayList<>();
    String boxid = "0";
    String cutid = "0";
    String sinstruction = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeblue);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category);
        viewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        getIntentValues();
        initListener();
        getCartCount();
        setNavBar(0);
        binding.showAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CategoryActivity.this, BestProductListActivity.class));
            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }

    }

    private void initListener() {
        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

        binding.showMore.setOnClickListener(view -> {
            startActivity(new Intent(this, AdsActivity.class));
        });

    }

    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCategoryValue();
        setProductTopAdapter();
        setAdapters();
        getCartCount();
    }

    private void setAdapters() {

        for (int i=0;i<SessionManager.getInstance().getBannerData().size();i++){
            if(SessionManager.getInstance().getBannerData().get(i).getImage() == null || SessionManager.getInstance().getBannerData().get(i).getImage().equals("")){
                SessionManager.getInstance().getBannerData().remove(i);
            }
        }

        binding.adsView.setAdapter(new AdsHomeAdapter(this, SessionManager.getInstance().getBannerData()));
        binding.indicator.setViewPager2(binding.adsView);


        binding.searchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CategoryActivity.this, SearchCategoryActivity.class)
                        .putExtra("id", selectedCategoryId));
            }
        });


    }

    private void setProductTopAdapter() {

        ArrayList<DashboardResponse.Categories> list = SessionManager.getInstance().getCategoryStore();

        if (fromSearch) {
            list = new ArrayList<>();
            for (int i = 0; i < SessionManager.getInstance().getCategoryStore().size(); i++) {
                if (selectedCategoryId == SessionManager.getInstance().getCategoryStore().get(i).getId()) {
                    list.add(SessionManager.getInstance().getCategoryStore().get(i));
                    list.get(0).setSelected(true);
                    binding.time.setText(list.get(0).getOrderProcessing());
                    binding.amount.setText(list.get(0).getMinimum() + " SAR");
                }
            }

        } else {
            for (int i = 0; i < list.size(); i++) {

                if (selectedCategoryId == list.get(i).getId()) {
                    list.get(i).setSelected(true);
                    setSelectedTexts(SessionManager.getInstance().getCategoryStore(), i);
                } else {
                    list.get(i).setSelected(false);
                }

            }

        }

        binding.tabView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        ArrayList<DashboardResponse.Categories> finalList = list;
        binding.tabView.setAdapter(new CategoryTopAdapter(this, list,
                position -> {
                    selectedCategoryId = finalList.get(position).getId();
                    setSelectedTexts(finalList, position);
                    getCategoryValue();
                }));
    }

    private void setSelectedTexts(ArrayList<DashboardResponse.Categories> finalList, int position) {

        binding.time.setText(finalList.get(position).getOrderProcessing()+" "+getString(R.string.mins));
        binding.amount.setText(finalList.get(position).getMinimum() + " SAR");

    }

    private void getIntentValues() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedCategoryId = bundle.getInt("categoryId", 0);
            fromSearch = bundle.getBoolean("fromSearch", false);
            viewModel.selectedCategoryId = selectedCategoryId;
        }
    }

    private void getCategoryValue() {

        DialogUtils.showLoader(this);
        viewModel.getCategoryDetails(this, selectedCategoryId).observe(this, categoryResponse -> {
            DialogUtils.dismissLoader();
            if (categoryResponse.getError()) {
//                Utils.showSnack(binding.parentLayout, categoryResponse.getMessage());
            } else {

                binding.bestProduct1.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

                if (categoryResponse.getData() != null) {
                    bestAdapter = new BestProductAdapter(this, categoryResponse.getData().getBestProducts(), new OnItemClick() {
                        @Override
                        public void onClick(int position) {
                            if(categoryResponse.getData().getBestProducts().get(position).getBoxStyle().size()!=0 || categoryResponse.getData().getBestProducts().get(position).getCuttingStyle().size()!=0){
                                if(checkiscart(categoryResponse.getData().getBestProducts().get(position).getId(),Integer.parseInt(categoryResponse.getData().getBestProducts().get(position).getStoreId()))){
                                    DialogUtils.showLoader(CategoryActivity.this);
                                    productViewModel.addtoCartWithStyle(CategoryActivity.this, categoryResponse.getData().getBestProducts().get(position).getId(), categoryResponse.getData().getBestProducts().get(position).getStoreId(), cutid, boxid, sinstruction)
                                            .observe(CategoryActivity.this, new Observer<CommonResponse>() {
                                                @Override
                                                public void onChanged(CommonResponse commonResponse) {
                                                    DialogUtils.dismissLoader();
                                                    if (!commonResponse.getError()) {
                                                        bestAdapter.notifyAddData(position);
                                                        getCartCount();
                                                    }
                                                }
                                            });
                                }
                                else {
                                    DialogUtils.showLoader(CategoryActivity.this);
                                    productViewModel.addItemToCart(CategoryActivity.this, categoryResponse.getData().getBestProducts().get(position).getId(), categoryResponse.getData().getBestProducts().get(position).getStoreId())
                                            .observe(CategoryActivity.this, new Observer<CommonResponse>() {
                                                @Override
                                                public void onChanged(CommonResponse commonResponse) {
                                                    DialogUtils.dismissLoader();
                                                    if (!commonResponse.getError()) {
                                                        bestAdapter.notifyAddData(position);
                                                        getCartCount();
                                                    }
                                                }
                                            });
                                }
                            }
                            else {
                                DialogUtils.showLoader(CategoryActivity.this);
                                productViewModel.addItemToCart(CategoryActivity.this, categoryResponse.getData().getBestProducts().get(position).getId(), categoryResponse.getData().getBestProducts().get(position).getStoreId())
                                        .observe(CategoryActivity.this, new Observer<CommonResponse>() {
                                            @Override
                                            public void onChanged(CommonResponse commonResponse) {
                                                DialogUtils.dismissLoader();
                                                if (!commonResponse.getError()) {
                                                    bestAdapter.notifyAddData(position);
                                                    getCartCount();
                                                }
                                            }
                                        });
                            }
                        }
                    }, new OnItemClick() {
                        @Override
                        public void onClick(int position) {
                            if(categoryResponse.getData().getBestProducts().get(position).getBoxStyle().size()!=0 || categoryResponse.getData().getBestProducts().get(position).getCuttingStyle().size()!=0){
                                if(checkiscart(categoryResponse.getData().getBestProducts().get(position).getId(),Integer.parseInt(categoryResponse.getData().getBestProducts().get(position).getStoreId()))){
                                    DialogUtils.showLoader(CategoryActivity.this);
                                    productViewModel.removeItemToCartwithstyle(CategoryActivity.this,  categoryResponse.getData().getBestProducts().get(position).getId(), categoryResponse.getData().getBestProducts().get(position).getStoreId(), cutid, boxid, sinstruction)
                                            .observe(CategoryActivity.this, new Observer<CommonResponse>() {
                                                @Override
                                                public void onChanged(CommonResponse commonResponse) {
                                                    DialogUtils.dismissLoader();
                                                    if (!commonResponse.getError()) {
                                                        bestAdapter.notifyRemoveData(position);
                                                        getCartCount();
                                                    }
                                                }
                                            });
                                }
                                else {
                                    DialogUtils.showLoader(CategoryActivity.this);
                                    productViewModel.removeItemToCart(CategoryActivity.this, categoryResponse.getData().getBestProducts().get(position).getId(), categoryResponse.getData().getBestProducts().get(position).getStoreId())
                                            .observe(CategoryActivity.this, new Observer<CommonResponse>() {
                                                @Override
                                                public void onChanged(CommonResponse commonResponse) {
                                                    DialogUtils.dismissLoader();
                                                    if (!commonResponse.getError()) {
                                                        bestAdapter.notifyRemoveData(position);
                                                        getCartCount();
                                                    }

                                                }
                                            });
                                }
                            }
                            else {
                                DialogUtils.showLoader(CategoryActivity.this);
                                productViewModel.removeItemToCart(CategoryActivity.this, categoryResponse.getData().getBestProducts().get(position).getId(), categoryResponse.getData().getBestProducts().get(position).getStoreId())
                                        .observe(CategoryActivity.this, new Observer<CommonResponse>() {
                                            @Override
                                            public void onChanged(CommonResponse commonResponse) {
                                                DialogUtils.dismissLoader();
                                                if (!commonResponse.getError()) {
                                                    bestAdapter.notifyRemoveData(position);
                                                    getCartCount();
                                                }

                                            }
                                        });
                            }
                        }
                    });

                    binding.bestProduct1.setAdapter(bestAdapter);
                }

                SessionManager.getInstance().setProductCategoryList(categoryResponse.getData().getData());
                binding.categoryList.setLayoutManager(new GridLayoutManager(this, 4));
                binding.categoryList.setAdapter(new ProductCategoryAdapter(this, categoryResponse.getData().getData(), new OnItemClick() {
                    @Override
                    public void onClick(int position) {


                        startActivity(new Intent(CategoryActivity.this, SubCategoryActivity.class)
                                .putExtra("categoryId", categoryResponse.getData().getData().get(position).getId())
                                .putExtra("havingSubCategory", categoryResponse.getData().getData().get(position).isSubcate()));

                    }
                }));
            }
        });
    }



    private void setNavBar(int position) {

        if (position == 0) {
            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));


        } else if (position == 1) {

            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
        } else if (position == 2) {


            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));






        } else if (position == 3) {
            binding.foot.shoppingImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.shoppingText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.orderImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.orderText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.cartImage.setColorFilter(ContextCompat.getColor(this, R.color.edit_text_color_disabled));
            binding.foot.cartText.setTextColor(ContextCompat.getColor(this, R.color.edit_text_color_disabled));

            binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));


        }

    }

    private void getCartCount() {

        productViewModel.getCartDetails(this).observe(this, response -> {
            if (!response.getError()) {
                if (response.getData().getMyCart().size() == 0) {
                    binding.foot.badge.setVisibility(View.GONE);
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                } else {
                    binding.foot.badge.setVisibility(View.VISIBLE);
                    binding.foot.badge.setText(response.getData().getMyCart().size() + "");
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                    cartlist = response.getData().getMyCart();
                }
            }
        });

    }

    private Boolean checkiscart(int productid,int storeid){
        boxid = "0";
        cutid = "0";
        sinstruction = "";
        for (int i=0;i<cartlist.size();i++){
            if(cartlist.get(i).getProductId() == productid && cartlist.get(i).getStoreId() == storeid){
                if(cartlist.get(i).getBoxStyle()!=null){
                    boxid = cartlist.get(i).getBoxStyle();
                }
                if(cartlist.get(i).getBoxStyle()!=null){
                    cutid = cartlist.get(i).getCuttingStyle();
                }
                if(cartlist.get(i).getSpecialInstructions()!=null){
                    sinstruction = cartlist.get(i).getSpecialInstructions();
                }
                return true;
            }
        }
        return false;
    }

}
