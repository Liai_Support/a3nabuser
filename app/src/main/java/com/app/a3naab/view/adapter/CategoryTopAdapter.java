package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildCategoryTopBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.utils.SharedHelper;

import java.util.ArrayList;

public class CategoryTopAdapter extends RecyclerView.Adapter<CategoryTopAdapter.MyViewHolder> {

    Context context;
    ArrayList<DashboardResponse.Categories> list;
    OnItemClick onItemClick;
    SharedHelper sharedHelper;

    public CategoryTopAdapter(Context context, ArrayList<DashboardResponse.Categories> list, OnItemClick onItemClick) {
        this.context = context;
        this.list = list;
        this.onItemClick = onItemClick;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public CategoryTopAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_category_top, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryTopAdapter.MyViewHolder holder, int position) {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
            holder.binding.name.setText(list.get(position).getArabicName());
        else
            holder.binding.name.setText(list.get(position).getCategoryName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(position).getIsComingSoon() == 0) {
                    setSelected(position);
                    onItemClick.onClick(position);
                }
            }
        });

        if (list.get(position).getIsComingSoon() == 1) {
            holder.binding.name.setAlpha(0.4f);
        } else {
            holder.binding.name.setAlpha(1f);
        }

        if (list.get(position).isSelected()) {
            holder.binding.view.setVisibility(View.VISIBLE);
        } else {
            holder.binding.view.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setValues(ArrayList<DashboardResponse.Categories> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildCategoryTopBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildCategoryTopBinding.bind(itemView);
        }

    }

    private void setSelected(int position) {

        for (int i = 0; i < list.size(); i++) {
            list.get(i).setSelected(position == i);
        }
        notifyDataSetChanged();
    }
}
