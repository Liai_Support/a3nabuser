package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityProductDetailsBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.BestProductAdapter;
import com.app.a3naab.view.adapter.ProductImagesAdapter;
import com.app.a3naab.viewmodel.ProductViewModel;

import java.util.ArrayList;

public class ProductDetailActivity extends BaseActivity {

    ActivityProductDetailsBinding binding;
    DashboardResponse.BestProduct data;
    ArrayList<DashboardResponse.ProductImages> imageData;
    ProductImagesAdapter adapter;
    SharedHelper sharedHelper;
    ProductViewModel productViewModel;

    BestProductAdapter bestAdapter;
    BestProductAdapter relatedAdapter;
    ProductViewModel viewModel;
    int proId = 0;
    String storeId = "0";
    String boxid = "0";
    String cutid = "0";
    String sinstruction = "";
    boolean iscart = false;
    ArrayList<ViewCartResponse.CartDetails> cartlist = new ArrayList<>();

    private boolean isFavo = false;

    private String selectedBox = "";
    private String selectedcut = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setTheme(R.style.AppThemewhite);
        setTheme(R.style.AppTheme);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_details);
        sharedHelper = new SharedHelper(this);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        getIntentValues();
        initListener();

        setForArabic();
    }

    private void setForArabic() {

        if (sharedHelper.getSelectedLanguage().equals("ar")) {

            binding.backImage.setRotation(180f);
            binding.frontImage.setRotation(0f);
            binding.imageView16.setRotation(180f);

            binding.view4.setBackground(ContextCompat.getDrawable(this, R.drawable.product_detail_bg_arabic));

        }

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        getDetails();
    }

    private void initListener() {

        if (sharedHelper.getLoggedIn()) {
            binding.favorite.setVisibility(View.VISIBLE);
        } else {
            binding.favorite.setVisibility(View.GONE);
        }

        binding.showAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetailActivity.this, BestProductListActivity.class));
            }
        });

        binding.showAllRelated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetailActivity.this, RelatedProductListActivity.class)
                        .putExtra("id", String.valueOf(data.getId())));
            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.back2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.addToCartText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sharedHelper.getLoggedIn()) {
                    addItemToCart();
                } else {
                    Intent intent = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }
            }
        });

        binding.addToCartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sharedHelper.getLoggedIn()) {
                    addItemToCart();
                } else {
                    Intent intent = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }
            }
        });

        binding.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtils.showLoader(ProductDetailActivity.this);
                Log.d("v cv",""+data.getIsFavourite());
                if (data.getIsFavourite().equals("true")) {
                    viewModel.setFavorite(ProductDetailActivity.this, proId,Integer.parseInt(storeId), "false").observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                        @Override
                        public void onChanged(CommonResponse commonResponse) {
                            DialogUtils.dismissLoader();
                            if (!commonResponse.getError()) {
                                getDetails();
                            }
                        }
                    });
                } else {
                    DialogUtils.showLoader(ProductDetailActivity.this);
                    viewModel.setFavorite(ProductDetailActivity.this, proId,Integer.parseInt(storeId), "true").observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                        @Override
                        public void onChanged(CommonResponse commonResponse) {
                            DialogUtils.dismissLoader();
                            if (!commonResponse.getError()) {
                                getDetails();
                            }
                        }
                    });
                }


            }
        });

        binding.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.getMaxQty() > data.getCount())
                    addItemToCart();
                else
                    DialogUtils.showMaximumLimit(ProductDetailActivity.this);
            }
        });

        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeItemToCart();
            }
        });

        binding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeItemToCart();
            }
        });


        binding.cuttingStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data != null && data.getCuttingStyle() != null)
                    DialogUtils.getCuttingDialog(cutid,ProductDetailActivity.this, data.getCuttingStyle(), new OnItemClick() {
                        @Override
                        public void onClick(int position) {
                            if(iscart == false) {
                                if (position != -1) {
                                    selectedcut = String.valueOf(data.getCuttingStyle().get(position).getId());
                                    cutid = String.valueOf(data.getCuttingStyle().get(position).getId());
                                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar") && !data.getCuttingStyle().get(position).getArabicName().equalsIgnoreCase("")) {
                                        binding.cuttingStyleText.setText(data.getCuttingStyle().get(position).getArabicName());
                                    } else {
                                        binding.cuttingStyleText.setText(data.getCuttingStyle().get(position).getCuttingName());
                                    }
                                }
                                else {
                                    selectedcut = "";
                                    cutid = "0";
                                    binding.cuttingStyleText.setText(getString(R.string.cutting_style));
                                }
                            }
                            else if(iscart == true) {
                                if (position != -1) {
                                    selectedcut = String.valueOf(data.getCuttingStyle().get(position).getId());
                                    cutid = String.valueOf(data.getCuttingStyle().get(position).getId());
                                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar") && !data.getCuttingStyle().get(position).getArabicName().equalsIgnoreCase("")) {
                                        binding.cuttingStyleText.setText(data.getCuttingStyle().get(position).getArabicName());
                                    } else {
                                        binding.cuttingStyleText.setText(data.getCuttingStyle().get(position).getCuttingName());
                                    }
                                }
                                else {
                                    selectedcut = "";
                                    cutid = "0";
                                    binding.cuttingStyleText.setText(getString(R.string.cutting_style));
                                }

                                removeItemToCart();
                                addItemToCart();
                            }

                        }
                    });
            }
        });



        binding.boxStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data != null && data.getBoxStyle() != null) {
                    DialogUtils.getBoxDialog(boxid, ProductDetailActivity.this, data.getBoxStyle(), position -> {
                        if (iscart == false) {
                            if (position != -1) {
                                selectedBox = String.valueOf(data.getBoxStyle().get(position).getId());
                                boxid = String.valueOf(data.getBoxStyle().get(position).getId());
                                if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar") && !data.getCuttingStyle().get(position).getArabicName().equalsIgnoreCase("")) {
                                    binding.boxStyleText.setText(data.getBoxStyle().get(position).getArabicName());
                                } else {
                                    binding.boxStyleText.setText(data.getBoxStyle().get(position).getBoxName());
                                }
                            } else {
                                selectedBox = "";
                                boxid = "0";
                                binding.boxStyleText.setText(getString(R.string.box_style));
                            }
                        } else if (iscart == true) {
                            if (position != -1) {
                                selectedBox = String.valueOf(data.getBoxStyle().get(position).getId());
                                boxid = String.valueOf(data.getBoxStyle().get(position).getId());
                                if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar") && !data.getCuttingStyle().get(position).getArabicName().equalsIgnoreCase("")) {
                                    binding.boxStyleText.setText(data.getBoxStyle().get(position).getArabicName());
                                } else {
                                    binding.boxStyleText.setText(data.getBoxStyle().get(position).getBoxName());
                                }
                            } else {
                                selectedBox = "";
                                boxid = "0";
                                binding.boxStyleText.setText(getString(R.string.box_style));
                            }

                            removeItemToCart();
                            addItemToCart();
                        }
                    });
                }
            }
        });

    }

    private void getIntentValues() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            proId = bundle.getInt("data");
            storeId = bundle.getString("storeid");
        }

    }

    private void getDetails() {
        viewModel.getProductDetails(this, proId,storeId).observe(this, value -> {
            if (!value.getError()) {
                if (value.getData().getProductDetails() != null) {
                    data = value.getData().getProductDetails();
                    imageData = new ArrayList<>();
                    imageData.addAll(value.getData().getProductDetails().getProductImages());
                    setValues();
                    notifyText();
                    setAdapter(value.getData().getBestProducts(), value.getData().getRelatedProducts());
                    setCountUi();
                    getCartDetails();
                }
            }

        });
    }

    private void setValues() {

        storeId = data.getStoreId();

        if (data.getProductDiscount().equals("0")) {
            binding.imageView16.setVisibility(View.INVISIBLE);
            binding.discount.setVisibility(View.INVISIBLE);
        } else {
            binding.imageView16.setVisibility(View.VISIBLE);
            binding.discount.setVisibility(View.VISIBLE);
        }

//        if (data.getInstruction() == null || data.getInstruction().trim().equals("")) {
//            binding.cardView7.setVisibility(View.GONE);
//        } else {
//            binding.cardView7.setVisibility(View.VISIBLE);
//        }

        binding.discount.setText(data.getProductDiscount() + "% off");

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.textView7.setText(data.getArabicName());
        } else {
            binding.textView7.setText(data.getProductName());
        }

        if (data.getStoreStock() == null) {
            binding.noStock.setVisibility(View.VISIBLE);
        } else {
            if (data.getStoreStock() == 0) {
                binding.noStock.setVisibility(View.VISIBLE);
            } else {
                binding.noStock.setVisibility(View.GONE);
            }
        }


        if (data.getProductDiscount() != null & !data.getProductDiscount().equals("0") && !data.getProductDiscount().equals("")) {
            binding.price.setText(String.valueOf(Double.parseDouble(data.getProductPrice()) - ((Double.parseDouble(data.getProductPrice()) * Double.parseDouble(data.getProductDiscount())) / 100)));
        } else {
            binding.price.setText(data.getProductPrice());
        }
//        binding.price.setText(data.getProductPrice());


        binding.quandity.setText(data.getProductWeight());
//        binding.instructions.setText(data.getInstruction());

        if (imageData != null) {
            if (imageData.size() <= 1) {
                if(imageData.size()==0){
                    binding.dummy.setVisibility(View.VISIBLE);
                    binding.adsView.setVisibility(View.GONE);
                    binding.backImage.setVisibility(View.GONE);
                    binding.frontImage.setVisibility(View.GONE);
                }
                else {
                    binding.backImage.setVisibility(View.GONE);
                    binding.frontImage.setVisibility(View.GONE);
                }
            } else {
                binding.backImage.setVisibility(View.VISIBLE);
                binding.frontImage.setVisibility(View.VISIBLE);
            }

            adapter = new ProductImagesAdapter(this, imageData);
            binding.adsView.setAdapter(adapter);
            binding.backImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (binding.adsView.getCurrentItem() != 0) {
                        binding.adsView.setCurrentItem(binding.adsView.getCurrentItem() - 1);
                    }

                }
            });

            binding.frontImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (binding.adsView.getCurrentItem() < imageData.size() - 1) {
                        binding.adsView.setCurrentItem(binding.adsView.getCurrentItem() + 1);
                    }
                }
            });

            binding.indicator.setViewPager2(binding.adsView);
        }


        if (data.getIsFavourite().equals("true")) {
            binding.favorite.setImageDrawable(ContextCompat.getDrawable(ProductDetailActivity.this, R.drawable.favourite));
        } else {
            binding.favorite.setImageDrawable(ContextCompat.getDrawable(ProductDetailActivity.this, R.drawable.unfavorite));
        }

        if (data.getBoxStyle().size() == 0) {
            binding.boxStyle.setVisibility(View.GONE);
            binding.boxStyleImage.setVisibility(View.GONE);
            binding.boxStyleText.setVisibility(View.GONE);
        } else {
            binding.boxStyle.setVisibility(View.VISIBLE);
            binding.boxStyleImage.setVisibility(View.VISIBLE);
            binding.boxStyleText.setVisibility(View.VISIBLE);
        }

        if (data.getCuttingStyle().size() == 0) {
            binding.cuttingStyle.setVisibility(View.GONE);
            binding.cuttingStyleImage.setVisibility(View.GONE);
            binding.cuttingStyleText.setVisibility(View.GONE);
        } else {
            binding.cuttingStyle.setVisibility(View.VISIBLE);
            binding.cuttingStyleImage.setVisibility(View.VISIBLE);
            binding.cuttingStyleText.setVisibility(View.VISIBLE);
        }


        if (data.getBoxStyle().size() == 0 && data.getCuttingStyle().size() == 0) {
            binding.viewStyle.setVisibility(View.GONE);
            binding.cardView7.setVisibility(View.GONE);
        } else {
            binding.viewStyle.setVisibility(View.VISIBLE);
            binding.cardView7.setVisibility(View.VISIBLE);
        }

        if(data.getInstruction()){
            binding.cardView7.setVisibility(View.VISIBLE);
            binding.instructions.setText(""+data.getSpecialInstructions());
        }

    }

    void setAdapter(ArrayList<DashboardResponse.BestProduct> bestProducts, ArrayList<DashboardResponse.BestProduct> relatedProduct) {


        bestAdapter = new BestProductAdapter(this, bestProducts, new OnItemClick() {
            @Override
            public void onClick(int position) {
                if(bestProducts.get(position).getBoxStyle().size()!=0 || bestProducts.get(position).getCuttingStyle().size()!=0){
                    if(checkiscart(bestProducts.get(position).getId(),Integer.parseInt(bestProducts.get(position).getStoreId()))){
                        DialogUtils.showLoader(ProductDetailActivity.this);
                        viewModel.addtoCartWithStyle(ProductDetailActivity.this, bestProducts.get(position).getId(), bestProducts.get(position).getStoreId(), cutid, boxid, sinstruction)
                                .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyAddData(position);
                                            if (bestProducts.get(position).getId() == proId) {
                                                getDetails();
                                            }                                        }
                                    }
                                });

                    }
                    else {
                        DialogUtils.showLoader(ProductDetailActivity.this);
                        viewModel.addItemToCart(ProductDetailActivity.this, bestProducts.get(position).getId(), bestProducts.get(position).getStoreId())
                                .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyAddData(position);
                                            if (bestProducts.get(position).getId() == proId) {
                                                getDetails();
                                            }
                                        }
                                    }
                                });

                    }
                }
                else {
                    DialogUtils.showLoader(ProductDetailActivity.this);
                    viewModel.addItemToCart(ProductDetailActivity.this, bestProducts.get(position).getId(), bestProducts.get(position).getStoreId())
                            .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        bestAdapter.notifyAddData(position);
                                        if (bestProducts.get(position).getId() == proId) {
                                            getDetails();
                                        }
                                    }
                                }
                            });
                }
            }
        }, new OnItemClick() {
            @Override
            public void onClick(int position) {
                if(bestProducts.get(position).getBoxStyle().size()!=0 || bestProducts.get(position).getCuttingStyle().size()!=0){
                    if(checkiscart(bestProducts.get(position).getId(),Integer.parseInt(bestProducts.get(position).getStoreId()))){

                        DialogUtils.showLoader(ProductDetailActivity.this);
                        productViewModel.removeItemToCartwithstyle(ProductDetailActivity.this,  bestProducts.get(position).getId(), bestProducts.get(position).getStoreId(), cutid, boxid, sinstruction)
                                .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyRemoveData(position);
                                            if (bestProducts.get(position).getId() == proId) {
                                                getDetails();
                                            }
                                        }
                                    }
                                });
                    }
                    else {
                        DialogUtils.showLoader(ProductDetailActivity.this);
                        viewModel.removeItemToCart(ProductDetailActivity.this, bestProducts.get(position).getId(), bestProducts.get(position).getStoreId())
                                .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                    @Override
                                    public void onChanged(CommonResponse commonResponse) {
                                        DialogUtils.dismissLoader();
                                        if (!commonResponse.getError()) {
                                            bestAdapter.notifyRemoveData(position);
                                            if (bestProducts.get(position).getId() == proId) {
                                                getDetails();
                                            }
                                        }

                                    }
                                });

                    }
                }
                else {
                    DialogUtils.showLoader(ProductDetailActivity.this);
                    viewModel.removeItemToCart(ProductDetailActivity.this, bestProducts.get(position).getId(), bestProducts.get(position).getStoreId())
                            .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        bestAdapter.notifyRemoveData(position);
                                        if (bestProducts.get(position).getId() == proId) {
                                            getDetails();
                                        }
                                    }

                                }
                            });
                }
            }
        });


        if (relatedProduct.size() == 0) {
            binding.textView9.setVisibility(View.GONE);
            binding.showAllRelated.setVisibility(View.GONE);
        } else {
            binding.textView9.setVisibility(View.VISIBLE);
            binding.showAllRelated.setVisibility(View.VISIBLE);
        }

        binding.bestProduct1.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        binding.bestProduct1.setAdapter(bestAdapter);

        relatedAdapter = new BestProductAdapter(this, relatedProduct, position -> {
            if(relatedProduct.get(position).getBoxStyle().size()!=0 || relatedProduct.get(position).getCuttingStyle().size()!=0){
                if(checkiscart(relatedProduct.get(position).getId(),Integer.parseInt(relatedProduct.get(position).getStoreId()))){
                    DialogUtils.showLoader(ProductDetailActivity.this);
                    viewModel.addtoCartWithStyle(ProductDetailActivity.this, relatedProduct.get(position).getId(), relatedProduct.get(position).getStoreId(), cutid, boxid, sinstruction)
                            .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        relatedAdapter.notifyAddData(position);
                                        if (relatedProduct.get(position).getId() == proId) {
                                            getDetails();
                                        }
                                    }
                                }
                            });
                }
                else {
                    DialogUtils.showLoader(ProductDetailActivity.this);
                    viewModel.addItemToCart(ProductDetailActivity.this, relatedProduct.get(position).getId(), relatedProduct.get(position).getStoreId())
                            .observe(ProductDetailActivity.this, commonResponse -> {
                                DialogUtils.dismissLoader();
                                if (!commonResponse.getError()) {
                                    relatedAdapter.notifyAddData(position);
                                    if (relatedProduct.get(position).getId() == proId) {
                                        getDetails();
                                    }
                                }
                            });
                }
            }
            else {
                DialogUtils.showLoader(ProductDetailActivity.this);
                viewModel.addItemToCart(ProductDetailActivity.this, relatedProduct.get(position).getId(), relatedProduct.get(position).getStoreId())
                        .observe(ProductDetailActivity.this, commonResponse -> {
                            DialogUtils.dismissLoader();
                            if (!commonResponse.getError()) {
                                relatedAdapter.notifyAddData(position);
                                if (relatedProduct.get(position).getId() == proId) {
                                    getDetails();
                                }
                            }
                        });
            }
        }, position -> {
            if(relatedProduct.get(position).getBoxStyle().size()!=0 || relatedProduct.get(position).getCuttingStyle().size()!=0){
                if(checkiscart(relatedProduct.get(position).getId(),Integer.parseInt(relatedProduct.get(position).getStoreId()))){
                    DialogUtils.showLoader(ProductDetailActivity.this);
                    productViewModel.removeItemToCartwithstyle(ProductDetailActivity.this,  relatedProduct.get(position).getId(), relatedProduct.get(position).getStoreId(), cutid, boxid, sinstruction)
                            .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        relatedAdapter.notifyRemoveData(position);
                                        if (relatedProduct.get(position).getId() == proId) {
                                            getDetails();
                                        }
                                    }
                                }
                            });

                }
                else {
                    DialogUtils.showLoader(ProductDetailActivity.this);
                    viewModel.removeItemToCart(ProductDetailActivity.this, relatedProduct.get(position).getId(), relatedProduct.get(position).getStoreId())
                            .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                                @Override
                                public void onChanged(CommonResponse commonResponse) {
                                    DialogUtils.dismissLoader();
                                    if (!commonResponse.getError()) {
                                        relatedAdapter.notifyRemoveData(position);
                                        if (relatedProduct.get(position).getId() == proId) {
                                            getDetails();
                                        }
                                    }

                                }
                            });

                }
            }
            else {
                DialogUtils.showLoader(ProductDetailActivity.this);
                viewModel.removeItemToCart(ProductDetailActivity.this, relatedProduct.get(position).getId(), relatedProduct.get(position).getStoreId())
                        .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                            @Override
                            public void onChanged(CommonResponse commonResponse) {
                                DialogUtils.dismissLoader();
                                if (!commonResponse.getError()) {
                                    relatedAdapter.notifyRemoveData(position);
                                    if (relatedProduct.get(position).getId() == proId) {
                                        getDetails();
                                    }
                                }

                            }
                        });
            }
        });

        binding.relatedProduct.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        binding.relatedProduct.setAdapter(relatedAdapter);

    }

    private void setProductAvailable(int id) {
        ArrayList<String> list = sharedHelper.getAddedCategory();
        if (isProductAvailable(id)) {
            list.remove(String.valueOf(id));
        } else {
            list.add(String.valueOf(id));
        }
        sharedHelper.setAddedCategory(list);
        notifyText();
    }

    private void notifyText() {
        if (isProductAvailable(data.getId())) {
            binding.addToCartText.setText(getString(R.string.added));
        } else {
            binding.addToCartText.setText(getString(R.string.add_to_cart));
        }

    }

    private boolean isProductAvailable(int id) {
        return sharedHelper.getAddedCategory().contains(String.valueOf(id));

    }


    private void setCountUi() {
        if (data.getCount() == 0) {
            binding.grpAddCart.setVisibility(View.VISIBLE);

            binding.add.setVisibility(View.GONE);
            binding.count.setVisibility(View.GONE);
            binding.minus.setVisibility(View.GONE);
            binding.delete.setVisibility(View.GONE);
        } else {

            if (data.getCount() == 1) {
                binding.delete.setVisibility(View.VISIBLE);
                binding.minus.setVisibility(View.GONE);
            } else {
                binding.delete.setVisibility(View.GONE);
                binding.minus.setVisibility(View.VISIBLE);
            }

            binding.grpAddCart.setVisibility(View.GONE);

            binding.add.setVisibility(View.VISIBLE);
            binding.count.setVisibility(View.VISIBLE);


        }

        binding.count.setText(data.getCount() + "");
    }

    private void addItemToCart() {
        DialogUtils.showLoader(ProductDetailActivity.this);
        viewModel.addtoCartWithStyle(ProductDetailActivity.this, proId, storeId, selectedcut, selectedBox, binding.instructions.getText().toString())
                .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                    @Override
                    public void onChanged(CommonResponse commonResponse) {
                        DialogUtils.dismissLoader();
                        if (!commonResponse.getError()) {
                            data.setCount(data.getCount() + 1);
                            getDetails();
                            getCartDetails();
                        }
                    }
                });
    }


    private void removeItemToCart() {
        DialogUtils.showLoader(ProductDetailActivity.this);
        viewModel.removeItemToCartwithstyle(ProductDetailActivity.this, proId, storeId, selectedcut, selectedBox, binding.instructions.getText().toString())
                .observe(ProductDetailActivity.this, new Observer<CommonResponse>() {
                    @Override
                    public void onChanged(CommonResponse commonResponse) {
                        DialogUtils.dismissLoader();
                        if (!commonResponse.getError()) {
                            data.setCount(data.getCount() - 1);
                            getDetails();
                            getCartDetails();
                        }
                    }
                });
    }

    private void getCartDetails() {

        productViewModel.getCartDetails(this).observe(this, response -> {
            if (!response.getError()) {
                if (response.getData().getMyCart().size() == 0) {
                    // binding.badge.setVisibility(View.GONE);
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                    iscart = false;
                    boxid = "0";
                    cutid = "0";
                    selectedcut = "";
                    selectedBox = "";
                } else {
                    // binding.badge.setVisibility(View.VISIBLE);
                    // binding.badge.setText(response.getData().getMyCart().size() + "");
                    cartlist = response.getData().getMyCart();
                    sharedHelper.setCartCount(response.getData().getMyCart().size());
                    for (int i = 0; i < response.getData().getMyCart().size(); i++) {
                        if (proId == response.getData().getMyCart().get(i).getProductId() && Integer.parseInt(storeId) == response.getData().getMyCart().get(i).getStoreId()) {
                            iscart = true;
                            if (response.getData().getMyCart().get(i).getBoxStyle() != null) {
                                boxid = response.getData().getMyCart().get(i).getBoxStyle();
                                selectedBox = response.getData().getMyCart().get(i).getBoxStyle();
                            }
                            if (response.getData().getMyCart().get(i).getCuttingStyle() != null) {
                                cutid = response.getData().getMyCart().get(i).getCuttingStyle();
                                selectedcut = response.getData().getMyCart().get(i).getCuttingStyle();
                            }
                            break;
                        }
                    }

                    if(data.getCuttingStyle().size() != 0){
                        Log.d("scsc1",""+iscart);
                        if(iscart == true){
                            Log.d("vbbxn x",""+data.getCuttingStyle());
                            for(int i=0;i<data.getCuttingStyle().size();i++){
                                if(Integer.parseInt(cutid) == data.getCuttingStyle().get(i).getId()){
                                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar") && !data.getCuttingStyle().get(i).getArabicName().equalsIgnoreCase("")) {
                                        binding.cuttingStyleText.setText(data.getCuttingStyle().get(i).getArabicName());
                                    } else {
                                        binding.cuttingStyleText.setText(data.getCuttingStyle().get(i).getCuttingName());
                                    }
                                }
                            }
                        }
                    }

                    if(data.getBoxStyle().size() != 0){
                        if(iscart==true){
                            for(int i=0;i<data.getBoxStyle().size();i++){
                                if(Integer.parseInt(boxid) == data.getBoxStyle().get(i).getId()){
                                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar") && !data.getBoxStyle().get(i).getArabicName().equalsIgnoreCase("")) {
                                        binding.boxStyleText.setText(data.getBoxStyle().get(i).getArabicName());
                                    } else {
                                        binding.boxStyleText.setText(data.getBoxStyle().get(i).getBoxName());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

    }

    private Boolean checkiscart(int productid,int storeid){
        boxid = "0";
        cutid = "0";
        sinstruction = "";
        for (int i=0;i<cartlist.size();i++){
            if(cartlist.get(i).getProductId() == productid && cartlist.get(i).getStoreId() == storeid){
                if(cartlist.get(i).getBoxStyle()!=null){
                    boxid = cartlist.get(i).getBoxStyle();
                }
                if(cartlist.get(i).getBoxStyle()!=null){
                    cutid = cartlist.get(i).getCuttingStyle();
                }
                if(cartlist.get(i).getSpecialInstructions()!=null){
                    sinstruction = cartlist.get(i).getSpecialInstructions();
                }
                return true;
            }
        }
        return false;
    }

}
