package com.app.a3naab.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildCategoryHome2Binding;
import com.app.a3naab.databinding.ChildCategoryHomeBinding;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.CategoryActivity;

import java.util.ArrayList;

public class CategoryHomeAdapter extends RecyclerView.Adapter<CategoryHomeAdapter.MyViewHolder> {


    Context context;
    ArrayList<ArrayList<DashboardResponse.Categories>> stores;
    SharedHelper sharedHelper;

    public CategoryHomeAdapter(Context requireContext, ArrayList<ArrayList<DashboardResponse.Categories>> stores) {
        this.context = requireContext;
        this.stores = stores;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public CategoryHomeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new MyViewHolder(
                    LayoutInflater.from(context).inflate(R.layout.child_category_home, parent, false)
                    , 0);
        } else {
            return new MyViewHolder(
                    LayoutInflater.from(context).inflate(R.layout.child_category_home2, parent, false)
                    , 1);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHomeAdapter.MyViewHolder holder, int position) {

        if (position == 0) {
            if (stores.get(position).size() == 1) {
                holder.binding.cardView11.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams layoutParams = holder.binding.cardView11.getLayoutParams();
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                holder.binding.cardView11.setLayoutParams(layoutParams);
                holder.binding.card2.setVisibility(View.GONE);
            } else {
                holder.binding.cardView11.setVisibility(View.VISIBLE);
                holder.binding.card2.setVisibility(View.VISIBLE);
            }

            for (int i = 0; i < stores.get(position).size(); i++) {
                if (i == 0) {
                    Utils.loadImage(holder.binding.storeImage, stores.get(position).get(i).getCategoryImage());
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
                        holder.binding.title.setText(stores.get(position).get(i).getArabicName());
                    else
                        holder.binding.title.setText(stores.get(position).get(i).getCategoryName());
//                    holder.binding.bgView.setBackground(ContextCompat.getDrawable(context, getDrawable(i)));

                    if (stores.get(position).get(i).getIsComingSoon() == 1) {
                        holder.binding.commingSoon1.setVisibility(View.VISIBLE);
                    } else {
                        holder.binding.commingSoon1.setVisibility(View.GONE);
                    }
                } else {
                    Utils.loadImage(holder.binding.storeImage2, stores.get(position).get(i).getCategoryImage());
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
                        holder.binding.title2.setText(stores.get(position).get(i).getArabicName());
                    else
                        holder.binding.title2.setText(stores.get(position).get(i).getCategoryName());
//                    holder.binding.bgView2.setBackground(ContextCompat.getDrawable(context, getDrawable(i)));
                    if (stores.get(position).get(i).getIsComingSoon() == 1) {
                        holder.binding.commingSoon2.setVisibility(View.VISIBLE);
                    } else {
                        holder.binding.commingSoon2.setVisibility(View.GONE);
                    }

                }
            }
        }
        else {

            if (stores.get(position).size() == 1) {
                holder.binding2.cardView11.setVisibility(View.VISIBLE);
                holder.binding2.cardView12.setVisibility(View.GONE);
                holder.binding2.cardView13.setVisibility(View.GONE);
            } else if (stores.get(position).size() == 2) {
                holder.binding2.cardView11.setVisibility(View.VISIBLE);
                holder.binding2.cardView12.setVisibility(View.VISIBLE);
                holder.binding2.cardView13.setVisibility(View.GONE);
            } else if (stores.get(position).size() == 3){
                holder.binding2.cardView11.setVisibility(View.VISIBLE);
                holder.binding2.cardView12.setVisibility(View.VISIBLE);
                holder.binding2.cardView13.setVisibility(View.VISIBLE);
            }else {
                holder.binding2.cardView11.setVisibility(View.GONE);
                holder.binding2.cardView12.setVisibility(View.GONE);
                holder.binding2.cardView13.setVisibility(View.GONE);
            }
            for (int i = 0; i < stores.get(position).size(); i++) {
                if (i == 0) {
                    Utils.loadImage(holder.binding2.storeImage, stores.get(position).get(i).getCategoryImage());
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
                        holder.binding2.title.setText(stores.get(position).get(i).getArabicName());
                    else
                        holder.binding2.title.setText(stores.get(position).get(i).getCategoryName());
//                    holder.binding2.bgView.setBackground(ContextCompat.getDrawable(context, getDrawable(position + ((position + position) - 1))));

                    if (stores.get(position).get(i).getIsComingSoon() == 1) {
                        holder.binding2.commingSoon1.setVisibility(View.VISIBLE);
                    } else {
                        holder.binding2.commingSoon1.setVisibility(View.GONE);
                    }

                } else if (i == 1) {
                    Utils.loadImage(holder.binding2.storeImage2, stores.get(position).get(i).getCategoryImage());
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
                        holder.binding2.title2.setText(stores.get(position).get(i).getArabicName());
                    else
                        holder.binding2.title2.setText(stores.get(position).get(i).getCategoryName());
//                    holder.binding2.bgView2.setBackground(ContextCompat.getDrawable(context, getDrawable(position * 3)));

                    if (stores.get(position).get(i).getIsComingSoon() == 1) {
                        holder.binding2.commingSoon2.setVisibility(View.VISIBLE);
                    } else {
                        holder.binding2.commingSoon2.setVisibility(View.GONE);
                    }


                } else {
                    Utils.loadImage(holder.binding2.storeImage3, stores.get(position).get(i).getCategoryImage());
                    if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
                        holder.binding2.title3.setText(stores.get(position).get(i).getArabicName());
                    else
                    holder.binding2.title3.setText(stores.get(position).get(i).getCategoryName());
//                    holder.binding2.bgView3.setBackground(ContextCompat.getDrawable(context, getDrawable((position + ((position + position) - 1)) + 2)));

                    if (stores.get(position).get(i).getIsComingSoon() == 1) {
                        holder.binding2.commingSoon3.setVisibility(View.VISIBLE);
                    } else {
                        holder.binding2.commingSoon3.setVisibility(View.GONE);
                    }


                }
            }
        }

        if (position == 0) {
            holder.binding.cardView11.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (stores.get(position).get(0).getIsComingSoon() == 0)
                        context.startActivity(new Intent(context, CategoryActivity.class)
                                .putExtra("categoryId", stores.get(position).get(0).getId()));
                }
            });

            holder.binding.card2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (stores.get(position).get(1).getIsComingSoon() == 0)
                        context.startActivity(new Intent(context, CategoryActivity.class)
                                .putExtra("categoryId", stores.get(position).get(1).getId()));
                }
            });
        }
        else {
            holder.binding2.cardView11.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (stores.get(position).get(0).getIsComingSoon() == 0)
                        context.startActivity(new Intent(context, CategoryActivity.class)
                                .putExtra("categoryId", stores.get(position).get(0).getId()));
                }
            });

            holder.binding2.cardView12.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (stores.get(position).get(1).getIsComingSoon() == 0)
                        context.startActivity(new Intent(context, CategoryActivity.class)
                                .putExtra("categoryId", stores.get(position).get(1).getId()));
                }
            });

            holder.binding2.cardView13.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (stores.get(position).get(2).getIsComingSoon() == 0)
                        context.startActivity(new Intent(context, CategoryActivity.class)
                                .putExtra("categoryId", stores.get(position).get(2).getId()));
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return stores.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildCategoryHomeBinding binding;
        ChildCategoryHome2Binding binding2;

        public MyViewHolder(@NonNull View itemView, int i) {
            super(itemView);
            if (i == 0) {
                binding = ChildCategoryHomeBinding.bind(itemView);
            } else {
                binding2 = ChildCategoryHome2Binding.bind(itemView);
            }

        }

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private int getDrawable(int position) {
        switch (position) {
            case 0: {
                return R.drawable.green_bg;
            }
            case 1: {
                return R.drawable.red_bg;
            }
            case 2: {
                return R.drawable.yellow_bg;
            }
            case 3: {
                return R.drawable.blue_bg;
            }

            default:
                return R.drawable.green_bg;
        }
    }
}
