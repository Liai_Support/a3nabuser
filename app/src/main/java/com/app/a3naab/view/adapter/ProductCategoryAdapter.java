package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildProductCategoryBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.ProductCategoryData;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;

import java.util.ArrayList;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.MyViewHolder> {

    Context context;
    ArrayList<ProductCategoryData> bannerData;
    OnItemClick onItemClick;
    SharedHelper sharedHelper;

    public ProductCategoryAdapter(Context context, ArrayList<ProductCategoryData> bannerData, OnItemClick onItemClick) {
        this.context = context;
        this.bannerData = bannerData;
        this.onItemClick = onItemClick;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public ProductCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_product_category, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ProductCategoryAdapter.MyViewHolder holder, int position) {

        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
            holder.binding.name.setText(bannerData.get(position).getArabicName());
        else
            holder.binding.name.setText(bannerData.get(position).getProductCategoryName());
        Utils.loadImage(holder.binding.productImage, bannerData.get(position).getProductCategoryImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bannerData == null ? 0 : bannerData.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildProductCategoryBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildProductCategoryBinding.bind(itemView);
        }

    }
}
