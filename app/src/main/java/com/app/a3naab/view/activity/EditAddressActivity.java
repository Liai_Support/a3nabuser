package com.app.a3naab.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityAddAddressBinding;
import com.app.a3naab.model.AutoCompleteResponse;
import com.app.a3naab.model.ListMyAddressResponse;
import com.app.a3naab.model.Prediction;
import com.app.a3naab.network.InputForAPI;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.utils.Constants;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.MapViewModel;
import com.app.a3naab.viewmodel.ShoppingViewModel;

import java.util.ArrayList;
import java.util.List;

public class EditAddressActivity extends BaseActivity {

    ActivityAddAddressBinding binding;
    String selectedItem = "";
    ShoppingViewModel shoppingViewModel;
    MapViewModel mapViewModel;

    String latitude = "", longitude = "";

    ListMyAddressResponse.AddressData data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_address);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);

        selectedItem = getString(R.string.select_the_type_of_address);
        getIntentValues();
        initSpinner();
        initListener();
    }

    private void getIntentValues() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            data = bundle.getParcelable("data");
        }

    }

    private void initListener() {

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isValidInput().equals("true")) {
                    shoppingViewModel.updateAddress(EditAddressActivity.this, selectedItem, latitude, longitude, binding.pinDetails.getText().toString(), binding.apartmentNumber.getText().toString(),
                            binding.nearestTeacher.getText().toString(), binding.deliveryInstruction.getText().toString(), binding.othertag.getText().toString().trim(), data.getId()).observe(EditAddressActivity.this, commonResponse -> {
                        if (commonResponse.getError()) {
                            Utils.showSnack(findViewById(android.R.id.content), commonResponse.getMessage());
                        } else {
                            finish();
                        }
                    });
                } else {
                    Utils.showSnack(findViewById(android.R.id.content), isValidInput());
                }
            }
        });

        binding.selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(
                        new Intent(EditAddressActivity.this, SelectAddressActivity.class)
                                .putExtra("isEdit", true)
                                .putExtra("lat", latitude)
                                .putExtra("lng", longitude)
                        , Constants.RequestCode.ADDRESS_REQUEST);
            }
        });

        binding.identifyOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(
                        new Intent(EditAddressActivity.this, SelectAddressActivity.class)
                                .putExtra("isEdit", true)
                                .putExtra("lat", latitude)
                                .putExtra("lng", longitude)
                        , Constants.RequestCode.ADDRESS_REQUEST);
            }
        });


    }

    private void initSpinner() {

        ArrayList<String> value = new ArrayList<>();

        value.add("Home");
        value.add("Office");
        value.add("Other");


//        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, value);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        binding.addressType.setAdapter(adapter);


        if (data.getAddressType().equalsIgnoreCase("Home") || data.getAddressType().equalsIgnoreCase("الصفحة الرئيسية")) {
            binding.cardView8.setVisibility(View.GONE);
            selectedItem = data.getAddressType();
        } else if (data.getAddressType().equalsIgnoreCase("Office") || data.getAddressType().equalsIgnoreCase("مقرنا")) {
            binding.cardView8.setVisibility(View.GONE);
            selectedItem = data.getAddressType();
        } else {
            binding.cardView8.setVisibility(View.VISIBLE);
            binding.othertag.setText(data.getAddressType());
            selectedItem = getString(R.string.other);
        }

        binding.addressType.setHint("");
        binding.addressType.setText(selectedItem);
        binding.addressType.setEnabled(false);

        binding.pinDetails.setText(data.getAddressPinDetails());
        binding.textView12.setText(data.getAddressPinDetails());
        binding.apartmentNumber.setText(data.getBuildingName());

        if (data.getLandmark().equalsIgnoreCase(" ")) {
            binding.nearestTeacher.setText("");
        } else {
            binding.nearestTeacher.setText(data.getLandmark());
        }

        if (data.getInstruction().equalsIgnoreCase(" ")) {
            binding.deliveryInstruction.setText("");
        } else {
            binding.deliveryInstruction.setText(data.getInstruction());
        }


        latitude = data.getLatitude();
        longitude = data.getLongitude();

//        if (!latitude.equals(""))
//            searchPlaces(latitude, longitude);

    }

    private String isValidInput() {

        if (selectedItem.equals("") || selectedItem.equals("Select the type of address")) {
            return getString(R.string.select_the_type_of_address);
        } else if (binding.pinDetails.getText().toString().trim().equals("")) {
            return getString(R.string.select_address_pin_details);
        } else if (latitude.equals("")) {
            return getString(R.string.pick_location);
        } else if (selectedItem.equalsIgnoreCase(getString(R.string.other)) && binding.othertag.getText().toString().trim().equals("")) {
            return getString(R.string.add_tag);
        } else {
            return "true";
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Constants.RequestCode.ADDRESS_REQUEST == requestCode) {
            if (resultCode == Activity.RESULT_OK) {
                latitude = data.getStringExtra("latitude");
                longitude = data.getStringExtra("longitude");
                binding.pinDetails.setText(data.getStringExtra("address"));
                binding.textView12.setText(data.getStringExtra("address"));
            }
        }
    }

    private void searchPlaces(String lat, String lng) {
        String url = URLHelper.getAddress(lat, lng);
        InputForAPI inputForAPI = new InputForAPI(this,url);
        inputForAPI.setUrl(url);

        mapViewModel.getAddress(this, inputForAPI).observe(this, new Observer<AutoCompleteResponse>() {
            @Override
            public void onChanged(AutoCompleteResponse autoCompleteResponse) {
                if (!autoCompleteResponse.getError()) {
                    getAddressFromResponse(autoCompleteResponse.getPredictions());
                }
            }
        });

    }

    private void getAddressFromResponse(List<Prediction> value) {


        if (value.size() != 0) {
            binding.textView12.setText(value.get(0).getFormatted_address());
        }

//        if (value.size() != 0) {
//
//            if (value.get(0).getAddressComponents() != null && value.get(0).getAddressComponents().size() != 0) {

//                currentAddress = value.get(0).getAddressComponents().get(0).getLong_name();
//                if (sharedHelper.getSelectedDeliveryToTag().equals(""))
//                    binding.address.setText(currentAddress);

//            } else {
//                binding.address.setText(getString(R.string.current_location));
//            }

//        } else {
//            binding.address.setText(getString(R.string.current_location));
//        }
    }
}
