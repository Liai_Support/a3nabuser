package com.app.a3naab.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;

import com.app.a3naab.databinding.ChildFaqBinding;

import com.app.a3naab.model.ListFaqResponse;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.activity.HelpActivity;

import java.util.ArrayList;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.MyViewHolder> {

    Context context;
    ArrayList<ListFaqResponse.FaqDetails> addressData;
    HelpActivity shoppingFragment;
    SharedHelper sharedHelper;


    public FaqAdapter(HelpActivity shoppingFragment, Context context, ArrayList<ListFaqResponse.FaqDetails> addressData) {
        this.context = context;
        this.addressData = addressData;
        this.shoppingFragment = shoppingFragment;
    }

    @NonNull
    @Override
    public FaqAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        sharedHelper = new SharedHelper(context);
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_faq, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull FaqAdapter.MyViewHolder holder, int position) {
        Log.d("cdxc",""+addressData.get(position).getQuestione());

        if(sharedHelper.getSelectedLanguage().equals("en")){
            holder.binding.question.setText(addressData.get(position).getQuestione());
            holder.binding.answer.setText(addressData.get(position).getAnswere());

            holder.binding.question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(holder.binding.answer.getVisibility() == View.GONE){
                        holder.binding.question.setCompoundDrawablesWithIntrinsicBounds( null, null,context.getDrawable(R.drawable.ic_baseline_expand_more_24), null);

                        // holder.binding.question.setCompoundDrawables(null,null,context.getDrawable(R.drawable.ic_baseline_expand_less_24),null);
                        holder.binding.answer.setVisibility(View.VISIBLE);
                    }
                    else {
                        holder.binding.question.setCompoundDrawablesWithIntrinsicBounds( null, null,context.getDrawable(R.drawable.ic_baseline_expand_less_24), null);
                        holder.binding.answer.setVisibility(View.GONE);
                    }
                }
            });
        }

        else {
            holder.binding.question.setText(addressData.get(position).getQuestiona());
            holder.binding.answer.setText(addressData.get(position).getAnswera());

            holder.binding.question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(holder.binding.answer.getVisibility() == View.GONE){
                        holder.binding.question.setCompoundDrawablesWithIntrinsicBounds( context.getDrawable(R.drawable.ic_baseline_expand_more_24), null,null, null);

                        // holder.binding.question.setCompoundDrawables(null,null,context.getDrawable(R.drawable.ic_baseline_expand_less_24),null);
                        holder.binding.answer.setVisibility(View.VISIBLE);
                    }
                    else {
                        holder.binding.question.setCompoundDrawablesWithIntrinsicBounds( context.getDrawable(R.drawable.ic_baseline_expand_less_24),null, null, null);
                        holder.binding.answer.setVisibility(View.GONE);
                    }

                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return addressData.size();
    }

    public void notifyDataChanged(ArrayList<ListFaqResponse.FaqDetails> addressList) {
        addressData = addressList;
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildFaqBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildFaqBinding.bind(itemView);
        }

    }
}
