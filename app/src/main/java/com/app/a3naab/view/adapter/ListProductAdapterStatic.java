package com.app.a3naab.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildProductBinding;
import com.app.a3naab.interfaces.OnItemClick;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.view.activity.LoginActivity;
import com.app.a3naab.view.activity.ProductDetailActivity;

import java.util.ArrayList;

public class ListProductAdapterStatic extends RecyclerView.Adapter<ListProductAdapterStatic.MyViewHolder> {


    Context context;
    ArrayList<DashboardResponse.BestProduct> list;
    SharedHelper sharedHelper;
    OnItemClick onAddItem, ondeleteItem;

    public ListProductAdapterStatic(Context context, ArrayList<DashboardResponse.BestProduct> list, OnItemClick onAddItem, OnItemClick ondeleteItem) {
        this.context = context;
        this.list = list;
        this.onAddItem = onAddItem;
        this.ondeleteItem = ondeleteItem;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public ListProductAdapterStatic.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       // View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_product, parent, false);
        //ListProductAdapterStatic.MyViewHolder myViewHolder = new ListProductAdapterStatic.MyViewHolder(view);
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_product, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListProductAdapterStatic.MyViewHolder holder, int position) {

        DashboardResponse.BestProduct data = list.get(position);

        if (data != null) {
            if (data.getProductDiscount().equals("") || data.getProductDiscount().equals("0")) {
                holder.binding.discount.setVisibility(View.GONE);
            } else {
                holder.binding.discount.setVisibility(View.VISIBLE);
                holder.binding.discount.setText(data.getProductDiscount() + "%");
            }


            if (new SharedHelper(context).getSelectedLanguage().equals("en")) {
                holder.binding.discount.setBackgroundResource(R.drawable.product_discount_bg);
            } else {
                holder.binding.discount.setBackgroundResource(R.drawable.product_discount_bg_arabic);
            }


            if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
                holder.binding.productName.setText(data.getArabicName());
            } else {
                holder.binding.productName.setText(data.getProductName());
            }


            if (data.getProductDiscount() != null & !data.getProductDiscount().equals("0") && !data.getProductDiscount().equals("")) {
                holder.binding.oldAmount.setVisibility(View.VISIBLE);
                holder.binding.oldCurrency.setVisibility(View.VISIBLE);
                holder.binding.amount.setText(String.valueOf(Double.parseDouble(data.getProductPrice()) - ((Double.parseDouble(data.getProductPrice()) * Double.parseDouble(data.getProductDiscount())) / 100)));
                holder.binding.oldAmount.setText(data.getProductPrice());
            } else {
                holder.binding.oldAmount.setVisibility(View.GONE);
                holder.binding.oldCurrency.setVisibility(View.GONE);
                holder.binding.amount.setText(data.getProductPrice());
            }
            holder.binding.weight.setText(data.getProductWeight() + context.getString(R.string.kg));

            if (data.getProductImages() != null && data.getProductImages().size() != 0) {
                Utils.loadImage(holder.binding.imageView20, data.getProductImages().get(0).getProductImage());
            }
        }


        if (list.get(position).getStoreStock() != null) {

            if (list.get(position).getStoreStock() != 0) {

                holder.binding.outOfStock.setVisibility(View.GONE);
                holder.binding.disableView.setVisibility(View.GONE);

            } else {
                holder.binding.outOfStock.setVisibility(View.VISIBLE);
                holder.binding.disableView.setVisibility(View.VISIBLE);

            }

        } else {
            holder.binding.outOfStock.setVisibility(View.VISIBLE);
            holder.binding.disableView.setVisibility(View.VISIBLE);

        }

        holder.binding.disableView.setOnClickListener(view -> {

        });

        holder.binding.selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductDetailActivity.class)
                        .putExtra("data", list.get(position).getId()).putExtra("storeid",list.get(position).getStoreId()));

            }
        });

//        if (isProductAvailable(list.get(position).getId())) {
//            holder.binding.addToCartText.setText(context.getString(R.string.added));
//        } else {
//            holder.binding.addToCartText.setText(context.getString(R.string.add_to_cart));
//        }

        int refIds[] = holder.group.getReferencedIds();
        //Log.d("dcvd00",""+refIds);
       // Log.d("dcvd00",""+refIds.length);
        for (int id : refIds) {
            holder.view.findViewById(id).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                setProductAvailable(list.get(position).getId());
//                plus(position);
                    if (new SharedHelper(context).getLoggedIn()) {
                        onAddItem.onClick(position);
                    } else {
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                    }                }
            });
        }

       /* holder.binding.addToCartText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                setProductAvailable(list.get(position).getId());
//                plus(position);
                if (new SharedHelper(context).getLoggedIn()) {
                    onAddItem.onClick(position);
                } else {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                }

            }
        });
*/

        holder.binding.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.getMaxQty() > data.getCount())
                    onAddItem.onClick(position);
                else
                    DialogUtils.showMaximumLimit(context);
//                plus(position);
            }
        });
        holder.binding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ondeleteItem.onClick(position);
//                minus(position);
            }
        });
        holder.binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                minus(position);
                ondeleteItem.onClick(position);
            }
        });


        if (list.get(position).getProductDiscount().equals("") || list.get(position).getProductDiscount().equals("0")) {
            holder.binding.discount.setVisibility(View.INVISIBLE);
        } else {
            holder.binding.discount.setVisibility(View.VISIBLE);
        }


        setView(holder, position);

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void notifyData(ArrayList<DashboardResponse.BestProduct> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void notifySetChanged() {
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildProductBinding binding;
        Group group;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildProductBinding.bind(itemView);
            group = itemView.findViewById(R.id.grpAddCart);
            view = itemView;
        }
    }


    private void setProductAvailable(int id) {
        ArrayList<String> list = sharedHelper.getAddedCategory();
        if (isProductAvailable(id)) {
            list.remove(String.valueOf(id));
        } else {
            list.add(String.valueOf(id));
        }
        sharedHelper.setAddedCategory(list);
        notifyDataSetChanged();
    }

    private boolean isProductAvailable(int id) {
        return sharedHelper.getAddedCategory().contains(String.valueOf(id));

    }

    private void setView(ListProductAdapterStatic.MyViewHolder holder, int position) {

        holder.binding.count.setText(list.get(position).getCount() + "");

        if (list.get(position).getCount() == 0) {
            holder.binding.viewCart.setBackgroundResource(R.drawable.add_to_cart_bg);
            holder.binding.grpAddCart.setVisibility(View.VISIBLE);

            holder.binding.add.setVisibility(View.GONE);
            holder.binding.count.setVisibility(View.GONE);
            holder.binding.minus.setVisibility(View.GONE);
            holder.binding.delete.setVisibility(View.GONE);
        } else {

            if (list.get(position).getCount() == 1) {
                holder.binding.delete.setVisibility(View.VISIBLE);
                holder.binding.minus.setVisibility(View.GONE);
            } else {
                holder.binding.delete.setVisibility(View.GONE);
                holder.binding.minus.setVisibility(View.VISIBLE);
            }

            holder.binding.viewCart.setBackgroundResource(R.drawable.add_to_cart_bg_selected);
            holder.binding.grpAddCart.setVisibility(View.GONE);

            holder.binding.add.setVisibility(View.VISIBLE);
            holder.binding.count.setVisibility(View.VISIBLE);

        }
    }

    private void minus(int id) {
        list.get(id).setCount(list.get(id).getCount() - 1);

        notifyDataSetChanged();

    }

    private void plus(int id) {

        list.get(id).setCount(list.get(id).getCount() + 1);

        notifyDataSetChanged();

    }

    public void notifyAddData(int position) {
        try {
            list.get(position).setCount(list.get(position).getCount() + 1);
            notifyDataSetChanged();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public void notifyRemoveData(int position) {
        try {
            list.get(position).setCount(list.get(position).getCount() - 1);
            notifyDataSetChanged();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

}
