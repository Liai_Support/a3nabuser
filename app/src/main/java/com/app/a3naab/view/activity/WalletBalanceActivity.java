package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityWalletBalanceBinding;
import com.app.a3naab.model.WalletAmountTransactionResponse;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.WalletHistoryAdapter;
import com.app.a3naab.viewmodel.ProductViewModel;

public class WalletBalanceActivity extends BaseActivity {

    ActivityWalletBalanceBinding binding;
    ProductViewModel productViewModel;
    ProductViewModel viewModel;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wallet_balance);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        initListener();
        binding.count.setText(""+sharedHelper.getAvaliableWalletamount());
        if (sharedHelper.getCartCount() == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setVisibility(View.VISIBLE);
            binding.foot.badge.setText(""+sharedHelper.getCartCount());
        }
    }

    private void initListener() {
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

        binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

        viewModel.getWalletAmountTransaction(this, 1).observe(this, new Observer<WalletAmountTransactionResponse>() {
            @Override
            public void onChanged(WalletAmountTransactionResponse walletAmountTransactionResponse) {
                if (walletAmountTransactionResponse.getError()==false) {
                    binding.pointsList.setVisibility(View.VISIBLE);
                    binding.pointsList.setLayoutManager(new LinearLayoutManager(WalletBalanceActivity.this));
                    binding.pointsList.setAdapter(new WalletHistoryAdapter( WalletBalanceActivity.this, walletAmountTransactionResponse.getData().getList()));
                }
                else {
                    binding.pointsList.setVisibility(View.GONE);
                }
            }
        });
    }

    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

}
