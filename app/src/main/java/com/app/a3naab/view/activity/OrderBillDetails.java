package com.app.a3naab.view.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityBillDetailsBinding;
import com.app.a3naab.interfaces.DialogCallback;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.GetProfileResponse;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.utils.CartSessionManager;
import com.app.a3naab.utils.DialogUtils;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.ShoppingViewModel;

public class  OrderBillDetails extends BaseActivity {

    ActivityBillDetailsBinding binding;
    ShoppingViewModel shoppingViewModel;
    private boolean dataSuccessfullyObtained = false;
    SharedHelper sharedHelper;
    Double walletpointSAR = 0.0;
    Double walletamount = 0.0;
    Double fsubtotal=0.0;
    Double vat = 0.0;
    Double coupondiscount = 0.0;
    Double totalamount = 0.0;
    ActivityResultLauncher<Intent> ActivityResultLauncher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bill_details);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        sharedHelper = new SharedHelper(this);
        getIntentValues();

        initListener();

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
            binding.imageView24.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.cart_top_3));
        }

        ActivityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();
                            placeOrder();
                        }
                        else {
                            Utils.showSnack(binding.parent,getString(R.string.paymentfailed));
                        }
                    }
                });

    }

    private void getIntentValues() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String subtotal = bundle.getString("subtotal");
            String dcharge = bundle.getString("dcharge");
            String vat = bundle.getString("vat");
            String tax = bundle.getString("tax");
            String usedcoupondiscount = bundle.getString("usedcoupondiscount");
            String usedwalletpoint = bundle.getString("usedwalletpoint");
            String usedwalletamount = bundle.getString("usedwalletamount");
            String total = bundle.getString("total");
            totalamount = Double.parseDouble(total);

            if(usedwalletamount.equalsIgnoreCase("0") || usedwalletamount.equalsIgnoreCase("0.00")){
                binding.textView35.setVisibility(View.GONE);
                binding.walletBalance.setVisibility(View.GONE);
                CartSessionManager.getInstance().setwallet(false);
            }
            else {
                binding.textView35.setVisibility(View.VISIBLE);
                binding.walletBalance.setVisibility(View.VISIBLE);
                binding.walletBalance.setText("SAR " + usedwalletamount);
                CartSessionManager.getInstance().setwallet(true);
            }

            if(usedwalletpoint.equalsIgnoreCase("0") || usedwalletpoint.equalsIgnoreCase("0.00")){
                binding.textView36.setVisibility(View.GONE);
                binding.walletpoint.setVisibility(View.GONE);
                CartSessionManager.getInstance().setEnablePoints(false);
            }
            else {
                binding.textView36.setVisibility(View.VISIBLE);
                binding.walletpoint.setVisibility(View.VISIBLE);
                binding.walletpoint.setText("SAR "+usedwalletpoint);
                CartSessionManager.getInstance().setEnablePoints(true);
            }

            if(usedcoupondiscount.equalsIgnoreCase("0") || usedcoupondiscount.equalsIgnoreCase("0.00")){
                binding.discount.setVisibility(View.GONE);
                binding.textView30.setVisibility(View.GONE);
            }
            else {
                binding.discount.setVisibility(View.VISIBLE);
                binding.textView30.setVisibility(View.VISIBLE);
                binding.discount.setText("SAR "+"-"+usedcoupondiscount);
            }

            binding.subtotal.setText("SAR " + subtotal);
            binding.deliveryFee.setText("SAR "+dcharge);
            binding.textView29.setText(""+getString(R.string.tax)+"("+vat +" %)");
            binding.tax.setText("SAR " +tax);
            binding.totalAmount.setText("SAR "+total);

        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();

      /*  if(Double.parseDouble(CartSessionManager.getInstance().getavailableWalletamount()) != 0.0){
            if(Double.parseDouble(CartSessionManager.getInstance().getavailableWalletamount()) < Double.parseDouble(CartSessionManager.getInstance().getTotalAmountcpy())){
                binding.textView35.setVisibility(View.VISIBLE);
                binding.walletBalance.setVisibility(View.VISIBLE);
                walletamount = Double.parseDouble(CartSessionManager.getInstance().getavailableWalletamount());
                binding.walletBalance.setText("SAR " + walletamount);
                CartSessionManager.getInstance().setwallet(true);
                Log.d("zcvdxc", "" + CartSessionManager.getInstance().getwallet());
            }
            else {
                binding.textView35.setVisibility(View.GONE);
                binding.walletBalance.setVisibility(View.GONE);
                walletamount = 0.0;
                CartSessionManager.getInstance().setwallet(false);
            }
        }
        else {
            binding.textView35.setVisibility(View.GONE);
            binding.walletBalance.setVisibility(View.GONE);
            walletamount = 0.0;
            CartSessionManager.getInstance().setwallet(false);
        }

        if (CartSessionManager.getInstance().isEnablePoints()) {
            binding.textView36.setVisibility(View.VISIBLE);
            binding.walletpoint.setVisibility(View.VISIBLE);
            Double dsd = Double.parseDouble(sharedHelper.getwalletpointsar())/Double.parseDouble(sharedHelper.getwalletpoint());
            Double sds = dsd*Double.parseDouble(CartSessionManager.getInstance().getavailableWalletpoint());
            binding.walletpoint.setText("SAR "+String.format("%.2f",sds));
            walletpointSAR = Double.parseDouble(String.format("%.2f",sds));
            Log.d("kjvhdf",""+walletpointSAR);
        }
        else {
            binding.textView36.setVisibility(View.GONE);
            binding.walletpoint.setVisibility(View.GONE);
            walletpointSAR = 0.0;
        }

        fsubtotal = Double.parseDouble(CartSessionManager.getInstance().getTotalAmount());
        vat = Double.parseDouble(CartSessionManager.getInstance().getFlatDiscount());
        coupondiscount = Double.parseDouble(CartSessionManager.getInstance().getDiscount());
        binding.subtotal.setText("SAR " + CartSessionManager.getInstance().getTotalAmount());

        if (CartSessionManager.getInstance().getFlatDiscount() != null && (!CartSessionManager.getInstance().getFlatDiscount().equalsIgnoreCase("0.0")) && (!CartSessionManager.getInstance().getFlatDiscount().equalsIgnoreCase("0.00")) && (!CartSessionManager.getInstance().getFlatDiscount().equalsIgnoreCase("0"))) {
           // binding.tax.setText("SAR " + CartSessionManager.getInstance().getFlatDiscount() + " %");
            binding.textView29.setText(""+getString(R.string.tax)+"("+CartSessionManager.getInstance().getFlatDiscount() +" %)");
            if(CartSessionManager.getInstance().getIsFastDeliverySelected() == 1) {
                binding.tax.setText("SAR " +((((fsubtotal-(coupondiscount+walletpointSAR+walletamount))+Double.parseDouble(sharedHelper.getFastDeliverycharge())) *
                        vat / 100)));
              //  CartSessionManager.getInstance().setFordertax(String.valueOf((((fsubtotal+Double.parseDouble(sharedHelper.getFastDeliverycharge())) * vat / 100))));

                totalamount = ((fsubtotal-(coupondiscount+walletpointSAR+walletamount))
                        +Double.parseDouble(sharedHelper.getFastDeliverycharge())
                        +((((fsubtotal-(coupondiscount+walletpointSAR+walletamount))+Double.parseDouble(sharedHelper.getFastDeliverycharge())) *
                        vat / 100)));
                binding.totalAmount.setText("SAR "+totalamount);
            }
            else {
                binding.tax.setText("SAR " +((((fsubtotal-(coupondiscount+walletpointSAR+walletamount))+Double.parseDouble(sharedHelper.getDeliverycharge())) *
                        vat / 100)));
              //  CartSessionManager.getInstance().setFordertax(String.valueOf((((fsubtotal+Double.parseDouble(sharedHelper.getDeliverycharge())) * vat / 100))));
                totalamount = ((fsubtotal-(coupondiscount+walletpointSAR+walletamount))
                        +Double.parseDouble(sharedHelper.getDeliverycharge())
                        +((((fsubtotal-(coupondiscount+walletpointSAR+walletamount))+Double.parseDouble(sharedHelper.getDeliverycharge())) *
                        vat / 100)));
                binding.totalAmount.setText("SAR "+totalamount);
            }
        }
        else {
            binding.totalAmount.setText("SAR " + CartSessionManager.getInstance().getTotalAmount());
        }

        if(CartSessionManager.getInstance().getIsFastDeliverySelected() == 1){
            binding.deliveryFee.setText("SAR "+sharedHelper.getFastDeliverycharge());
        }
        else {
            binding.deliveryFee.setText("SAR "+sharedHelper.getDeliverycharge());
        }

        if(CartSessionManager.getInstance().getCouponCode().equals("")){
            binding.discount.setVisibility(View.GONE);
            binding.textView30.setVisibility(View.GONE);
        }
        else {
            binding.discount.setVisibility(View.VISIBLE);
            binding.textView30.setVisibility(View.VISIBLE);
            binding.discount.setText("SAR "+ CartSessionManager.getInstance().getDiscount());
        }
*/

        binding.time.setText(CartSessionManager.getInstance().getSelectedTimeStr());
       // binding.place.setText(CartSessionManager.getInstance().getSelectedAddress());
        binding.place.setText(""+CartSessionManager.getInstance().getSelectedAddressType()+" : "+CartSessionManager.getInstance().getSelectedAddress());


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* Log.d("kxvxnv 0",""+requestCode);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                placeOrder();
            }
            else {
                Utils.showSnack(binding.parent,getString(R.string.paymentfailed));
            }
        }*/
    }

    private void initListener() {

        binding.editAddress.setOnClickListener((view) -> {
            finish();
        });

        binding.confirm.setOnClickListener((view) -> {
            if (CartSessionManager.getInstance().getPaymentOption() == 2 || CartSessionManager.getInstance().getPaymentOption() == 4) {
                placeOrder();
            }
            else {
                if (CartSessionManager.getInstance().getFlatDiscount() != null && (!CartSessionManager.getInstance().getFlatDiscount().equalsIgnoreCase("0.0")) && (!CartSessionManager.getInstance().getFlatDiscount().equalsIgnoreCase("0.00")) && (!CartSessionManager.getInstance().getFlatDiscount().equalsIgnoreCase("0"))) {
                    if(totalamount == 0 || totalamount == 0.0){
                        placeOrder();
                    }
                    else {
                       // startActivityForResult(new Intent(OrderBillDetails.this, PaymentActivity.class).putExtra("amount",String.valueOf(totalamount)), 100);
                        ActivityResultLauncher.launch(new Intent(OrderBillDetails.this, PaymentActivity.class).putExtra("amount",String.valueOf(totalamount)));
                    }
                    Log.d("vbcxvhn","first");
                }
                else {
                    if(totalamount == 0 || totalamount == 0.0){
                        placeOrder();
                    }
                    else {
                        Log.d("vbcxvhn","second");
                        // startActivityForResult(new Intent(OrderBillDetails.this, PaymentActivity.class).putExtra("amount",String.valueOf(totalamount)), 100);
                        ActivityResultLauncher.launch(new Intent(OrderBillDetails.this, PaymentActivity.class).putExtra("amount",String.valueOf(totalamount)));
                    }
                }
            }

        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void placeOrder() {

        DialogUtils.showLoader(this);
        if (CartSessionManager.getInstance().getReorder()) {
            shoppingViewModel.placeReOrder(this).observe(this, new Observer<CommonResponse>() {
                @Override
                public void onChanged(CommonResponse commonResponse) {
                    DialogUtils.dismissLoader();
                    if (commonResponse.getError()) {
                        Utils.showSnack(binding.parent, commonResponse.getMessage());
                        if(CartSessionManager.getInstance().getPaymentOption() == 1){
                            shoppingViewModel.refundpayment(OrderBillDetails.this, totalamount.toString(),CartSessionManager.getInstance().getpaymentorderid()).observe(OrderBillDetails.this, paymentRefundResponse -> {
                                if(paymentRefundResponse.getRcode() == 0){
                                    Utils.showSnack(binding.parent, getString(R.string.refundsuccess));
                                }
                                else {
                                    Utils.showSnack(binding.parent, getString(R.string.refundfailure));
                                }
                            });
                        }
                    } else {
                        CartSessionManager.removeAllValues();
                        DialogUtils.showOrderSuccessDialog(OrderBillDetails.this, new DialogCallback() {
                            @Override
                            public void onPositiveClick() {

                                Intent intent = new Intent(OrderBillDetails.this, DashboardActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("page",2);
                                startActivity(intent);

                            }

                            @Override
                            public void onNegativeClick() {

                            }
                        });
                    }
                }
            });
        }
        else {
            shoppingViewModel.placeOrder(this).observe(this, commonResponse -> {
                DialogUtils.dismissLoader();
                if (commonResponse.getError()) {
                    Utils.showSnack(binding.parent, commonResponse.getMessage());
                    if(CartSessionManager.getInstance().getPaymentOption() == 1) {
                        shoppingViewModel.refundpayment(OrderBillDetails.this, totalamount.toString(), CartSessionManager.getInstance().getpaymentorderid()).observe(OrderBillDetails.this, paymentRefundResponse -> {
                            if (paymentRefundResponse.getRcode() == 0) {
                                Utils.showSnack(binding.parent, getString(R.string.refundsuccess));
                            } else {
                                Utils.showSnack(binding.parent, getString(R.string.refundfailure));
                            }
                        });
                    }
                } else {
                    CartSessionManager.removeAllValues();
                    DialogUtils.showOrderSuccessDialog(OrderBillDetails.this, new DialogCallback() {
                        @Override
                        public void onPositiveClick() {

                            Intent intent = new Intent(OrderBillDetails.this, DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("page",2);
                            startActivity(intent);

                        }

                        @Override
                        public void onNegativeClick() {

                        }
                    });
                }
            });
        }


    }
}
