package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildAdsBinding;
import com.app.a3naab.model.AdsResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.utils.Utils;

import java.util.ArrayList;

public class AdsSearchAdapter extends RecyclerView.Adapter<AdsSearchAdapter.MyViewHolder> {


    Context context;
    ArrayList<AdsResponse.AdsData> list;

    public AdsSearchAdapter(Context context, ArrayList<AdsResponse.AdsData> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public AdsSearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_ads, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AdsSearchAdapter.MyViewHolder holder, int position) {

        Utils.loadImageBanner(holder.binding.ads, list.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void notifyData() {
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildAdsBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildAdsBinding.bind(itemView);
        }
    }
}
