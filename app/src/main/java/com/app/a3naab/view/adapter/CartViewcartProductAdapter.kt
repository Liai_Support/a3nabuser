package com.app.a3naab.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.a3naab.R
import com.app.a3naab.databinding.ChildViewCartProductBinding
import com.app.a3naab.interfaces.OnItemClick
import com.app.a3naab.model.OrderList
import com.app.a3naab.utils.SharedHelper
import com.app.a3naab.utils.Utils
import java.util.*
import kotlin.collections.ArrayList

class CartViewcartProductAdapter(
    val context: Context,
    var bestProducts: ArrayList<OrderList>,
    var addItem: OnItemClick, var deleteItem: OnItemClick
) :
    RecyclerView.Adapter<CartViewcartProductAdapter.BestProductViewHolder>() {

    var sharedHelper: SharedHelper = SharedHelper(context)

    class BestProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildViewCartProductBinding = ChildViewCartProductBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestProductViewHolder =
        BestProductViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_view_cart_product, parent, false)
        )


    override fun getItemCount(): Int = bestProducts.size

    override fun onBindViewHolder(holder: BestProductViewHolder, position: Int) {


        if (sharedHelper.selectedLanguage.equals("ar", ignoreCase = true))
            holder.binding.productName.text =
                bestProducts[position].arabicName
        else
            holder.binding.productName.text =
                bestProducts[position].productName



        if (bestProducts[position].productImage.size != 0)
            Utils.loadImage(
                holder.binding.productImage,
                bestProducts[position].productImage[0].productImage
            )

        //var tot = 0.0
        //var cbstyleprice = 0.0
        var cbstyleprice = (bestProducts[position].cuttingPrice!!.toDouble()+bestProducts[position].boxPrice!!.toDouble())

        if(bestProducts[position].productDiscount!!.toInt()!=0){
            holder.binding.amount.text = ((bestProducts[position].productPrice!!.toDouble() - (bestProducts[position].productPrice!!.toDouble()*(bestProducts[position].productDiscount!!.toDouble()/100))).toInt() + cbstyleprice).toString();
        }
        else{
            holder.binding.amount.text = ((bestProducts[position].productPrice!!.toDouble()+cbstyleprice).toString())
        }
        holder.binding.quandity.text = "X" + bestProducts[position].quantity.toString()

         var tot = (holder.binding.amount.text.toString().toDouble()) * (bestProducts[position].quantity!!.toInt())

     //   holder.binding.totalAmount.text = bestProducts[position].totalPrice
        holder.binding.totalAmount.text = String.format(Locale.ENGLISH, "%.1f", tot)


        holder.binding.add.setOnClickListener {

            addItem.onClick(position)
        }

        holder.binding.minus.setOnClickListener {

            deleteItem.onClick(position)
        }

        holder.binding.delete.setOnClickListener {

            deleteItem.onClick(position)
        }

//        holder.binding.itemView1.setOnClickListener {
//
//            context.startActivity(
//                Intent(context, ProductDetailActivity::class.java)
//                    .putExtra("data", bestProducts[position].productId)
//
//            )
//        }

//        setView(holder, position)

    }

//    private fun setView(holder: BestProductViewHolder, position: Int) {

//        holder.binding.count.text = bestProducts[position].quantity.toString()

//        if (bestProducts[position].quantity == 1) {
//            holder.binding.delete.visibility = View.VISIBLE
//            holder.binding.minus.visibility = View.GONE
//        } else {
//            holder.binding.delete.visibility = View.GONE
//            holder.binding.minus.visibility = View.VISIBLE
//        }

//        holder.binding.viewCart.setBackgroundResource(R.drawable.add_to_cart_bg_selected)

//        holder.binding.add.visibility = View.VISIBLE
//        holder.binding.count.visibility = View.VISIBLE


//    }


//    fun notifyAddData(position: Int) {
//
//        bestProducts[position].quantity = bestProducts[position].quantity + 1
//        notifyData(bestProducts)
//        notifyDataSetChanged()
//    }
//
//    fun notifyRemoveData(position: Int) {
//        bestProducts[position].quantity = bestProducts[position].quantity - 1
//
//        if (bestProducts[position].quantity == 0) {
//            bestProducts.removeAt(position)
//        }
//
//        notifyData(bestProducts)
//        notifyDataSetChanged()
//    }
//
//    fun addItems(MyCart: ArrayList<ViewCartResponse.CartDetails>) {
//        bestProducts = MyCart
//        notifyDataSetChanged()
//    }
}