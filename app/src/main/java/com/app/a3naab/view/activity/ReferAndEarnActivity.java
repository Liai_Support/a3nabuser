package com.app.a3naab.view.activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityPoinetsBinding;
import com.app.a3naab.databinding.ActivityReferEarnBinding;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.utils.Utils;
import com.app.a3naab.viewmodel.ProductViewModel;

public class ReferAndEarnActivity extends BaseActivity {


    ActivityReferEarnBinding binding;
    ProductViewModel viewModel;
    String body1;
    public String rfcode = "";
    ProductViewModel productViewModel;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_refer_earn);
        viewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        getProfileDetails();

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }


        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

        binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        if (sharedHelper.getCartCount() == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setVisibility(View.VISIBLE);
            binding.foot.badge.setText(""+sharedHelper.getCartCount());
        }
        initListener();


    }


    private void getProfileDetails() {


        viewModel.getProfileDetails(this).observe(this, new Observer<ProfileDataResponse>() {
            @Override
            public void onChanged(ProfileDataResponse profileDataResponse) {
                if (profileDataResponse.getError()) {
                    Utils.showSnack(binding.parentLayout, profileDataResponse.getMessage());
                } else {
                    rfcode = profileDataResponse.getData().getProfile().getReferralCode();
                }
            }
        });
    }



    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void initListener() {
        binding.firstName.setText("https://www.a3nab.com/"+rfcode);
//        binding.firstName.setText("https://play.google.com/store/apps/details?id=com.app.a3naab/"+rfcode);

        binding.textView72.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
//                String body = getString(R.string.sharecont) + " : "+"https://play.google.com/store/apps/details?id=com.app.a3naab"+"\n"+getString(R.string.refercode)+rfcode;
                String body = getString(R.string.sharecont) + " : "+"https://www.a3nab.com/"+"\n"+getString(R.string.refercode)+rfcode;
                /* String sub = "Your Subject";
                myIntent.putExtra(Intent.EXTRA_SUBJECT,sub);*/
                myIntent.putExtra(Intent.EXTRA_TEXT, body);
                startActivity(Intent.createChooser(myIntent, "Share Using"));
            }
        });

        binding.copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                body1 = getString(R.string.sharecont) + " : "+"https://play.google.com/store/apps/details?id=com.app.a3naab"+"\n"+getString(R.string.refercode)+rfcode;
                body1 = getString(R.string.sharecont) + " : "+"https://www.a3nab.com/"+"\n"+getString(R.string.refercode)+rfcode;
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", body1);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(ReferAndEarnActivity.this,R.string.copiedtoclipboard,Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
