package com.app.a3naab.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildHomeSearchBinding;
import com.app.a3naab.model.ProductListData;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.activity.ProductDetailActivity;

import java.util.ArrayList;

public class SearchProductAdapter extends RecyclerView.Adapter<SearchProductAdapter.MyViewHolder> {

    Activity context;
    ArrayList<ProductListData> bannerData;
    SharedHelper sharedHelper;

    public SearchProductAdapter(Activity context, ArrayList<ProductListData> bannerData) {
        this.context = context;
        this.bannerData = bannerData;
        sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public SearchProductAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(
                LayoutInflater.from(context).inflate(R.layout.child_home_search, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull SearchProductAdapter.MyViewHolder holder, int position) {

//        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar"))
//            holder.binding.name.setText(bannerData.get(position).getArabicName());
//        else
        holder.binding.name.setText(bannerData.get(position).getProductName());
        Log.d("xmcxmcxc",""+bannerData.get(position).getProductName());

        Log.d("xmcxmcxccnbbbx",""+bannerData.get(position).getProductPrice());
        Log.d("xmcxmcxccnbbbx",""+bannerData.get(position).getStoreId());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToPreference(bannerData.get(position));
                context.startActivity(new Intent(context, ProductDetailActivity.class)
                        .putExtra("data", bannerData.get(position).getId())
                        .putExtra("storeid", bannerData.get(position).getStoreId())
                );
                context.finish();
            }
        });


        if (sharedHelper.getSelectedLanguage().equalsIgnoreCase("ar")) {
            holder.binding.imageGo.setRotation(0f);
        }
    }

    @Override
    public int getItemCount() {
        return bannerData.size();
    }

    public void setValues(ArrayList<ProductListData> list) {
        bannerData = list;
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildHomeSearchBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ChildHomeSearchBinding.bind(itemView);
        }

    }


    private void addToPreference(ProductListData data) {

        ArrayList<ProductListData> recent = new SharedHelper(context).getProductSearchRecent();

        int position = -1;
        for (int i = 0; i < recent.size(); i++) {
            if (data.getId() == recent.get(i).getId()) {
                position = i;
                break;
            }
        }

        if (position != -1) {
            recent.remove(position);
        }
        recent.add(0, data);
        new SharedHelper(context).setProductSearchRecent(recent);
    }


}
