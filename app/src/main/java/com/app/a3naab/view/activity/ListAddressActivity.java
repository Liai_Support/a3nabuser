package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityListMyaddressBinding;
import com.app.a3naab.interfaces.AddressSelection;
import com.app.a3naab.model.ListMyAddressResponse;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.AddressListingAdapter;
import com.app.a3naab.view.fragment.ShoppingFragment;
import com.app.a3naab.viewmodel.ProductViewModel;
import com.app.a3naab.viewmodel.ShoppingViewModel;

public class ListAddressActivity extends BaseActivity {


    ActivityListMyaddressBinding binding;
    ShoppingViewModel shoppingViewModel;
    ProductViewModel productViewModel;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_myaddress);
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);
        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        sharedHelper = new SharedHelper(this);
        getAddressList();
        binding.view22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ListAddressActivity.this, SelectStartAddressActivity.class));
            }
        });

        if (new SharedHelper(this).getSelectedLanguage().equalsIgnoreCase("ar")) {
            binding.back.setRotation(180);
        }

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.foot.shopping.setOnClickListener(view -> {
            moveToActivity(0);
        });

        binding.foot.order.setOnClickListener(view -> {
            moveToActivity(2);
        });

        binding.foot.cart.setOnClickListener(view -> {
            moveToActivity(1);
        });

        binding.foot.profile.setOnClickListener(view -> {
            moveToActivity(3);
        });

        binding.foot.profileImage.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.foot.profileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        if (sharedHelper.getCartCount() == 0) {
            binding.foot.badge.setVisibility(View.GONE);
        } else {
            binding.foot.badge.setVisibility(View.VISIBLE);
            binding.foot.badge.setText(""+sharedHelper.getCartCount());
        }
    }

    private void moveToActivity(int position) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("page", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        getAddressList();
    }


    private void getAddressList() {

        shoppingViewModel.getAddressList(this).observe(this, new Observer<ListMyAddressResponse>() {
            @Override
            public void onChanged(ListMyAddressResponse listMyAddressResponse) {

                if (!listMyAddressResponse.getError()) {
                    binding.addressView.setLayoutManager(new LinearLayoutManager(ListAddressActivity.this));
                    binding.addressView.setAdapter(new AddressListingAdapter(false,new ShoppingFragment(), ListAddressActivity.this, listMyAddressResponse.getAddressData(), new AddressSelection() {
                        @Override
                        public void addressSelected(int position) {

                        }

                        @Override
                        public void addressEdit(int position) {

                        }

                        @Override
                        public void addressDelete(int position) {

                        }
                    }, false));
                }
            }
        });
    }
}
