package com.app.a3naab.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ActivityOnboardBinding;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.adapter.OnBoardAdapter;

import java.util.ArrayList;

public class OnBoardActivity extends BaseActivity {

    ActivityOnboardBinding binding;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_onboard);
        sharedHelper = new SharedHelper(this);
        initAdapter();
    }


    private void initAdapter() {

        ArrayList<String> headerList = new ArrayList<>();

        headerList.add("");
        headerList.add(getString(R.string.the_first_step));
        headerList.add(getString(R.string.the_second_step));
        headerList.add(getString(R.string.the_third_step));

        ArrayList<String> contentList = new ArrayList<>();
        contentList.add(getString(R.string.desc1));
        contentList.add(getString(R.string.desc2));
        contentList.add(getString(R.string.desc3));
        contentList.add(getString(R.string.desc4));

        ArrayList<Integer> imageList = new ArrayList<>();
        imageList.add(R.drawable.onboard_1_new1);
        imageList.add(R.drawable.onboard_2);
        imageList.add(R.drawable.onboard_3);
        imageList.add(R.drawable.welcom4);

        binding.viewPager.setAdapter(new OnBoardAdapter(this, contentList, headerList, imageList, (position) -> {
            return null;
        }));
        binding.indicator.setViewPager2(binding.viewPager);


        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (binding.viewPager.getCurrentItem() == contentList.size() - 1) {
                    Intent intent = new Intent(OnBoardActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() + 1);
                }
            }
        });

        binding.skip.setOnClickListener(view -> {
            Intent intent = new Intent(OnBoardActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });

        binding.changeLanguage.setOnClickListener(view -> {
            if (sharedHelper.getSelectedLanguage().equals("en")) {
                sharedHelper.setSelectedLanguage("ar");
            } else {
                sharedHelper.setSelectedLanguage("en");
            }

            Intent intent = new Intent(this, OnBoardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        });

        binding.imageView5.setOnClickListener(view -> {
            if (sharedHelper.getSelectedLanguage().equals("en")) {
                sharedHelper.setSelectedLanguage("ar");
            } else {
                sharedHelper.setSelectedLanguage("en");
            }

            Intent intent = new Intent(this, OnBoardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        });
    }
}
