package com.app.a3naab.view.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.a3naab.R
import com.app.a3naab.databinding.ChildCartBinding
import com.app.a3naab.interfaces.OnItemClick
import com.app.a3naab.model.ViewCartResponse
import com.app.a3naab.utils.DialogUtils
import com.app.a3naab.utils.SharedHelper
import com.app.a3naab.utils.Utils
import com.app.a3naab.utils.Utils.formatedValues
import com.app.a3naab.view.activity.ProductDetailActivity

class CartListAdapter(
    val context: Context,
    var bestProducts: ArrayList<ViewCartResponse.CartDetails>,
    var addItem: OnItemClick, var deleteItem: OnItemClick,
    var notifyData: (ArrayList<ViewCartResponse.CartDetails>) -> Unit
) :
    RecyclerView.Adapter<CartListAdapter.BestProductViewHolder>() {

    var sharedHelper: SharedHelper = SharedHelper(context)
    var value = 0

    class BestProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildCartBinding = ChildCartBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestProductViewHolder =
        BestProductViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_cart, parent, false)
        )


    override fun getItemCount(): Int = bestProducts.size

    override fun onBindViewHolder(holder: BestProductViewHolder, position: Int) {

        if(sharedHelper.selectedLanguage.equals("ar")){
            holder.binding.productName.text = bestProducts[position].arabicName
        }
        else{
            holder.binding.productName.text = bestProducts[position].productName
        }

        if (bestProducts[position].productImages.size != 0)
            Utils.loadImage(
                holder.binding.productImage,
                bestProducts[position].productImages[0].productImage
            )

        if (bestProducts[position].storeStock == 0) {
            holder.binding.outOfStock.visibility = View.VISIBLE
        } else {
            holder.binding.outOfStock.visibility = View.GONE
        }


        holder.binding.quandity.text = "X" + bestProducts[position].quantity.toString()

        var valu1 = 0.0
        if (bestProducts[position].getBoxStyle() != null && bestProducts[position].getCuttingStyle() != null) {
            val valu: Double = bestProducts[position].getBoxPrice().toDouble() + bestProducts[position].getCuttingPrice().toDouble()
            valu1 = valu1 + valu
        }
        else {
            if (bestProducts[position].getBoxStyle() != null) {
                valu1 = valu1 + bestProducts[position].getBoxPrice().toDouble()
            } else if (bestProducts[position].getCuttingStyle() != null) {
                valu1 = valu1 + bestProducts[position].getCuttingPrice().toDouble()
            }
        }

        holder.binding.amount.text = formatedValues(bestProducts[position].singlePrice.toDouble()+valu1)
        holder.binding.totalAmount.text = formatedValues((bestProducts[position].singlePrice.toDouble()+valu1) * (bestProducts[position].quantity))


        holder.binding.add.setOnClickListener {
            if (bestProducts[position].maxQty > bestProducts[position].quantity)
                addItem.onClick(position)
            else
                DialogUtils.showMaximumLimit(context)
        }

        holder.binding.minus.setOnClickListener {

            deleteItem.onClick(position)
        }

        holder.binding.delete.setOnClickListener {

            deleteItem.onClick(position)
        }

        holder.binding.itemView1.setOnClickListener {
            Log.d("zmcbznhc",""+bestProducts[position].storeId);

            context.startActivity(
                Intent(context, ProductDetailActivity::class.java)
                    .putExtra("data", bestProducts[position].productId).putExtra("storeid", bestProducts[position].storeId.toString())

            )
        }

        if(bestProducts[position].sp == true){
            holder.binding.categoryn.visibility = View.VISIBLE
            if(sharedHelper.selectedLanguage.equals("ar")){
                holder.binding.categoryn.text = bestProducts[position].categoryNameArabic.toString()
            }
            else{
                holder.binding.categoryn.text = bestProducts[position].categoryName.toString()
            }

            Log.d("sdcshdv"+position,""+bestProducts[position].movC)
            if(bestProducts[position].movC == true){
                holder.binding.categoryn1.visibility = View.VISIBLE
            }
            else{
                holder.binding.categoryn1.visibility = View.GONE
            }
        }
        else{
            holder.binding.categoryn.visibility = View.GONE
            holder.binding.categoryn1.visibility = View.GONE
        }

      /*  if(bestProducts[position].movP == true){
            holder.binding.minimumOrderRequired.visibility = View.VISIBLE
        }
        else{
            holder.binding.minimumOrderRequired.visibility = View.GONE
        }*/

        /*if (bestProducts[position].sub_minimum.toFloat() != 0.0f) {
          if (bestProducts[position].totalPrice.toFloat() < bestProducts[position].sub_minimum.toFloat()) {
              holder.binding.minimumOrderRequired.visibility = View.VISIBLE
          } else {
              holder.binding.minimumOrderRequired.visibility = View.GONE
          }
      } else {
          if (bestProducts[position].totalPrice.toFloat() < bestProducts[position].cate_minimum.toFloat()) {
              holder.binding.minimumOrderRequired.visibility = View.VISIBLE
          } else {
              holder.binding.minimumOrderRequired.visibility = View.GONE
          }
      }*/

        setView(holder, position)

    }

    private fun setView(holder: BestProductViewHolder, position: Int) {

        holder.binding.count.text = bestProducts[position].quantity.toString()

        if (bestProducts[position].quantity == 1) {
            holder.binding.delete.visibility = View.VISIBLE
            holder.binding.minus.visibility = View.GONE
        } else {
            holder.binding.delete.visibility = View.GONE
            holder.binding.minus.visibility = View.VISIBLE
        }

        holder.binding.viewCart.setBackgroundResource(R.drawable.add_to_cart_bg_selected)

        holder.binding.add.visibility = View.VISIBLE
        holder.binding.count.visibility = View.VISIBLE


    }


    fun notifyAddData(position: Int) {

        bestProducts[position].quantity = bestProducts[position].quantity + 1
        notifyData(bestProducts)
        notifyDataSetChanged()
    }

    fun notifyRemoveData(position: Int) {
        bestProducts[position].quantity = bestProducts[position].quantity - 1

        if (bestProducts[position].quantity == 0) {
            bestProducts.removeAt(position)
        }

        notifyData(bestProducts)
        notifyDataSetChanged()
    }

    fun addItems(MyCart: ArrayList<ViewCartResponse.CartDetails>) {
        bestProducts = MyCart
        notifyDataSetChanged()
    }
}