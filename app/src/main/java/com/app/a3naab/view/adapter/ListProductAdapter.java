package com.app.a3naab.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3naab.R;
import com.app.a3naab.databinding.ChildProductBinding;
import com.app.a3naab.model.ProductListData;
import com.app.a3naab.utils.SharedHelper;

public class ListProductAdapter extends PagedListAdapter<ProductListData, ListProductAdapter.MyViewHolder> {

    Context context;
    SharedHelper sharedHelper;

    public ListProductAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_product, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        ProductListData data = getItem(position);

        if (data != null) {
            if (data.getProductDiscount().equals("") || data.getProductDiscount().equals("0")) {
                holder.binding.discount.setVisibility(View.GONE);
            } else {
                holder.binding.discount.setVisibility(View.VISIBLE);
                holder.binding.discount.setText(data.getProductDiscount() + "%");
            }

            if (new SharedHelper(context).getSelectedLanguage().equals("en")) {
                holder.binding.productName.setText(data.getArabicName());
            } else {
                holder.binding.productName.setText(data.getProductName());
            }

            holder.binding.amount.setText(data.getProductPrice());
            holder.binding.weight.setText(data.getProductWeight() + context.getString(R.string.kg));
        }


    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ChildProductBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildProductBinding.bind(itemView);

        }
    }

    private static DiffUtil.ItemCallback<ProductListData> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<ProductListData>() {
                @Override
                public boolean areItemsTheSame(ProductListData oldItem, ProductListData newItem) {
                    return oldItem.getId() == oldItem.getId();
                }

                @Override
                public boolean areContentsTheSame(ProductListData oldItem, ProductListData newItem) {
                    return (oldItem.getId() == newItem.getId() && oldItem.getAddedInCart() == newItem.getAddedInCart());
                }
            };

}
