package com.app.a3naab.repository;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.FireBaseVerificationResponse;
import com.app.a3naab.model.LoginResponse;
import com.app.a3naab.model.RegisterResponse;
import com.app.a3naab.network.ApiCall;
import com.app.a3naab.network.InputForAPI;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class SignupRepository {

    public MutableLiveData<FireBaseVerificationResponse> requestVerificationCode(Activity context, String mobile) {

        MutableLiveData<FireBaseVerificationResponse> value = new MutableLiveData<>();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                context, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                        FireBaseVerificationResponse firebaseOtpVerification = new FireBaseVerificationResponse();
                        firebaseOtpVerification.setError("false");
                        firebaseOtpVerification.setVerificationCode("verified");

                        value.setValue(firebaseOtpVerification);
                    }

                    @Override
                    public void onVerificationFailed(@NonNull FirebaseException e) {

                        FireBaseVerificationResponse firebaseOtpVerification = new FireBaseVerificationResponse();
                        firebaseOtpVerification.setError("true");
                        firebaseOtpVerification.setMessage(e.toString());

                        value.setValue(firebaseOtpVerification);


                    }

                    @Override
                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        super.onCodeSent(s, forceResendingToken);
                        FireBaseVerificationResponse firebaseOtpVerification = new FireBaseVerificationResponse();
                        firebaseOtpVerification.setError("false");
                        firebaseOtpVerification.setVerificationCode(s);

                        value.setValue(firebaseOtpVerification);

                    }
                });
        return value;
    }


    public MutableLiveData<FireBaseVerificationResponse> verifyOtp(PhoneAuthCredential credential) {

        MutableLiveData<FireBaseVerificationResponse> responseModel = new MutableLiveData<>();

        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener((task) ->
        {
            if (task.isSuccessful()) {

                FireBaseVerificationResponse commonResponseModel = new FireBaseVerificationResponse();
                commonResponseModel.setError("false");
                responseModel.setValue(commonResponseModel);
            } else {
                FireBaseVerificationResponse commonResponseModel = new FireBaseVerificationResponse();
                commonResponseModel.setError("true");
                if (task.getException() != null) {
                    commonResponseModel.setMessage(task.getException().getMessage());
                } else {
                    commonResponseModel.setMessage("Something went wrong");
                }
                responseModel.setValue(commonResponseModel);
            }
        });

        return responseModel;
    }

    public LiveData<LoginResponse> login(InputForAPI inputForAPI) {

        MutableLiveData<LoginResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                LoginResponse result = gson.fromJson(response.toString(), LoginResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                LoginResponse result = new LoginResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public MutableLiveData<RegisterResponse> createAccount(InputForAPI inputs) {

        MutableLiveData<RegisterResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                RegisterResponse result = gson.fromJson(response.toString(), RegisterResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                RegisterResponse result = new RegisterResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;

    }

    public MutableLiveData<CommonResponse> verifyotp(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;

    }
}
