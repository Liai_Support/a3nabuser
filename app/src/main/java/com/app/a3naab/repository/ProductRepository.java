package com.app.a3naab.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.model.BestProductResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ProductDetailsResponse;
import com.app.a3naab.model.ProfileDataResponse;
import com.app.a3naab.model.ViewCartResponse;
import com.app.a3naab.model.WalletAmountTransactionResponse;
import com.app.a3naab.model.WalletPointsTransactionResponse;
import com.app.a3naab.network.ApiCall;
import com.app.a3naab.network.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ProductRepository {


    public MutableLiveData<ProductDetailsResponse> getProductDetails(InputForAPI inputs) {

        MutableLiveData<ProductDetailsResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ProductDetailsResponse result = gson.fromJson(response.toString(), ProductDetailsResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ProductDetailsResponse result = new ProductDetailsResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<CommonResponse> addItemToCart(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> addtoCartWithStyle(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> removeItemFromCart(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> setFavorite(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<BestProductResponse> getBestProduct(InputForAPI inputs) {

        MutableLiveData<BestProductResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                BestProductResponse result = gson.fromJson(response.toString(), BestProductResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                BestProductResponse result = new BestProductResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<WalletAmountTransactionResponse> getWalletAmountTransaction(InputForAPI inputs) {

        MutableLiveData<WalletAmountTransactionResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                WalletAmountTransactionResponse result = gson.fromJson(response.toString(), WalletAmountTransactionResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                WalletAmountTransactionResponse result = new WalletAmountTransactionResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<WalletPointsTransactionResponse> getWalletPointsTransaction(InputForAPI inputs) {

        MutableLiveData<WalletPointsTransactionResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                WalletPointsTransactionResponse result = gson.fromJson(response.toString(), WalletPointsTransactionResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                WalletPointsTransactionResponse result = new WalletPointsTransactionResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }


    public MutableLiveData<BestProductResponse> getFavouriteList(InputForAPI inputs) {

        MutableLiveData<BestProductResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                BestProductResponse result = gson.fromJson(response.toString(), BestProductResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                BestProductResponse result = new BestProductResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<BestProductResponse> getRelatedProduct(InputForAPI inputs) {

        MutableLiveData<BestProductResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                BestProductResponse result = gson.fromJson(response.toString(), BestProductResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                BestProductResponse result = new BestProductResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<ViewCartResponse> getAddCart(InputForAPI inputs) {
        MutableLiveData<ViewCartResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ViewCartResponse result = gson.fromJson(response.toString(), ViewCartResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                ViewCartResponse result = new ViewCartResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public LiveData<CommonResponse> removeCartFromCart(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }


    public LiveData<ProfileDataResponse> getUserProfile(InputForAPI inputs) {

        MutableLiveData<ProfileDataResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ProfileDataResponse result = gson.fromJson(response.toString(), ProfileDataResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                ProfileDataResponse result = new ProfileDataResponse(true, error, null);

                responseValue.setValue(result);
            }
        });
        return responseValue;

    }


    public LiveData<CommonResponse> updateProfile(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public LiveData<CommonResponse> setFeedBack(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

  public LiveData<CommonResponse> updateSettings(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);
            }

            @Override
            public void setResponseError(String error) {
                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

}
