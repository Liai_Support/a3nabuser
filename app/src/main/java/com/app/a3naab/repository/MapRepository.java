package com.app.a3naab.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.model.AutoCompleteAddressResponseModel;
import com.app.a3naab.model.AutoCompleteResponse;
import com.app.a3naab.model.GetLatLongFromIdResponseModel;
import com.app.a3naab.network.ApiCall;
import com.app.a3naab.network.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class MapRepository {


    public LiveData<AutoCompleteResponse> getAddress(InputForAPI input) {

        MutableLiveData<AutoCompleteResponse> apiResponse = new MutableLiveData<AutoCompleteResponse>();


        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response1) {

                Gson gson = new Gson();
                AutoCompleteResponse response = gson.fromJson(response1.toString(), AutoCompleteResponse.class);
                response.setError(false);
                response.setErrorMessage("");
                apiResponse.setValue(response);
            }

            @Override
            public void setResponseError(String error) {
                AutoCompleteResponse response = new AutoCompleteResponse();
                response.setError(true);
                response.setErrorMessage(error);
                apiResponse.setValue(response);
            }
        });
        return apiResponse;
    }


    public LiveData<AutoCompleteAddressResponseModel> getAutoCompleteAddress(InputForAPI input) {

        MutableLiveData<AutoCompleteAddressResponseModel> apiResponse = new MutableLiveData<AutoCompleteAddressResponseModel>();


        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response1) {

                Gson gson = new Gson();
                AutoCompleteAddressResponseModel response = gson.fromJson(response1.toString(), AutoCompleteAddressResponseModel.class);
                response.setError(false);
                response.setErrorMessage("");
                apiResponse.setValue(response);
            }

            @Override
            public void setResponseError(String error) {
                AutoCompleteAddressResponseModel response = new AutoCompleteAddressResponseModel();
                response.setError(true);
                response.setErrorMessage(error);
                apiResponse.setValue(response);
            }
        });
        return apiResponse;
    }

    public LiveData<GetLatLongFromIdResponseModel> getLatLngDetails(InputForAPI input) {

        MutableLiveData<GetLatLongFromIdResponseModel> apiResponse = new MutableLiveData<>();


        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response1) {

                Gson gson = new Gson();
                GetLatLongFromIdResponseModel response = gson.fromJson(response1.toString(), GetLatLongFromIdResponseModel.class);
                response.setError(false);
                response.setErrorMessage("");
                apiResponse.setValue(response);
            }

            @Override
            public void setResponseError(String error) {
                GetLatLongFromIdResponseModel response = new GetLatLongFromIdResponseModel();
                response.setError(true);
                response.setErrorMessage(error);
                apiResponse.setValue(response);
            }
        });
        return apiResponse;
    }


}
