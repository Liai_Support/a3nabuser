package com.app.a3naab.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.model.CategoryResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.ProducSearchtListResponse;
import com.app.a3naab.model.ProductListResponse;
import com.app.a3naab.model.SearchCategoryDataResponse;
import com.app.a3naab.model.SubCategoryResponse;
import com.app.a3naab.network.ApiCall;
import com.app.a3naab.network.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class CategoryRepository {


    public MutableLiveData<CategoryResponse> getCategoryDetails(InputForAPI inputs) {

        MutableLiveData<CategoryResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CategoryResponse result = gson.fromJson(response.toString(), CategoryResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CategoryResponse result = new CategoryResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public void getProductList(InputForAPI inputForTripsLists, ApiCall.ResponseHandler responseHandler) {
        ApiCall.PostMethod(inputForTripsLists, responseHandler);
    }

    public MutableLiveData<ProductListResponse> getProductListStatis(InputForAPI inputs) {

        MutableLiveData<ProductListResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ProductListResponse result = gson.fromJson(response.toString(), ProductListResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ProductListResponse result = new ProductListResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<SubCategoryResponse> getSubCategory(InputForAPI inputs) {

        MutableLiveData<SubCategoryResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                SubCategoryResponse result = gson.fromJson(response.toString(), SubCategoryResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                SubCategoryResponse result = new SubCategoryResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public LiveData<SearchCategoryDataResponse> getSearchedCategory(InputForAPI inputs) {

        MutableLiveData<SearchCategoryDataResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                SearchCategoryDataResponse result = gson.fromJson(response.toString(), SearchCategoryDataResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                SearchCategoryDataResponse result = new SearchCategoryDataResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public LiveData<ProducSearchtListResponse> getProductSearch(InputForAPI inputs) {

        MutableLiveData<ProducSearchtListResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ProducSearchtListResponse result = gson.fromJson(response.toString(), ProducSearchtListResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ProducSearchtListResponse result = new ProducSearchtListResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<CommonResponse> requestTypingText(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }
}
