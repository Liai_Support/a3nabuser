package com.app.a3naab.repository;

import androidx.lifecycle.MutableLiveData;

import com.app.a3naab.model.AdsResponse;
import com.app.a3naab.model.CheckReorderResponse;
import com.app.a3naab.model.CommonResponse;
import com.app.a3naab.model.CouponResponse;
import com.app.a3naab.model.CurrentLocationIdResponse;
import com.app.a3naab.model.DashboardResponse;
import com.app.a3naab.model.DashboardSearchResponse;
import com.app.a3naab.model.DeliveryTimeResponse;
import com.app.a3naab.model.GetProfileResponse;
import com.app.a3naab.model.ListFaqResponse;
import com.app.a3naab.model.ListMyAddressResponse;
import com.app.a3naab.model.OrderListResponse;
import com.app.a3naab.model.PaymentRefundResponse;
import com.app.a3naab.model.PaymentResponse;
import com.app.a3naab.model.PaymentResponse1;
import com.app.a3naab.model.ViewOrdersResponse;
import com.app.a3naab.network.ApiCall;
import com.app.a3naab.network.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ShoppingRepository {


    public void updateToken(InputForAPI inputs) {
        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
            }

            @Override
            public void setResponseError(String error) {
            }
        });
    }

    public MutableLiveData<DashboardResponse> getDashBoardDetails(InputForAPI inputs) {

        MutableLiveData<DashboardResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                DashboardResponse result = gson.fromJson(response.toString(), DashboardResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                DashboardResponse result = new DashboardResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<AdsResponse> getAds(InputForAPI inputs) {

        MutableLiveData<AdsResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                AdsResponse result = gson.fromJson(response.toString(), AdsResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                AdsResponse result = new AdsResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<AdsResponse> getSearchAds(InputForAPI inputs) {

        MutableLiveData<AdsResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                AdsResponse result = gson.fromJson(response.toString(), AdsResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                AdsResponse result = new AdsResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<ListMyAddressResponse> getAddressList(InputForAPI inputs) {

        MutableLiveData<ListMyAddressResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ListMyAddressResponse result = gson.fromJson(response.toString(), ListMyAddressResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ListMyAddressResponse result = new ListMyAddressResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<ListFaqResponse> getfaqlist(InputForAPI inputs) {

        MutableLiveData<ListFaqResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ListFaqResponse result = gson.fromJson(response.toString(), ListFaqResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ListFaqResponse result = new ListFaqResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> addAddress(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }

    public MutableLiveData<CommonResponse> updateAddress(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }

    public MutableLiveData<DashboardSearchResponse> getDashboardSearch(InputForAPI inputs) {

        MutableLiveData<DashboardSearchResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                DashboardSearchResponse result = gson.fromJson(response.toString(), DashboardSearchResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                DashboardSearchResponse result = new DashboardSearchResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;
    }

    public MutableLiveData<DeliveryTimeResponse> getDeliverydate(InputForAPI inputs) {

        MutableLiveData<DeliveryTimeResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                DeliveryTimeResponse result = gson.fromJson(response.toString(), DeliveryTimeResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                DeliveryTimeResponse result = new DeliveryTimeResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CouponResponse> checkCouponCode(InputForAPI inputs) {
        MutableLiveData<CouponResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CouponResponse result = gson.fromJson(response.toString(), CouponResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CouponResponse result = new CouponResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }

    public MutableLiveData<CommonResponse> placeOrder(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> placeReOrder(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> rescheduelOrder(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<OrderListResponse> getActiveOrders(InputForAPI inputs) {

        MutableLiveData<OrderListResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                OrderListResponse result = gson.fromJson(response.toString(), OrderListResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                OrderListResponse result = new OrderListResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<OrderListResponse> getPastOrders(InputForAPI inputs) {

        MutableLiveData<OrderListResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                OrderListResponse result = gson.fromJson(response.toString(), OrderListResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                OrderListResponse result = new OrderListResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<ViewOrdersResponse> getOrderDetails(InputForAPI inputs) {

        MutableLiveData<ViewOrdersResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ViewOrdersResponse result = gson.fromJson(response.toString(), ViewOrdersResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ViewOrdersResponse result = new ViewOrdersResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }

    public MutableLiveData<CheckReorderResponse> checkreorder(InputForAPI inputs) {

        MutableLiveData<CheckReorderResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CheckReorderResponse result = gson.fromJson(response.toString(), CheckReorderResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CheckReorderResponse result = new CheckReorderResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }

    public MutableLiveData<CommonResponse> cancelOrder(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }



    public MutableLiveData<CommonResponse> deleteAddress(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> setRating(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<GetProfileResponse> getProfileData(InputForAPI inputs) {


        MutableLiveData<GetProfileResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                GetProfileResponse result = gson.fromJson(response.toString(), GetProfileResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                GetProfileResponse result = new GetProfileResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public MutableLiveData<CommonResponse> getcartclear(InputForAPI inputs) {


        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }


    public MutableLiveData<CurrentLocationIdResponse> getCurrentAddressId(InputForAPI inputs) {

        MutableLiveData<CurrentLocationIdResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CurrentLocationIdResponse result = gson.fromJson(response.toString(), CurrentLocationIdResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CurrentLocationIdResponse result = new CurrentLocationIdResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;

    }

    public void updateUnAvailableProduct(InputForAPI inputs) {

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {

            @Override
            public void setDataResponse(JSONObject response) {

            }

            @Override
            public void setResponseError(String error) {

            }
        });
    }

    public MutableLiveData<PaymentResponse> startPayment(InputForAPI inputForAPI) {


        MutableLiveData<PaymentResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethodRaw(inputForAPI, new ApiCall.ResponseHandler2() {
            @Override
            public void setDataResponse(String response) {

                Gson gson = new Gson();
                PaymentResponse result = gson.fromJson(response, PaymentResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                PaymentResponse result = new PaymentResponse();
                result.setResultCode("1");
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }

    public MutableLiveData<PaymentResponse> getorder(InputForAPI inputForAPI) {


        MutableLiveData<PaymentResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethodRaw(inputForAPI, new ApiCall.ResponseHandler2() {
            @Override
            public void setDataResponse(String response) {

                Gson gson = new Gson();
                PaymentResponse result = gson.fromJson(response, PaymentResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                PaymentResponse result = new PaymentResponse();
                result.setResultCode("1");
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }

    public MutableLiveData<PaymentResponse1> sale(InputForAPI inputForAPI) {


        MutableLiveData<PaymentResponse1> responseValue = new MutableLiveData<>();

        ApiCall.PostMethodRaw(inputForAPI, new ApiCall.ResponseHandler2() {
            @Override
            public void setDataResponse(String response) {

                Gson gson = new Gson();
                PaymentResponse1 result = gson.fromJson(response, PaymentResponse1.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                PaymentResponse1 result = new PaymentResponse1();
                result.setResultCode("1");
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }


    public MutableLiveData<PaymentRefundResponse> refundpayment(InputForAPI inputForAPI) {


        MutableLiveData<PaymentRefundResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethodRaw(inputForAPI, new ApiCall.ResponseHandler2() {
            @Override
            public void setDataResponse(String response) {

                Gson gson = new Gson();
                PaymentRefundResponse result = gson.fromJson(response, PaymentRefundResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                PaymentRefundResponse result = new PaymentRefundResponse();
                result.setRcode(1);
                result.setMessage(error);
                responseValue.setValue(result);
            }
        });
        return responseValue;


    }



}
