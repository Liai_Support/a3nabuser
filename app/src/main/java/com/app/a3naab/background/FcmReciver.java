package com.app.a3naab.background;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.app.a3naab.R;
import com.app.a3naab.utils.SharedHelper;
import com.app.a3naab.view.activity.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

public class FcmReciver extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        if (remoteMessage.getData().size() > 0) {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(remoteMessage.getData());
            sendNotification(jsonObject);
        }


    }

    private void sendNotification(JSONObject jsonObject) {
        Intent intent = new Intent(this, SplashActivity.class);
        //intent.putExtra(Constants.LOGIN_TYPE, Constants.HOME_TAB);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Log.d("sendNotification", jsonObject.toString());
        if ((!jsonObject.has("title")) || (!jsonObject.has("body"))) {
            return;
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "Notification")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(jsonObject.optString("title"))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX);

        if (jsonObject.has("body")) {
            notificationBuilder.setContentText(jsonObject.optString("body"));
        } else if(jsonObject.has("message")){
            notificationBuilder.setContentText(jsonObject.optString("message"));

        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }




    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

        SharedHelper sharedHelper = new SharedHelper(this);
        sharedHelper.setFirebaseToken(s);
    }


}
