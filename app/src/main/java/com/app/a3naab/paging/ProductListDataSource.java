package com.app.a3naab.paging;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.app.a3naab.model.ProductListData;
import com.app.a3naab.model.ProductListResponse;
import com.app.a3naab.network.ApiCall;
import com.app.a3naab.network.InputForAPI;
import com.app.a3naab.network.URLHelper;
import com.app.a3naab.repository.CategoryRepository;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductListDataSource extends PageKeyedDataSource<Integer, ProductListData> {

    public static int PAGENUMBER = 1;
    public static int TOTAL_PAGENUMBER = 50;

    private String type;
    private int id;
    private Context context;
    CategoryRepository yourTripRepository;

    public ProductListDataSource() {
        yourTripRepository = new CategoryRepository();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, ProductListData> callback) {

        PAGENUMBER = 1;
        yourTripRepository.getProductList(getInputForTripsLists(), new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject responseStr) {
                Gson gson = new Gson();
                ProductListResponse response = gson.fromJson(responseStr.toString(), ProductListResponse.class);
                if (response.getProductListDataData().getProducts() != null)
//                    if (response.getProducts().size() != 0)
//                        callback.onResult(response.getProducts(), null, PAGENUMBER + 1);
//                    else
//                        callback.onResult(response.getProducts(), null, null);

                PAGENUMBER = PAGENUMBER + 1;
            }

            @Override
            public void setResponseError(String error) {

            }
        });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, ProductListData> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull final LoadCallback<Integer, ProductListData> callback) {

        if (params.key != null) {
            yourTripRepository.getProductList(getInputForTripsLists(), new ApiCall.ResponseHandler() {
                @Override
                public void setDataResponse(JSONObject responseStr) {
                    Gson gson = new Gson();
                    ProductListResponse response = gson.fromJson(responseStr.toString(), ProductListResponse.class);
                    if (response.getProductListDataData().getProducts() != null)
//                        if (response.getProducts().size() != 0)
//                            callback.onResult(response.getProducts(), PAGENUMBER + 1);
//                        else
//                            callback.onResult(response.getProducts(), null);

                    PAGENUMBER = PAGENUMBER + 1;
                }

                @Override
                public void setResponseError(String error) {

                }
            });
        }

    }


    private InputForAPI getInputForTripsLists() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("type", type);
            jsonObject.put("pageNumber", PAGENUMBER);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(context,URLHelper.GETPRODUCTLIST);
        inputForAPI.setUrl(URLHelper.GETPRODUCTLIST);
        inputForAPI.setJsonObject(jsonObject);
        return inputForAPI;
    }


    public void setInputs(Activity activity, String type, int id) {
        this.type = type;
        this.id = id;
        context = activity;
    }
}
