package com.app.a3naab.paging;

import android.app.Activity;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.app.a3naab.model.ProductListData;


public class ProductListDataSourceFactory extends DataSource.Factory<Integer, ProductListData> {

    private MutableLiveData<PageKeyedDataSource<Integer, ProductListData>> itemLiveDataSource = new MutableLiveData<>();

    ProductListDataSource dataSource;

    Activity activity;
    String type;
    int id;

    @Override
    public DataSource<Integer, ProductListData> create() {
        dataSource = new ProductListDataSource();
        dataSource.setInputs(activity, type, id);

        itemLiveDataSource.postValue(dataSource);

        return dataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, ProductListData>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }

    public void setInputs(Activity activity, String type, int id) {
        this.activity = activity;
        this.type = type;
        this.id = id;
    }
}
