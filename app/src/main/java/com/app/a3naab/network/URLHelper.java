package com.app.a3naab.network;

import com.app.a3naab.BuildConfig;
import com.app.a3naab.R;
import com.app.a3naab.background.MyApp;

public class URLHelper {

    private static final String BASE_URL = BuildConfig.BaseDEV;
    private static final String USER_BASE_URL = BASE_URL + "/user/";
   // public static final String PAYMENT_URL = BuildConfig.PAYMENTURL;
   // public static final String PAYMENT_URL = "https://api-test.noonpayments.com/payment/v1/order";
   // public static final String PAYMENT_KEY = "Key_Test YWFuYWFiLmFhbmFhYjo5ODMxYTU3MjE2YmU0Y2RkOThlNTM1MGVjMTRjZWIyYw==";
    public static final String PAYMENT_URL = "https://api.noonpayments.com/payment/v1/order";
    public static final String PAYMENT_KEY = "Key_Live YWFuYWFiLmFhbmFhYjo1N2Y4NDQwODFlNmY0Y2M1OTdhOGRkNTgxOTk3ZTM0MQ==";

    public static final String LOGIN = USER_BASE_URL + "login";
    public static final String REGISTER = USER_BASE_URL + "createAccount";
    public static final String VERIFYOTP = USER_BASE_URL+"verifyOtp";
    public static final String UPDATEDEVICETOKEN = USER_BASE_URL + "updateDeviceToken";

    public static final String HOMEDASHBOARD = USER_BASE_URL + "homeDashboard";
    public static final String GETALLADS = USER_BASE_URL + "getAllAdds";
    public static final String SEARCHADS = USER_BASE_URL + "searchAdds";
    public static final String GETMYADDRESS = USER_BASE_URL + "getMyAddresses";
    public static final String ADDADDRESS = USER_BASE_URL + "addAddresses";
    public static final String DELETEADDRESS = USER_BASE_URL + "deleteAddresses";
    public static final String UPDATEADDRESS = USER_BASE_URL + "updateAddresses";
    public static final String HOMESEARCH = USER_BASE_URL + "homeSearch";
    public static final String GETPRODUCTCATEGORY = USER_BASE_URL + "getProductCategory";
    public static final String GETPRODUCTSUBCATEGORY = USER_BASE_URL + "getProductSubCategory";
    public static final String GETPRODUCTLIST = USER_BASE_URL + "getProductList";
    public static final String PRODUCTCATEGORYSEARCH = USER_BASE_URL + "productCategorySearch";
    public static final String PRODUCTSEARCH = USER_BASE_URL + "productSearch";
    public static final String VIEWPRODUCTDETAILS = USER_BASE_URL + "viewProductDetails";

    public static final String ADDCART = USER_BASE_URL + "addCart";
    public static final String ADDFAVOURITEPRODUCT = USER_BASE_URL + "addFavouriteProduct";
    public static final String VIEWBESTPRODUCT = USER_BASE_URL + "viewBestProducts";
    public static final String VIEWFAVOURITEPRODUCT = USER_BASE_URL + "viewFavList";
    public static final String VIEWRELATEDPRODUCT = USER_BASE_URL + "viewRelatedProduct";

    public static final String VIEWCART = USER_BASE_URL + "viewCart";
    public static final String GETDELIVERYTIME = USER_BASE_URL + "getDeliveryTime";
    public static final String CHECKCOUPONCODE = USER_BASE_URL + "checkCouponCode";
    public static final String PLACEORDER = USER_BASE_URL + "placeOrder";
    public static final String REORDER = USER_BASE_URL + "reOrder";
    public static final String CHECKREORDER = USER_BASE_URL + "userCheckReorder";
    public static final String RESCHEDULE = USER_BASE_URL + "userUpdateDeliveryDate";

    public static final String USERORDERLIST = USER_BASE_URL + "userOrderList";
    public static final String USERVIEWORDER = USER_BASE_URL + "userViewOrder";
    public static final String USERORDERCANCELLED = USER_BASE_URL + "userOrderCancelled";

    public static final String USERSEARCH = USER_BASE_URL + "userSearch";
    public static final String GETUSERPROFILE = USER_BASE_URL + "getUserProfile";
    public static final String DELETE_CART = USER_BASE_URL + "deleteCart";
    public static final String CLEARCART = USER_BASE_URL + "removeCartItem";
    public static final String GETFAQ = USER_BASE_URL + "getUserfaq";
    public static final String USERWALLETAMOUNTTRASACTION = USER_BASE_URL + "walletTransactionList";
    public static final String USERWALLETPOINTSTRASACTION = USER_BASE_URL + "pointTransactionList";


    public static final String UPDATEPROFILE = USER_BASE_URL + "updateProfile";

    public static final String UPDATECURRENTADDRESS = USER_BASE_URL + "updateCurrentAddress";
    public static final String UPDATEUNAVAILABLEPRODUCT = USER_BASE_URL + "unavailableProduct";
    public static final String ADDRATING = USER_BASE_URL + "addRating";
    public static final String ADDFEEDBACK = USER_BASE_URL + "appFeedback";
    public static final String UPDATEUSERSETTINGS = USER_BASE_URL + "updateUserSettings";


    public static final String GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/";
    public static final String GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?";
    public static final String GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?";
    public static final String GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?";
    public static final String GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?";
    public static final String GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?";

    public static String getAddress(
            String latitude,
            String longitude
    ) {

        return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + latitude + ","
                + longitude + "&sensor=true&key=" + MyApp.getInstance().getApplicationContext().getString(
                R.string.map_api_key
        ));
    }
}
